package org.acme.spring.data.jpa.jovani.test;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import org.acme.spring.data.jovani.institucion.external.rest.controllers.AulaCuatroController;
import org.acme.spring.data.jovani.institucion.external.rest.dto.AulaCuatroCreateDto;
import org.acme.spring.data.util.error.ErrorCodes;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestHTTPEndpoint(AulaCuatroController.class)
public class AulaCuatroControllerTest {

    @Test
    void list_200(){
        given().pathParam("idEdificio", 1)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().get("{idEdificio}").then()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void list_404(){
        given().pathParam("idEdificio", 100000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().get("{idEdificio}").then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    /*
    @Test
    void post_200(){
        AulaCuatroCreateDto aula = AulaCuatroCreateDto.builder()
                .idEdificio(2)
                .nombre("aula dos")
                .idTipoEquipamiento(1)
                .cantidadEquipamiento(10)
                .capacidadEquipamiento(10)
                .observaciones("observaciones aula dos edificio dos")
                .build();
        given().body(aula)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
    */

    @Test
    void post_404(){
        AulaCuatroCreateDto aula = AulaCuatroCreateDto.builder()
                .idEdificio(1000000)
                .nombre("aula dos")
                .idTipoEquipamiento(1)
                .cantidadEquipamiento(10)
                .capacidadEquipamiento(10)
                .observaciones("observaciones aula dos edificio dos")
                .build();
        given().body(aula)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void post_RNN009(){
        AulaCuatroCreateDto aula = AulaCuatroCreateDto.builder()
                .idEdificio(2)
                .nombre("aula uno")
                .idTipoEquipamiento(1)
                .cantidadEquipamiento(10)
                .capacidadEquipamiento(10)
                .observaciones("observaciones aula dos edificio dos")
                .build();
        given().body(aula)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN009.name()));
    }

    @Test
    void post_RNS001(){
        AulaCuatroCreateDto aula = AulaCuatroCreateDto.builder().build();
        given().body(aula)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(7)).body(
                        CoreMatchers.containsString("create.aulaCuatroCreateDto.capacidadEquipamiento"),
                        CoreMatchers.containsString("create.aulaCuatroCreateDto.idTipoEquipamiento"),
                        CoreMatchers.containsString("create.aulaCuatroCreateDto.idEdificio"),
                        CoreMatchers.containsString("create.aulaCuatroCreateDto.observaciones"),
                        CoreMatchers.containsString("create.aulaCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.aulaCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.aulaCuatroCreateDto.cantidadEquipamiento")
                );
    }

    @Test
    void post_RNN128(){
        AulaCuatroCreateDto aula = AulaCuatroCreateDto.builder()
                .idEdificio(2)
                .nombre("aula dos")
                .idTipoEquipamiento(1)
                .cantidadEquipamiento(-10)
                .capacidadEquipamiento(-10)
                .observaciones("observaciones aula dos edificio dos")
                .build();
        given().body(aula)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN128.name()));
    }

    @Test
    void update_200(){
        AulaCuatroCreateDto aula = AulaCuatroCreateDto.builder()
                .idEdificio(1)
                .nombre("aula dos test update")
                .idTipoEquipamiento(1)
                .cantidadEquipamiento(15)
                .capacidadEquipamiento(15)
                .observaciones("observaciones aula dos edificio dos test")
                .build();
        given().body(aula).pathParam("idAula", 2)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idAula}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void update_404(){
        AulaCuatroCreateDto aula = AulaCuatroCreateDto.builder()
                .idEdificio(1)
                .nombre("aula dos test update")
                .idTipoEquipamiento(1)
                .cantidadEquipamiento(15)
                .capacidadEquipamiento(15)
                .observaciones("observaciones aula dos edificio dos test")
                .build();
        given().body(aula).pathParam("idAula", 10000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idAula}").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void put_RNN009AndIdNot(){
        AulaCuatroCreateDto aula = AulaCuatroCreateDto.builder()
                .idEdificio(1)
                .nombre("aula uno update")
                .idTipoEquipamiento(1)
                .cantidadEquipamiento(10)
                .capacidadEquipamiento(10)
                .observaciones("observaciones aula dos edificio dos")
                .build();
        given().body(aula).pathParam("idAula", 2)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idAula}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN009.name()));
    }

    /*
    @Test
    void delete_200(){
        given().pathParam("idAula", 7)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idAula}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
     */

    @Test
    void delete_404(){
        given().pathParam("idAula", 10000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idAula}").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }
}
