package org.acme.spring.data.jpa.jovani.test;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import org.acme.spring.data.jovani.institucion.external.rest.controllers.EdificioCuatroController;
import org.acme.spring.data.jovani.institucion.external.rest.dto.EdificioCuatroCreateDto;
import org.acme.spring.data.util.error.ErrorCodes;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestHTTPEndpoint(EdificioCuatroController.class)
class EdificioCuatroControllerTest {

    @Test
    void list_200(){
        given().pathParam("idSede", 2)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().get("{idSede}").then()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void list_404(){
        given().pathParam("idSede", 10000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().get("{idSede}").then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    /*
    @Test
    void post_200(){
        EdificioCuatroCreateDto edificio = EdificioCuatroCreateDto.builder()
                .idSede(3)
                .nombre("edificio sede 3")
                .acronimo("edf3 s3")
                .referencia("referencia edificio 3 sede 3")
                .build();
        given().body(edificio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
     */

    @Test
    void post_404(){
        EdificioCuatroCreateDto edificio = EdificioCuatroCreateDto.builder()
                .idSede(10000)
                .nombre("nombre edificio sede dos")
                .acronimo("acronimo de sede")
                .referencia("")
                .build();
        given().body(edificio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void post_RNN002_nombre(){
        EdificioCuatroCreateDto edificio = EdificioCuatroCreateDto.builder()
                .idSede(2)
                .nombre("edificio dos")
                .acronimo("acronimo dos")
                .referencia("")
                .build();
        given().body(edificio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN002.name()));
    }

    @Test
    void post_RNN002_acronimo(){
        EdificioCuatroCreateDto edificio = EdificioCuatroCreateDto.builder()
                .idSede(2)
                .nombre("nombre edificio sede dos")
                .acronimo("edf2")
                .referencia("")
                .build();
        given().body(edificio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN002.name()));
    }

    @Test
    void post_RNS001(){
        EdificioCuatroCreateDto edificio = EdificioCuatroCreateDto.builder().build();
        given().body(edificio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(6)).body(
                        CoreMatchers.containsString("create.edificioCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.edificioCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.edificioCuatroCreateDto.referencia"),
                        CoreMatchers.containsString("create.edificioCuatroCreateDto.acronimo"),
                        CoreMatchers.containsString("create.edificioCuatroCreateDto.acronimo"),
                        CoreMatchers.containsString("create.edificioCuatroCreateDto.idSede")
                );
    }

    @Test
    void put_200(){
        EdificioCuatroCreateDto edificio = EdificioCuatroCreateDto.builder()
                .idSede(4)
                .nombre("edificio uno")
                .acronimo("edf1")
                .referencia("edificio uno referencia update")
                .build();
        given().body(edificio).pathParam("idEdificio", 1)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idEdificio}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void put_404(){
        EdificioCuatroCreateDto edificio = EdificioCuatroCreateDto.builder()
                .idSede(4)
                .nombre("edificio uno")
                .acronimo("edf1")
                .referencia("edificio uno referencia update")
                .build();
        given().body(edificio).pathParam("idEdificio", 100000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idEdificio}").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void put_RNN002AndIdNot_nombre(){
        EdificioCuatroCreateDto edificio = EdificioCuatroCreateDto.builder()
                .idSede(4)
                .nombre("edificio tres")
                .acronimo("edf1")
                .referencia("")
                .build();
        given().body(edificio).pathParam("idEdificio", 1)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idEdificio}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN002.name()));
    }

    @Test
    void put_RNN002AndIdNot_acronimo(){
        EdificioCuatroCreateDto edificio = EdificioCuatroCreateDto.builder()
                .idSede(4)
                .nombre("edificio uno")
                .acronimo("edf3")
                .referencia("")
                .build();
        given().body(edificio).pathParam("idEdificio", 1)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idEdificio}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN002.name()));
    }

    /*
    @Test
    void delete_200(){
        given().pathParam("idEdificio", 7)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idEdificio}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
    */

    @Test
    void delete_404(){
        given().pathParam("idEdificio", 100000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idEdificio}").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void delete_RNN025(){
        given().pathParam("idEdificio", 1)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idEdificio}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN025.name()));
    }
}
