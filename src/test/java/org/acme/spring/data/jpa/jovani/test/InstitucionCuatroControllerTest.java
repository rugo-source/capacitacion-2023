package org.acme.spring.data.jpa.jovani.test;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import org.acme.spring.data.jovani.institucion.external.rest.controllers.InstitucionCuatroController;
import org.acme.spring.data.jovani.institucion.external.rest.dto.InstitucionCuatroCreateDto;
import org.acme.spring.data.jovani.institucion.external.rest.dto.InstitucionCuatroGetDto;
import org.acme.spring.data.util.error.ErrorCodes;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static org.hamcrest.Matchers.equalTo;

import static io.restassured.RestAssured.given;

@QuarkusTest
@TestHTTPEndpoint(InstitucionCuatroController.class)
class InstitucionCuatroControllerTest {

    @Test
    void list_200(){
        InstitucionCuatroGetDto institucion = InstitucionCuatroGetDto.builder()
                .acronimo("CECYT")
                .identificador("")
                .nombre("").build();
        given().body(institucion).
                headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post("/busqueda").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void list_404(){
        InstitucionCuatroGetDto institucion = InstitucionCuatroGetDto.builder()
                .acronimo("BAD")
                .identificador("")
                .nombre("")
                .build();
        given().body(institucion).
                headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post("/busqueda").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    /*
    @Test
    void post_200(){
        InstitucionCuatroCreateDto institucion = InstitucionCuatroCreateDto.builder()
                .acronimo("CECYT")
                .cct("")
                .idCategoria(1)
                .idClasificacion(1)
                .idSubsistemaBachillerato(1)
                .idSubsistemaUniversidad(1)
                .idTipo(1)
                .identificador("0011")
                .nombre("Centro de estudios cientificos y tecnológicos tres")
                .numeroSedesRegistradas(0)
                .build();
        given().body(institucion).
                header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
    */

    @Test
    void post_RNS001(){
        InstitucionCuatroCreateDto institucion = InstitucionCuatroCreateDto.builder().build();
        given().body(institucion).headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON,
                HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when().post().then().log().all().statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(12)).body(
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.idTipo"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.idClasificacion"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.idCategoria"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.idSubsistemaUniversidad"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.idSubsistemaBachillerato"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.identificador"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.acronimo"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.acronimo"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.cct"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.numeroSedesRegistradas")
                );
    }

    @Test
    void post_RNS002(){
        InstitucionCuatroCreateDto institucion = InstitucionCuatroCreateDto.builder()
                .acronimo("")
                .cct("MASDEVEINTEPALABRASPARAESTEATRIBUTO")
                .idCategoria(1)
                .idClasificacion(1)
                .idSubsistemaBachillerato(1)
                .idSubsistemaUniversidad(1)
                .idTipo(1)
                .identificador("0011111")
                .nombre("")
                .numeroSedesRegistradas(0)
                .build();
        given().body(institucion).headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON,
                HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .when().post().then().log().all().statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(6)).body(
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.acronimo"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.acronimo"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.cct"),
                        CoreMatchers.containsString("create.institucionCuatroCreateDto.identificador")
                );
    }

    @Test
    void post_RNN013(){
        InstitucionCuatroCreateDto institucion = InstitucionCuatroCreateDto.builder()
                .acronimo("CECYT")
                .cct("")
                .idCategoria(1)
                .idClasificacion(1)
                .idSubsistemaBachillerato(1)
                .idSubsistemaUniversidad(1)
                .idTipo(1)
                .identificador("0011")
                .nombre("Centro de estudios cientificos y tecnológicos test")
                .numeroSedesRegistradas(0)
                .build();
        given().body(institucion).
                headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN013.name()));
    }



    @Test
    void post_RNN094(){
        InstitucionCuatroCreateDto institucion = InstitucionCuatroCreateDto.builder()
                .acronimo("CECYT")
                .cct("")
                .idCategoria(1)
                .idClasificacion(1)
                .idSubsistemaBachillerato(1)
                .idSubsistemaUniversidad(1)
                .idTipo(1)
                .identificador("0014")
                .nombre("Centro de estudios cientificos y tecnológicos")
                .numeroSedesRegistradas(0)
                .build();
        given().body(institucion)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN094.name()));
    }

    @Test
    void put_200(){
        InstitucionCuatroCreateDto institucion = InstitucionCuatroCreateDto.builder()
                .acronimo("CECYT")
                .cct("")
                .idCategoria(1)
                .idClasificacion(1)
                .idSubsistemaBachillerato(1)
                .idSubsistemaUniversidad(1)
                .idTipo(1)
                .identificador("0012")
                .nombre("Nombre institucion")
                .numeroSedesRegistradas(0)
                .build();
        given().pathParam("idInstitucion", 12).body(institucion)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idInstitucion}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void put_RNN013(){
        InstitucionCuatroCreateDto institucion = InstitucionCuatroCreateDto.builder()
                .acronimo("CECYT")
                .cct("")
                .idCategoria(1)
                .idClasificacion(1)
                .idSubsistemaBachillerato(1)
                .idSubsistemaUniversidad(1)
                .idTipo(1)
                .identificador("0011")
                .nombre("Nombre institucion update")
                .numeroSedesRegistradas(0)
                .build();
        given().pathParam("idInstitucion", 12).body(institucion)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idInstitucion}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN013.name()));
    }

    @Test
    void put_RNN094(){
        InstitucionCuatroCreateDto institucion = InstitucionCuatroCreateDto.builder()
                .acronimo("CECYT")
                .cct("")
                .idCategoria(1)
                .idClasificacion(1)
                .idSubsistemaBachillerato(1)
                .idSubsistemaUniversidad(1)
                .idTipo(1)
                .identificador("0012")
                .nombre("Escuela Superior de Cómputo")
                .numeroSedesRegistradas(0)
                .build();
        given().pathParam("idInstitucion", 12).body(institucion)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idInstitucion}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN094.name()));
    }
}
