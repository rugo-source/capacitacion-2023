package org.acme.spring.data.jpa.jovani.test;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import org.acme.spring.data.jovani.institucion.external.rest.controllers.SedeCuatroController;
import org.acme.spring.data.jovani.institucion.external.rest.dto.SedeCuatroCreateDto;
import org.acme.spring.data.util.error.ErrorCodes;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestHTTPEndpoint(SedeCuatroController.class)
public class SedeCuatroControllerTest {

    @Test
    void get_200(){
        given().pathParam("idSede", 2)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().get("{idSede}").then()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void get_404(){
        given().pathParam("idSede", 10000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().get("{idSede}").then().statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void list_200(){
        given().pathParam("idInstitucion", 5)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().get("sedes-institucion/{idInstitucion}").then()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void list_404(){
        given().pathParam("idInstitucion", 10000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().get("sedes-institucion/{idInstitucion}").then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    /*
    @Test
    void post_200(){
        SedeCuatroCreateDto sede = SedeCuatroCreateDto.builder()
                .idInstitucion(6)
                .nombre("Preparatoria No. 1")
                .acronimo("P1")
                .capacidad(1000)
                .propia(true)
                .build();
        given().body(sede)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
     */

    @Test
    void post_RNN096_nombre(){
        SedeCuatroCreateDto sede = SedeCuatroCreateDto.builder()
                .idInstitucion(5)
                .nombre("Escuela superior de cómputo")
                .acronimo("escuela")
                .capacidad(1000)
                .propia(true)
                .build();
        given().body(sede)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN096.name()));
    }

    @Test
    void post_RNN096_acronimo(){
        SedeCuatroCreateDto sede = SedeCuatroCreateDto.builder()
                .idInstitucion(5)
                .nombre("Escuela superior")
                .acronimo("ESC")
                .capacidad(1000)
                .propia(true)
                .build();
        given().body(sede)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN096.name()));
    }

    @Test
    void post_404(){
        SedeCuatroCreateDto sede = SedeCuatroCreateDto.builder()
                .idInstitucion(10000)
                .nombre("Preparatoria No. 2")
                .acronimo("P2")
                .capacidad(1000)
                .propia(true)
                .build();
        given().body(sede)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void post_RNS001(){
        SedeCuatroCreateDto sede = SedeCuatroCreateDto.builder().build();
        given().body(sede)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(6)).body(
                        CoreMatchers.containsString("create.sedeCuatroCreateDto.acronimo"),
                        CoreMatchers.containsString("create.sedeCuatroCreateDto.acronimo"),
                        CoreMatchers.containsString("create.sedeCuatroCreateDto.propia"),
                        CoreMatchers.containsString("create.sedeCuatroCreateDto.idInstitucion"),
                        CoreMatchers.containsString("create.sedeCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.sedeCuatroCreateDto.nombre")
                );
    }

    @Test
    void post_RNN03(){
        SedeCuatroCreateDto sede = SedeCuatroCreateDto.builder()
                .idInstitucion(6)
                .nombre("Preparatoria No. 1")
                .acronimo("P1")
                .capacidad(0)
                .propia(true)
                .build();
        given().body(sede)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(1)).body(
                        CoreMatchers.containsString("create.sedeCuatroCreateDto.capacidad")
                );
    }

    @Test
    void put_200(){
        SedeCuatroCreateDto sede = SedeCuatroCreateDto.builder()
                .idInstitucion(6)
                .nombre("Preparatoria No. 1")
                .acronimo("P1 update")
                .capacidad(200)
                .propia(false)
                .build();
        given().body(sede).pathParam("idSede", 6)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idSede}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void put_RNN096AndIdNot_nombre(){
        SedeCuatroCreateDto sede = SedeCuatroCreateDto.builder()
                .idInstitucion(6)
                .nombre("Centro de estudios superiores")
                .acronimo("P1 update")
                .capacidad(200)
                .propia(false)
                .build();
        given().body(sede).pathParam("idSede", 6)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idSede}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN096.name()));
    }

    @Test
    void put_RNN096AndIdNot_acronimo(){
        SedeCuatroCreateDto sede = SedeCuatroCreateDto.builder()
                .idInstitucion(6)
                .nombre("Preparatoria No. 1")
                .acronimo("CES")
                .capacidad(200)
                .propia(false)
                .build();
        given().body(sede).pathParam("idSede", 6)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idSede}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN096.name()));
    }

    /*
    @Test
    void delete_200(){
        given().pathParam("idSede", 9)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idSede}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
    */

    @Test
    void delete_RNN016(){
        given().pathParam("idSede", 4)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idSede}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN016.name()));
    }

    @Test
    void delete_404(){
        given().pathParam("idSede", 10000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idSede}").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }
}
