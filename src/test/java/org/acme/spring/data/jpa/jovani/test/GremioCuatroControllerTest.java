package org.acme.spring.data.jpa.jovani.test;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import org.acme.spring.data.jovani.institucion.external.rest.controllers.GremiosCuatroController;
import org.acme.spring.data.jovani.institucion.external.rest.dto.GremioCuatroCreateDto;
import org.acme.spring.data.jovani.institucion.external.rest.dto.MiembroGremioCuatroCreateDto;
import org.acme.spring.data.util.error.ErrorCodes;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestHTTPEndpoint(GremiosCuatroController.class)
public class GremioCuatroControllerTest {

    @Test
    void list_200(){
        given().headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                MediaType.APPLICATION_JSON)
                .when().get().then().statusCode(Response.Status.OK.getStatusCode());
    }

    /*
    @Test
    void post_200(){
        GremioCuatroCreateDto gremio = GremioCuatroCreateDto.builder()
                .nombre("gremio4")
                .acronimo("grm4")
                .build();
        given().body(gremio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
    */

    @Test
    void post_RNN097_nombre(){
        GremioCuatroCreateDto gremio = GremioCuatroCreateDto.builder()
                .nombre("gremio1")
                .acronimo("gremioRNN097")
                .build();
        given().body(gremio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN097.name()));
    }

    @Test
    void post_RNN097_acronimo(){
        GremioCuatroCreateDto gremio = GremioCuatroCreateDto.builder()
                .nombre("gremioRNN097")
                .acronimo("grm1")
                .build();
        given().body(gremio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN097.name()));
    }


    @Test
    void post_RNS001(){
        GremioCuatroCreateDto gremio = GremioCuatroCreateDto.builder().build();
        given().body(gremio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(4)).body(
                        CoreMatchers.containsString("create.gremioCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.gremioCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.gremioCuatroCreateDto.acronimo"),
                        CoreMatchers.containsString("create.gremioCuatroCreateDto.acronimo")
                );
    }

    @Test
    void post_RNS002(){
        GremioCuatroCreateDto gremio = GremioCuatroCreateDto.builder()
                .nombre("")
                .acronimo("")
                .build();
        given().body(gremio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(4)).body(
                        CoreMatchers.containsString("create.gremioCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.gremioCuatroCreateDto.nombre"),
                        CoreMatchers.containsString("create.gremioCuatroCreateDto.acronimo"),
                        CoreMatchers.containsString("create.gremioCuatroCreateDto.acronimo")
                );
    }

    @Test
    void put_200(){
        GremioCuatroCreateDto gremio = GremioCuatroCreateDto.builder()
                .nombre("gremio update")
                .acronimo("acronimo update")
                .build();
        given().pathParam("idGremio", 6).body(gremio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idGremio}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void put_404(){
        GremioCuatroCreateDto gremio = GremioCuatroCreateDto.builder()
                .nombre("gremio update")
                .acronimo("acronimo update")
                .build();
        given().pathParam("idGremio", 100000).body(gremio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idGremio}").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void put_RNN097AndIdNot_nombre(){
        GremioCuatroCreateDto gremio = GremioCuatroCreateDto.builder()
                .nombre("gremio1")
                .acronimo("acronimo update")
                .build();
        given().pathParam("idGremio", 6).body(gremio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idGremio}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN097.name()));
    }

    @Test
    void put_RNN097AndIdNot_acronima(){
        GremioCuatroCreateDto gremio = GremioCuatroCreateDto.builder()
                .nombre("gremio update")
                .acronimo("grm1")
                .build();
        given().pathParam("idGremio", 6).body(gremio)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idGremio}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN097.name()));
    }

    /*
    @Test
    void delete_200(){
        given().pathParam("idGremio", 5)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idGremio}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
     */

    @Test
    void delete_404(){
        given().pathParam("idGremio", 1000000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idGremio}").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void delete_RNN024(){
        given().pathParam("idGremio", 1)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idGremio}").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN024.name()));
    }

    @Test
    void post_asociar_institucion_200(){
        MiembroGremioCuatroCreateDto miembro = MiembroGremioCuatroCreateDto.builder()
                .idInstitucion(4)
                .idGremio(1)
                .build();
        given().body(miembro)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post("/asociar-institucion").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void post_asociar_institucion_404_gremio(){
        MiembroGremioCuatroCreateDto miembro = MiembroGremioCuatroCreateDto.builder()
                .idInstitucion(3)
                .idGremio(10000)
                .build();
        given().body(miembro)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post("/asociar-institucion").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void post_asociar_institucion_404_institucion(){
        MiembroGremioCuatroCreateDto miembro = MiembroGremioCuatroCreateDto.builder()
                .idInstitucion(10000)
                .idGremio(1)
                .build();
        given().body(miembro)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post("/asociar-institucion").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void post_asociar_institucion_RNN026(){
        MiembroGremioCuatroCreateDto miembro = MiembroGremioCuatroCreateDto.builder()
                .idInstitucion(2)
                .idGremio(4)
                .build();
        given().body(miembro)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post("/asociar-institucion").then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN026.name()));
    }

    @Test
    void delete_deasociar_institucion_200(){
        MiembroGremioCuatroCreateDto miembro = MiembroGremioCuatroCreateDto.builder()
                .idInstitucion(4)
                .idGremio(1)
                .build();
        given().body(miembro)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("/asociar-institucion").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void delete_desasociar_institucion_404(){
        MiembroGremioCuatroCreateDto miembro = MiembroGremioCuatroCreateDto.builder()
                .idInstitucion(10000)
                .idGremio(10000)
                .build();
        given().body(miembro)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("/asociar-institucion").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }
}
