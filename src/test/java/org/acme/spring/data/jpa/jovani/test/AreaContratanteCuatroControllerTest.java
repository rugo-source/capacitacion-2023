package org.acme.spring.data.jpa.jovani.test;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import org.acme.spring.data.jovani.institucion.external.rest.controllers.AreaContratanteCuatroController;
import org.acme.spring.data.jovani.institucion.external.rest.dto.AreaContratanteCuatroCreateDto;
import org.acme.spring.data.util.error.ErrorCodes;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestHTTPEndpoint(AreaContratanteCuatroController.class)
public class AreaContratanteCuatroControllerTest {

    @Test
    void list_200(){
        given().queryParam("idInstitucion", 1)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().get("/").then().statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void list_404(){
        given().queryParam("idInstitucion", 100)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().get("/").then().statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    /*
    @Test
    void post_200(){
        AreaContratanteCuatroCreateDto area = AreaContratanteCuatroCreateDto.builder()
                .idInstitucion(1)
                .area("area test")
                .subarea("subarea test")
                .telefono("1234567890")
                .extension("12345")
                .build();
        given().body(area)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
    */

    @Test
    void post_404(){
        AreaContratanteCuatroCreateDto area = AreaContratanteCuatroCreateDto.builder()
                .idInstitucion(10000)
                .area("area test")
                .subarea("subarea test")
                .telefono("1234567890")
                .extension("12345")
                .build();
        given().body(area)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void post_RNS001(){
        AreaContratanteCuatroCreateDto area = AreaContratanteCuatroCreateDto.builder().build();
        given().body(area)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(8)).body(
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.extension"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.telefono"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.telefono"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.subarea"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.subarea"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.idInstitucion"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.area"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.area")
                );
    }

    @Test
    void post_RNN014(){
        AreaContratanteCuatroCreateDto area = AreaContratanteCuatroCreateDto.builder()
                .idInstitucion(1)
                .area("area test")
                .subarea("subarea test")
                .telefono("12345")
                .extension("12345")
                .build();
        given().body(area)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(1)).body(
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.telefono")
                );
    }

    @Test
    void post_RNS002(){
        AreaContratanteCuatroCreateDto area = AreaContratanteCuatroCreateDto.builder()
                .idInstitucion(1)
                .area("")
                .subarea("")
                .telefono("1234567890")
                .extension("1234567")
                .build();
        given().body(area)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().post().then().log().all()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.size()", CoreMatchers.is(5)).body(
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.area"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.subarea"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.extension"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.area"),
                        CoreMatchers.containsString("create.areaContratanteCuatroCreateDto.subarea")
                );
    }

    @Test
    void put_200(){
        AreaContratanteCuatroCreateDto area = AreaContratanteCuatroCreateDto.builder()
                .idInstitucion(1)
                .area("area update")
                .subarea("subarea update")
                .telefono("0987654321")
                .extension("54321")
                .build();
        given().pathParam("idAreaContratante", 4).body(area)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idAreaContratante}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    void put_404_idInstitucion(){
        AreaContratanteCuatroCreateDto area = AreaContratanteCuatroCreateDto.builder()
                .idInstitucion(1000000)
                .area("area update")
                .subarea("subarea update")
                .telefono("0987654321")
                .extension("54321")
                .build();
        given().pathParam("idAreaContratante", 1).body(area)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idAreaContratante}").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void put_404_idAreaContratante(){
        AreaContratanteCuatroCreateDto area = AreaContratanteCuatroCreateDto.builder()
                .idInstitucion(1)
                .area("area update")
                .subarea("subarea update")
                .telefono("0987654321")
                .extension("54321")
                .build();
        given().pathParam("idAreaContratante", 1000000).body(area)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().put("{idAreaContratante}").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    /*
    @Test
    void delete_200(){
        given().pathParam("idAreaContratante", 7)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idAreaContratante}").then().log().all()
                .statusCode(Response.Status.OK.getStatusCode());
    }
    */

    @Test
    void delete_404(){
        given().pathParam("idAreaContratante", 10000)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idAreaContratante}").then().log().all()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    void delete_RNN023(){
        given().pathParam("idAreaContratante", 1)
                .headers(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON, HttpHeaders.ACCEPT,
                        MediaType.APPLICATION_JSON)
                .when().delete("{idAreaContratante}").then().log().all().
                statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                .body("details.code.get(0)", equalTo(ErrorCodes.RNN023.name()));

    }
}
