package org.acme.spring.data.ruben.institucionDos.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SedeDosJpaRepository extends JpaRepository<SedeJpa,Integer> {
    List<SedeJpa> findAllByIdInstitucion(Integer idInstitucion);
    Boolean existsByNombreOrAcronimo(String nombre, String acronimo);
}
