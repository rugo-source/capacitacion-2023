package org.acme.spring.data.ruben.institucionDos.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.ruben.institucionDos.core.business.input.GremioDosService;
import org.acme.spring.data.ruben.institucionDos.core.business.output.GremioDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.business.output.MiembroGremioDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.entity.AreasContratanteDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.GremioDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.GremioInstitucionDosCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class GremioDosBs implements GremioDosService {
    @Inject
    GremioDosRepository gremioDosRepository;
    @Inject
    MiembroGremioDosRepository miembroGremioDosRepository;

    @Override
    @Transactional
    public Either<ErrorCodes, List<Gremio>> get() {
        Either<ErrorCodes, List<Gremio>> resultado;
        var gremios = gremioDosRepository.findAll();
        if (gremios.isEmpty()) {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        } else {
            resultado = Either.right(gremios);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(GremioDosCreate gremioDosCreate) {
        Either<ErrorCodes, Boolean> resultado;
        if (validateRNN097(gremioDosCreate.getNombre(), gremioDosCreate.getAcronimo())) {
            resultado = Either.left(ErrorCodes.RNN097);
        } else {
            var gremioPersist = Gremio.builder()
                    .nombre(gremioDosCreate.getNombre())
                    .acronimo(gremioDosCreate.getAcronimo())
                    .build();
            resultado = Either.right(true);
            gremioDosRepository.save(gremioPersist);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer id, GremioDosCreate gremioDosCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var gremioSearc = gremioDosRepository.findBy(id);
        if (gremioSearc.isPresent()) {
            if (validateRNN097(gremioDosCreate.getNombre(), gremioDosCreate.getAcronimo())) {
                resultado = Either.left(ErrorCodes.RNN097);
            } else {
                var gremioChange = gremioSearc.get();
                gremioChange.setNombre(gremioDosCreate.getNombre());
                gremioChange.setAcronimo(gremioDosCreate.getAcronimo());
                gremioDosRepository.update(gremioChange);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> delete(Integer id) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var gremio = gremioDosRepository.findBy(id);
        if (gremio.isPresent()) {
            if (validateRNN024(gremio.get().getInstitucionList())) {
                resultado = Either.left(ErrorCodes.RNN024);
            } else {
                gremioDosRepository.deleteBy(id);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> asociarInstitucion(GremioInstitucionDosCreate gremioInstitucionDosCreate) {
        Either<ErrorCodes, Boolean> resultado;
        if (miembroGremioDosRepository.existByIdInstitucion(gremioInstitucionDosCreate.getIdInstitucion())) {
            resultado = Either.left(ErrorCodes.RNN026);
        } else {
            var gremioInstitucionPersist = MiembroGremio.builder()
                    .idGremio(gremioInstitucionDosCreate.getIdGremio())
                    .idInstitucion(gremioInstitucionDosCreate.getIdInstitucion())
                    .build();
            resultado = Either.right(true);
            gremioDosRepository.crearAsociacion(gremioInstitucionPersist);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> desasociarInstitucion(GremioInstitucionDosCreate gremioInstitucionDosCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var gremioInstSearch = miembroGremioDosRepository.findById(gremioInstitucionDosCreate);
        if (gremioInstSearch.isPresent()) {
            miembroGremioDosRepository.delete(gremioInstSearch.get());
            resultado = Either.right(true);
        }
        return resultado;
    }

    public Boolean validateRNN097(String nombre, String acronimo) {
        return gremioDosRepository.existByNombreOrAcronimo(nombre, acronimo);
    }

    public Boolean validateRNN024(List<Institucion> instituciones) {
        return instituciones.isEmpty();
    }
}
