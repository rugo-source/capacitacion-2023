package org.acme.spring.data.ruben.institucionDos.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.ruben.institucionDos.core.business.input.InstitucionDosService;
import org.acme.spring.data.ruben.institucionDos.core.business.output.InstitucionDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.entity.InstitucionDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.InstitutoDosBuscado;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class InstitucionDosBs implements InstitucionDosService {

    @Inject
    InstitucionDosRepository institucionDosRepository;

    @Override
    public Either<ErrorCodes, List<Institucion>> getInstitucionByAtt(InstitutoDosBuscado institutoDosBuscado) {
        Either<ErrorCodes, List<Institucion>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var institucionesEncontradas = institucionDosRepository.findByAtt(institutoDosBuscado);
        if (!institucionesEncontradas.isEmpty()) {
            resultado = Either.right(institucionesEncontradas);
        }
        return resultado;
    }

    @Override
    public List<Institucion> getAll() {
        return institucionDosRepository.findAllWithoutEje();
    }

    @Override
    public Either<ErrorCodes, Boolean> create(InstitucionDosCreate institucionDosCreate) {
        Either<ErrorCodes, Boolean> resultado;
        if (validateRNN013(institucionDosCreate.getIdentificador())) {
            resultado = Either.left(ErrorCodes.RNN013);
        } else if (validateRNN094(institucionDosCreate.getNombre())) {
            resultado = Either.left(ErrorCodes.RNN094);
        } else {
            var institutoPersits = Institucion.builder()
                    .idCategoria(institucionDosCreate.getIdCategoria())
                    .acronimo(institucionDosCreate.getAcronimo())
                    .cct(institucionDosCreate.getCct())
                    .idClasificacion(institucionDosCreate.getIdClasificacion())
                    .identificador(institucionDosCreate.getIdentificador())
                    .idSubsistemaBachillerato(institucionDosCreate.getIdSubsistemaBachillerato())
                    .idSubsistemaUniversidad(institucionDosCreate.getIdSubsistemaUniversidad())
                    .idTipo(institucionDosCreate.getIdTipo())
                    .nombre(institucionDosCreate.getNombre())
                    .numeroSedesRegistradas(2)
                    .build();
            institucionDosRepository.save(institutoPersits);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer id, InstitucionDosCreate institucionDosCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var instutoSearc = institucionDosRepository.findBy(id);
        if (instutoSearc.isPresent()) {
            if (validateRNN013(institucionDosCreate.getIdentificador())) {
                resultado = Either.left(ErrorCodes.RNN013);
            } else if (validateRNN094(institucionDosCreate.getNombre())) {
                resultado = Either.left(ErrorCodes.RNN094);
            } else {
                var institucionChange = instutoSearc.get();
                institucionChange.setAcronimo(institucionDosCreate.getAcronimo());
                institucionChange.setCct(institucionDosCreate.getCct());
                institucionChange.setNombre(institucionDosCreate.getNombre());
                institucionChange.setIdCategoria(institucionDosCreate.getIdCategoria());
                institucionChange.setIdClasificacion(institucionDosCreate.getIdClasificacion());
                institucionChange.setIdSubsistemaBachillerato(institucionDosCreate.getIdSubsistemaBachillerato());
                institucionChange.setIdSubsistemaUniversidad(institucionDosCreate.getIdSubsistemaUniversidad());
                institucionChange.setIdTipo(institucionDosCreate.getIdTipo());
                institucionChange.setIdentificador(institucionDosCreate.getIdentificador());
                institucionDosRepository.update(institucionChange);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    public Boolean validateRNN013(String identificador) {
        return institucionDosRepository.existIdentificador(identificador);
    }

    public Boolean validateRNN094(String nombre) {
        return institucionDosRepository.existNombre(nombre);
    }
}

