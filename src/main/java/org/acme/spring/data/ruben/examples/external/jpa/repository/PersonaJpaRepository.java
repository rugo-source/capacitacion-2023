package org.acme.spring.data.ruben.examples.external.jpa.repository;


import org.acme.spring.data.ruben.examples.external.jpa.model.PersonaDosJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaJpaRepository extends JpaRepository<PersonaDosJpa,Integer> {
}
