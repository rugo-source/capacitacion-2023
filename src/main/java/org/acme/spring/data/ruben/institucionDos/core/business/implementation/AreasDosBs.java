package org.acme.spring.data.ruben.institucionDos.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.ruben.institucionDos.core.business.input.AreasDosService;
import org.acme.spring.data.ruben.institucionDos.core.business.output.AreasDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.entity.AreasContratanteDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.business.output.InstitucionDosRepository;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class AreasDosBs implements AreasDosService {
    @Inject
    AreasDosRepository areasDosRepository;
    @Inject
    InstitucionDosRepository institucionDosRepository;


    @Override
    public Either<ErrorCodes, List<AreaContratante>> get(Integer id) {
        Either<ErrorCodes, List<AreaContratante>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if (institucionDosRepository.existById(id)) {
            var areasList = areasDosRepository.findAllById(id);
            resultado = Either.right(areasList);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(Integer idInstitucion, AreasContratanteDosCreate areasContratanteDosCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if (institucionDosRepository.existById(idInstitucion)) {
            var areasPersist = AreaContratante.builder()
                    .area(areasContratanteDosCreate.getArea())
                    .subarea(areasContratanteDosCreate.getSubArea())
                    .telefono(areasContratanteDosCreate.getTelefono())
                    .extension(areasContratanteDosCreate.getExtension())
                    .idInstitucion(idInstitucion)
                    .build();
            areasDosRepository.save(areasPersist);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer id, AreasContratanteDosCreate areasContratanteDosCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var areasSearc = areasDosRepository.findBy(id);
        if (areasSearc.isPresent()) {
            var areaChange = areasSearc.get();
            areaChange.setArea(areasContratanteDosCreate.getArea());
            areaChange.setSubarea(areasContratanteDosCreate.getSubArea());
            areaChange.setTelefono(areasContratanteDosCreate.getTelefono());
            areaChange.setExtension(areasContratanteDosCreate.getExtension());
            areasDosRepository.update(areaChange);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> delete(Integer id) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var areas = areasDosRepository.findBy(id);
        if (areas.isPresent()) {
            if (validateRNN023(areas.get().getIdInstitucion())) {
                resultado = Either.left(ErrorCodes.RNN023);
            } else {
                areasDosRepository.deleteBy(id);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    public Boolean validateRNN023(Integer idInstitucion) {
        var band = areasDosRepository.existOperativoByIdInstitucion(idInstitucion);
        if (!band.isEmpty()) {
            return true;
        }
        return false;
    }
}
