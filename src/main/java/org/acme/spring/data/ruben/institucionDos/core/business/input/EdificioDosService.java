package org.acme.spring.data.ruben.institucionDos.core.business.input;

import io.netty.util.AsyncMapping;
import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.ruben.institucionDos.core.entity.EdificioDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.SedeDosCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface EdificioDosService {
    Either<ErrorCodes, List<Edificio>> getEdificiosBy(Integer idSede);

    Either<ErrorCodes, Boolean> create( EdificioDosCreate edificioDosCreate);

    Either<ErrorCodes, Boolean>  update(Integer idEdificio, EdificioDosCreate edificioDosCreate);

    Either<ErrorCodes, Boolean>  delete(Integer idEdificio);
}
