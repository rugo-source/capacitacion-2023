package org.acme.spring.data.ruben.institucionDos.core.business.input;

import io.netty.util.AsyncMapping;
import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.ruben.institucionDos.core.entity.AreasContratanteDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.GremioDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.GremioInstitucionDosCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface GremioDosService {
    Either <ErrorCodes, List<Gremio>> get();

    Either <ErrorCodes, Boolean> create(GremioDosCreate gremioDosCreate);

    Either <ErrorCodes, Boolean> update(Integer id, GremioDosCreate gremioDosCreate);

    Either <ErrorCodes, Boolean> delete(Integer id);

    Either <ErrorCodes, Boolean> asociarInstitucion(GremioInstitucionDosCreate gremioInstitucionDosCreate);

    Either <ErrorCodes, Boolean> desasociarInstitucion(GremioInstitucionDosCreate gremioInstitucionDosCreate);
}
