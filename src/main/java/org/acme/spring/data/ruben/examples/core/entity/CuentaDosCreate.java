package org.acme.spring.data.ruben.examples.core.entity;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CuentaDosCreate {
    Integer idPersona;
    String rol;
    LocalDate inicio;
    LocalDate  fin;


}
