package org.acme.spring.data.ruben.institucionDos.external.rest.controllers;

import org.acme.spring.data.jovani.institucion.external.rest.dto.MiembroGremioCuatroCreateDto;
import org.acme.spring.data.ruben.institucionDos.core.business.input.GremioDosService;
import org.acme.spring.data.ruben.institucionDos.external.rest.dto.AreasContratantesDosCreateDto;
import org.acme.spring.data.ruben.institucionDos.external.rest.dto.GremioDosCreateDto;
import org.acme.spring.data.ruben.institucionDos.external.rest.dto.GremioInstitucionDosCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/catalogo-instituciones/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)


@Tag(name = " Catalogo de Institución - Ruben")

public class GremiosDosController {
    @Inject
    GremioDosService gremioDosService;

    @GET
    @Path("gremio-dos/")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY,implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response getAll() {
        return gremioDosService.get().map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @Path("gremio-dos/")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create( @Valid GremioDosCreateDto gremioDosCreateDto) {
        return gremioDosService.create(gremioDosCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }
    @PUT
    @Path("gremio-dos/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response update(@PathParam("id") Integer id, @Valid GremioDosCreateDto gremioDosCreateDto) {
        return gremioDosService.update(id,gremioDosCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("gremio-dos/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("id") Integer id) {
        return gremioDosService.delete(id).map(Response::ok).
                getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
    @POST
    @Path("gremio-dos/asociar-institucion")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response crearAsociacion(@Valid GremioInstitucionDosCreateDto gremioInstitucionDosCreateDto) {
        return gremioDosService.asociarInstitucion(gremioInstitucionDosCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
    @DELETE
    @Path("gremio-dos/desasociar-institucion")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response deleteAsociacion(@Valid  GremioInstitucionDosCreateDto gremioInstitucionDosCreateDto) {
        return gremioDosService.desasociarInstitucion(gremioInstitucionDosCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
