package org.acme.spring.data.ruben.examples.core.business.implementation;

import io.vavr.control.Either;

import org.acme.spring.data.ruben.examples.core.business.input.ExampleService;
import org.acme.spring.data.ruben.examples.core.business.output.UserRepository;

import org.acme.spring.data.cristian.examples.core.entity.Usuario;


import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class ExampleBs implements ExampleService {

    @Inject
    UserRepository userRepository;

    @Override
    public List<String> listaEjemplo() {
        List<Usuario> examples = userRepository.findAllNative();
        /*List<String> examples = new ArrayList<>();
        examples.add("uno");
        examples.add("dos");s
        examples.add("tres");
        examples.stream().forEach(letra -> System.out.println(letra));*/
        return examples.stream().map(usuario -> usuario.getLogin()).collect(Collectors.toList());
    }

    @Override
    public Either<Integer, String> get(Integer idProducto) {
        Either<Integer,String> result =Either.left(404);
        if (idProducto.equals(10)){
            result=Either.right("es el producto ");
        }
        return result;
    }
}
