package org.acme.spring.data.ruben.examples.core.business.output;

import org.acme.spring.data.ruben.examples.core.entity.Persona;
import org.acme.spring.data.ruben.examples.external.jpa.model.PersonaDosJpa;

import java.util.List;
import java.util.Optional;

public interface PersonaRepository {
    Optional<Persona> findBy(Integer idPersona);

    PersonaDosJpa save(Persona personaPersits);


    PersonaDosJpa update(Persona personaChange);

    void deleteBy(Integer id);

    List <Persona> findAll();
}
