package org.acme.spring.data.ruben.institucionDos.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ruben.institucionDos.core.entity.SedeDosCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SedeUnoCreate", description = "Esta en la entidad crea la sede")
public class SedeDosCreateDto {
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size ( min = 1,max = 100, message = "RNS002")
    @Schema(description = "este hace referencia tipo al nombre de una sede")
    private String nombre;
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size ( min = 1,max = 20, message = "RNS002")
    @Schema(description = "este hace referencia acronimo de una sede")
    private String acronimo;

    @JsonProperty
    @Positive
    @Schema(description = "este hace referencia a la capacidad de una sede")
    private Integer capacidad;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "este hace referencia al atributo si pertenece a una institucion")
    private Boolean isPertenece;

    public SedeDosCreate toEntity(){
        return SedeDosCreate.builder()
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .capacidad(this.capacidad)
                .IsPertenece(this.isPertenece)
                .build();
    }

}
