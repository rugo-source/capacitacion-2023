package org.acme.spring.data.ruben.institucionDos.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AreasContratanteDosCreate {

    private Integer id;
    private Integer idInstitucion;
    private Integer idDireccion;

    private String area;
    private String subArea;
    private String telefono;
    private String extension;

}
