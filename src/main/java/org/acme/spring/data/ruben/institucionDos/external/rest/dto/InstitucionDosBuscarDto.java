package org.acme.spring.data.ruben.institucionDos.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ruben.institucionDos.core.entity.InstitutoDosBuscado;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "InstitucionBusquedaByAtt", description = "Esta en la entidad buscar la institucion")
public class InstitucionDosBuscarDto {


    @JsonProperty
    @Size (min = 0, max = 4, message = "RNS002")
    @Schema(description = "este hace referencia al id de institucion")
    private String id;

    @JsonProperty
    @Schema(description = "este hace referencia al nombre de institucion")
    private String nombre;

    @JsonProperty
    @Size(min = 0, max = 20, message = "RNS002")
    @Schema(description = "este hace referencia al acronimo de institucion")
    private String acronimo;

    public InstitutoDosBuscado toEntity(){
        return InstitutoDosBuscado.builder()
                .identificador(this.id)
                .acronimo(this.acronimo)
                .nombre(this.nombre)
                .build();
    }

}
