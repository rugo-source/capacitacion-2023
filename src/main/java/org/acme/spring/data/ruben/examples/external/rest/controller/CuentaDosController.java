package org.acme.spring.data.ruben.examples.external.rest.controller;

import org.acme.spring.data.ruben.examples.core.business.input.CuentaDosService;
import org.acme.spring.data.ruben.examples.external.rest.dto.CuentaDosCreateUpdateDto;
import org.acme.spring.data.ruben.examples.external.rest.dto.CuentaDosDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.stream.Collectors;

@Path("/data/example/ruben/cuentaDos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion")
public class CuentaDosController {
    @Inject
    CuentaDosService cuentaDosService;
    @POST
    @Path("post")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create( @Valid CuentaDosDto cuentaDosDto) {
        return cuentaDosService.create(cuentaDosDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }

    @GET
    @Path("{idCuenta}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = CuentaDosDto.class)))
    @APIResponse(responseCode = "404", description = "Petición erronea", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response get(@PathParam("idCuenta") Integer idCuenta) {
        return cuentaDosService.get(idCuenta).map(CuentaDosDto::fromEntity).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("delete/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response delete(@PathParam("id") Integer id) {
        return cuentaDosService.delete(id).map(Response::ok).
                getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("put/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response update(@PathParam("id") Integer id, CuentaDosCreateUpdateDto cuentaDosCreateUpdateDto) {
        return cuentaDosService.update(id,cuentaDosCreateUpdateDto).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @GET
    @Path("{idPersona}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = CuentaDosDto.class)))
    @APIResponse(responseCode = "404", description = "Petición erronea", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response getBy(@PathParam("idPersona") Integer idPersona) {

        return cuentaDosService.getBy(idPersona).map(cuentaDos -> cuentaDos.stream().map(CuentaDosDto::fromEntity).collect(Collectors.toList())).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }
}
