package org.acme.spring.data.ruben.institucionDos.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ruben.institucionDos.core.entity.AreasContratanteDosCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AreasContratanteCreate", description = "Esta en la entidad regristra una area contratante")
public class AreasContratantesDosCreateDto {
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size ( min = 1,max = 255, message = "RNS002")
    @Schema(description = "este hace referencia tipo al area de area contratante")
    private String area;
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size ( min = 1,max = 255, message = "RNS002")
    @Schema(description = "este hace referencia tipo al sub area de area contratante")
    private String subArea;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")

    @Size ( min = 1,max = 10, message = "RNS002")
    @Schema(description = "este hace referencia tipo al telefono de area contratante")
    private String telefono;
    @JsonProperty
    @Size ( min = 1,max = 5, message = "RNS002")
    @Schema(description = "este hace referencia extension al area de area contratante")
    private String extension;

public AreasContratanteDosCreate toEntity(){
    return  AreasContratanteDosCreate.builder()
            .area(this.area)
            .subArea(this.subArea)
            .telefono(this.telefono)
            .extension(this.extension)
            .build();
}

}
