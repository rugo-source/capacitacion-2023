package org.acme.spring.data.ruben.institucionDos.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Edificio;

import java.util.List;
import java.util.Optional;

public interface EdificioDosRepository {
    List<Edificio> findByIdSede(Integer idSede);

    void save(Edificio edificioPersist);

    Optional<Edificio> findById(Integer idEdificio);

    void update(Edificio edificioChange);

    void deleteBy(Integer idEdificio);

    Boolean existsByNombreAndIdSede(String nombre, Integer idSede);

    Boolean existsByAcronimoAndIdSede(String acronimo, Integer idSede);

    Boolean existsByNombreAndIdSedeAndId(String nombre, Integer idSede, Integer id);

    Boolean existsByAcronimoAndIdSedeAndId(String acronimo, Integer idSede, Integer id);

    Boolean existsByIdSede(Integer idSede);
}
