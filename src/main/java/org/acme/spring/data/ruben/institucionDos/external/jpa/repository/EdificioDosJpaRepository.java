package org.acme.spring.data.ruben.institucionDos.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EdificioDosJpaRepository extends JpaRepository<EdificioJpa, Integer> {

    List<EdificioJpa> findAllByIdSede(Integer idSede);

    boolean existsByNombreAndIdSede(String nombre, Integer idSede);

    boolean existsByAcronimoAndIdSede(String acronimo, Integer idSede);

    boolean existsByNombreAndIdSedeAndId(String nombre, Integer idSede,Integer id);

    boolean existsByAcronimoAndIdSedeAndId(String acronimo, Integer idSede,Integer id);
    boolean existsByIdSede(Integer idSede);
}
