package org.acme.spring.data.ruben.examples.external.rest.controller;


import org.acme.spring.data.ruben.examples.core.business.input.PersonaService;
import org.acme.spring.data.ruben.examples.core.entity.PersonaCreate;

import org.acme.spring.data.ruben.examples.external.rest.dto.PersonaCreateDto;
import org.acme.spring.data.ruben.examples.external.rest.dto.PersonaDosDto;

import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/data/example/ruben/personaDos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion")
public class CrudController {

    @Inject
    PersonaService personaService;

    @GET
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class)))
    public Response list() {
        var list = personaService.listaEjemplo();
        return Response.ok(list).build();
    }
    @GET
    @Path("{idPersona}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = PersonaDosDto.class)))
    @APIResponse(responseCode = "404", description = "Petición erronea", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response get(@PathParam("idPersona") Integer idPersona) {
        return personaService.get(idPersona).map(PersonaDosDto::fromEntity).map(Response::ok)
                .getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

    @POST
    @Path("post")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = Boolean.class)))
    public Response create(PersonaCreateDto personaCreateDto) {
        return personaService.create(personaCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

    @PUT
    @Path("put/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response update(@PathParam("id") Integer id, PersonaCreate personaCreate) {
        return personaService.update(id,personaCreate).map(Response::ok)
                .getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }
    @DELETE
    @Path("delete/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response delete(@PathParam("id") Integer id) {
        return personaService.delete(id).map(Response::ok).
                getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

}
