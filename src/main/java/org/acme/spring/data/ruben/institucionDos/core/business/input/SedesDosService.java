package org.acme.spring.data.ruben.institucionDos.core.business.input;

import io.netty.util.AsyncMapping;
import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.jovani.institucion.core.entity.AreaContratanteCuatroCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.AreasContratanteDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.SedeDosCreate;
import org.acme.spring.data.ruben.institucionDos.external.rest.dto.SedeDosCreateDto;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface SedesDosService {
    Either<ErrorCodes, List<Sede >> getSedesById(Integer idInstitucion);

    Either<ErrorCodes, Boolean > create(Integer idInstitucion, SedeDosCreate sedeDosCreate);

    Either<ErrorCodes, Boolean > update(Integer idSede, SedeDosCreate sedeDosCreate);

    Either<ErrorCodes, Boolean > delete(Integer idSede);
}
