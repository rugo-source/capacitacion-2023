package org.acme.spring.data.ruben.examples.external.jpa.dao;

import org.acme.spring.data.ruben.examples.core.business.output.CuentaDosRepository;
import org.acme.spring.data.ruben.examples.core.entity.CuentaDos;
import org.acme.spring.data.ruben.examples.external.jpa.model.CuentaDosJpa;
import org.acme.spring.data.ruben.examples.external.jpa.repository.CuentaDosJpaRepositpory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class CuentaDosDao implements CuentaDosRepository {
    @Inject
    CuentaDosJpaRepositpory cuentaDosJpaRepositpory;

    @Override
    public List<CuentaDos> findByidPersona(Integer idPersona) {
        return cuentaDosJpaRepositpory.findByIdPersona(idPersona).stream().map(CuentaDosJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public CuentaDosJpa save(CuentaDos cuentaPersits) {
        return cuentaDosJpaRepositpory.saveAndFlush(CuentaDosJpa.fromEntity(cuentaPersits));
    }

    @Override
    public Optional<CuentaDos> findBy(Integer idCuenta) {
        return cuentaDosJpaRepositpory.findById(idCuenta).map(cuentaDosJpa ->{
            var cuentaEntidad =cuentaDosJpa.toEntity();
            return cuentaEntidad;
        } );
    }

    @Override
    public void deleteBy(Integer id) {
    cuentaDosJpaRepositpory.deleteById(id);
    }

    @Override
    public CuentaDosJpa update(CuentaDos cuentaChange) {
        return cuentaDosJpaRepositpory.saveAndFlush(CuentaDosJpa.fromEntity(cuentaChange));
    }
}
