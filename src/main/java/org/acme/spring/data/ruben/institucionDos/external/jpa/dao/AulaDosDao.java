package org.acme.spring.data.ruben.institucionDos.external.jpa.dao;


import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AulaJpa;

import org.acme.spring.data.ruben.institucionDos.core.business.output.AulaDosRepository;
import org.acme.spring.data.ruben.institucionDos.external.jpa.repository.AulaDosJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@ApplicationScoped
public class AulaDosDao implements AulaDosRepository {
    @Inject
    AulaDosJpaRepository aulaDosJpaRepository;

    @Override
    public Boolean existsByIdEdificio(Integer idEdificio) {
        return aulaDosJpaRepository.existsByIdEdificio(idEdificio);
    }


    @Override
    public List<Aula> findAllByIdEdificio(Integer idEdificio) {
        return aulaDosJpaRepository.findAllByIdEdificio(idEdificio).stream().map(AulaJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Aula aulaPersist) {
        aulaDosJpaRepository.saveAndFlush(AulaJpa.fromEntity(aulaPersist));
    }

    @Override
    public Optional<Aula> findById(Integer idAula) {
        return aulaDosJpaRepository.findById(idAula).map(aulaJpa -> {
            var aulaEntidad=aulaJpa.toEntity();
            return  aulaEntidad;
        });
    }

    @Override
    public void update(Aula aulaChange) {
    aulaDosJpaRepository.saveAndFlush(AulaJpa.fromEntity(aulaChange));
    }

    @Override
    public void deleteBy(Integer idAula) {
    aulaDosJpaRepository.deleteById(idAula);
    }

    @Override
    public Boolean existsByNombreAndIdEdificio(String nombre,Integer idEdificio) {
        return aulaDosJpaRepository.existsByNombreAndIdEdificio(nombre,idEdificio);
    }

}
