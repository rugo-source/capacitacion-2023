package org.acme.spring.data.ruben.institucionDos.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Sede;

import java.util.List;
import java.util.Optional;

public interface SedeDosRepository {
    List<Sede> findAllById(Integer idInstitucion);

    void save(Sede sedePersist);

    Optional<Sede> findBy(Integer idSede);

    void update(Sede sedeChange);

    void deleteBy(Integer idSede);

    Boolean existByNombreOrAcronimo(String nombre, String acronimo);

    List<String> findAllIdentificadores();
}
