package org.acme.spring.data.ruben.examples.external.jpa.repository;

import org.acme.spring.data.ruben.examples.external.jpa.model.CuentaDosJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CuentaDosJpaRepositpory extends JpaRepository<CuentaDosJpa,Integer> {

    @Query("from CuentaDosJpa where inicio <= current_date and fin >= current_date and  idPersona=:idPersona")
    List<CuentaDosJpa> findByIdPersona(@Param("idPersona") Integer idPersona);
}
