package org.acme.spring.data.ruben.institucionDos.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AreasDosJpaRepository extends JpaRepository<AreaContratanteJpa,Integer> {
    List<AreaContratanteJpa> findAllByIdInstitucion(Integer idInstitucion);
}
