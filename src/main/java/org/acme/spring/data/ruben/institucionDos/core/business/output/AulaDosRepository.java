package org.acme.spring.data.ruben.institucionDos.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Aula;

import java.util.List;
import java.util.Optional;

public interface AulaDosRepository {
    Boolean existsByIdEdificio(Integer idEdificio);

    List<Aula> findAllByIdEdificio(Integer idEdificio);

    void save(Aula aulaPersist);

   Optional<Aula>findById(Integer idAula);

    void update(Aula aulaChange);

    void deleteBy(Integer idAula);

    Boolean existsByNombreAndIdEdificio(String nombre,Integer idEdificio);
}
