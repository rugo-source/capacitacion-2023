package org.acme.spring.data.ruben.institucionDos.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.ruben.institucionDos.core.entity.InstitucionDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.InstitutoDosBuscado;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface InstitucionDosService {



    Either<ErrorCodes, List<Institucion>> getInstitucionByAtt(InstitutoDosBuscado institutoDosBuscado);

    List<Institucion> getAll();

    Either<ErrorCodes, Boolean> create(InstitucionDosCreate institucionDosCreate);

    Either<ErrorCodes, Boolean> update(Integer id, InstitucionDosCreate institucionDosCreate);
}
