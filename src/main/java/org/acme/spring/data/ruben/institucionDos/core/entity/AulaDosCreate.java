package org.acme.spring.data.ruben.institucionDos.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AulaDosCreate {
    private Integer id;
    private Integer idEdificio;
    private Integer idTipoEquipamiento;
    private String nombre;
    private Integer cantidadEquipamiento;
    private Integer capacidadEquipamiento;
    private String observaciones;
}

