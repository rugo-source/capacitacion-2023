package org.acme.spring.data.ruben.institucionDos.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;

import java.util.List;
import java.util.Optional;

public interface AreasDosRepository {
    List<AreaContratante> findAllById(Integer id);

    void save(AreaContratante areasPersist);

    Optional<AreaContratante> findBy(Integer id);

    void update(AreaContratante areaChange);

    void deleteBy(Integer id);

    List<AreaContratante> existOperativoByIdInstitucion(Integer idInstitucion);
}
