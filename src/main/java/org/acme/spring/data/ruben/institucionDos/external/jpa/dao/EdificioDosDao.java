package org.acme.spring.data.ruben.institucionDos.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.acme.spring.data.ruben.institucionDos.core.business.output.EdificioDosRepository;
import org.acme.spring.data.ruben.institucionDos.external.jpa.repository.EdificioDosJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class EdificioDosDao implements EdificioDosRepository {
    @Inject
    EdificioDosJpaRepository edificioDosJpaRepository;

    @Override
    public List<Edificio> findByIdSede(Integer idSede) {
        return edificioDosJpaRepository.findAllByIdSede(idSede).stream().map(EdificioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Edificio edificioPersist) {
    edificioDosJpaRepository.saveAndFlush(EdificioJpa.fromEntity(edificioPersist));
    }

    @Override
    public Optional<Edificio> findById(Integer idEdificio) {
        return edificioDosJpaRepository.findById(idEdificio).map(edificioJpa -> {
            var edificioEntidad = edificioJpa.toEntity();
            return edificioEntidad;
        });
    }

    @Override
    public void update(Edificio edificioChange) {
        edificioDosJpaRepository.saveAndFlush(EdificioJpa.fromEntity(edificioChange));
    }

    @Override
    public void deleteBy(Integer idEdificio) {
    edificioDosJpaRepository.deleteById(idEdificio);
    }

    @Override
    public Boolean existsByNombreAndIdSede(String nombre, Integer idSede) {
        return edificioDosJpaRepository.existsByNombreAndIdSede(nombre,idSede);
    }

    @Override
    public Boolean existsByAcronimoAndIdSede(String acronimo, Integer idSede) {
        return edificioDosJpaRepository.existsByAcronimoAndIdSede(acronimo,idSede);
    }

    @Override
    public Boolean existsByNombreAndIdSedeAndId(String nombre, Integer idSede, Integer id) {
        return edificioDosJpaRepository.existsByNombreAndIdSedeAndId(nombre, idSede, id);
    }

    @Override
    public Boolean existsByAcronimoAndIdSedeAndId(String acronimo, Integer idSede, Integer id) {
        return edificioDosJpaRepository.existsByAcronimoAndIdSedeAndId(acronimo, idSede, id);
    }

    @Override
    public Boolean existsByIdSede(Integer idSede) {
        return edificioDosJpaRepository.existsByIdSede(idSede);
    }
}
