package org.acme.spring.data.ruben.examples.external.rest.controller;

import org.acme.spring.data.ruben.examples.core.business.input.ExampleService;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/data/example/ruben")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion")
public class FirstController {

    @Inject
    ExampleService exampleService;

    @GET
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY,implementation = String.class)))
    public Response list() {
        var list=  exampleService.listaEjemplo();
        return Response.ok(list).build();
    }


    @GET
    @Path("{idProducto}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response get(@PathParam("idProducto") Integer idProducto) {
        return exampleService.get(idProducto).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

    @POST
    @Path("post")
    @APIResponse(responseCode = "400", description = "Petición error", content = @Content(schema = @Schema(implementation = Boolean.class)))
    public Response create() {
        return Response.ok(true).status(400).build();
    }

    @PUT
    @Path("put/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response update(@PathParam("id") Integer id) {
        return Response.ok(id).build();
    }

    @DELETE
    @Path("delete/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response delete(@PathParam("id") Integer id) {
        return Response.ok(id).build();
    }

}
