package org.acme.spring.data.ruben.institucionDos.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.GremioJpa;

import java.util.List;
import java.util.Optional;

public interface GremioDosRepository {
    List<Gremio> findAll();

    void save(Gremio gremioPersist);

    Optional<Gremio> findBy(Integer id);

    void update(Gremio gremioChange);

    void deleteBy(Integer id);

    Boolean existByNombreOrAcronimo(String nombre, String acronimo);


    void crearAsociacion(MiembroGremio gremioInstitucionPersist);
}
