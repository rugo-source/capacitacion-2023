package org.acme.spring.data.ruben.examples.core.business.output;

import org.acme.spring.data.cristian.examples.core.entity.Usuario;

import java.util.List;

public interface UserRepository {
    List<Usuario> findAll();
    List<Usuario> findAllNative();
    List<Usuario> findAllOrm();
}
