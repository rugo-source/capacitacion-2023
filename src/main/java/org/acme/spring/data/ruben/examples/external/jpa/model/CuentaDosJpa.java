package org.acme.spring.data.ruben.examples.external.jpa.model;

import lombok.*;

import org.acme.spring.data.ruben.examples.core.entity.CuentaDos;


import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "cuenta_dos")
public class CuentaDosJpa {
    @Id
    @Column(name = "id_cuenta") //cambiar name and generator
    @SequenceGenerator(name = "cuenta_dos_id_cuenta_seq", sequenceName = "cuenta_dos_id_cuenta_seq", allocationSize = 1)
    @GeneratedValue(generator = "cuenta_dos_id_cuenta_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "tx_rol")
    private String rol;
    @Column(name ="fk_id_persona")
    private Integer idPersona;
    @Column(name = "fh_inicio")
    private LocalDate inicio;
    @Column(name = "fh_fin")
    private LocalDate fin;

    @ManyToOne
    @JoinColumn(name = "fk_id_persona", referencedColumnName = "id_persona", insertable = false, updatable = false)
    private PersonaDosJpa personaDosJpa;

    public CuentaDos toEntity(){
        return CuentaDos.builder().id(this.id).rol(this.rol).fh_inicio(this.inicio).fh_fin(this.fin).idPersona(this.idPersona).build();
    }
    public static CuentaDosJpa fromEntity(CuentaDos cuentaDos){
        return CuentaDosJpa.builder().id(cuentaDos.getId()).rol(cuentaDos.getRol())
                .inicio(cuentaDos.getFh_inicio()).fin(cuentaDos.getFh_fin()).idPersona(cuentaDos.getIdPersona()).build();
    }


}
