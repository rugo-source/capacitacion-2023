package org.acme.spring.data.ruben.institucionDos.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.ruben.institucionDos.core.business.input.AulaDosService;
import org.acme.spring.data.ruben.institucionDos.core.business.output.AulaDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.business.output.EdificioDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.entity.AulaDosCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class AulaDosBs implements AulaDosService {
    @Inject
    AulaDosRepository aulaDosRepository;
    @Inject
    EdificioDosRepository edificioDosRepository;

    @Override
    public Either<ErrorCodes, List<Aula>> getAulasBy(Integer idEdificio) {
        Either<ErrorCodes, List<Aula>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var aulasEncontradas = aulaDosRepository.findAllByIdEdificio(idEdificio);
        if (!aulasEncontradas.isEmpty()) {
            var aulas = validateRNN129(aulasEncontradas);
            resultado = Either.right(aulas);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> create(AulaDosCreate aulaDosCreate) {
        Either<ErrorCodes, Boolean> resultado;
        if (validateRNN009(aulaDosCreate.getNombre(),aulaDosCreate.getIdEdificio())) {
            resultado = Either.left(ErrorCodes.RNN009);
        } else {
            var aulaPersist = Aula.builder()
                    .nombre(aulaDosCreate.getNombre())
                    .capacidadEquipamiento(aulaDosCreate.getCapacidadEquipamiento())
                    .cantidadEquipamiento(aulaDosCreate.getCantidadEquipamiento())
                    .idTipoEquipamiento(aulaDosCreate.getIdTipoEquipamiento())
                    .observaciones(aulaDosCreate.getObservaciones())
                    .idEdificio(aulaDosCreate.getIdEdificio())
                    .build();
            aulaDosRepository.save(aulaPersist);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer idAula, AulaDosCreate aulaDosCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var aulaSearch = aulaDosRepository.findById(idAula);
        if (aulaSearch.isPresent()) {
            if (validateRNN009(aulaDosCreate.getNombre(),aulaDosCreate.getIdEdificio())) {
                resultado = Either.left(ErrorCodes.RNN009);
            } else {
                var aulaChange = aulaSearch.get();
                aulaChange.setNombre(aulaDosCreate.getNombre());
                aulaChange.setCapacidadEquipamiento(aulaDosCreate.getCapacidadEquipamiento());
                aulaChange.setCantidadEquipamiento(aulaDosCreate.getCantidadEquipamiento());
                aulaChange.setIdTipoEquipamiento(aulaDosCreate.getIdTipoEquipamiento());
                aulaChange.setObservaciones(aulaDosCreate.getObservaciones());
                aulaDosRepository.update(aulaChange);
            }
            resultado = Either.right(true);

        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idAula) {

        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var aulaSearch = aulaDosRepository.findById(idAula);
        if (aulaSearch.isPresent()) {
            aulaDosRepository.deleteBy(idAula);
            resultado = Either.right(true);
        }
        return resultado;
    }

    public Boolean validateRNN009(String nombre ,Integer idEdificio) {
        return aulaDosRepository.existsByNombreAndIdEdificio(nombre,idEdificio);
    }

    public List<Aula> validateRNN129(List<Aula> aulas) {
        return aulas.stream().map(aula -> {
            aula.setCapacidad(aula.getCapacidadEquipamiento() * aula.getCantidadEquipamiento());
            return aula;
        }).collect(Collectors.toList());
    }
}
