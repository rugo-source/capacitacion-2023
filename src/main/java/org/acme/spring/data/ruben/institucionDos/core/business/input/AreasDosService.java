package org.acme.spring.data.ruben.institucionDos.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.ruben.institucionDos.core.entity.AreasContratanteDosCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface AreasDosService {
    Either<ErrorCodes, List<AreaContratante>> get(Integer id);


    Either<ErrorCodes, Boolean> create(Integer idInstitucion, AreasContratanteDosCreate areasContratanteDosCreate);

    Either<ErrorCodes, Boolean> update(Integer id, AreasContratanteDosCreate areasContratanteDosCreate);

    Either<ErrorCodes, Boolean> delete(Integer id);
}
