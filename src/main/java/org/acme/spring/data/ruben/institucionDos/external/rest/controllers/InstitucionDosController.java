package org.acme.spring.data.ruben.institucionDos.external.rest.controllers;

import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.ruben.examples.core.entity.PersonaCreate;
import org.acme.spring.data.ruben.examples.external.rest.dto.CuentaDosDto;
import org.acme.spring.data.ruben.institucionDos.core.business.input.InstitucionDosService;
import org.acme.spring.data.ruben.institucionDos.external.rest.dto.InstitucionDosBuscarDto;
import org.acme.spring.data.ruben.institucionDos.external.rest.dto.InstitucionDosCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/*
* Para que no repitas tantas veces institucion subelo  al nivel del path de la clase, con ello donde lo colocabas puedes borrar la anotacion path.
* trata de no dejar cosas comentadas en el codigo a menos que le pongas TODO por alguna futura modificacion.
* */
@Path("/catalogos-instituciones-dos/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Ruben")
public class InstitucionDosController {
    @Inject
    InstitucionDosService institucionDosService;

    //Gestionar Instituciones

    /*Aqui recuerda los nombres de las convenciones que les di, en este caso como estas regresando una coleccion, lo correcto seria qu eel metodo se llamase
    * listBy en este caso como no hay un metodo parecido lo puedes dejar simplemente asi, si hubira uno similar ahi si especificas despues del by lo que sea
    * que vas a buscar.
    * */
    @POST
    @Path("institucion/busquedas")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY,implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response getByAtt(InstitucionDosBuscarDto institucionDosBuscarDto) {
        return institucionDosService.getInstitucionByAtt(institucionDosBuscarDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }

    /*Aqui lo mismo que el anterior como regresas un array aqui el metodo deberia llamarse listAll*/
    @GET
    @Path("institucion")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY,implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response getAll( ) {
        var institutoList =institucionDosService.getAll();
        return Response.ok(institutoList).build();

    }

    //Registrar Instituciones
    @POST
    @Path("institucion")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(InstitucionDosCreateDto institucionDosCreateDto) {
        return institucionDosService.create(institucionDosCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }

    /*aqui puedes omitir la palabra put para el path.*/
    @PUT
    @Path("put/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response update(@PathParam("id") Integer id, InstitucionDosCreateDto institucionDosCreateDto) {
        return institucionDosService.update(id,institucionDosCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }


}
