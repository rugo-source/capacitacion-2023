package org.acme.spring.data.ruben.institucionDos.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.AulaJpa;

import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AulaDosJpaRepository extends JpaRepository<AulaJpa,Integer> {
    boolean existsByIdEdificio(Integer idEdificio);
    List<AulaJpa> findAllByIdEdificio(Integer idEdificio);
    boolean existsByNombreAndIdEdificio(String nombre,Integer idEficio);

}
