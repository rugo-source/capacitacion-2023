package org.acme.spring.data.ruben.examples.core.entity;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class CuentaDos {
    private Integer id;
    private Integer idPersona;

    private String rol;

    private LocalDate fh_inicio;
    private LocalDate fh_fin;

}
