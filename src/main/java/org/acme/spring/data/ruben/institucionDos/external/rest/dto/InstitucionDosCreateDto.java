package org.acme.spring.data.ruben.institucionDos.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ruben.institucionDos.core.entity.InstitucionDosCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "InstitucionCreate", description = "Esta en la entidad regristra una institucion")
public class InstitucionDosCreateDto {

    @JsonProperty
    @Positive
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Schema(description = "este hace tipo al id de institucion")
    private Integer idTipo;
    @JsonProperty
    @Positive
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Schema(description = "este hace clasificacion al id de institucion")
    private Integer idClasificacion;
    @JsonProperty
    @Positive
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Schema(description = "este hace categoria al id de institucion")
    private Integer idCategoria;
    @JsonProperty
    @Positive
    @Schema(description = "este hace sistema universidad al id de institucion")
    private Integer idSubsistemaUniversidad;
    @JsonProperty
    @Positive
    @Schema(description = "este hace tipo al sistema bachillerato de institucion")
    private Integer idSubsistemaBachillerato;

    @JsonProperty
    @Size(min = 0, max = 4, message = "RNS002")
    @Schema(description = "este hace referencia al id de institucion")
    private String identificador;
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Schema(description = "este hace referencia al nombre de institucion")
    private String nombre;
    @JsonProperty
    @Size(min = 0, max = 20, message = "RNS002")
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Schema(description = "este hace referencia al acronimo de institucion")
    private String acronimo;
    @JsonProperty
    @Size(min = 0, max = 20, message = "RNS002")
    @Schema(description = "este hace referencia al cct de institucion")
    private String cct;
 public InstitucionDosCreate toEntity(){
     return InstitucionDosCreate.builder()
             .acronimo(this.acronimo)
             .cct(this.cct)
             .idCategoria(this.idCategoria)
             .identificador(this.identificador)
             .idClasificacion(this.idClasificacion)
             .idTipo(this.idTipo)
             .nombre(this.nombre)
             .idSubsistemaBachillerato(this.idSubsistemaBachillerato)
             .idSubsistemaUniversidad(this.idSubsistemaUniversidad)
             .build();
 }

}
