package org.acme.spring.data.ruben.institucionDos.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.jovani.institucion.core.entity.AreaContratanteCuatroCreate;
import org.acme.spring.data.ruben.institucionDos.core.business.input.SedesDosService;
import org.acme.spring.data.ruben.institucionDos.core.business.output.EdificioDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.business.output.InstitucionDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.business.output.SedeDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.entity.AreasContratanteDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.SedeDosCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class SedeDosBs implements SedesDosService {
    @Inject
    InstitucionDosRepository institucionDosRepository;
    @Inject
    SedeDosRepository sedeDosRepository;
    @Inject
    EdificioDosRepository edificioDosRepository;

    @Override
    public Either<ErrorCodes, List<Sede>> getSedesById(Integer idInstitucion) {
        Either<ErrorCodes, List<Sede>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var instSearch = institucionDosRepository.existById(idInstitucion);
        if (instSearch) {
            var listaSede = sedeDosRepository.findAllById(idInstitucion);
            if (listaSede.isEmpty()) {
                resultado = Either.left(ErrorCodes.NOT_FOUND);
            } else {
                resultado = Either.right(listaSede);
            }
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(Integer idInstitucion, SedeDosCreate sedeDosCreate) {
        Either<ErrorCodes, Boolean> result;
        if (validateRNN096(sedeDosCreate.getNombre(), sedeDosCreate.getAcronimo())) {
            result = Either.left(ErrorCodes.RNN096);
        } else {
            var sedePersist = Sede.builder()
                    .idInstitucion(idInstitucion)
                    .nombre(sedeDosCreate.getNombre())
                    .acronimo(sedeDosCreate.getAcronimo())
                    .capacidad(sedeDosCreate.getCapacidad())
                    .identificador("" + idInstitucion + validateRNN004())
                    .propia(sedeDosCreate.getIsPertenece())
                    .build();
            result = Either.right(true);
            sedeDosRepository.save(sedePersist);
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer idSede, SedeDosCreate sedeDosCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var sedeSearch = sedeDosRepository.findBy(idSede);
        if (sedeSearch.isPresent()) {
            if (validateRNN096(sedeDosCreate.getNombre(), sedeDosCreate.getAcronimo())) {
                resultado = Either.left(ErrorCodes.RNN096);
            } else {
                var sedeChange = sedeSearch.get();
                sedeChange.setNombre(sedeDosCreate.getNombre());
                sedeChange.setAcronimo(sedeDosCreate.getAcronimo());
                sedeChange.setCapacidad(sedeDosCreate.getCapacidad());
                sedeChange.setPropia(sedeDosCreate.getIsPertenece());
                sedeDosRepository.update(sedeChange);
                resultado = Either.right(true);
            }
        }

        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> delete(Integer idSede) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var sedeSearch = sedeDosRepository.findBy(idSede);
        if (sedeSearch.isPresent()) {
            if (validateRNN016(idSede)) {
                resultado = Either.left(ErrorCodes.RNN016);
            } else {
                sedeDosRepository.deleteBy(idSede);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    public Boolean validateRNN096(String nombre, String acronimo) {
        return sedeDosRepository.existByNombreOrAcronimo(nombre, acronimo);
    }

    public String validateRNN004() {
        String numero = "";
        var identificador = sedeDosRepository.findAllIdentificadores();
        if (identificador.isEmpty()) {
            numero = String.format("%04d", 1);
        } else {
            var ident = Integer.parseInt(identificador.get(0));
            numero = String.format("%04d", ident + 1);
        }
        return numero;
    }

    public Boolean validateRNN016(Integer idSede) {
        return edificioDosRepository.existsByIdSede(idSede);
    }
}
