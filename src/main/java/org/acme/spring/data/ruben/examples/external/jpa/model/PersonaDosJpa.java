package org.acme.spring.data.ruben.examples.external.jpa.model;

import lombok.*;

import org.acme.spring.data.ruben.examples.core.entity.Persona;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "persona_dos")
public class PersonaDosJpa {
    @Id
    @Column(name = "id_persona") //cambiar name and generator
    @SequenceGenerator(name = "persona_dos_id_persona_seq", sequenceName = "persona_dos_id_persona_seq", allocationSize = 1)
    @GeneratedValue(generator = "persona_dos_id_persona_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "edad")
    private Integer edad;
    @Column(name ="fk_id_estado")
    private Integer idEstado;


    @ManyToOne
    @JoinColumn(name = "fk_id_estado", referencedColumnName = "id_estado", insertable = false, updatable = false)
    private EstadoPersonaDosJpa estadoPersonaDosJpa;
    public Persona toEntity() {
        return Persona.builder().id(this.id).nombre(this.nombre).edad(this.edad).idEstado(this.idEstado).build();
    }

    public static PersonaDosJpa fromEntity(Persona persona) {
        return PersonaDosJpa.builder().id(persona.getId()).nombre(persona.getNombre()).edad(persona.getEdad()).idEstado(persona.getIdEstado()).build();
    }
}
