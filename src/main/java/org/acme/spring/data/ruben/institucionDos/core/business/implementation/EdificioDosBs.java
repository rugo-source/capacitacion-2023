package org.acme.spring.data.ruben.institucionDos.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.ruben.institucionDos.core.business.input.EdificioDosService;
import org.acme.spring.data.ruben.institucionDos.core.business.output.AulaDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.business.output.EdificioDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.entity.EdificioDosCreate;
import org.acme.spring.data.ruben.institucionDos.core.entity.SedeDosCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class EdificioDosBs implements EdificioDosService {
    @Inject
    EdificioDosRepository edificioDosRepository;
    @Inject
    AulaDosRepository aulaDosRepository;

    @Override
    public Either<ErrorCodes, List<Edificio>> getEdificiosBy(Integer idSede) {
        Either<ErrorCodes, List<Edificio>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var institucionesEncontradas = edificioDosRepository.findByIdSede(idSede);
        if (!institucionesEncontradas.isEmpty()) {
            resultado = Either.right(institucionesEncontradas);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(EdificioDosCreate edificioDosCreate) {
        Either<ErrorCodes, Boolean> resultado;
        if (validateRNN002(edificioDosCreate.getNombre(), edificioDosCreate.getAcronimo(), edificioDosCreate.getIdSede())) {
            resultado = Either.left(ErrorCodes.RNN002);
        } else {
            var edificioPersist = Edificio.builder()
                    .idSede(edificioDosCreate.getIdSede())
                    .acronimo(edificioDosCreate.getAcronimo())
                    .nombre(edificioDosCreate.getNombre())
                    .referencia(edificioDosCreate.getReferencia())
                    .build();
            edificioDosRepository.save(edificioPersist);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer idEdificio, EdificioDosCreate edificioDosCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var edificioSearch = edificioDosRepository.findById(idEdificio);
        if (edificioSearch.isPresent()) {
            if (validateRNN002Update(edificioDosCreate.getNombre(), edificioDosCreate.getAcronimo(), edificioDosCreate.getIdSede(), idEdificio)) {
                resultado = Either.left(ErrorCodes.RNN002);
            } else {
                var edificioChange = edificioSearch.get();
                edificioChange.setNombre(edificioDosCreate.getNombre());
                edificioChange.setAcronimo(edificioDosCreate.getAcronimo());
                edificioChange.setReferencia(edificioDosCreate.getReferencia());
                edificioDosRepository.update(edificioChange);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> delete(Integer idEdificio) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var edificioSearch = edificioDosRepository.findById(idEdificio);
        if (edificioSearch.isPresent()) {
            if (validateRNN025(idEdificio)) {
                resultado = Either.left(ErrorCodes.RNN025);
            } else {
                edificioDosRepository.deleteBy(idEdificio);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    public Boolean validateRNN002(String nombre, String acronimo, Integer idSede) {
        return edificioDosRepository.existsByNombreAndIdSede(nombre, idSede) || edificioDosRepository.existsByAcronimoAndIdSede(acronimo, idSede);
    }

    public Boolean validateRNN002Update(String nombre, String acronimo, Integer idSede, Integer id) {
        return edificioDosRepository.existsByNombreAndIdSedeAndId(nombre, idSede, id) || edificioDosRepository.existsByAcronimoAndIdSedeAndId(acronimo, idSede, id);
    }

    public Boolean validateRNN025(Integer idEdificio) {
        return aulaDosRepository.existsByIdEdificio(idEdificio);
    }
}
