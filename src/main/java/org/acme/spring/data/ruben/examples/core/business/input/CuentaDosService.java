package org.acme.spring.data.ruben.examples.core.business.input;

import io.vavr.control.Either;

import org.acme.spring.data.ruben.examples.core.entity.CuentaDos;
import org.acme.spring.data.ruben.examples.core.entity.CuentaDosCreate;
import org.acme.spring.data.ruben.examples.external.rest.dto.CuentaDosCreateUpdateDto;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;


public interface CuentaDosService {
    Either<ErrorCodes,Boolean> create(CuentaDosCreate cuentaDosCreate);


    Either<ErrorCodes, CuentaDos> get(Integer idCuenta);

    Either<ErrorCodes,Boolean>  delete(Integer id);

    Either<ErrorCodes,Boolean> update(Integer id, CuentaDosCreateUpdateDto cuentaDosCreate);

    Either<ErrorCodes, List<CuentaDos>> getBy(Integer idPersona);

}
