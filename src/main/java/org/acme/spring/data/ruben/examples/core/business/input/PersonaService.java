package org.acme.spring.data.ruben.examples.core.business.input;


import io.vavr.control.Either;

import org.acme.spring.data.ruben.examples.core.entity.Persona;
import org.acme.spring.data.ruben.examples.core.entity.PersonaCreate;


import java.util.List;

public interface PersonaService {
    List<Persona> listaEjemplo();
    Either<Integer, Persona> get(Integer idPersona);

    Either<Integer,Boolean> create(PersonaCreate personaCreate);


    Either<Integer,Boolean>  update(Integer id, PersonaCreate personaCreate);

    Either<Integer,Boolean>  delete(Integer id);

}
