package org.acme.spring.data.ruben.institucionDos.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InstitucionDosJpaRepository extends JpaRepository<InstitucionJpa,Integer> {
    List<InstitucionJpa> findAllByIdentificadorOrNombreOrAcronimo(String identificador,String nombre,String acronimo);
    Boolean existsByIdentificador(String identificador);
    Boolean existsByNombre(String nombre);
    boolean existsById(Integer id);
}
