package org.acme.spring.data.ruben.examples.core.business.input;

import io.vavr.control.Either;

import java.util.List;

public interface ExampleService {
    List<String> listaEjemplo();
    Either<Integer,String> get(Integer idProducto);
}
