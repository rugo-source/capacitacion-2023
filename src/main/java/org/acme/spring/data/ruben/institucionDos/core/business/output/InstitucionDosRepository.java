package org.acme.spring.data.ruben.institucionDos.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.ruben.institucionDos.core.entity.InstitutoDosBuscado;

import java.util.List;
import java.util.Optional;

public interface InstitucionDosRepository {
    List<Institucion> findByAtt(InstitutoDosBuscado institutoDosBuscado);

    List<Institucion> findAllWithoutEje();

    Optional<Institucion> findBy(Integer id);

    void save(Institucion institucion);

    void update(Institucion institucionChange);

    Boolean existIdentificador(String identificador);

    Boolean existNombre(String nombre);
    Boolean existById(Integer id);
}
