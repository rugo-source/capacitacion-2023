package org.acme.spring.data.ruben.examples.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.ruben.examples.core.business.input.CuentaDosService;
import org.acme.spring.data.ruben.examples.core.business.output.CuentaDosRepository;
import org.acme.spring.data.ruben.examples.core.business.output.PersonaRepository;


import org.acme.spring.data.ruben.examples.core.entity.CuentaDosCreate;
import org.acme.spring.data.ruben.examples.external.rest.dto.CuentaDosCreateUpdateDto;

import org.acme.spring.data.ruben.examples.core.entity.CuentaDos;

import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.LocalDate;

import java.util.List;


@ApplicationScoped
public class CuentaDosBs implements CuentaDosService {
    @Inject
    CuentaDosRepository cuentaDosRepository;
    @Inject
    PersonaRepository personaRepository;


    @Override
    public Either<ErrorCodes, Boolean> create(CuentaDosCreate cuentaDosCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var personaExist = personaRepository.findBy(cuentaDosCreate.getIdPersona());
        if (personaExist.isPresent()) {
            if (validateRNN001(cuentaDosCreate.getInicio(), cuentaDosCreate.getFin())) {
                if (validateRNN002(cuentaDosCreate.getInicio())) {
                    var cuentaPersits = CuentaDos.builder()
                            .rol(cuentaDosCreate.getRol())
                            .fh_inicio(cuentaDosCreate.getInicio())
                            .fh_fin(cuentaDosCreate.getFin())
                            .idPersona(cuentaDosCreate.getIdPersona())
                            .build();
                    cuentaDosRepository.save(cuentaPersits);
                    resultado = Either.right(true);

                } else {
                    resultado = Either.left(ErrorCodes.RNN002);
                }
            } else {
                resultado = Either.left(ErrorCodes.RNN001);
            }
        }

        return resultado;
    }

    @Override
    public Either<ErrorCodes, CuentaDos> get(Integer idCuenta) {
        Either<ErrorCodes, CuentaDos> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var cuenta = cuentaDosRepository.findBy(idCuenta);
        if (cuenta.isPresent()) {
            var cuentaGet = cuenta.get();
            resultado = Either.right(cuentaGet);
        }

        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer id) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var cuenta = cuentaDosRepository.findBy(id);
        if (cuenta.isPresent()) {
            cuentaDosRepository.deleteBy(id);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer id, CuentaDosCreateUpdateDto cuentaDosCreateUpdateDto) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var cuenta = cuentaDosRepository.findBy(id);
        if (cuenta.isPresent()) {
            var cuentaChange = cuenta.get();
            cuentaChange.setRol(cuentaDosCreateUpdateDto.getRol());
            cuentaChange.setFh_fin(cuentaDosCreateUpdateDto.getFin());
            cuentaChange.setFh_inicio(cuentaDosCreateUpdateDto.getInicio());
            cuentaDosRepository.update(cuentaChange);
            resultado = Either.right(true);
        }

        return resultado;
    }

    @Override
    public Either<ErrorCodes, List<CuentaDos>> getBy(Integer idPersona) {
        Either<ErrorCodes, List<CuentaDos>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var cuentaSearched = personaRepository.findBy(idPersona);
        if (cuentaSearched.isPresent()) {
            var lista = cuentaDosRepository.findByidPersona(idPersona);
            resultado = Either.right(lista);
        }
        return  resultado;
    }
        public Boolean validateRNN001 (LocalDate inicio, LocalDate fin){
            return inicio.isBefore(fin);
        }

        public Boolean validateRNN002 (LocalDate inicio){
            return inicio.isAfter(LocalDate.now());
        }


    }




