package org.acme.spring.data.ruben.institucionDos.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.acme.spring.data.ruben.institucionDos.core.entity.GremioInstitucionDosCreate;

import java.util.Optional;

public interface MiembroGremioDosRepository {
    Optional<MiembroGremio> findById(GremioInstitucionDosCreate gremioInstitucionDosCreate);

    Boolean existByIdInstitucion(Integer idInstitucion);
    void delete(MiembroGremio miembroGremio);
}
