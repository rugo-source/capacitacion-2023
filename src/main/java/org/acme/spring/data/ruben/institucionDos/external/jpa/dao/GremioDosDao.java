package org.acme.spring.data.ruben.institucionDos.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.GremioJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.acme.spring.data.ruben.institucionDos.core.business.output.GremioDosRepository;
import org.acme.spring.data.ruben.institucionDos.external.jpa.repository.GremioDosJpaRepository;
import org.acme.spring.data.ruben.institucionDos.external.jpa.repository.MiembroGremioDosJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class GremioDosDao implements GremioDosRepository {
    @Inject
    GremioDosJpaRepository gremioDosJpaRepository;
    @Inject
    MiembroGremioDosJpaRepository miembroGremioDosJpaRepository;

    @Override
    public List<Gremio> findAll() {
        return gremioDosJpaRepository.findAll().stream().map(GremioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Gremio gremioPersist) {
        gremioDosJpaRepository.saveAndFlush(GremioJpa.fromEntity(gremioPersist));
    }

    @Override
    public Optional<Gremio> findBy(Integer id) {
        return gremioDosJpaRepository.findById(id).map(gremioJpa -> {
            var areasEntidad = gremioJpa.toEntity();
            return areasEntidad;
        });
    }

    @Override
    public void update(Gremio gremioChange) {
        gremioDosJpaRepository.saveAndFlush(GremioJpa.fromEntity(gremioChange));
    }

    @Override
    public void deleteBy(Integer id) {
        gremioDosJpaRepository.deleteById(id);
    }

    @Override
    public Boolean existByNombreOrAcronimo(String nombre, String acronimo) {
        return gremioDosJpaRepository.existsByNombreOrAcronimo(nombre, acronimo);
    }

    @Override
    public void crearAsociacion(MiembroGremio gremioInstitucionPersist) {
        var miembroGremioJpa = MiembroGremioJpa.builder()
                .id(MiembroGremioIdJpa.builder()
                        .idGremio(gremioInstitucionPersist.getIdGremio())
                        .idInstitucion(gremioInstitucionPersist.getIdInstitucion())
                        .build())
                .build();
        miembroGremioDosJpaRepository.saveAndFlush(miembroGremioJpa);
    }


}
