package org.acme.spring.data.ruben.institucionDos.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InstitutoDosBuscado {

    private String identificador;

    private String nombre;

    private String acronimo;

}
