package org.acme.spring.data.ruben.institucionDos.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.acme.spring.data.ruben.institucionDos.core.business.output.InstitucionDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.entity.InstitutoDosBuscado;
import org.acme.spring.data.ruben.institucionDos.external.jpa.repository.InstitucionDosJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class InstitucionDosDao implements InstitucionDosRepository {
    @Inject
    InstitucionDosJpaRepository institucionDosJpaRepository;
    @Inject
    EntityManager entityManager;

    @Override
    public List<Institucion> findByAtt(InstitutoDosBuscado institutoDosBuscado) {
        return institucionDosJpaRepository.findAllByIdentificadorOrNombreOrAcronimo(institutoDosBuscado.getIdentificador(), institutoDosBuscado.getNombre(), institutoDosBuscado.getAcronimo())
                .stream().map(InstitucionJpa::toEntity).collect(Collectors.toList());
    }

    /*aqui ruben declara los querys como un public final static string esto solo con el fin de que sean mas facil del ver al darles mantenimiento.
    *
    * Cuando un queri reciba parametros los nombres que des igual declaralos como public final static string por si en algun momento implementas una nueva funcion
    *
    * que utilice al menos uno de esos nombres de parametros, simplemente llames esa variable y acon eso queda pactado el mismo nombre.
    * */
    @Override
    public List<Institucion> findAllWithoutEje() {
        Stream<InstitucionJpa> resultado = entityManager.createNativeQuery("select distinct tin01.* from tin01_institucion tin01\n"
                + "left join tin04_ejecutivo tin04 on tin04.id_institucion=tin01.id_institucion\n"
                + "where tin04.id_ejecutivo is null ", InstitucionJpa.class).getResultStream();
        return resultado.map(InstitucionJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<Institucion> findBy(Integer id) {
        return institucionDosJpaRepository.findById(id).map(institucionJpa -> {
            var institucionEntidad = institucionJpa.toEntity();
            return institucionEntidad;
        });
    }

    @Override
    public void save(Institucion institucion) {
        institucionDosJpaRepository.saveAndFlush(InstitucionJpa.fromEntity(institucion));
    }

    @Override
    public void update(Institucion institucionChange) {
        institucionDosJpaRepository.saveAndFlush(InstitucionJpa.fromEntity(institucionChange));
    }

    @Override
    public Boolean existIdentificador(String identificador) {
        return institucionDosJpaRepository.existsByIdentificador(identificador);
    }

    @Override
    public Boolean existNombre(String nombre) {
        return institucionDosJpaRepository.existsByNombre(nombre);
    }

    @Override
    public Boolean existById(Integer id) {
        return  institucionDosJpaRepository.existsById(id);
    }

}
