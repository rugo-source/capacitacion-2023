package org.acme.spring.data.ruben.examples.core.business.output;

import org.acme.spring.data.ruben.examples.core.entity.CuentaDos;
import org.acme.spring.data.ruben.examples.external.jpa.model.CuentaDosJpa;

import java.util.List;
import java.util.Optional;

public interface CuentaDosRepository {
 List <CuentaDos> findByidPersona(Integer idPersona);
 CuentaDosJpa save(CuentaDos cuentaPersits);
 Optional<CuentaDos> findBy(Integer idCuenta);

 void deleteBy(Integer id);

 CuentaDosJpa update(CuentaDos cuentaChange);
}
