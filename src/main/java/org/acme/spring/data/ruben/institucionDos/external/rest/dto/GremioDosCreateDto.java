package org.acme.spring.data.ruben.institucionDos.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ruben.institucionDos.core.entity.GremioDosCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "GremioCreate", description = "Esta en la entidad regristra un gremio")
public class GremioDosCreateDto {
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 255, message = "RNS002")
    @Schema(description = "este hace referencia nombre de un gremio")
    private String nombre;
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 20, message = "RNS002")
    @Schema(description = "este hace referencia acronimo de gremio")
    private String acronimo;

    public GremioDosCreate toEntity() {
        return GremioDosCreate.builder()
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .build();
    }
}
