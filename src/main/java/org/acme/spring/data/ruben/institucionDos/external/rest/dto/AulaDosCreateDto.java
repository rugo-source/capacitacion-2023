package org.acme.spring.data.ruben.institucionDos.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ruben.institucionDos.core.entity.AulaDosCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AulaDosCreate", description = "Esta en la entidad regristra una aula")
public class AulaDosCreateDto {
    @JsonProperty
    @NotNull(message = "RNS001")
    @Positive(message = "RNS001")
    @Schema(description = "este atributo hace referencia al id del edificio al que pertenece el aula")
    Integer idEdifcio;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al nombre del aula")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Positive(message = "RNS001")
    @Schema(description = "este es el atributo que hace refencia al id del tipo de equipamiento del aula")
    Integer idTipoEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @PositiveOrZero(message = "RNN128")
    @Schema(description = "este es el atributo que hace referencia a la cantidad de equipamiento del aula")
    Integer cantidadEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @PositiveOrZero(message = "RNN128")
    @Schema(description = "este es el atributo que hace referencia a la capacidad de equipamiento del aula")
    Integer capacidadEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia a las observaciones del aula")
    String observaciones;

    public AulaDosCreate toEntity() {
        return AulaDosCreate.builder()
                .idTipoEquipamiento(this.idTipoEquipamiento)
                .cantidadEquipamiento(this.cantidadEquipamiento)
                .capacidadEquipamiento(this.capacidadEquipamiento)
                .idEdificio(this.idEdifcio)
                .nombre(this.nombre)
                .observaciones(this.observaciones)
                .build();
    }
}
