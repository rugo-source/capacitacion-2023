package org.acme.spring.data.ruben.examples.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonaCreate {
    private  String nombre;
    private Integer edad;
    private Integer idEstado;
}
