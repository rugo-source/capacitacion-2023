package org.acme.spring.data.ruben.examples.external.jpa.dao;

import org.acme.spring.data.ruben.examples.core.business.output.PersonaRepository;
import org.acme.spring.data.ruben.examples.core.entity.Persona;
import org.acme.spring.data.ruben.examples.external.jpa.model.PersonaDosJpa;
import org.acme.spring.data.ruben.examples.external.jpa.repository.PersonaJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class PersonaDao implements PersonaRepository {

    @Inject
    PersonaJpaRepository personaJpaRepository;

    @Override
    public Optional<Persona> findBy(Integer idPersona) {
        return personaJpaRepository.findById(idPersona).map(personaDosJpa ->{
            var personaEntidad =personaDosJpa.toEntity();
            personaEntidad.setEstadoPersonaDos(personaDosJpa.getEstadoPersonaDosJpa().toEntity());
            return personaEntidad;
        } );
    }

    @Override
    public PersonaDosJpa save(Persona personaPersits) {

        return personaJpaRepository.saveAndFlush(PersonaDosJpa.fromEntity(personaPersits));
    }


    @Override
    public PersonaDosJpa update(Persona personaChange) {
        return personaJpaRepository.saveAndFlush(PersonaDosJpa.fromEntity(personaChange));
    }

    @Override
    public void deleteBy(Integer id) {
        personaJpaRepository.deleteById(id);
    }

    @Override
    public List<Persona> findAll() {
        return personaJpaRepository.findAll().stream().map(personaDosJpa -> {
            var personaEntidad = personaDosJpa.toEntity();
            personaEntidad.setEstadoPersonaDos(personaDosJpa.getEstadoPersonaDosJpa().toEntity());
            return personaEntidad;
        }).collect(Collectors.toList());
    }

}
