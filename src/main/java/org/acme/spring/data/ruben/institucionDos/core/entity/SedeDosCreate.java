package org.acme.spring.data.ruben.institucionDos.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SedeDosCreate {

    private Integer id;
    private Integer idInstitucion;
    private Integer idDireccion;
    private Integer capacidad;

    private String nombre;
    private String acronimo;
    private String identificador;

    private Boolean IsPertenece;
}
