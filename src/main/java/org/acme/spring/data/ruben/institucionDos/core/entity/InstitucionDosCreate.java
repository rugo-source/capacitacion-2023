package org.acme.spring.data.ruben.institucionDos.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InstitucionDosCreate {

    private Integer id;
    private Integer idTipo;
    private Integer idClasificacion;
    private Integer idCategoria;
    private Integer idSubsistemaUniversidad;
    private Integer idSubsistemaBachillerato;

    private String identificador;
    private String nombre;
    private String acronimo;
    private String cct;

    private Integer numeroSedesRegistradas;
}
