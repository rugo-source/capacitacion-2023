package org.acme.spring.data.ruben.institucionDos.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.acme.spring.data.ruben.institucionDos.core.business.output.AreasDosRepository;
import org.acme.spring.data.ruben.institucionDos.external.jpa.repository.AreasDosJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class AreasDosDao implements AreasDosRepository {
    @Inject
    EntityManager entityManager;
    @Inject
    AreasDosJpaRepository areasDosJpaRepository;

    @Override
    public List<AreaContratante> findAllById(Integer id) {
        return areasDosJpaRepository.findAllByIdInstitucion(id).stream().map(areaContratanteJpa -> {
            var areaContratanteEntidad = areaContratanteJpa.toEntity();
            return areaContratanteEntidad;
        }).collect(Collectors.toList());
    }

    @Override
    public void save(AreaContratante areasPersist) {
        areasDosJpaRepository.save(AreaContratanteJpa.fromEntity(areasPersist));
    }

    @Override
    public Optional<AreaContratante> findBy(Integer id) {
        return areasDosJpaRepository.findById(id).map(areaContratanteJpa -> {
            var areasEntidad = areaContratanteJpa.toEntity();
            return areasEntidad;
        });
    }

    @Override
    public void update(AreaContratante areaChange) {
        areasDosJpaRepository.saveAndFlush(AreaContratanteJpa.fromEntity(areaChange));
    }

    @Override
    public void deleteBy(Integer id) {
        areasDosJpaRepository.deleteById(id);
    }

    @Override
    public List <AreaContratante> existOperativoByIdInstitucion(Integer idInstitucion) {
        Query query=entityManager.createNativeQuery("select distinct tin10.* from tin10_area_contratante tin10\n" +
                "left join tin15_responsable_operativo tin15 on tin15.fk_id_institucion=tin10.fk_id_institucion\n" +
                "where tin10.fk_id_institucion=:idInstitucion",AreaContratanteJpa.class);
            query.setParameter("idInstitucion",idInstitucion);
        return query.getResultList();
    }

}
