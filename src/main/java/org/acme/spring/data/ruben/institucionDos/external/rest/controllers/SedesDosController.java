package org.acme.spring.data.ruben.institucionDos.external.rest.controllers;

import org.acme.spring.data.jovani.institucion.external.rest.dto.AreaContratanteCuatroCreateDto;
import org.acme.spring.data.ruben.institucionDos.core.business.input.SedesDosService;
import org.acme.spring.data.ruben.institucionDos.external.rest.dto.AreasContratantesDosCreateDto;
import org.acme.spring.data.ruben.institucionDos.external.rest.dto.InstitucionDosBuscarDto;
import org.acme.spring.data.ruben.institucionDos.external.rest.dto.SedeDosCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/catalogo-instituciones/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)


@Tag(name = " Catalogo de Institución - Ruben")
public class SedesDosController {
    @Inject
    SedesDosService sedesDosService;

    @POST
    @Path("sedes-dos/busqueda/{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY,implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response get(@PathParam("idInstitucion")Integer idInstitucion) {
        return sedesDosService.getSedesById(idInstitucion).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }
    @POST
    @Path("sedes-dos/{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@PathParam("idInstitucion")  Integer idInstitucion, @Valid SedeDosCreateDto sedeDosCreateDto) {
        return sedesDosService.create(idInstitucion,sedeDosCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }
    @PUT
    @Path("sedes-dos/{idSede}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idSede") Integer idSede, @Valid SedeDosCreateDto sedeDosCreateDto) {
        return sedesDosService.update(idSede, sedeDosCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
    @DELETE
    @Path("sedes-dos/{idSede}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idSede") Integer idSede) {
        return sedesDosService.delete(idSede).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
