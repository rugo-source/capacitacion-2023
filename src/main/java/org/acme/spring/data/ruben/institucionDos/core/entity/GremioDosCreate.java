package org.acme.spring.data.ruben.institucionDos.core.entity;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GremioDosCreate {
    private Integer id;
    private String nombre;
    private String acronimo;


}
