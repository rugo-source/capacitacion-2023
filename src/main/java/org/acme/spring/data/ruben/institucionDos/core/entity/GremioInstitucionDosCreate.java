package org.acme.spring.data.ruben.institucionDos.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GremioInstitucionDosCreate {
    private Integer idGremio;
    private Integer IdInstitucion;
}
