package org.acme.spring.data.ruben.institucionDos.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ruben.institucionDos.core.entity.GremioInstitucionDosCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "GremioCreate", description = "Esta en la entidad regristra un gremio")
public class GremioInstitucionDosCreateDto {
    @JsonProperty
    @Positive
    @NotNull(message = "RNS001")
    @Schema(description = "este hace tipo al id de institucion")
    private Integer idGremio;
    @JsonProperty
    @Positive
    @NotNull(message = "RNS001")
    @Schema(description = "este hace tipo al id de institucion")
    private Integer idInstitucion;

    public GremioInstitucionDosCreate toEntity(){
        return GremioInstitucionDosCreate.builder()
                .IdInstitucion(this.idInstitucion)
                .idGremio(this.idGremio)
                .build();
    }
}
