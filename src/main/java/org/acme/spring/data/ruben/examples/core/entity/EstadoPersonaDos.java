package org.acme.spring.data.ruben.examples.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EstadoPersonaDos {
    private Integer id;
    private String nombre;
}
