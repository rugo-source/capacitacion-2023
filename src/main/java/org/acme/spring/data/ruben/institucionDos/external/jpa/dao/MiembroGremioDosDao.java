package org.acme.spring.data.ruben.institucionDos.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.acme.spring.data.ruben.institucionDos.core.business.output.MiembroGremioDosRepository;
import org.acme.spring.data.ruben.institucionDos.core.entity.GremioInstitucionDosCreate;
import org.acme.spring.data.ruben.institucionDos.external.jpa.repository.MiembroGremioDosJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Optional;

@ApplicationScoped
public class MiembroGremioDosDao implements MiembroGremioDosRepository {
    public static final String QUERY_BUSCAR_INSTITUCIONES_ASOCIADAS = "select count(tin06.fk_id_institucion) > 0 from tin06_gremio_institucion tin06 where tin06.fk_id_institucion = :idInstitucion";

    @Inject
    EntityManager entityManager;
    @Inject
    MiembroGremioDosJpaRepository miembroGremioDosJpaRepository;

    @Override
    public Optional<MiembroGremio> findById(GremioInstitucionDosCreate gremioInstitucionDosCreate) {
        return miembroGremioDosJpaRepository.findById(MiembroGremioIdJpa.builder()
                .idGremio(gremioInstitucionDosCreate.getIdGremio())
                .idInstitucion(gremioInstitucionDosCreate.getIdInstitucion())
                .build()).map(MiembroGremioJpa::toEntity);

    }

    @Override
    public void delete(MiembroGremio miembroGremio) {
        miembroGremioDosJpaRepository.deleteById(MiembroGremioIdJpa.builder()
                .idGremio(miembroGremio.getIdGremio())
                .idInstitucion(miembroGremio.getIdInstitucion())
                .build());
    }
    @Override
    public Boolean existByIdInstitucion(Integer idInstitucion) {
        Query q = entityManager.createNativeQuery(QUERY_BUSCAR_INSTITUCIONES_ASOCIADAS);
        q.setParameter("idInstitucion", idInstitucion);
        Boolean result = ((Boolean) q.getSingleResult()).booleanValue();
        return result;
    }

}
