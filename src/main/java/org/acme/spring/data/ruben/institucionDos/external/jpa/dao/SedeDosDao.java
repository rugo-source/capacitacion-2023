package org.acme.spring.data.ruben.institucionDos.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;
import org.acme.spring.data.ruben.institucionDos.core.business.output.SedeDosRepository;
import org.acme.spring.data.ruben.institucionDos.external.jpa.repository.SedeDosJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class SedeDosDao implements SedeDosRepository {
    private static final String QUERY_OBTENER_IDENTIFICADOR="select  substring( tx_identificador, LENGTH(tx_identificador)-2, LENGTH(tx_identificador))identificador from tin05_sede order by identificador desc ";
    @Inject
    EntityManager entityManager;
    @Inject
    SedeDosJpaRepository sedeDosJpaRepository;

    @Override
    public List<Sede> findAllById(Integer idInstitucion) {
        return sedeDosJpaRepository.findAllByIdInstitucion(idInstitucion).stream()
                .map(SedeJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Sede sedePersist) {
    sedeDosJpaRepository.saveAndFlush(SedeJpa.fromEntity(sedePersist));
    }

    @Override
    public Optional<Sede> findBy(Integer idSede) {
        return sedeDosJpaRepository.findById(idSede).map(sedeJpa -> {
            var sedeEntidad = sedeJpa.toEntity();
            return sedeEntidad;
        });
    }

    @Override
    public void update(Sede sedeChange) {
    sedeDosJpaRepository.saveAndFlush(SedeJpa.fromEntity(sedeChange));
    }

    @Override
    public void deleteBy(Integer idSede) {
    sedeDosJpaRepository.deleteById(idSede);
    }

    @Override
    public Boolean existByNombreOrAcronimo(String nombre, String acronimo) {
        return sedeDosJpaRepository.existsByNombreOrAcronimo(nombre,acronimo);
    }

    @Override
    public List<String> findAllIdentificadores() {
        Query query=entityManager.createNativeQuery(QUERY_OBTENER_IDENTIFICADOR);
        return query.getResultList();
    }
}

