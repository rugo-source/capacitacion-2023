package org.acme.spring.data.ruben.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ruben.examples.core.entity.CuentaDos;
import org.acme.spring.data.ruben.examples.core.entity.CuentaDosCreate;
import org.acme.spring.data.util.StringConstants;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "CuentaDosCreate", description = "Esta en la entidad para guardar una cuenta")
public class CuentaDosDto {
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3, max = 100, message = "RNS002")
    @Schema(description = "este es el atributo que referencia al rol de la cuenta")
    private String rol;
    @JsonProperty
    @NotNull(message = "RNS001")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringConstants.LOCAL_DATE_FORMAT)
    @Schema(description = "este es el atributo que referencia a la fecha de la creacion", format = "string", implementation = String.class)
    private LocalDate inicio;
    @JsonProperty
    @NotNull(message = "RNS001")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringConstants.LOCAL_DATE_FORMAT)
    @Schema(description = "este es el atributo que referencia a la fecha del final", format = "string", implementation = String.class)
    private LocalDate  fin;
    @JsonProperty
    @Schema(description = "este es el atributo que referencia al id de persona")
    private Integer idPersona;

    public CuentaDosCreate toEntity(){
        return CuentaDosCreate.builder()
                .rol(this.rol)
                .fin(this.fin)
                .inicio(this.inicio)
                .idPersona(this.idPersona)
                .build();
    }
    public static CuentaDosDto fromEntity(CuentaDos cuentaDos){
        return CuentaDosDto.builder()
                .rol(cuentaDos.getRol())
                .inicio(cuentaDos.getFh_inicio())
                .fin(cuentaDos.getFh_fin())
                .idPersona(cuentaDos.getIdPersona())
                .build();

    }

}
