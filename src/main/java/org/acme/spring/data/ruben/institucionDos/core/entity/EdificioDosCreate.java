package org.acme.spring.data.ruben.institucionDos.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EdificioDosCreate {
    private Integer idSede;
    private String nombre;
    private String acronimo;
    private String referencia;
}
