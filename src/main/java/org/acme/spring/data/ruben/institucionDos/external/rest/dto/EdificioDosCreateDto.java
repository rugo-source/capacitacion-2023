package org.acme.spring.data.ruben.institucionDos.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ruben.institucionDos.core.entity.EdificioDosCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "EdificioDosCreate", description = "Esta en la entidad regristra un edificio")
public class EdificioDosCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @Positive(message = "RNS001")
    Integer idSede;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    String acronimo;

    @JsonProperty
    @NotNull(message = "RNS001")
    String referencia;


    public EdificioDosCreate toEntity(){
        return EdificioDosCreate.builder()
                .acronimo(this.acronimo)
                .idSede(this.idSede)
                .nombre(this.nombre)
                .referencia(this.referencia)
                .build();
    }
}


