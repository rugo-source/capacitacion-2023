package org.acme.spring.data.ruben.institucionDos.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.GremioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GremioDosJpaRepository extends JpaRepository<GremioJpa,Integer> {

    Boolean existsByNombreOrAcronimo(String nombre,String acronimo);
}
