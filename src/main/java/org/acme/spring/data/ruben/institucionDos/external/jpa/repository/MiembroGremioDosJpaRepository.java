package org.acme.spring.data.ruben.institucionDos.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MiembroGremioDosJpaRepository extends JpaRepository<MiembroGremioJpa, MiembroGremioIdJpa> {
}
