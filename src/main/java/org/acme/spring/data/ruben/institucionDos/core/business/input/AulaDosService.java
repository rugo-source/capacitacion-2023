package org.acme.spring.data.ruben.institucionDos.core.business.input;

import io.netty.util.AsyncMapping;
import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;

import org.acme.spring.data.ruben.institucionDos.core.entity.AulaDosCreate;

import org.acme.spring.data.ruben.institucionDos.core.entity.EdificioDosCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface AulaDosService {
    Either<ErrorCodes, List<Aula>> getAulasBy(Integer idEdificio);

    Either<ErrorCodes, Boolean>  create( AulaDosCreate aulaDosCreate);

    Either<ErrorCodes, Boolean>  update(Integer idAula, AulaDosCreate aulaDosCreate);

    Either<ErrorCodes, Boolean> delete(Integer idAula);
}
