package org.acme.spring.data.ruben.institucionDos.external.rest.controllers;

import org.acme.spring.data.ruben.institucionDos.core.business.input.AulaDosService;
import org.acme.spring.data.ruben.institucionDos.core.business.input.EdificioDosService;

import org.acme.spring.data.ruben.institucionDos.external.rest.dto.AulaDosCreateDto;

import org.acme.spring.data.ruben.institucionDos.external.rest.dto.EdificioDosCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/catalogo-instituciones/aulas-dos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)


@Tag(name = " Catalogo de Institución - Ruben")
public class AulasDosController {
    @Inject
    AulaDosService aulaDosService;

    @GET
    @Path("")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY,implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response get(@QueryParam("idEdificio") Integer idEdificio) {
        return aulaDosService.getAulasBy(idEdificio).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }
    @POST
    @Path("")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid AulaDosCreateDto aulaDosCreateDto) {
        return aulaDosService.create(aulaDosCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }
    @PUT
    @Path("/{idAula}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))

    public Response update(@PathParam("idAula") Integer idAula, @Valid AulaDosCreateDto aulaDosCreateDto) {
        return aulaDosService.update(idAula, aulaDosCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
    @DELETE
    @Path("/{idAula}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idAula") Integer idAula) {
        return aulaDosService.delete(idAula).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
