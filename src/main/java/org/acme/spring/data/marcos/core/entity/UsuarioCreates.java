package org.acme.spring.data.marcos.core.entity;


import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor

public class UsuarioCreates {
    private String login;
    private String password;

}
