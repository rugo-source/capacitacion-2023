package org.acme.spring.data.marcos.external.jpa.model;

import lombok.*;
import org.acme.spring.data.marcos.core.entity.Usuario;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tca02_usuario")
public class UsuarioDosJpa {

    @Id
    @Column(name = "id_usuario")
    @SequenceGenerator(name = "tca02_usuario_id_usuario_seq", sequenceName = "tca02_usuario_id_usuario_seq", allocationSize = 1)
    @GeneratedValue(generator = "tca02_usuario_id_usuario_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "fk_id_estado")
    private Integer idEstado;
    @Column(name = "tx_login")
    private String login;
    @Column(name = "tx_password")
    private String password;
    @Column(name = "nu_intento")
    private Integer intento;
    @Column(name = "fh_bloqueo")
    private LocalDateTime bloqueo;
    @Column(name = "fh_aviso_privacidad")
    private LocalDateTime avisoPrivacidad;

    @ManyToOne
    @JoinColumn(name = "fk_id_estado", referencedColumnName = "id_estado", insertable = false, updatable = false)
    private EstadoUsuarioDosJpa estadoUsuario;

    public Usuario toEntity() {
        return Usuario.builder().id(this.id).idEstado(this.idEstado).login(this.login).password(this.password)
                .intento(this.intento).bloqueo(this.bloqueo).avisoPrivacidad(this.avisoPrivacidad).build();
    }

    public static UsuarioDosJpa fromEntity(Usuario usuario) {
        return UsuarioDosJpa.builder().id(usuario.getId()).idEstado(usuario.getIdEstado()).login(usuario.getLogin())
                .password(usuario.getPassword()).intento(usuario.getIntento()).bloqueo(usuario.getBloqueo())
                .avisoPrivacidad(usuario.getAvisoPrivacidad()).build();
    }
}
