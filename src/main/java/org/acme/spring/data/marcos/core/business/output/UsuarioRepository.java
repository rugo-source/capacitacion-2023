package org.acme.spring.data.marcos.core.business.output;

import org.acme.spring.data.marcos.core.entity.Usuario;


import java.util.List;
import java.util.Optional;

public interface UsuarioRepository {
    List<Usuario> findAll();
    List<Usuario> findAllNative();
    List<Usuario> findAllOrm();

    Optional<Usuario> findBy(Integer idUsuario);

    void save(Usuario usuarioPersits);

    void update(Usuario usuarioChange);

    void deleteBy(Integer id);

    //void save(Usuario usuarioPersits);

    //void update(Usuario usuarioChange);

    //void deleteBy(Integer id);



}
