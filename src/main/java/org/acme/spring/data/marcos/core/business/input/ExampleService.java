package org.acme.spring.data.marcos.core.business.input;

import io.netty.util.AsyncMapping;
import io.vavr.control.Either;
import org.acme.spring.data.marcos.core.entity.UsuarioCreates;

import javax.ws.rs.core.Response;
import java.util.List;

public interface ExampleService {

    List<String> listaEjemplos();
    Either<Integer,String> get(Integer idProducto);

    Either<Integer, Boolean> create(UsuarioCreates toEntity);

    Either<Integer, Boolean> update(Integer id, UsuarioCreates usuarioCreates);

    Either<Integer, Boolean> delete(Integer id);
}
