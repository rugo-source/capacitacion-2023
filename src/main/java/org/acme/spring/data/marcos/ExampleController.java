package org.acme.spring.data.marcos;

import org.acme.spring.data.marcos.core.business.input.ExampleService;
import org.acme.spring.data.marcos.core.entity.UsuarioCreates;
import org.acme.spring.data.marcos.external.rest.dto.UsuarioCreateDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
//import java.util.ArrayList;

@Path("/data/example/mark")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion - Marcos")

public class ExampleController {

    @Inject
    ExampleService exampleService;

    @GET
    @APIResponse(responseCode = "200", description = "Conexion Exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class )))
    public Response list(){
        var list = exampleService.listaEjemplos();
        return Response.ok(list).build();
    }


  /*  @GET
    @Path("{idProducto}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response get(@PathParam("idProducto") Integer idProducto){
        return exampleService.get(idProducto).map(Response::ok)
                .getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }
    */

    @Path("{idUsuario}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response get(@PathParam("idUsuario") Integer idUsuario){
         return exampleService.get(idUsuario).map(Response::ok)
              .getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

    @POST
    @Path("post")
    @APIResponse(responseCode = "200", description = "Conexion Exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "404", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = Boolean.class)))
    public Response create(UsuarioCreateDto usuarioCreateDto){
        return exampleService.create(usuarioCreateDto.toEntity()).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

    @PUT
    @Path("put/{id}")
    @APIResponse(responseCode = "200", description = "Conexion Exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response update(@PathParam("id") Integer id, UsuarioCreates usuarioCreates){
        return exampleService.update(id, usuarioCreates).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

    @DELETE
    @Path("delete/{id}")
    @APIResponse(responseCode = "200", description = "Conexion Exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response delete(@PathParam("id") Integer id){
        return exampleService.delete(id).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }













}
