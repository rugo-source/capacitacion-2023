package org.acme.spring.data.marcos.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.marcos.core.entity.UsuarioCreates;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "UsuarioCreates",description = "Esta es la entidad para guardar un usuario")
public class UsuarioCreateDto {

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al correo del usuario")
    private String login;

    @JsonProperty
    @Schema(description = "Este es el password con el que creara el programa")
    private String password;

    public UsuarioCreates toEntity(){
        return UsuarioCreates.builder()
                .login(this.login)
                .password(this.password)
                .build();
    }
}
