package org.acme.spring.data.marcos.core.entity;


import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class EstadoUsuario {

    private Integer id;
    private String nombre;
}
