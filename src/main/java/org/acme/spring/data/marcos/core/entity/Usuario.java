package org.acme.spring.data.marcos.core.entity;

import lombok.*;


import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class Usuario {

    private Integer id;
    private Integer idEstado;
    private String login;
    private String password;
    private Integer intento;
    private LocalDateTime bloqueo;
    private LocalDateTime avisoPrivacidad;

    private Boolean registrar;
    private Boolean editar;
    private Boolean eliminar;
    private Boolean consultar;
    private Boolean configurar;

    private EstadoUsuario estadoUsuario;
}
