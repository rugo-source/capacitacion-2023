package org.acme.spring.data.marcos.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.marcos.core.business.input.ExampleService;
import org.acme.spring.data.marcos.core.business.output.UsuarioRepository;
import org.acme.spring.data.marcos.core.entity.Usuario;
import org.acme.spring.data.marcos.core.entity.UsuarioCreates;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class ExmapleBs implements ExampleService {

    @Inject
    UsuarioRepository usuarioRepository;

    @Override
    public List<String> listaEjemplos() {
        List<String> examples = new ArrayList<>();
        examples.add("A");
        examples.add("B");
        examples.add("C");
        examples.add("D");
        examples.stream().forEach(letra -> System.out.println(letra));

        return examples;
    }

    @Override
    public Either<Integer, String> get(Integer idUsuario) {
        Either<Integer, String> resultado = Either.left(404);
        var usuario = usuarioRepository;
      /*  if(idUsuario){
            resultado = Either.right("Es el producto");
        }*/
        return resultado;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> create(UsuarioCreates usuarioCreates) {
        Either<Integer, Boolean> resultado = Either.right(true);
        var usuarioPersits = Usuario.builder()
                .login(usuarioCreates.getLogin())
                .password(usuarioCreates.getPassword())
                .intento(0)
                .id(5)
                .idEstado(1)
                .build();

        usuarioRepository.save(usuarioPersits);
        return resultado;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> update(Integer id, UsuarioCreates usuarioCreates) {
        Either<Integer, Boolean> resultado = Either.left(404);
        var usuarioSearch = usuarioRepository.findBy(id);
        if(usuarioSearch.isPresent()){
            var usuarioChange = usuarioSearch.get();
            usuarioChange.setLogin(usuarioCreates.getLogin());
            usuarioChange.setPassword(usuarioCreates.getPassword());

            usuarioRepository.update(usuarioChange);
            resultado= Either.right(true);
        }


        return resultado;
    }

    @Override
    public Either<Integer, Boolean> delete(Integer id) {
        Either<Integer, Boolean> resultado = Either.left(404);
        var usuarioSearch = usuarioRepository.findBy(id);
        if(usuarioSearch.isPresent()){
            usuarioRepository.deleteBy(id);
            resultado = Either.right(true);
        }

        return resultado;
    }
}
