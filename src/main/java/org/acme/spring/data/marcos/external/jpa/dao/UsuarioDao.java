package org.acme.spring.data.marcos.external.jpa.dao;

import org.acme.spring.data.marcos.core.business.output.UsuarioRepository;
import org.acme.spring.data.marcos.core.entity.Usuario;


import org.acme.spring.data.marcos.external.jpa.model.UsuarioDosJpa;
import org.acme.spring.data.marcos.external.jpa.repository.UsuarioJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class UsuarioDao implements UsuarioRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    UsuarioJpaRepository usuarioJpaRepository;

    @Override
    public List<Usuario> findAll() {
        return usuarioJpaRepository.findAll().stream().map(usuarioJpa -> {
            var usuarioEntidad = usuarioJpa.toEntity();
            usuarioEntidad.setEstadoUsuario(usuarioJpa.getEstadoUsuario().toEntity());
            return usuarioEntidad;
        }).collect(Collectors.toList());
    }

    @Override
    public List<Usuario> findAllNative() {
        Stream<UsuarioDosJpa> resultado = entityManager.createNativeQuery("Select * from tca02_usuario", UsuarioDosJpa.class).getResultStream();
        return resultado.map(UsuarioDosJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<Usuario> findAllOrm() {
        return usuarioJpaRepository.findAllOrm().stream().map(UsuarioDosJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<Usuario> findBy(Integer idUsuario) {
        return usuarioJpaRepository.findById(idUsuario).map(UsuarioDosJpa::toEntity);
    }

    @Override
    public void save(Usuario usuarioPersits) {
        usuarioJpaRepository.saveAndFlush(UsuarioDosJpa.fromEntity(usuarioPersits));

    }

    @Override
    public void update(Usuario usuarioChange) {
        usuarioJpaRepository.saveAndFlush(UsuarioDosJpa.fromEntity(usuarioChange));
    }

    @Override
    public void deleteBy(Integer id) {
        usuarioJpaRepository.deleteById(id);
    }


/*

    @Override
    public void deleteBy(Integer id) {
        usuarioJpaRepository.deleteById(id);
    }
 */
}
