package org.acme.spring.data.marcos.external.jpa.repository;

import org.acme.spring.data.marcos.external.jpa.model.UsuarioDosJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UsuarioJpaRepository extends JpaRepository<UsuarioDosJpa,Integer> {
    @Query("from UsuarioJpa uj")
    List<UsuarioDosJpa> findAllOrm();
}
