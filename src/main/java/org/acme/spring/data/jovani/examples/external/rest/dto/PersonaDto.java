package org.acme.spring.data.jovani.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.jovani.examples.core.entity.Persona;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PersonaDto {

    @JsonProperty
    @Schema(description = "este es el atributo que referencia al nombre de la persona.")
    private String name;
    @JsonProperty
    @Schema(description = "Bandera que indica que si se puede realizar el registro de una persona.")
    private Boolean registrar;
    @JsonProperty
    @Schema(description = "Bandera que indica que si se puede realizar la edicion de una persona.")
    private Boolean editar;
    @JsonProperty
    @Schema(description = "Bandera que indica que si se puede realizar la eliminacion de una persona.")
    private Boolean eliminar;
    @JsonProperty
    @Schema(description = "Bandera que indica que si se puede realizar la consulta de una persona.")
    private Boolean consultar;
    @JsonProperty
    @Schema(description = "Bandera que indica que si se puede realizar la configuracion de una persona.")
    private Boolean configurar;

    public static PersonaDto fromEntity(Persona persona) {
        return PersonaDto.builder()
                .name(persona.getNombre())
                .registrar(persona.getRegistrar())
                .editar(persona.getEditar())
                .eliminar(persona.getEliminar())
                .consultar(persona.getEliminar())
                .configurar(persona.getConfigurar())
                .build();
    }
}
