package org.acme.spring.data.jovani.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AulaJpa;
import org.acme.spring.data.jovani.institucion.core.business.output.AulaCuatroRepository;
import org.acme.spring.data.jovani.institucion.external.jpa.repository.AulaCuatroJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class AulaCuatroDao implements AulaCuatroRepository {
    @Inject
    AulaCuatroJpaRepository aulaCuatroJpaRepository;

    @Override
    public Optional<Aula> findById(Integer idAula) {
        return aulaCuatroJpaRepository.findById(idAula).map(AulaJpa::toEntity);
    }

    @Override
    public List<Aula> findAllByIdEdificio(Integer idEdifcio) {
        return aulaCuatroJpaRepository.findAllByIdEdificio(idEdifcio).stream().map(AulaJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Aula aulaPersist) {
        aulaCuatroJpaRepository.saveAndFlush(AulaJpa.fromEntity(aulaPersist));
    }

    @Override
    public void update(Aula aulaChange) {
        aulaCuatroJpaRepository.saveAndFlush(AulaJpa.fromEntity(aulaChange));
    }

    @Override
    public void delete(Integer idAula) {
        aulaCuatroJpaRepository.deleteById(idAula);
    }

    @Override
    public Boolean existsById(Integer id) {
        return aulaCuatroJpaRepository.existsById(id);
    }

    @Override
    public Boolean existsByIdEdificio(Integer idEdificio) {
        return aulaCuatroJpaRepository.existsByIdEdificio(idEdificio);
    }

    @Override
    public Boolean existsByNombreAndIdEdificio(String nombre, Integer idEdificio) {
        return aulaCuatroJpaRepository.existsByNombreAndIdEdificio(nombre, idEdificio);
    }

    @Override
    public Boolean existsByNombreAndIdEdificioAndIdNot(String nombre, Integer idEdificio, Integer id) {
        return aulaCuatroJpaRepository.existsByNombreAndIdEdificioAndIdNot(nombre, idEdificio, id);
    }
}
