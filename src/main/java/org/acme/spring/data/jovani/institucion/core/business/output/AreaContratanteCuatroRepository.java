package org.acme.spring.data.jovani.institucion.core.business.output;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import java.util.List;
import java.util.Optional;

public interface AreaContratanteCuatroRepository {
    List<AreaContratante> findAreasContratantes(Integer idInstitucion);
    Optional<AreaContratante> findById(Integer idAreaContrantante);
    void save(AreaContratante areaContratantePersist);
    void update(AreaContratante areaContratanteChange);
    void delete(Integer idAreaContratante);

    Boolean existsById(Integer idAreaContrantante);
}
