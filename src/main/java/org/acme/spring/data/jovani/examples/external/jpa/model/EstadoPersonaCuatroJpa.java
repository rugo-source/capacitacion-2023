package org.acme.spring.data.jovani.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.jovani.examples.core.entity.EstadoPersonaCuatro;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "estado_persona_cuatro")
public class EstadoPersonaCuatroJpa {

    @Id
    @Column(name = "id_estado")
    private Integer id;

    @Column(name = "tx_nombre")
    private String nombre;

    public EstadoPersonaCuatro toEntity(){
        return EstadoPersonaCuatro.builder()
                .id(this.id)
                .name(this.nombre)
                .build();
    }

}
