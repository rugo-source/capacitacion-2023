package org.acme.spring.data.jovani.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MiembroGremioCuatroJpaRepository extends JpaRepository<MiembroGremioJpa, MiembroGremioIdJpa> {

}
