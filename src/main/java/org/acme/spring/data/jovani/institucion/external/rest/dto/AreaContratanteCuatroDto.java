package org.acme.spring.data.jovani.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AreaContratanteCuatroDto {

    @JsonProperty
    @Schema(description = "este atributo hace referencia al id del area contratante")
    Integer id;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al id de una institucion")
    Integer idInstitucion;

    @JsonProperty
    @Schema(description = "este atributo hace refencia al área del área contratante")
    String area;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al subarea del área contratante")
    String subarea;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al teléfono del área contratante")
    String telefono;

    @JsonProperty
    @Schema(description = "este atributo hace referencia a la extension del teléfono del área contratante")
    String extension;

    public static AreaContratanteCuatroDto fromEntity(AreaContratante areaContratante) {
        return AreaContratanteCuatroDto.builder()
                .id(areaContratante.getId())
                .idInstitucion(areaContratante.getIdInstitucion())
                .area(areaContratante.getArea())
                .subarea(areaContratante.getSubarea())
                .telefono(areaContratante.getTelefono())
                .extension(areaContratante.getExtension())
                .build();
    }
}
