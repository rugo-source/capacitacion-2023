package org.acme.spring.data.jovani.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AulaCuatroCreate {
    private Integer idEdificio;
    private Integer idTipoEquipamiento;
    private String nombre;
    private Integer cantidadEquipamiento;
    private Integer capacidadEquipamiento;
    private String observaciones;
}
