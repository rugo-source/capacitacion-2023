package org.acme.spring.data.jovani.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SedeCuatroCreate {
    private Integer idInstitucion;
    private String nombre;
    private String acronimo;
    private Integer capacidad;
    private Boolean propia;
}
