package org.acme.spring.data.jovani.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.jovani.institucion.core.entity.GremioCuatroCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GremioCuatroCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 255, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al nombre del gremio")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 20, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al acronimo del gremio")
    String acronimo;

    public GremioCuatroCreate toEntity(){
        return GremioCuatroCreate.builder()
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .build();
    }
}
