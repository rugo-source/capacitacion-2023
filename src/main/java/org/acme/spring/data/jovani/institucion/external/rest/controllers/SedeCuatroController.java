package org.acme.spring.data.jovani.institucion.external.rest.controllers;

import org.acme.spring.data.jovani.institucion.core.business.input.SedeCuatroService;
import org.acme.spring.data.jovani.institucion.external.rest.dto.SedeCuatroCreateDto;
import org.acme.spring.data.jovani.institucion.external.rest.dto.SedeCuatroDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("catalogos-institucion/sede-cuatro")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Jovani")
public class SedeCuatroController {

    @Inject
    SedeCuatroService sedeCuatroService;

    @GET
    @Path("{idSede}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = SedeCuatroDto.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response get(@PathParam("idSede") Integer idSede) {
        return sedeCuatroService.get(idSede).map(SedeCuatroDto::fromEntity).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @GET
    @Path("sedes-institucion/{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = SedeCuatroDto.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response listByInstitucion(@PathParam("idInstitucion") Integer idInstitucion ) {
        return sedeCuatroService.listByInstitucion(idInstitucion).map(sedes -> sedes.stream().map(SedeCuatroDto::fromEntity)).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid SedeCuatroCreateDto sedeCuatroCreateDto) {
        return sedeCuatroService.create(sedeCuatroCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("{idSede}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@Valid SedeCuatroCreateDto sedeCuatroCreateDto, @PathParam("idSede") Integer idSede) {
        return sedeCuatroService.update(sedeCuatroCreateDto.toEntity(), idSede).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("{idSede}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idSede") Integer idSede) {
        return sedeCuatroService.delete(idSede).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }


}
