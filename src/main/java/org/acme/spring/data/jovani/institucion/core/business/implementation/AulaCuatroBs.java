package org.acme.spring.data.jovani.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.jovani.institucion.core.business.input.AulaCuatroService;
import org.acme.spring.data.jovani.institucion.core.business.output.AulaCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.business.output.EdificioCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.entity.AulaCuatroCreate;
import org.acme.spring.data.jovani.institucion.external.rest.dto.AulaCuatroDto;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class AulaCuatroBs implements AulaCuatroService {
    @Inject
    AulaCuatroRepository aulaCuatroRepository;

    @Inject
    EdificioCuatroRepository edificioCuatroRepository;


    @Override
    public Either<ErrorCodes, List<AulaCuatroDto>> listByIdEdificio(Integer idEdificio) {
        Either<ErrorCodes, List<AulaCuatroDto>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var existsEdificio = edificioCuatroRepository.existsById(idEdificio);
        if(existsEdificio){
            var aulas =  aulaCuatroRepository.findAllByIdEdificio(idEdificio);
            var capacidadTotal = capacidadTotalRNN129(aulas);
            var aulasDto = aulas.stream().map( (aula) -> {
                return AulaCuatroDto.builder()
                        .idEdifcio(aula.getId())
                        .nombre(aula.getNombre())
                        .idTipoEquipamiento(aula.getIdTipoEquipamiento())
                        .cantidadEquipamiento(aula.getCantidadEquipamiento())
                        .capacidadEquipamiento(aula.getCapacidadEquipamiento())
                        .observaciones(aula.getObservaciones())
                        .capacidadTotal(capacidadTotal)
                        .build();
            }).collect(Collectors.toList());
            resultado = Either.right(aulasDto);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(AulaCuatroCreate aulaCuatroCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var existsEdificio = edificioCuatroRepository.existsById(aulaCuatroCreate.getIdEdificio());
        if(existsEdificio) {
            if(validateRNN009(aulaCuatroCreate.getNombre(), aulaCuatroCreate.getIdEdificio())){
                resultado = Either.left(ErrorCodes.RNN009);
            }else{
                var aulaPersist = Aula.builder()
                        .idEdificio(aulaCuatroCreate.getIdEdificio())
                        .idTipoEquipamiento(aulaCuatroCreate.getIdTipoEquipamiento())
                        .nombre(aulaCuatroCreate.getNombre())
                        .cantidadEquipamiento(aulaCuatroCreate.getCantidadEquipamiento())
                        .capacidadEquipamiento(aulaCuatroCreate.getCapacidadEquipamiento())
                        .observaciones(aulaCuatroCreate.getObservaciones())
                        .build();
                aulaCuatroRepository.save(aulaPersist);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(AulaCuatroCreate aulaCuatroUpdate, Integer idAula) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var existsEdificio = edificioCuatroRepository.existsById(aulaCuatroUpdate.getIdEdificio());
        if(existsEdificio) {
            var searchAula = aulaCuatroRepository.findById(idAula);
            if(searchAula.isPresent()){
                if(validateRNN009AndIdNot(aulaCuatroUpdate.getNombre(), aulaCuatroUpdate.getIdEdificio(), idAula)){
                    resultado = Either.left(ErrorCodes.RNN009);
                }else{
                    var aulaChange = searchAula.get();
                    aulaChange.setNombre(aulaCuatroUpdate.getNombre());
                    aulaChange.setIdTipoEquipamiento(aulaCuatroUpdate.getIdTipoEquipamiento());
                    aulaChange.setCantidadEquipamiento(aulaCuatroUpdate.getCantidadEquipamiento());
                    aulaChange.setCapacidadEquipamiento(aulaCuatroUpdate.getCapacidadEquipamiento());
                    aulaChange.setObservaciones(aulaCuatroUpdate.getObservaciones());
                    aulaCuatroRepository.update(aulaChange);
                    resultado = Either.right(true);
                }
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idAula) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var existsAula = aulaCuatroRepository.existsById(idAula);
        if(existsAula){
            aulaCuatroRepository.delete(idAula);
            resultado = Either.right(true);
        }
        return resultado;
    }

    public Integer capacidadTotalRNN129(List<Aula> aulas){
        return aulas.stream().map(aula -> (aula.getCapacidadEquipamiento() * aula.getCantidadEquipamiento())).reduce(0,Integer::sum);
    }

    public Boolean validateRNN009(String nombre, Integer idEdificio){
        return aulaCuatroRepository.existsByNombreAndIdEdificio(nombre, idEdificio);
    }

    public Boolean validateRNN009AndIdNot(String nombre, Integer idEdificio, Integer id){
        return aulaCuatroRepository.existsByNombreAndIdEdificioAndIdNot(nombre, idEdificio, id);
    }
}
