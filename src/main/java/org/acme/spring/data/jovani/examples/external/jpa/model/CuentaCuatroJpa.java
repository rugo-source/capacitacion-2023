package org.acme.spring.data.jovani.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.jovani.examples.core.entity.CuentaCuatro;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "cuenta_cuatro")
public class CuentaCuatroJpa {

    @Id
    @Column(name = "id_cuenta")
    @SequenceGenerator(name = "cuenta_cuatro_id_cuenta_seq", sequenceName = "cuenta_cuatro_id_cuenta_seq", allocationSize = 1)
    @GeneratedValue(generator = "cuenta_cuatro_id_cuenta_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "fk_id_persona")
    private Integer idPersona;
    @Column(name = "tx_rol")
    private String rol;
    @Column(name = "fh_inicio")
    private LocalDate inicio;
    @Column(name = "fh_fin")
    private LocalDate fin;

    public CuentaCuatro toEntity() {
        return CuentaCuatro.builder().idCuenta(this.id).idPersona(this.idPersona).rol(this.rol).inicio(this.inicio).fin(this.fin).build();
    }

    public static CuentaCuatroJpa fromEntity(CuentaCuatro cuentaCuatro) {
        return CuentaCuatroJpa.builder()
                .id(cuentaCuatro.getIdCuenta())
                .idPersona(cuentaCuatro.getIdPersona())
                .rol(cuentaCuatro.getRol())
                .inicio(cuentaCuatro.getInicio())
                .fin(cuentaCuatro.getFin())
                .build();
    }
}
