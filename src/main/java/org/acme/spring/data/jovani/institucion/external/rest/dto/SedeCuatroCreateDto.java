package org.acme.spring.data.jovani.institucion.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.jovani.institucion.core.entity.SedeCuatroCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SedeCuatroCreateDto {

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este atributo hace referencia al id de la instucion a la que pertenece la sede")
    Integer idInstitucion;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Schema(description = "este atributo hace referencia al nombre de la sede")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Schema(description = "este atributo hace referencia al acronimo de la sede")
    String acronimo;

    @JsonProperty
    @Positive(message = "RNN003")
    @Schema(description = "este atributo hace referencia a la capacidad de la sede")
    Integer capacidad;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "este atributo hace referencia a si la sede pertence o no a la institucion")
    Boolean propia;

    public SedeCuatroCreate toEntity() {
        return SedeCuatroCreate.builder()
                .idInstitucion(this.idInstitucion)
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .capacidad(this.capacidad)
                .propia(this.propia)
                .build();
    }
}


