package org.acme.spring.data.jovani.examples.core.entity;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class CuentaCuatro {

    private Integer idCuenta;
    private Integer idPersona;
    private String rol;
    private LocalDate inicio;
    private LocalDate fin;
}
