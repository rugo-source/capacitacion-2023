package org.acme.spring.data.jovani.institucion.external.rest.controllers;

import org.acme.spring.data.jovani.institucion.core.business.input.EdificioCuatroService;
import org.acme.spring.data.jovani.institucion.external.rest.dto.EdificioCuatroCreateDto;
import org.acme.spring.data.jovani.institucion.external.rest.dto.EdificioCuatroDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("catalogos-institucion/edificio-cuatro")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Jovani")
public class EdificioCuatroController {
    @Inject
    EdificioCuatroService edificioCuatroService;

    @GET
    @Path("{idSede}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = EdificioCuatroDto.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response listByIdSede(@PathParam("idSede") Integer idSede) {
        return edificioCuatroService.listByIdSede(idSede).map(edificios -> edificios.stream().map(EdificioCuatroDto::fromEntity)).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid EdificioCuatroCreateDto edificioCuatroCreateDto) {
        return edificioCuatroService.create(edificioCuatroCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("{idEdificio}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@Valid EdificioCuatroCreateDto edificioCuatroCreateDto, @PathParam("idEdificio") Integer idEdificio){
        return edificioCuatroService.update(edificioCuatroCreateDto.toEntity(), idEdificio).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("{idEdificio}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idEdificio") Integer idEdificio){
        return edificioCuatroService.delete(idEdificio).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

}
