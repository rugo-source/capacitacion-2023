package org.acme.spring.data.jovani.institucion.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Aula;

import java.util.List;
import java.util.Optional;

public interface AulaCuatroRepository {
    Optional<Aula> findById(Integer idAula);
    List<Aula> findAllByIdEdificio(Integer idEdifcio);
    void save(Aula aulaPersist);
    void update(Aula aulaChange);
    void delete(Integer idAula);
    Boolean existsById(Integer id);
    Boolean existsByIdEdificio(Integer idEdificio);
    Boolean existsByNombreAndIdEdificio(String nombre, Integer idEdificio);
    Boolean existsByNombreAndIdEdificioAndIdNot(String nombre, Integer idEdificio, Integer id);
}
