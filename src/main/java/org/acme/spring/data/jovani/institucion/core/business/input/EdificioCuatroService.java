package org.acme.spring.data.jovani.institucion.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.jovani.institucion.core.entity.EdificioCuatroCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface EdificioCuatroService {
    Either<ErrorCodes, List<Edificio>> listByIdSede(Integer idSede);
    Either<ErrorCodes, Boolean> create(EdificioCuatroCreate edificioCuatroCreate);
    Either<ErrorCodes, Boolean> update(EdificioCuatroCreate edificioCuatroCreate, Integer idEdificio);
    Either<ErrorCodes, Boolean> delete(Integer idEdificio);
}
