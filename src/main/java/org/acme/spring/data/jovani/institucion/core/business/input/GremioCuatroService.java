package org.acme.spring.data.jovani.institucion.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.jovani.institucion.core.entity.GremioCuatroCreate;
import org.acme.spring.data.jovani.institucion.core.entity.MiembroGremioCuatro;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface GremioCuatroService {
    Either<ErrorCodes, List<Gremio>> list();
    Either<ErrorCodes, Boolean> create(GremioCuatroCreate gremioCuatroCreate);
    Either<ErrorCodes, Boolean> update(Integer idGremio, GremioCuatroCreate gremioCuatroChange);
    Either<ErrorCodes, Boolean> delete(Integer idGremio);
    Either<ErrorCodes, Boolean> asociarInstitucion(MiembroGremioCuatro miembroGremioCuatroGet);
    Either<ErrorCodes, Boolean> desasociarInstitucion(MiembroGremioCuatro miembroGremioCuatro);
}
