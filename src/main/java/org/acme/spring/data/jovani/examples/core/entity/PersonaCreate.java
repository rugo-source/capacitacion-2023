package org.acme.spring.data.jovani.examples.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PersonaCreate {

    private String nombre;
    private Integer edad;
    private Integer idEstado;
}
