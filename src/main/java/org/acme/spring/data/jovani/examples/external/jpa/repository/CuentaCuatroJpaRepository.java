package org.acme.spring.data.jovani.examples.external.jpa.repository;


import org.acme.spring.data.jovani.examples.external.jpa.model.CuentaCuatroJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CuentaCuatroJpaRepository extends JpaRepository<CuentaCuatroJpa, Integer> {
    @Query("from CuentaCuatroJpa where inicio <= current_date and fin >= current_date and idPersona = :idPersona")
    List<CuentaCuatroJpa> findByIdPersona(@Param("idPersona") Integer idPersona);
}
