package org.acme.spring.data.jovani.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.jovani.institucion.core.business.input.EdificioCuatroService;
import org.acme.spring.data.jovani.institucion.core.business.output.AulaCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.business.output.EdificioCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.business.output.SedeCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.entity.EdificioCuatroCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class EdificioCuatroBs implements EdificioCuatroService {
    @Inject
    EdificioCuatroRepository edificioCuatroRepository;

    @Inject
    SedeCuatroRepository sedeCuatroRepository;

    @Inject
    AulaCuatroRepository aulaCuatroRepository;

    @Override
    public Either<ErrorCodes, List<Edificio>> listByIdSede(Integer idSede) {
        Either<ErrorCodes, List<Edificio>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var sedeExists = sedeCuatroRepository.existsById(idSede);
        if(sedeExists) {
            var edificios = edificioCuatroRepository.findAllByIdSede(idSede);
            resultado = Either.right(edificios);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(EdificioCuatroCreate edificioCuatroCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var existsSede = sedeCuatroRepository.existsById(edificioCuatroCreate.getIdSede());
        if(existsSede) {
            if(validateRNN002(edificioCuatroCreate.getNombre(), edificioCuatroCreate.getAcronimo(), edificioCuatroCreate.getIdSede())){
                resultado = Either.left(ErrorCodes.RNN002);
            }else{
                var edificioPersist = Edificio.builder()
                        .idSede(edificioCuatroCreate.getIdSede())
                        .nombre(edificioCuatroCreate.getNombre())
                        .acronimo(edificioCuatroCreate.getAcronimo())
                        .referencia(edificioCuatroCreate.getReferencia())
                        .build();
                edificioCuatroRepository.save(edificioPersist);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(EdificioCuatroCreate edificioCuatroCreate, Integer idEdificio) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var existsSede = sedeCuatroRepository.existsById(edificioCuatroCreate.getIdSede());
        if(existsSede){
            var searchEdificio = edificioCuatroRepository.findById(idEdificio);
            if(searchEdificio.isPresent()){
                var edificioChange = searchEdificio.get();
                if(validateRNN002AndIdNot(edificioCuatroCreate.getNombre(), edificioCuatroCreate.getAcronimo(), edificioChange.getIdSede(), idEdificio)){
                    resultado = Either.left(ErrorCodes.RNN002);
                }else{
                    edificioChange.setNombre(edificioCuatroCreate.getNombre());
                    edificioChange.setAcronimo(edificioCuatroCreate.getAcronimo());
                    edificioChange.setReferencia(edificioCuatroCreate.getReferencia());
                    edificioCuatroRepository.update(edificioChange);
                    resultado = Either.right(true);
                }
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idEdificio) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var existsEdificio = edificioCuatroRepository.existsById(idEdificio);
        if(existsEdificio){
            if(validateRNN025(idEdificio)){
                resultado = Either.left(ErrorCodes.RNN025);
            } else{
                edificioCuatroRepository.delete(idEdificio);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    public Boolean validateRNN002(String nombre, String acronimo, Integer idSede){
        return edificioCuatroRepository.existsByNombreAndIdSede(nombre, idSede) || edificioCuatroRepository.existsByAcronimoAndIdSede(acronimo, idSede);
    }

    public Boolean validateRNN002AndIdNot(String nombre, String acronimo,Integer idSede,  Integer id){
        return edificioCuatroRepository.existsByNombreAndIdSedeAndIdNot(nombre, idSede, id) || edificioCuatroRepository.existsByAcronimoAndIdSedeAndIdNot(acronimo, idSede, id);
    }

    public Boolean validateRNN025(Integer idEdificio) {
        return aulaCuatroRepository.existsByIdEdificio(idEdificio);
    }
}
