package org.acme.spring.data.jovani.examples.core.business.input;
import io.vavr.control.Either;
import org.acme.spring.data.jovani.examples.core.entity.Persona;
import org.acme.spring.data.jovani.examples.core.entity.PersonaCreate;

import java.util.List;
public interface ExampleService {

    List<Persona> listaEjemplo();
    Either<Integer, Persona> get(Integer idPersona);

    Either<Integer,Boolean> create(PersonaCreate toEntity);
    Either<Integer,Boolean> update(Integer id, PersonaCreate personaCreate);
    Either<Integer,Boolean> delete(Integer id);
}
