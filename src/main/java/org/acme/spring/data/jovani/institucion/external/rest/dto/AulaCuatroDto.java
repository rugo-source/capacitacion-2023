package org.acme.spring.data.jovani.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AulaCuatroDto {

    @JsonProperty
    @Schema(description = "este atributo hace referencia al id del edificion al que pertenece el aula")
    Integer idEdifcio;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al nombre del aula")
    String nombre;

    @JsonProperty
    @Schema(description = "este es el atributo que hace refencia al id del tipo de equipamiento del aula")
    Integer idTipoEquipamiento;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia a la cantidad de equipamiento del aula")
    Integer cantidadEquipamiento;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia a la capacidad de equipamiento del aula")
    Integer capacidadEquipamiento;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia a las observaciones del aula")
    String observaciones;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia a la capaciadad total de todas las aulas dentro del edificio")
    Integer capacidadTotal;

    public static AulaCuatroDto fromEntity(Aula aula){
        return AulaCuatroDto.builder()
                .idEdifcio(aula.getIdEdificio())
                .nombre(aula.getNombre())
                .idTipoEquipamiento(aula.getIdTipoEquipamiento())
                .cantidadEquipamiento(aula.getCantidadEquipamiento())
                .capacidadEquipamiento(aula.getCapacidadEquipamiento())
                .observaciones(aula.getObservaciones())
                .build();
    }
}
