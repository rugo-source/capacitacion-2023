package org.acme.spring.data.jovani.institucion.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Sede;

import java.util.List;
import java.util.Optional;

public interface SedeCuatroRepository {
    Optional<Sede> findById(Integer idSede);
    List<Sede> findAllByIdInstitucion(Integer idInstitucion);
    void save(Sede sedePersist);
    void update(Sede sedeChange);
    void delete(Integer idSede);
    Boolean existsByNombreAndIdInstitucion(String nombre, Integer idInstitucion);
    Boolean existsByAcronimoAndIdInstitucion(String acronimo, Integer idInstitucion);
    Boolean existsByNombreAndIdInstitucionAndIdNot(String nombre, Integer idInstitucion, Integer id);
    Boolean existsByAcronimoAndIdInstitucionAndIdNot(String acronimo, Integer idInstitucion, Integer id);
    Boolean existsById(Integer idSede);
}
