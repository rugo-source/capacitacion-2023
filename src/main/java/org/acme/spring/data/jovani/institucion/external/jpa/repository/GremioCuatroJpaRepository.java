package org.acme.spring.data.jovani.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.GremioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GremioCuatroJpaRepository extends JpaRepository<GremioJpa, Integer> {
    boolean existsByNombre(String nombre);
    boolean existsByAcronimo(String acronimo);
    boolean existsByNombreAndIdNot(String nombre, Integer id);
    boolean existsByAcronimoAndIdNot(String acronimo, Integer id);
}
