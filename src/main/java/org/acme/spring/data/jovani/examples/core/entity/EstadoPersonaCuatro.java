package org.acme.spring.data.jovani.examples.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EstadoPersonaCuatro {

    private Integer id;
    private String name;
}
