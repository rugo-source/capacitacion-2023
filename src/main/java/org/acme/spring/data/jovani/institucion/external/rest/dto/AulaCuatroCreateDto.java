package org.acme.spring.data.jovani.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.jovani.institucion.core.entity.AulaCuatroCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AulaCuatroCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @Positive(message = "RNS001")
    @Schema(description = "este atributo hace referencia al id del edificion al que pertenece el aula")
    Integer idEdificio;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al nombre del aula")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Positive(message = "RNS001")
    @Schema(description = "este es el atributo que hace refencia al id del tipo de equipamiento del aula")
    Integer idTipoEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @PositiveOrZero(message = "RNN128")
    @Schema(description = "este es el atributo que hace referencia a la cantidad de equipamiento del aula")
    Integer cantidadEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @PositiveOrZero(message = "RNN128")
    @Schema(description = "este es el atributo que hace referencia a la capacidad de equipamiento del aula")
    Integer capacidadEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia a las observaciones del aula")
    String observaciones;

    public AulaCuatroCreate toEntity() {
        return AulaCuatroCreate.builder()
                .idEdificio(this.idEdificio)
                .nombre(this.nombre)
                .idTipoEquipamiento(this.idTipoEquipamiento)
                .cantidadEquipamiento(this.cantidadEquipamiento)
                .capacidadEquipamiento(this.capacidadEquipamiento)
                .observaciones(this.observaciones)
                .build();
    }
}
