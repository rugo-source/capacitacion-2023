package org.acme.spring.data.jovani.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.jovani.examples.core.entity.Persona;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "persona_cuatro")
public class PersonaCuatroJpa {

    @Id
    @Column(name = "id_persona")
    @SequenceGenerator(name = "persona_cuatro_id_persona_seq", sequenceName = "persona_cuatro_id_persona_seq", allocationSize = 1)
    @GeneratedValue(generator = "persona_cuatro_id_persona_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "fk_id_estado")
    private Integer idEstado;
    @Column(name = "edad")
    private  Integer edad;


    @ManyToOne
    @JoinColumn(name = "fk_id_estado", referencedColumnName = "id_estado", insertable = false, updatable = false)
    private EstadoPersonaCuatroJpa estadoPersonaCuatro;

    public Persona toEntity() {
        return Persona.builder().id(this.id).nombre(this.nombre).edad(this.edad).idEstado(this.idEstado).build();
    }

    public static PersonaCuatroJpa fromEntity(Persona persona) {
        return PersonaCuatroJpa.builder().id(persona.getId()).nombre(persona.getNombre()).edad(persona.getEdad()).idEstado(persona.getIdEstado()).build();
    }

}
