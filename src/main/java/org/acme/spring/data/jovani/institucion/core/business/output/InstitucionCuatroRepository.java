package org.acme.spring.data.jovani.institucion.core.business.output;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;


import java.util.List;
import java.util.Optional;

public interface InstitucionCuatroRepository {
    Optional<Institucion> findById(Integer idInstitucion);
    Optional<Institucion> findByIdentificador(String identificador);
    Optional<Institucion> findByNombre(String nombreInstitucion);
    Optional<Institucion> findInstitucion(String nombre);
    List<Institucion> findInstituciones(String identificador, String nombre, String acronimo);
    Optional<Institucion> validateRNN013(Integer idInstitucion, String identificador);
    Optional<Institucion> validateRNN094(Integer idInstitucion, String nombreInstitucion);
    void save(Institucion institucionPersist);
    void update(Institucion institucionChange);

    Boolean existsById(Integer idInstitucion);
    Boolean existsIdentificadorAndIdNot(String identificador, Integer idInstitucion);
    Boolean existsNombreAndIdNot(String nombre, Integer idInstitucion);
}
