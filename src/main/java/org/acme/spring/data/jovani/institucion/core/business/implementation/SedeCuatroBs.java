package org.acme.spring.data.jovani.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.jovani.institucion.core.business.input.SedeCuatroService;
import org.acme.spring.data.jovani.institucion.core.business.output.EdificioCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.business.output.InstitucionCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.business.output.SedeCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.entity.SedeCuatroCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class SedeCuatroBs  implements SedeCuatroService {

    @Inject
    SedeCuatroRepository sedeCuatroRepository;

    @Inject
    InstitucionCuatroRepository institucionCuatroRepository;

    @Inject
    EdificioCuatroRepository edificioCuatroRepository;

    @Override
    public Either<ErrorCodes, Sede> get(Integer idSede) {
        Either<ErrorCodes, Sede> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var sedeSearch = sedeCuatroRepository.findById(idSede);
        if(sedeSearch.isPresent()){
            var sede = sedeSearch.get();
            resultado = Either.right(sede);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, List<Sede>> listByInstitucion(Integer idInstitucion) {
        Either<ErrorCodes, List<Sede>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var institucionExists = institucionCuatroRepository.existsById(idInstitucion);
        if(institucionExists){
            var listSede = sedeCuatroRepository.findAllByIdInstitucion(idInstitucion);
            resultado = Either.right(listSede);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(SedeCuatroCreate sedeCuatroCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var searchInstitucion = institucionCuatroRepository.findById(sedeCuatroCreate.getIdInstitucion());
        if(searchInstitucion.isPresent()){
            var institucion = searchInstitucion.get();
            if (validateRNN096(sedeCuatroCreate.getNombre(),sedeCuatroCreate.getAcronimo(), institucion.getId())){
                resultado = Either.left(ErrorCodes.RNN096);
            } else {
                var sedesByInstitucion = sedeCuatroRepository.findAllByIdInstitucion(institucion.getId());
                var identificador =  generarIdentificadorRNN004(institucion.getIdentificador(), sedesByInstitucion.size());
                var sedePersist = Sede.builder()
                        .idInstitucion(institucion.getId())
                        .nombre(sedeCuatroCreate.getNombre())
                        .acronimo(sedeCuatroCreate.getAcronimo())
                        .capacidad(sedeCuatroCreate.getCapacidad())
                        .propia(sedeCuatroCreate.getPropia())
                        .identificador(identificador)
                        .build();
                sedeCuatroRepository.save(sedePersist);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(SedeCuatroCreate sedeCuatroCreate, Integer idSede) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var searchInstitucion = institucionCuatroRepository.findById(sedeCuatroCreate.getIdInstitucion());
        if(searchInstitucion.isPresent()){
            var institucion = searchInstitucion.get();
            var searchSede = sedeCuatroRepository.findById(idSede);
            if(searchSede.isPresent()){
                if(validateRNN096AndIdNot(sedeCuatroCreate.getNombre(), sedeCuatroCreate.getAcronimo(), institucion.getId(), idSede)){
                    resultado =  Either.left(ErrorCodes.RNN096);
                }else{
                    var sedeChange = searchSede.get();
                    sedeChange.setNombre(sedeCuatroCreate.getNombre());
                    sedeChange.setAcronimo(sedeCuatroCreate.getAcronimo());
                    sedeChange.setCapacidad(sedeCuatroCreate.getCapacidad());
                    sedeChange.setPropia(sedeCuatroCreate.getPropia());
                    sedeCuatroRepository.update(sedeChange);
                    resultado = Either.right(true);
                }
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idSede) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if(sedeCuatroRepository.existsById(idSede)){
            if(validateRNN016(idSede)){
                resultado = Either.left(ErrorCodes.RNN016);
            }else{
                sedeCuatroRepository.delete(idSede);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    public Boolean validateRNN096(String nombre, String acronimo, Integer idInstitucion) {
        return sedeCuatroRepository.existsByNombreAndIdInstitucion(nombre, idInstitucion) || sedeCuatroRepository.existsByAcronimoAndIdInstitucion(acronimo, idInstitucion);
    }

    public Boolean validateRNN096AndIdNot(String nombre, String acronimo, Integer idInstitucion, Integer idSede) {
        return sedeCuatroRepository.existsByNombreAndIdInstitucionAndIdNot(nombre, idInstitucion, idSede) || sedeCuatroRepository.existsByAcronimoAndIdInstitucionAndIdNot(acronimo, idInstitucion, idSede);
    }

    public String generarIdentificadorRNN004(String identificadorInstitucion, Integer tamanioArregloSedes) {
        return identificadorInstitucion + String.format("%03d", tamanioArregloSedes + 1);
    }

    public Boolean validateRNN016(Integer idSede){
        return edificioCuatroRepository.existsByIdSede(idSede);
    }
}
