package org.acme.spring.data.jovani.examples.external.rest.controllers;

import org.acme.spring.data.jovani.examples.core.business.input.CuentaService;
import org.acme.spring.data.jovani.examples.external.rest.dto.CuentaCuatroCreateDto;
import org.acme.spring.data.jovani.examples.external.rest.dto.CuentaCuatroDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/data/cuenta-cuatro")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion")
public class CuentaCuatroController {

    @Inject
    CuentaService cuentaService;

    @GET
    @Path("account/{idPersona}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = CuentaCuatroDto.class)))
    @APIResponse(responseCode = "404", description = "Peticion erronea", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = ErrorResponseDto.class)))
    public Response getAllAccounts(@PathParam("idPersona") Integer idPersona){
        return  cuentaService.getAllAccountByIdPerson(idPersona).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @GET
    @Path("{idCuenta}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = CuentaCuatroDto.class)))
    @APIResponse(responseCode = "404", description = "Peticion erronea", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = ErrorResponseDto.class)))
    public Response get(@PathParam("idCuenta") Integer idCuenta){
        return  cuentaService.get(idCuenta).map(CuentaCuatroDto::fromEntity).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @Path("post")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid CuentaCuatroCreateDto cuentaCuatroCreateDto){
        return cuentaService.create(cuentaCuatroCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("put/{idCuenta}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idCuenta") Integer idCuenta,@Valid CuentaCuatroCreateDto cuentaCuatroCreateDto) {
        return cuentaService.update(idCuenta,cuentaCuatroCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("delete/{idCuenta}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idCuenta") Integer idCuenta) {
        return cuentaService.delete(idCuenta).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

}
