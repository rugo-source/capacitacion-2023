package org.acme.spring.data.jovani.institucion.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.jovani.institucion.core.entity.MiembroGremioCuatro;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.Positive;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MiembroGremioCuatroCreateDto {

    @JsonProperty
    @Positive
    @Schema(description = "este es el atributo que hace referencia al identificador de un gremio")
    Integer idGremio;

    @JsonProperty
    @Positive
    @Schema(description = "este es el atributo que hace referencia al identificador de la institucion")
    Integer idInstitucion;

    public MiembroGremioCuatro toEntity() {
        return MiembroGremioCuatro.builder()
                .idGremio(this.idGremio)
                .idInstitucion(this.idInstitucion)
                .build();
    }
}
