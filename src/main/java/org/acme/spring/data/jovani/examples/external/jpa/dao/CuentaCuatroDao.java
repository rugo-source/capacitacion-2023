package org.acme.spring.data.jovani.examples.external.jpa.dao;

import org.acme.spring.data.jovani.examples.core.business.output.CuentaCuatroRepository;
import org.acme.spring.data.jovani.examples.core.entity.CuentaCuatro;
import org.acme.spring.data.jovani.examples.external.jpa.repository.CuentaCuatroJpaRepository;
import org.acme.spring.data.jovani.examples.external.jpa.model.CuentaCuatroJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class CuentaCuatroDao implements CuentaCuatroRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    CuentaCuatroJpaRepository cuentaCuatroJpaRepository;

    @Override
    public List<CuentaCuatro> findAllAccountsByIdPerson(Integer idPersona) {
        return cuentaCuatroJpaRepository.findByIdPersona(idPersona).stream().map(CuentaCuatroJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<CuentaCuatro> findById(Integer idCuenta) {
        return cuentaCuatroJpaRepository.findById(idCuenta).map(CuentaCuatroJpa::toEntity);
    }

    @Override
    public void save(CuentaCuatro cuentaPersist) {
        cuentaCuatroJpaRepository.saveAndFlush(CuentaCuatroJpa.fromEntity(cuentaPersist));
    }

    @Override
    public void update(CuentaCuatro cuentaCuatroChange) {
        cuentaCuatroJpaRepository.saveAndFlush(CuentaCuatroJpa.fromEntity(cuentaCuatroChange));
    }

    @Override
    public void deleteById(Integer idCuenta) {
        cuentaCuatroJpaRepository.deleteById(idCuenta);
    }

}
