package org.acme.spring.data.jovani.examples.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.jovani.examples.core.business.output.CuentaCuatroRepository;
import org.acme.spring.data.jovani.examples.core.entity.CuentaCuatro;
import org.acme.spring.data.jovani.examples.core.entity.CuentaCuatroCreate;
import org.acme.spring.data.jovani.examples.core.business.input.CuentaService;
import org.acme.spring.data.jovani.examples.core.business.output.PersonaRepository;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@ApplicationScoped
public class CuentaBs implements CuentaService {

    @Inject
    CuentaCuatroRepository cuentaCuatroRepository;

    @Inject
    PersonaRepository personaRepository;

    @Override
    public Either<ErrorCodes, List<CuentaCuatro>> getAllAccountByIdPerson(Integer idPerson) {
        Either<ErrorCodes, List<CuentaCuatro>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var personaSearch = personaRepository.findBy(idPerson);
        if(personaSearch.isPresent()){
            var listCuentaCuatro = cuentaCuatroRepository.findAllAccountsByIdPerson(idPerson);
            resultado = Either.right(listCuentaCuatro);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, CuentaCuatro> get(Integer idCuenta) {
        Either<ErrorCodes, CuentaCuatro> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var cuenta = cuentaCuatroRepository.findById(idCuenta);
        if(cuenta.isPresent()){
            var cuentaGet = cuenta.get();
            resultado = Either.right(cuentaGet);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(CuentaCuatroCreate cuentaCuatroCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var persona = personaRepository.findBy(cuentaCuatroCreate.getIdPersona());
        if(persona.isPresent()) {
            if(!validateRNN001(cuentaCuatroCreate.getInicio(),cuentaCuatroCreate.getFin())){
                resultado = Either.left(ErrorCodes.RNN001);
            }
            else if(!validateRNN002(cuentaCuatroCreate.getInicio())){
                resultado = Either.left(ErrorCodes.RNN002);
            } else {
                var cuentaPersist = CuentaCuatro.builder()
                        .idPersona(cuentaCuatroCreate.getIdPersona())
                        .rol(cuentaCuatroCreate.getRol())
                        .inicio(cuentaCuatroCreate.getInicio())
                        .fin(cuentaCuatroCreate.getFin())
                        .build();
                cuentaCuatroRepository.save(cuentaPersist);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer idCuenta, CuentaCuatroCreate cuentaCuatroCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var cuenta = cuentaCuatroRepository.findById(idCuenta);
        if(cuenta.isPresent()) {
            var persona = personaRepository.findBy(cuentaCuatroCreate.getIdPersona());
            if (persona.isPresent()) {
                if (!validateRNN001(cuentaCuatroCreate.getInicio(), cuentaCuatroCreate.getFin())) {
                    resultado = Either.left(ErrorCodes.RNN001);
                } else if (!validateRNN002(cuentaCuatroCreate.getInicio())) {
                    resultado = Either.left(ErrorCodes.RNN002);
                } else {
                    var cuentaChange = cuenta.get();
                    cuentaChange.setIdPersona(cuentaCuatroCreate.getIdPersona());
                    cuentaChange.setRol(cuentaCuatroCreate.getRol());
                    cuentaChange.setInicio(cuentaCuatroCreate.getInicio());
                    cuentaChange.setFin(cuentaCuatroCreate.getFin());
                    cuentaCuatroRepository.update(cuentaChange);
                    resultado = Either.right(true);
                }
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idCuenta) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var cuentaSearch = cuentaCuatroRepository.findById(idCuenta);
        if(cuentaSearch.isPresent()){
            cuentaCuatroRepository.deleteById(idCuenta);
            resultado = Either.right(true);
        }
        return resultado;
    }

    private Boolean validateRNN001(LocalDate inicio, LocalDate fin) {
        return inicio.isBefore(fin);
    }

    private Boolean validateRNN002(LocalDate inicio) {
        LocalDate now = LocalDate.now();
        return inicio.isAfter(now) || inicio.equals(now);
    }
}
