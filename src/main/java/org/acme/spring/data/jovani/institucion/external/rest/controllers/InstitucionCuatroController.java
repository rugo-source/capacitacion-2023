package org.acme.spring.data.jovani.institucion.external.rest.controllers;


import org.acme.spring.data.jovani.institucion.core.business.input.InstitucionCuatroService;
import org.acme.spring.data.jovani.institucion.external.rest.dto.InstitucionCuatroCreateDto;
import org.acme.spring.data.jovani.institucion.external.rest.dto.InstitucionCuatroDto;
import org.acme.spring.data.jovani.institucion.external.rest.dto.InstitucionCuatroGetDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("catalogos-instituciones/InstitucionCuatro")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Jovani")
public class InstitucionCuatroController {

    @Inject
    InstitucionCuatroService institucionCuatroService;

    /*aqui puedes omitir la palabre get y por ende quitar la anotacion path.
    * de igual manera como esto regresara una lista, renombra el metodo en lugar de ser get seria list.
    * Tambien revisa tu anotacion @APIResponse esta tendri que estar regresando un Array de tu entidad DTO.
    * */
    @POST
    @Path("busqueda")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = InstitucionCuatroDto.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response list(@Valid InstitucionCuatroGetDto institucionCuatroGetDto) {
        return institucionCuatroService.list(institucionCuatroGetDto.toEntity()).map((instituciones) -> instituciones.stream().map(InstitucionCuatroDto::fromEntity)).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid InstitucionCuatroCreateDto institucionCuatroCreateDto) {
        return institucionCuatroService.create(institucionCuatroCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    /*aqui puede omitir la palabra put*/
    @PUT
    @Path("{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idInstitucion") Integer idInstitucion, @Valid InstitucionCuatroCreateDto institucionCuatroCreateDto) {
        return institucionCuatroService.update(idInstitucion, institucionCuatroCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
