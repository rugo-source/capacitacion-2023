package org.acme.spring.data.jovani.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.jovani.institucion.core.business.input.GremioCuatroService;
import org.acme.spring.data.jovani.institucion.core.business.output.GremioCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.business.output.InstitucionCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.business.output.MiembroGremioCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.entity.GremioCuatroCreate;
import org.acme.spring.data.jovani.institucion.core.entity.MiembroGremioCuatro;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class GremioCuatroBs implements GremioCuatroService {

    @Inject
    GremioCuatroRepository gremioCuatroRepository;

    @Inject
    InstitucionCuatroRepository institucionCuatroRepository;

    @Inject
    MiembroGremioCuatroRepository miembroGremioCuatroRepository;


    @Override
    public Either<ErrorCodes, List<Gremio>> list() {
        Either<ErrorCodes, List<Gremio>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var listGremio  = gremioCuatroRepository.findAll();
        if(!listGremio.isEmpty()) {
            resultado = Either.right(listGremio);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> create(GremioCuatroCreate gremioCuatroCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.RNN097);
        if(!validateRNN097(gremioCuatroCreate.getNombre(), gremioCuatroCreate.getAcronimo())){
            var gremioPersist = Gremio.builder()
                    .nombre(gremioCuatroCreate.getNombre())
                    .acronimo(gremioCuatroCreate.getAcronimo())
                    .build();
            gremioCuatroRepository.save(gremioPersist);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer idGremio, GremioCuatroCreate gremioCuatroChange) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var gremioSearch = gremioCuatroRepository.findById(idGremio);
        if(gremioSearch.isPresent()){
            if(validateRNN097AndIdNot(gremioCuatroChange.getNombre(), gremioCuatroChange.getAcronimo(), idGremio)){
                resultado = Either.left(ErrorCodes.RNN097);
            }else{
                var gremioChange = gremioSearch.get();
                gremioChange.setNombre(gremioCuatroChange.getNombre());
                gremioChange.setAcronimo(gremioCuatroChange.getAcronimo());
                gremioCuatroRepository.update(gremioChange);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idGremio) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var gremioSearch = gremioCuatroRepository.findById(idGremio);
        if(gremioSearch.isPresent()){
            if(!gremioSearch.get().getInstitucionList().isEmpty()){
                resultado = Either.left(ErrorCodes.RNN024);
            }else{
                gremioCuatroRepository.delete(idGremio);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> asociarInstitucion(MiembroGremioCuatro miembroGremioCuatroCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var gremioSearch = gremioCuatroRepository.findById(miembroGremioCuatroCreate.getIdGremio());
        var institucionSearch = institucionCuatroRepository.existsById(miembroGremioCuatroCreate.getIdInstitucion());
        if(gremioSearch.isPresent() && institucionSearch){
            if(validateRNN026(miembroGremioCuatroCreate.getIdInstitucion())) {
                resultado = Either.left(ErrorCodes.RNN026);
            }else {
                gremioCuatroRepository.createMiembro(miembroGremioCuatroCreate);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> desasociarInstitucion(MiembroGremioCuatro miembroGremioCuatro) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var miembroGremioSearch = miembroGremioCuatroRepository.findById(miembroGremioCuatro);
        if(miembroGremioSearch.isPresent()){
            miembroGremioCuatroRepository.delete(miembroGremioCuatro);
            resultado = Either.right(true);
        }
        return resultado;
    }

    public Boolean validateRNN097(String nombre, String acronimo) {
        return gremioCuatroRepository.existsByNombre(nombre) || gremioCuatroRepository.existsByAcronimo(acronimo);
    }

    public Boolean validateRNN097AndIdNot(String nombre, String acronimo, Integer id) {
        return gremioCuatroRepository.existsByNombreAndIdNot(nombre, id) || gremioCuatroRepository.existsByAcronimoAndIdNot(acronimo, id);
    }

    public Boolean validateRNN026(Integer idInstitucion){
        return miembroGremioCuatroRepository.existByIdInstitucion(idInstitucion);
    }
}
