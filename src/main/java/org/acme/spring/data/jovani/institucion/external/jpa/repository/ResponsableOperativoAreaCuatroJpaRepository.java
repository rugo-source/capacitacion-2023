package org.acme.spring.data.jovani.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.ResponsableOperativoAreaJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResponsableOperativoAreaCuatroJpaRepository extends JpaRepository<ResponsableOperativoAreaJpa, Integer> {
    boolean existsByIdAreaContratante(Integer idAreaContratante);
}
