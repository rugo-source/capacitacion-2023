package org.acme.spring.data.jovani.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.jovani.institucion.core.entity.InstitucionCuatroGet;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class InstitucionCuatroGetDto {

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al identificador de la institucion")
    String identificador;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al nombre de la institucion")
    String nombre;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al acronimo de la institucion")
    String acronimo;

    public InstitucionCuatroGet toEntity() {
        return InstitucionCuatroGet.builder()
                .identificador(this.identificador)
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .build();
    }
}
