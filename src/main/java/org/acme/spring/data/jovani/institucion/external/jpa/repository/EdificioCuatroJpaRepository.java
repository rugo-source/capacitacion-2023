package org.acme.spring.data.jovani.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EdificioCuatroJpaRepository extends JpaRepository<EdificioJpa, Integer> {
    @Query("from EdificioJpa where idSede = :idSede")
    List<EdificioJpa> findAllByIdSede(@Param("idSede") Integer idSede);
    boolean existsById(Integer id);
    boolean existsByIdSede(Integer idSede);
    boolean existsByNombreAndIdSede(String nombre, Integer idSede);
    boolean existsByAcronimoAndIdSede(String acronimo, Integer idSede);
    boolean existsByNombreAndIdSedeAndIdNot(String nombre, Integer idSede, Integer id);
    boolean existsByAcronimoAndIdSedeAndIdNot(String acronimo, Integer idSede, Integer id);
}
