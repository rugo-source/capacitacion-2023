package org.acme.spring.data.jovani.examples.core.entity;

import lombok.*;

import java.time.LocalDate;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CuentaCuatroCreate {
    Integer idPersona;
    String rol;
    LocalDate inicio;
    LocalDate fin;
}
