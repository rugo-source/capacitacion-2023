package org.acme.spring.data.jovani.examples.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.jovani.examples.core.entity.Persona;
import org.acme.spring.data.jovani.examples.core.statemachine.PersonaCuatroSM;
import org.acme.spring.data.jovani.examples.core.business.input.ExampleService;
import org.acme.spring.data.jovani.examples.core.business.output.PersonaRepository;
import org.acme.spring.data.jovani.examples.core.entity.PersonaCreate;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class ExampleBs implements ExampleService {


    @Inject
    PersonaRepository personaRepository;

    @Inject
    PersonaCuatroSM personaCuatroSM;

    @Override
    public List<Persona> listaEjemplo() {
        List<Persona> examples = personaRepository.findAll();
        examples.stream().forEach(persona -> System.out.println(persona.getEstadoPersonaCuatro().getName()));
        return examples.stream().map(persona -> setFlags(persona)).collect(Collectors.toList());
    }

    public Persona setFlags(Persona persona){
        persona.setRegistrar(personaCuatroSM.isDoable(personaCuatroSM.getRegistrar(),personaCuatroSM.getStateById(persona.getIdEstado())));
        persona.setEditar(personaCuatroSM.isDoable(personaCuatroSM.getEditar(),personaCuatroSM.getStateById(persona.getIdEstado())));
        persona.setEliminar(personaCuatroSM.isDoable(personaCuatroSM.getEliminar(),personaCuatroSM.getStateById(persona.getIdEstado())));
        persona.setConsultar(personaCuatroSM.isDoable(personaCuatroSM.getConsultar(),personaCuatroSM.getStateById(persona.getIdEstado())));
        persona.setConfigurar(personaCuatroSM.isDoable(personaCuatroSM.getConfigurar(),personaCuatroSM.getStateById(persona.getIdEstado())));
        return persona;
    }

    @Override
    public Either<Integer, Persona> get(Integer idPersona) {
        Either<Integer,Persona> resultado = Either.left(404);
        var persona = personaRepository.findBy(idPersona);
        if(persona.isPresent()){
            var personaGet = persona.get();
            resultado = Either.right(setFlags(personaGet));
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> create(PersonaCreate personaCreate) {
        Either<Integer, Boolean> resultado = Either.right(true);
        var personaPersist = Persona.builder()
                .nombre(personaCreate.getNombre())
                .idEstado(personaCuatroSM.getRegistrado().getId())
                .edad(personaCreate.getEdad())
                .build();
        personaRepository.save(personaPersist);
        return resultado;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> update(Integer id, PersonaCreate personaCreate) {
        Either<Integer, Boolean> resultado = Either.left(404);
        var personaSearch = personaRepository.findBy(id);
        if(personaSearch.isPresent()){
            var personaChange = personaSearch.get();
            personaChange.setNombre(personaCreate.getNombre());
            personaChange.setEdad(personaCreate.getEdad());
            personaChange.setIdEstado(personaCreate.getIdEstado());
            personaRepository.update(personaChange);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    public Either<Integer, Boolean> delete(Integer id) {
        Either<Integer, Boolean> resultado = Either.left(404);
        var personaSearch = personaRepository.findBy(id);
        if (personaSearch.isPresent()) {
            personaRepository.deleteBy(id);
            resultado = Either.right(true);
        }
        return resultado;
    }
}
