package org.acme.spring.data.jovani.institucion.core.business.output;


import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.acme.spring.data.jovani.institucion.core.entity.MiembroGremioCuatro;

import java.util.Optional;

public interface MiembroGremioCuatroRepository {
    Optional<MiembroGremioJpa> findById(MiembroGremioCuatro miembroGremioCuatro);
    void save(MiembroGremioJpa miembroGremioJpa);
    void delete(MiembroGremioCuatro miembroGremioCuatro);
    Boolean existByIdInstitucion(Integer idInstitucion);
}
