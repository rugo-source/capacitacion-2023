package org.acme.spring.data.jovani.institucion.core.business.output;

public interface ResponsableOperativoAreaCuatroRepository {
    Boolean existsByIdAreaContratante(Integer idAreaContratante);
}
