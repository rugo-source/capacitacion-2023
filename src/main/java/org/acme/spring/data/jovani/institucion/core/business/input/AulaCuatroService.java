package org.acme.spring.data.jovani.institucion.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.jovani.institucion.core.entity.AulaCuatroCreate;
import org.acme.spring.data.jovani.institucion.external.rest.dto.AulaCuatroDto;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface AulaCuatroService {
    Either<ErrorCodes,List<AulaCuatroDto>> listByIdEdificio(Integer idEdificio);
    Either<ErrorCodes, Boolean> create(AulaCuatroCreate aulaCuatroCreate);
    Either<ErrorCodes, Boolean> update(AulaCuatroCreate aulaCuatroUpdate, Integer idAula);
    Either<ErrorCodes, Boolean> delete(Integer idAula);
}
