package org.acme.spring.data.jovani.examples.core.business.output;

import org.acme.spring.data.jovani.examples.core.entity.Persona;

import java.util.List;
import java.util.Optional;

public interface PersonaRepository {

    List<Persona> findAll();
    List<Persona> findAllNative();
    List<Persona> findAllOrm();

    Optional<Persona> findBy(Integer idUsuario);

    void save(Persona personaPersist);
    void update(Persona personaChange);
    void deleteBy(Integer id);
}
