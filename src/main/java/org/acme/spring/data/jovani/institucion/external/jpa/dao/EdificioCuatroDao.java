package org.acme.spring.data.jovani.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.acme.spring.data.jovani.institucion.core.business.output.EdificioCuatroRepository;
import org.acme.spring.data.jovani.institucion.external.jpa.repository.EdificioCuatroJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class EdificioCuatroDao implements EdificioCuatroRepository {
    @Inject
    EdificioCuatroJpaRepository edificioCuatroJpaRepository;

    @Override
    public Boolean existsById(Integer idEdificio) {
        return edificioCuatroJpaRepository.existsById(idEdificio);
    }

    @Override
    public Boolean existsByIdSede(Integer idSede) {
        return edificioCuatroJpaRepository.existsByIdSede(idSede);
    }

    @Override
    public Optional<Edificio> findById(Integer idEdificio) {
        return edificioCuatroJpaRepository.findById(idEdificio).map(EdificioJpa::toEntity);
    }

    @Override
    public List<Edificio> findAllByIdSede(Integer idSede) {
        return edificioCuatroJpaRepository.findAllByIdSede(idSede).stream().map(EdificioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Edificio edificioPersist) {
        edificioCuatroJpaRepository.saveAndFlush(EdificioJpa.fromEntity(edificioPersist));
    }

    @Override
    public void update(Edificio edificioChange) {
        edificioCuatroJpaRepository.saveAndFlush(EdificioJpa.fromEntity(edificioChange));
    }

    @Override
    public void delete(Integer idEdificio) {
        edificioCuatroJpaRepository.deleteById(idEdificio);
    }

    @Override
    public Boolean existsByNombreAndIdSede(String nombre, Integer idSede) {
        return edificioCuatroJpaRepository.existsByNombreAndIdSede(nombre, idSede);
    }

    @Override
    public Boolean existsByAcronimoAndIdSede(String acronimo, Integer idSede) {
        return edificioCuatroJpaRepository.existsByAcronimoAndIdSede(acronimo, idSede);
    }

    @Override
    public Boolean existsByNombreAndIdSedeAndIdNot(String nombre, Integer idSede, Integer id) {
        return edificioCuatroJpaRepository.existsByNombreAndIdSedeAndIdNot(nombre, idSede, id);
    }

    @Override
    public Boolean existsByAcronimoAndIdSedeAndIdNot(String acronimo, Integer idSede, Integer id) {
        return edificioCuatroJpaRepository.existsByAcronimoAndIdSedeAndIdNot(acronimo, idSede, id);
    }
}
