package org.acme.spring.data.jovani.examples.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.jovani.examples.core.entity.CuentaCuatro;
import org.acme.spring.data.jovani.examples.core.entity.CuentaCuatroCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface CuentaService {
    Either<ErrorCodes, List<CuentaCuatro>> getAllAccountByIdPerson(Integer idPerson);
    Either<ErrorCodes, CuentaCuatro> get(Integer idCuenta);
    Either<ErrorCodes,Boolean> create(CuentaCuatroCreate cuentaCuatroCreate);
    Either<ErrorCodes,Boolean> update(Integer idCuenta, CuentaCuatroCreate cuentaCuatroCreate);
    Either<ErrorCodes,Boolean> delete(Integer idCuenta);
}
