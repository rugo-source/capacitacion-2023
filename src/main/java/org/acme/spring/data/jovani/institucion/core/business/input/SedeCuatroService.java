package org.acme.spring.data.jovani.institucion.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.jovani.institucion.core.entity.SedeCuatroCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface SedeCuatroService {
    Either<ErrorCodes, Sede> get(Integer idSede);
    Either<ErrorCodes, List<Sede>> listByInstitucion(Integer idInstitucion);
    Either<ErrorCodes, Boolean> create(SedeCuatroCreate sedeCuatroCreate);
    Either<ErrorCodes, Boolean> update(SedeCuatroCreate sedeCuatroCreate, Integer idSede);
    Either<ErrorCodes, Boolean> delete(Integer idSede);
}
