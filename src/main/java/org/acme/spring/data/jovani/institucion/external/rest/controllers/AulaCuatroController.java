package org.acme.spring.data.jovani.institucion.external.rest.controllers;

import org.acme.spring.data.jovani.institucion.core.business.input.AulaCuatroService;
import org.acme.spring.data.jovani.institucion.external.rest.dto.AulaCuatroCreateDto;
import org.acme.spring.data.jovani.institucion.external.rest.dto.AulaCuatroDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("catalogos-institucion/aula-cuatro")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Jovani")
public class AulaCuatroController {
    @Inject
    AulaCuatroService aulaCuatroService;

    @GET
    @Path("{idEdificio}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = AulaCuatroDto.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response listByIdEdificio(@PathParam("idEdificio") Integer idEdificio){
        return aulaCuatroService.listByIdEdificio(idEdificio).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid AulaCuatroCreateDto aulaCuatroCreateDto){
        return aulaCuatroService.create(aulaCuatroCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("{idAula}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@Valid AulaCuatroCreateDto aulaCuatroCreateDto, @PathParam("idAula") Integer idAula){
        return aulaCuatroService.update(aulaCuatroCreateDto.toEntity(), idAula).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("{idAula}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idAula") Integer idAula){
        return aulaCuatroService.delete(idAula).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
