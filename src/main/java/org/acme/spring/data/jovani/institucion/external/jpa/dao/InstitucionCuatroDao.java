package org.acme.spring.data.jovani.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.acme.spring.data.jovani.institucion.core.business.output.InstitucionCuatroRepository;
import org.acme.spring.data.jovani.institucion.external.jpa.repository.InstitucionCuatroJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class InstitucionCuatroDao implements InstitucionCuatroRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    InstitucionCuatroJpaRepository institucionCuatroJpaRepository;

    @Override
    public Optional<Institucion> findById(Integer idInstitucion) {
        return institucionCuatroJpaRepository.findById(idInstitucion).map(InstitucionJpa::toEntity);
    }

    @Override
    public Optional<Institucion> findByIdentificador(String identificador) {
        return institucionCuatroJpaRepository.findByIdentificador(identificador).map(InstitucionJpa::toEntity);
    }

    @Override
    public Optional<Institucion> findByNombre(String nombreInstitucion) {
        return institucionCuatroJpaRepository.findByNombre(nombreInstitucion).map(InstitucionJpa::toEntity);
    }

    @Override
    public Optional<Institucion> findInstitucion(String nombreInstitucion) {
        return institucionCuatroJpaRepository.findInstitucion(nombreInstitucion).map(InstitucionJpa::toEntity);
    }


    /*
    * Aqui jovani coloca tu query en una constante public final static String esto con el fin de tener una mejor estructura y visualizacion de los queries que declares, estos
    * ponlos antes de tus injects, esto mismo aplicalo con los nombres de tus parametros esto por si llegas a tener otro implementacion que los requiera
    * solo llames esta constante y ya no tengas que volver a escribir el nombre.
    * */
    @Override
    public List<Institucion> findInstituciones(String identificador, String nombre, String acronimo) {
        Query q = entityManager.createNativeQuery("Select * from tin01_institucion where UNACCENT(LOWER(tin01_institucion.tx_identificador)) LIKE '%' || UNACCENT(LOWER(:identificador))  || '%' and UNACCENT(LOWER(tin01_institucion.tx_nombre)) LIKE '%' || UNACCENT(LOWER(:nombre)) || '%' and UNACCENT(LOWER(tin01_institucion.tx_acronimo)) LIKE '%' || UNACCENT(LOWER(:acronimo)) || '%'", InstitucionJpa.class);
        q.setParameter("identificador", identificador);
        q.setParameter("nombre", nombre);
        q.setParameter("acronimo", acronimo);
        List<InstitucionJpa> result = q.getResultList();
        return  result.stream().map(InstitucionJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<Institucion> validateRNN013(Integer idInstitucion, String identificador) {
        return institucionCuatroJpaRepository.validateRNN013(idInstitucion,identificador).map(InstitucionJpa::toEntity);
    }

    @Override
    public Optional<Institucion> validateRNN094(Integer idInstitucion, String nombreInstitucion) {
        return institucionCuatroJpaRepository.validateRNN094(idInstitucion, nombreInstitucion).map(InstitucionJpa::toEntity);
    }

    @Override
    public void save(Institucion institucionPersist) {
        institucionCuatroJpaRepository.saveAndFlush(InstitucionJpa.fromEntity(institucionPersist));
    }

    @Override
    public void update(Institucion institucionChange) {
        institucionCuatroJpaRepository.saveAndFlush(InstitucionJpa.fromEntity(institucionChange));
    }

    @Override
    public Boolean existsById(Integer idInstitucion) {
        return institucionCuatroJpaRepository.existsById(idInstitucion);
    }

    @Override
    public Boolean existsIdentificadorAndIdNot(String identificador, Integer idInstitucion) {
        return institucionCuatroJpaRepository.existsByIdentificadorAndIdNot(identificador, idInstitucion);
    }

    @Override
    public Boolean existsNombreAndIdNot(String nombre, Integer idInstitucion) {
        return institucionCuatroJpaRepository.existsByNombreAndIdNot(nombre, idInstitucion);
    }

}
