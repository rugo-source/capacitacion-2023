package org.acme.spring.data.jovani.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.GremioJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.acme.spring.data.jovani.institucion.core.business.output.GremioCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.entity.MiembroGremioCuatro;
import org.acme.spring.data.jovani.institucion.external.jpa.repository.GremioCuatroJpaRepository;
import org.acme.spring.data.jovani.institucion.external.jpa.repository.MiembroGremioCuatroJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class GremioCuatroDao implements GremioCuatroRepository {
    @Inject
    GremioCuatroJpaRepository gremioCuatroJpaRepository;

    @Inject
    MiembroGremioCuatroJpaRepository miembroGremioCuatroJpaRepository;

    @Override
    public Optional<Gremio> findById(Integer idGremio) {
        return gremioCuatroJpaRepository.findById(idGremio).map(GremioJpa -> {
            var gremio = GremioJpa.toEntity();
            gremio.setInstitucionList(GremioJpa.getInstituciones().stream().map(InstitucionJpa::toEntity).collect(Collectors.toList()));
            return gremio;
        });
    }

    @Override
    public List<Gremio> findAll() {
        return gremioCuatroJpaRepository.findAll().stream().map(GremioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Gremio gremioPersist) {
        gremioCuatroJpaRepository.saveAndFlush(GremioJpa.fromEntity(gremioPersist));
    }

    @Override
    public void update(Gremio gremioChange) {
        gremioCuatroJpaRepository.saveAndFlush(GremioJpa.fromEntity(gremioChange));
    }

    @Override
    public void delete(Integer idGremio) {
        gremioCuatroJpaRepository.deleteById(idGremio);
    }

    @Override
    public void createMiembro(MiembroGremioCuatro miembroGremioCuatroCreate) {
        var miembroGremioJpa = MiembroGremioJpa.builder()
                .id(MiembroGremioIdJpa.builder()
                        .idGremio(miembroGremioCuatroCreate.getIdGremio())
                        .idInstitucion(miembroGremioCuatroCreate.getIdInstitucion())
                        .build())
                .build();
        miembroGremioCuatroJpaRepository.saveAndFlush(miembroGremioJpa);
    }

    @Override
    public void deleteMiembro(MiembroGremioJpa miembroGremioJpa) {
        miembroGremioCuatroJpaRepository.delete(miembroGremioJpa);
    }

    @Override
    public Boolean existsByNombre(String nombre) {
        return gremioCuatroJpaRepository.existsByNombre(nombre);
    }

    @Override
    public Boolean existsByAcronimo(String acronimo) {

        return gremioCuatroJpaRepository.existsByAcronimo(acronimo);
    }

    @Override
    public Boolean existsByNombreAndIdNot(String nombre, Integer id) {
        return gremioCuatroJpaRepository.existsByNombreAndIdNot(nombre, id);
    }

    @Override
    public Boolean existsByAcronimoAndIdNot(String acronimo, Integer id) {
        return gremioCuatroJpaRepository.existsByAcronimoAndIdNot(acronimo, id);
    }
}
