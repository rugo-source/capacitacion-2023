package org.acme.spring.data.jovani.examples.external.jpa.dao;

import org.acme.spring.data.jovani.examples.core.business.output.PersonaRepository;
import org.acme.spring.data.jovani.examples.core.entity.Persona;
import org.acme.spring.data.jovani.examples.external.jpa.repository.PersonaJpaRepository;
import org.acme.spring.data.jovani.examples.external.jpa.model.PersonaCuatroJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class PersonaDao implements PersonaRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    PersonaJpaRepository personaJpaRepository;

    @Override
    public List<Persona> findAll() {
        return personaJpaRepository.findAll().stream().map(personaJpa -> {
            var personaEntidad = personaJpa.toEntity();
            personaEntidad.setEstadoPersonaCuatro(personaJpa.getEstadoPersonaCuatro().toEntity());
            return personaEntidad;
        }).collect(Collectors.toList());
    }

    @Override
    public List<Persona> findAllNative() {
        Stream<PersonaCuatroJpa> resultado = entityManager.createNativeQuery("Select * from persona_cuatro", PersonaCuatroJpa.class).getResultStream();
        return resultado.map(PersonaCuatroJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<Persona> findAllOrm() {
        return personaJpaRepository.findAllOrm().stream().map(PersonaCuatroJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<Persona> findBy(Integer idPersona) {
        return personaJpaRepository.findById(idPersona).map(PersonaCuatroJpa::toEntity);
    }

    @Override
    public void save(Persona personaPersist) {
        personaJpaRepository.saveAndFlush(PersonaCuatroJpa.fromEntity(personaPersist));
    }

    @Override
    public void update(Persona personaChange) {
        personaJpaRepository.saveAndFlush(PersonaCuatroJpa.fromEntity(personaChange));
    }

    @Override
    public void deleteBy(Integer id) { personaJpaRepository.deleteById(id); }
}
