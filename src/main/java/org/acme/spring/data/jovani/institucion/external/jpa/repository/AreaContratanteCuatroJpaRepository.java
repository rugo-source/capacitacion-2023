package org.acme.spring.data.jovani.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AreaContratanteCuatroJpaRepository extends JpaRepository<AreaContratanteJpa, Integer> {
    boolean existsById(Integer idAreaContratante);

    @Query("from AreaContratanteJpa where idInstitucion = :idInstitucion")
    List<AreaContratanteJpa> findAllByIdInstitucion(@Param("idInstitucion") Integer idInstitucion);
}
