package org.acme.spring.data.jovani.examples.external.jpa.repository;


import org.acme.spring.data.jovani.examples.external.jpa.model.PersonaCuatroJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PersonaJpaRepository extends JpaRepository<PersonaCuatroJpa,Integer> {
    @Query("from PersonaJpa pj")
    List<PersonaCuatroJpa> findAllOrm();
}
