package org.acme.spring.data.jovani.institucion.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.jovani.institucion.core.entity.InstitucionCuatroCreate;
import org.acme.spring.data.jovani.institucion.core.entity.InstitucionCuatroGet;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface InstitucionCuatroService {
    Either<ErrorCodes, Boolean> create(InstitucionCuatroCreate institucionCuatroCreate);
    Either<ErrorCodes, List<Institucion>> list(InstitucionCuatroGet institucionCuatroGet);
    Either<ErrorCodes, Boolean> update(Integer idInstitucion, InstitucionCuatroCreate institucionCuatroCreate);
}
