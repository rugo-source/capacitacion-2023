package org.acme.spring.data.jovani.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GremioCuatroDto {

    @JsonProperty
    @Schema(description = "este atributo hace referencia al id del Gremio")
    Integer id;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al nombre del Gremio")
    String nombre;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al acrónimo del Gremio")
    String acronimo;


    public static GremioCuatroDto fromEntity (Gremio gremio) {
        return GremioCuatroDto.builder()
                .id(gremio.getId())
                .nombre(gremio.getNombre())
                .acronimo(gremio.getAcronimo())
                .build();
    }
}
