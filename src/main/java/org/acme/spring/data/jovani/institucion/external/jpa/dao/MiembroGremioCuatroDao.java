package org.acme.spring.data.jovani.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.acme.spring.data.jovani.institucion.core.business.output.MiembroGremioCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.entity.MiembroGremioCuatro;
import org.acme.spring.data.jovani.institucion.external.jpa.repository.MiembroGremioCuatroJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Optional;

@ApplicationScoped
public class MiembroGremioCuatroDao implements MiembroGremioCuatroRepository {
    public static final String QUERY_BUSCAR_INSTITUCION_EN_GREMIO = "select count(tin06.fk_id_institucion) > 0 from tin06_gremio_institucion tin06 where tin06.fk_id_institucion = :idInstitucion";

    @Inject
    MiembroGremioCuatroJpaRepository miembroGremioCuatroJpaRepository;

    @Inject
    EntityManager entityManager;

    @Override
    public Optional<MiembroGremioJpa> findById(MiembroGremioCuatro miembroGremioCuatro) {
        return  miembroGremioCuatroJpaRepository.findById(MiembroGremioIdJpa.builder()
                .idGremio(miembroGremioCuatro.getIdGremio())
                .idInstitucion(miembroGremioCuatro.getIdInstitucion())
                .build());
    }

    @Override
    public void save(MiembroGremioJpa miembroGremioJpa) {
        miembroGremioCuatroJpaRepository.saveAndFlush(miembroGremioJpa);
    }

    @Override
    public void delete(MiembroGremioCuatro miembroGremioCuatro) {
        miembroGremioCuatroJpaRepository.deleteById(MiembroGremioIdJpa.builder()
                .idGremio(miembroGremioCuatro.getIdGremio())
                .idInstitucion(miembroGremioCuatro.getIdInstitucion())
                .build());
    }

    @Override
    public Boolean existByIdInstitucion(Integer idInstitucion) {
        Query q = entityManager.createNativeQuery(QUERY_BUSCAR_INSTITUCION_EN_GREMIO);
        q.setParameter("idInstitucion", idInstitucion);
        Boolean result = ((Boolean) q.getSingleResult()).booleanValue();
        return result;
    }

}
