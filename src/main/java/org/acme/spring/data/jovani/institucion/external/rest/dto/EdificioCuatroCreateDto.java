package org.acme.spring.data.jovani.institucion.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.jovani.institucion.core.entity.EdificioCuatroCreate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EdificioCuatroCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @Positive(message = "RNS001")
    Integer idSede;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    String acronimo;

    @JsonProperty
    @NotNull(message = "RNS001")
    String referencia;

    public EdificioCuatroCreate toEntity(){
        return EdificioCuatroCreate.builder()
                .idSede(this.idSede)
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .referencia(this.referencia)
                .build();
    }

}
