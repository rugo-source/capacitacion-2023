package org.acme.spring.data.jovani.institucion.external.rest.controllers;

import org.acme.spring.data.jovani.institucion.core.business.input.AreaContratanteCuatroService;
import org.acme.spring.data.jovani.institucion.external.rest.dto.AreaContratanteCuatroCreateDto;
import org.acme.spring.data.jovani.institucion.external.rest.dto.AreaContratanteCuatroDto;

import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("catalogos-instituciones/area-contratante-cuatro")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Jovani")
public class AreaContratanteCuatroController {
    @Inject
    AreaContratanteCuatroService areaContratanteCuatroService;

    @GET
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = AreaContratanteCuatroDto.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response list(@QueryParam("idInstitucion") Integer idInstitucion) {
        return areaContratanteCuatroService.list(idInstitucion).map((areas) -> areas.stream().map(AreaContratanteCuatroDto::fromEntity)).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid AreaContratanteCuatroCreateDto areaContratanteCuatroCreateDto) {
        return areaContratanteCuatroService.create(areaContratanteCuatroCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("{idAreaContratante}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idAreaContratante") Integer idAreaContratante, @Valid AreaContratanteCuatroCreateDto areaContratanteCuatroCreateDto) {
        return areaContratanteCuatroService.update(idAreaContratante, areaContratanteCuatroCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("{idAreaContratante}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idAreaContratante") Integer idAreaContratante) {
        return areaContratanteCuatroService.delete(idAreaContratante).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
