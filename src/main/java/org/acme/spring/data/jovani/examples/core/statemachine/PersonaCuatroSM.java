package org.acme.spring.data.jovani.examples.core.statemachine;

import io.quarkus.runtime.Startup;
import lombok.Getter;
import org.acme.spring.data.util.Action;
import org.acme.spring.data.util.CustomStateMachine;
import org.acme.spring.data.util.State;
import org.acme.spring.data.util.StateMachineLoader;

import javax.inject.Singleton;

@Startup
@Singleton
@Getter
public class PersonaCuatroSM extends CustomStateMachine {

    private final State registrado;
    private final State activo;
    private final State inactivo;

    private final Action registrar;
    private final Action editar;
    private final Action eliminar;
    private final Action consultar;
    private final Action configurar;

    public PersonaCuatroSM() {
        stateMachine = StateMachineLoader.load("persona-cuatro-sm.json").orElseThrow();
        this.registrado = stateMachine.getStates().get(0);
        this.activo = stateMachine.getStates().get(1);
        this.inactivo = stateMachine.getStates().get(2);

        this.registrar = stateMachine.getActions().get(0);
        this.editar = stateMachine.getActions().get(1);
        this.eliminar = stateMachine.getActions().get(2);
        this.consultar = stateMachine.getActions().get(3);
        this.configurar = stateMachine.getActions().get(4);
    }

}
