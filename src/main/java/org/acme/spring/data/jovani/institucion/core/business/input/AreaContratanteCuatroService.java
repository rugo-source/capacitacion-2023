package org.acme.spring.data.jovani.institucion.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.jovani.institucion.core.entity.AreaContratanteCuatroCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface AreaContratanteCuatroService {
    Either<ErrorCodes, List<AreaContratante>> list(Integer idInstitucion);
    Either<ErrorCodes, Boolean> create(AreaContratanteCuatroCreate areaContratanteCuatroCreate);
    Either<ErrorCodes, Boolean> update(Integer idAreaContratante, AreaContratanteCuatroCreate areaContratanteCuatroCreate);
    Either<ErrorCodes, Boolean> delete(Integer idAreaContratante);
}
