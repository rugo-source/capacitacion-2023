package org.acme.spring.data.jovani.institucion.external.jpa.dao;

import org.acme.spring.data.jovani.institucion.core.business.output.ResponsableOperativoAreaCuatroRepository;
import org.acme.spring.data.jovani.institucion.external.jpa.repository.ResponsableOperativoAreaCuatroJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ResponsableOperativoAreaCuatroDao implements ResponsableOperativoAreaCuatroRepository {
    @Inject
    ResponsableOperativoAreaCuatroJpaRepository responsableOperativoAreaCuatroJpaRepository;

    @Override
    public Boolean existsByIdAreaContratante(Integer idAreaContratante) {
        return responsableOperativoAreaCuatroJpaRepository.existsByIdAreaContratante(idAreaContratante);
    }
}
