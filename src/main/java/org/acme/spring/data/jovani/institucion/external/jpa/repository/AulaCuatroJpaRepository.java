package org.acme.spring.data.jovani.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.AulaJpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AulaCuatroJpaRepository extends JpaRepository<AulaJpa, Integer> {
    @Query("from AulaJpa where idEdificio = :idEdificio")
    List<AulaJpa> findAllByIdEdificio(@Param("idEdificio") Integer idEdificio);
    boolean existsById(Integer id);
    boolean existsByIdEdificio(Integer idEdificio);
    boolean existsByNombreAndIdEdificio(String nombre, Integer idEdificio);
    boolean existsByNombreAndIdEdificioAndIdNot(String nombre, Integer idEdificio, Integer id);
}
