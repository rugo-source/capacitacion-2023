package org.acme.spring.data.jovani.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;
import org.acme.spring.data.jovani.institucion.core.business.output.SedeCuatroRepository;
import org.acme.spring.data.jovani.institucion.external.jpa.repository.SedeCuatroJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class SedeCuatroDao implements SedeCuatroRepository {

    @Inject
    SedeCuatroJpaRepository sedeCuatroJpaRepository;

    @Override
    public Optional<Sede> findById(Integer idSede) {
        return sedeCuatroJpaRepository.findById(idSede).map(SedeJpa::toEntity);
    }

    @Override
    public List<Sede> findAllByIdInstitucion(Integer idInstitucion) {
        return sedeCuatroJpaRepository.findAllByIdInstitucion(idInstitucion).stream().map(SedeJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Sede sedePersist) {
        sedeCuatroJpaRepository.saveAndFlush(SedeJpa.fromEntity(sedePersist));
    }

    @Override
    public void update(Sede sedeChange) {
        sedeCuatroJpaRepository.saveAndFlush(SedeJpa.fromEntity(sedeChange));
    }

    @Override
    public void delete(Integer idSede) {
        sedeCuatroJpaRepository.deleteById(idSede);
    }

    @Override
    public Boolean existsByNombreAndIdInstitucion(String nombre, Integer idInstitucion) {
        return sedeCuatroJpaRepository.existsByNombreAndIdInstitucion(nombre, idInstitucion);
    }

    @Override
    public Boolean existsByAcronimoAndIdInstitucion(String acronimo, Integer idInstitucion) {
        return sedeCuatroJpaRepository.existsByAcronimoAndIdInstitucion(acronimo, idInstitucion);
    }

    @Override
    public Boolean existsByNombreAndIdInstitucionAndIdNot(String nombre, Integer idInstitucion, Integer id) {
        return sedeCuatroJpaRepository.existsByNombreAndIdInstitucionAndIdNot(nombre, idInstitucion, id);
    }

    @Override
    public Boolean existsByAcronimoAndIdInstitucionAndIdNot(String acronimo, Integer idInstitucion, Integer id) {
        return sedeCuatroJpaRepository.existsByAcronimoAndIdInstitucionAndIdNot(acronimo, idInstitucion, id);
    }

    @Override
    public Boolean existsById(Integer idSede) {
        return sedeCuatroJpaRepository.existsById(idSede);
    }
}
