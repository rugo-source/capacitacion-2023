package org.acme.spring.data.jovani.institucion.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Edificio;

import java.util.List;
import java.util.Optional;

public interface EdificioCuatroRepository {
    Boolean existsById(Integer idEdificio);
    Boolean existsByIdSede(Integer idSede);
    Optional<Edificio> findById(Integer idEdificio);
    List<Edificio> findAllByIdSede(Integer idSede);
    void save(Edificio edificioPersist);
    void update(Edificio edificioChange);
    void delete(Integer idEdificio);
    Boolean existsByNombreAndIdSede(String nombre, Integer idSede);
    Boolean existsByAcronimoAndIdSede(String acronimo, Integer idSede);
    Boolean existsByNombreAndIdSedeAndIdNot(String nombre, Integer idSede, Integer id);
    Boolean existsByAcronimoAndIdSedeAndIdNot(String acronimo, Integer idSede, Integer id);
}
