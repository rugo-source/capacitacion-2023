package org.acme.spring.data.jovani.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AreaContratanteCuatroCreate {
    private Integer idInstitucion;
    private String area;
    private String subarea;
    private String telefono;
    private String extension;
}
