package org.acme.spring.data.jovani.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EdificioCuatroCreate {
    private Integer idSede;
    private String nombre;
    private String acronimo;
    private String referencia;
}
