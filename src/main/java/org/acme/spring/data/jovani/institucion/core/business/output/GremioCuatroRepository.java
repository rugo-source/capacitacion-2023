package org.acme.spring.data.jovani.institucion.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.acme.spring.data.jovani.institucion.core.entity.MiembroGremioCuatro;

import java.util.List;
import java.util.Optional;

public interface GremioCuatroRepository {
    Optional<Gremio> findById(Integer idGremio);
    List<Gremio> findAll();
    void save(Gremio gremioPersist);
    void update(Gremio gremioChange);
    void delete(Integer idGremio);
    void createMiembro(MiembroGremioCuatro miembroGremioCuatro);
    void deleteMiembro(MiembroGremioJpa miembroGremioJpa);
    Boolean existsByNombre(String nombre);
    Boolean existsByAcronimo(String acronimo);
    Boolean existsByNombreAndIdNot(String nombre, Integer id);
    Boolean existsByAcronimoAndIdNot(String acronimo, Integer id);
}
