package org.acme.spring.data.jovani.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MiembroGremioCuatro {
    private Integer idGremio;
    private Integer idInstitucion;
}
