package org.acme.spring.data.jovani.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EdificioCuatroDto {

    @JsonProperty
    @Schema(description = "este atributo hace referencia al id del edificio")
    Integer id;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al id de la sede a la que pertenece el edificio")
    Integer idSede;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al nombre del edificion")
    String nombre;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al acronimo del edificio")
    String acronimo;

    @JsonProperty
    @Schema(description = "este atributo hace referencia a la referencia del edificio")
    String referencia;

    public static EdificioCuatroDto fromEntity(Edificio edificio){
        return EdificioCuatroDto.builder()
                .id(edificio.getId())
                .idSede(edificio.getIdSede())
                .nombre(edificio.getNombre())
                .acronimo(edificio.getAcronimo())
                .referencia(edificio.getReferencia())
                .build();
    }
}
