package org.acme.spring.data.jovani.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.jovani.institucion.core.business.input.AreaContratanteCuatroService;
import org.acme.spring.data.jovani.institucion.core.business.output.AreaContratanteCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.business.output.InstitucionCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.business.output.ResponsableOperativoAreaCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.entity.AreaContratanteCuatroCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class AreaContratanteCuatroBs implements AreaContratanteCuatroService {
    @Inject
    AreaContratanteCuatroRepository areaContratanteCuatroRepository;

    @Inject
    InstitucionCuatroRepository institucionCuatroRepository;

    @Inject
    ResponsableOperativoAreaCuatroRepository responsableOperativoAreaCuatroRepository;

    @Override
    public Either<ErrorCodes, List<AreaContratante>> list(Integer idInstitucion) {
        Either<ErrorCodes, List<AreaContratante>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if(existsInstitucion(idInstitucion)){
            var areas = areaContratanteCuatroRepository.findAreasContratantes(idInstitucion);
            resultado = Either.right(areas);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> create(AreaContratanteCuatroCreate areaContratanteCuatroCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if(existsInstitucion(areaContratanteCuatroCreate.getIdInstitucion())){
            var areaContratantePersist = AreaContratante.builder()
                    .idInstitucion(areaContratanteCuatroCreate.getIdInstitucion())
                    .area(areaContratanteCuatroCreate.getArea())
                    .subarea(areaContratanteCuatroCreate.getSubarea())
                    .telefono(areaContratanteCuatroCreate.getTelefono())
                    .extension(areaContratanteCuatroCreate.getExtension())
                    .build();
            areaContratanteCuatroRepository.save(areaContratantePersist);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer idAreaContratante, AreaContratanteCuatroCreate areaContratanteCuatroCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if(existsInstitucion(areaContratanteCuatroCreate.getIdInstitucion())) {
            var areaContratanteSearch = areaContratanteCuatroRepository.findById(idAreaContratante);
            if (areaContratanteSearch.isPresent()) {
                var areContratanteChange = areaContratanteSearch.get();
                areContratanteChange.setIdInstitucion(areaContratanteCuatroCreate.getIdInstitucion());
                areContratanteChange.setArea(areaContratanteCuatroCreate.getArea());
                areContratanteChange.setSubarea(areaContratanteCuatroCreate.getSubarea());
                areContratanteChange.setTelefono(areaContratanteCuatroCreate.getTelefono());
                areContratanteChange.setExtension(areaContratanteCuatroCreate.getExtension());
                areaContratanteCuatroRepository.update(areContratanteChange);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idAreaContratante) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if(validateRNN023(idAreaContratante)) {
            resultado = Either.left(ErrorCodes.RNN023);
        } else{
            if (existsAreaContratante(idAreaContratante)) {
                areaContratanteCuatroRepository.delete(idAreaContratante);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    public Boolean existsAreaContratante(Integer idAreaContratante) {
        return areaContratanteCuatroRepository.existsById(idAreaContratante);
    }

    public Boolean existsInstitucion(Integer idInstitucion) {
        return institucionCuatroRepository.existsById(idInstitucion);
    }

    public Boolean validateRNN023(Integer idAreaContratante) {
        return responsableOperativoAreaCuatroRepository.existsByIdAreaContratante(idAreaContratante);
    }
}
