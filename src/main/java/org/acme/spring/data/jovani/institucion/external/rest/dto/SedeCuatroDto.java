package org.acme.spring.data.jovani.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SedeCuatroDto {

    @JsonProperty
    @Schema(description = "este atributo hace referencia al id de la Sede")
    Integer id;

    @JsonProperty
    @Schema(description = "este atributo hace referenica al id de la instutucion a la que pertenece la sede")
    Integer idInstitucion;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al nombre de la sede")
    String nombre;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al acronimo de la sede")
    String acronimo;

    @JsonProperty
    @Schema(description = "este atributo hace referencia a la capacidad de la sede")
    Integer capacidad;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al estado de la sede")
    Boolean propia;

    @JsonProperty
    @Schema(description = "este atributo hace referencia al identificador de la sede")
    String identificador;

    public static SedeCuatroDto fromEntity(Sede sede){
        return SedeCuatroDto.builder()
                .id(sede.getId())
                .idInstitucion(sede.getIdInstitucion())
                .nombre(sede.getNombre())
                .acronimo(sede.getAcronimo())
                .capacidad(sede.getCapacidad())
                .propia(sede.getPropia())
                .identificador(sede.getIdentificador())
                .build();
    }
}
