package org.acme.spring.data.jovani.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.jovani.institucion.core.business.input.InstitucionCuatroService;
import org.acme.spring.data.jovani.institucion.core.business.output.InstitucionCuatroRepository;
import org.acme.spring.data.jovani.institucion.core.entity.InstitucionCuatroCreate;
import org.acme.spring.data.jovani.institucion.core.entity.InstitucionCuatroGet;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class InstitucionCuatroBs implements InstitucionCuatroService {

    @Inject
    InstitucionCuatroRepository institucionCuatroRepository;


    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(InstitucionCuatroCreate institucionCuatroCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.RNN013);
        var institucionIdentificadorSearch = institucionCuatroRepository.findByIdentificador(institucionCuatroCreate.getIdentificador());
        if(!institucionIdentificadorSearch.isPresent()) {
            resultado = Either.left(ErrorCodes.RNN094);
            var institucionNombreSearch = institucionCuatroRepository.findByNombre(institucionCuatroCreate.getNombre());
            if(!institucionNombreSearch.isPresent()) {
                var institucionPersist = Institucion.builder()
                        .idTipo(institucionCuatroCreate.getIdTipo())
                        .idClasificacion(institucionCuatroCreate.getIdClasificacion())
                        .idCategoria(institucionCuatroCreate.getIdCategoria())
                        .idSubsistemaUniversidad(institucionCuatroCreate.getIdSubsistemaUniversidad())
                        .idSubsistemaBachillerato(institucionCuatroCreate.getIdSubsistemaBachillerato())
                        .identificador(institucionCuatroCreate.getIdentificador())
                        .nombre(institucionCuatroCreate.getNombre())
                        .acronimo(institucionCuatroCreate.getAcronimo())
                        .cct(institucionCuatroCreate.getCct())
                        .numeroSedesRegistradas(0)
                        .build();
                institucionCuatroRepository.save(institucionPersist);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, List<Institucion>> list(InstitucionCuatroGet institucionCuatroGet) {
        Either<ErrorCodes, List<Institucion>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var instituciones = institucionCuatroRepository.findInstituciones(institucionCuatroGet.getIdentificador(),institucionCuatroGet.getNombre(),institucionCuatroGet.getAcronimo());
        if(!instituciones.isEmpty()) {
            System.out.println("LA LISTA NO ES VACIA");
            resultado = Either.right(instituciones);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer idInstitucion, InstitucionCuatroCreate institucionCuatroCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var institucionSearch = institucionCuatroRepository.findById(idInstitucion);
        if(institucionSearch.isPresent()) {
            if (validateRNN013(idInstitucion, institucionCuatroCreate.getIdentificador())) {
                resultado = Either.left(ErrorCodes.RNN013);
            } else if(validateRNN094(idInstitucion, institucionCuatroCreate.getNombre())){
                resultado = Either.left(ErrorCodes.RNN094);
            } else {
                var institucionChange = institucionSearch.get();
                institucionChange.setIdTipo(institucionCuatroCreate.getIdTipo());
                institucionChange.setIdClasificacion(institucionCuatroCreate.getIdClasificacion());
                institucionChange.setIdCategoria(institucionCuatroCreate.getIdCategoria());
                institucionChange.setIdSubsistemaUniversidad(institucionCuatroCreate.getIdSubsistemaUniversidad());
                institucionChange.setIdSubsistemaBachillerato(institucionCuatroCreate.getIdSubsistemaBachillerato());
                institucionChange.setIdentificador(institucionCuatroCreate.getIdentificador());
                institucionChange.setNombre(institucionCuatroCreate.getNombre());
                institucionChange.setAcronimo(institucionCuatroCreate.getAcronimo());
                institucionChange.setCct(institucionCuatroCreate.getCct());
                institucionChange.setNumeroSedesRegistradas(institucionCuatroCreate.getNumeroSedesRegistradas());
                institucionCuatroRepository.update(institucionChange);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }


    public Boolean validateRNN013(Integer idInstitucion, String identificador) {
        return institucionCuatroRepository.existsIdentificadorAndIdNot(identificador, idInstitucion);
    }

    public Boolean validateRNN094(Integer idInstitucion, String nombre) {
        return institucionCuatroRepository.existsNombreAndIdNot(nombre, idInstitucion);
    }

}
