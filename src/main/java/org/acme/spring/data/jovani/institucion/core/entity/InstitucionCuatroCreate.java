package org.acme.spring.data.jovani.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class InstitucionCuatroCreate {
    private Integer idTipo;
    private Integer idClasificacion;
    private Integer idCategoria;
    private Integer idSubsistemaUniversidad;
    private Integer idSubsistemaBachillerato;
    private String identificador;
    private String nombre;
    private String acronimo;
    private String cct;
    private Integer numeroSedesRegistradas;
}
