package org.acme.spring.data.jovani.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.jovani.examples.core.entity.CuentaCuatro;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDate;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CuentaCuatroDto {

    @JsonProperty
    @Schema(description = "este es el atributo que referencia al id de una persona.")
    private Integer idPersona;
    @JsonProperty
    @Schema(description = "este es el rol de la cuenta")
    private String rol;
    @JsonProperty
    @Schema(description = "esta es la fecha inicio de la cuenta")
    private LocalDate inicio;
    @JsonProperty
    @Schema(description = "esta es la fecha fin de la cuenta")
    private LocalDate fin;

    public static CuentaCuatroDto fromEntity(CuentaCuatro cuentaCuatro) {
        return CuentaCuatroDto.builder()
                .idPersona(cuentaCuatro.getIdPersona())
                .rol(cuentaCuatro.getRol())
                .inicio(cuentaCuatro.getInicio())
                .fin(cuentaCuatro.getFin())
                .build();
    }
}
