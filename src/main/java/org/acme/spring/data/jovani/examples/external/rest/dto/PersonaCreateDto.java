package org.acme.spring.data.jovani.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.jovani.examples.core.entity.PersonaCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "PersonaCreate", description = "Esta en la entidad para guardar una persona")
public class PersonaCreateDto {

    @JsonProperty
    @Schema(description = "este es el atributo que referencia al nombre de la persona")
    private String name;
    @JsonProperty
    @Schema(description = "este es el atributo que referencia a la edad de la persona")
    private Integer age;
    @JsonProperty
    @Schema(description = "este es el atributo que referencia a la tabla estado persona cuatro")
    private Integer idState;

    public PersonaCreate toEntity(){
        return PersonaCreate.builder()
                .nombre(this.name)
                .edad(this.age)
                .idEstado(this.idState)
                .build();
    }
}
