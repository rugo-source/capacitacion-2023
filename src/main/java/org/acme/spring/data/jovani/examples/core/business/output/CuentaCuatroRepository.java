package org.acme.spring.data.jovani.examples.core.business.output;

import org.acme.spring.data.jovani.examples.core.entity.CuentaCuatro;

import java.util.List;
import java.util.Optional;

public interface CuentaCuatroRepository {
    List<CuentaCuatro> findAllAccountsByIdPerson(Integer idPersona);
    Optional<CuentaCuatro> findById(Integer idCuenta);
    void save(CuentaCuatro cuentaPersist);
    void update(CuentaCuatro cuentaCuatroChange);
    void deleteById(Integer idCuenta);
}
