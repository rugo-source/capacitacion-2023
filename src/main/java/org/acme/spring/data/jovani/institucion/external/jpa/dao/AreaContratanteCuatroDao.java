package org.acme.spring.data.jovani.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.acme.spring.data.jovani.institucion.core.business.output.AreaContratanteCuatroRepository;
import org.acme.spring.data.jovani.institucion.external.jpa.repository.AreaContratanteCuatroJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class AreaContratanteCuatroDao implements AreaContratanteCuatroRepository {
    @Inject
    AreaContratanteCuatroJpaRepository areaContratanteCuatroJpaRepository;

    @Override
    public List<AreaContratante> findAreasContratantes(Integer idInstitucion) {
        return areaContratanteCuatroJpaRepository.findAllByIdInstitucion(idInstitucion).stream().map(AreaContratanteJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<AreaContratante> findById(Integer idAreaContrantante) {
        return areaContratanteCuatroJpaRepository.findById(idAreaContrantante).map(AreaContratanteJpa::toEntity);
    }

    @Override
    public void save(AreaContratante areaContratantePersist) {
        areaContratanteCuatroJpaRepository.saveAndFlush(AreaContratanteJpa.fromEntity(areaContratantePersist));
    }

    @Override
    public void update(AreaContratante areaContratanteChange) {
        areaContratanteCuatroJpaRepository.saveAndFlush(AreaContratanteJpa.fromEntity(areaContratanteChange));
    }

    @Override
    public void delete(Integer idAreaContratante) {
        areaContratanteCuatroJpaRepository.deleteById(idAreaContratante);
    }

    @Override
    public Boolean existsById(Integer idAreaContrantante) {
        return areaContratanteCuatroJpaRepository.existsById(idAreaContrantante);
    }
}
