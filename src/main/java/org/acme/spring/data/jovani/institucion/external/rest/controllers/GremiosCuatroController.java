package org.acme.spring.data.jovani.institucion.external.rest.controllers;
import org.acme.spring.data.jovani.institucion.core.business.input.GremioCuatroService;
import org.acme.spring.data.jovani.institucion.external.rest.dto.GremioCuatroCreateDto;
import org.acme.spring.data.jovani.institucion.external.rest.dto.GremioCuatroDto;
import org.acme.spring.data.jovani.institucion.external.rest.dto.MiembroGremioCuatroCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("catalogos-institucion/gremio-cuatro")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Jovani")
public class GremiosCuatroController {

    @Inject
    GremioCuatroService gremioCuatroService;

    @GET
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = GremioCuatroDto.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response list() {
        return gremioCuatroService.list().map(gremios -> gremios.stream().map(GremioCuatroDto::fromEntity)).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid GremioCuatroCreateDto gremioCuatroCreateDto) {
        return gremioCuatroService.create(gremioCuatroCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("{idGremio}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idGremio") Integer idGremio, @Valid GremioCuatroCreateDto gremioCuatroCreateDto) {
        return gremioCuatroService.update(idGremio, gremioCuatroCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("{idGremio}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idGremio") Integer idGremio) {
        return gremioCuatroService.delete(idGremio).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @Path("/asociar-institucion")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response crearAsociacion(MiembroGremioCuatroCreateDto miembroGremioCuatroGetDto) {
        return gremioCuatroService.asociarInstitucion(miembroGremioCuatroGetDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("/asociar-institucion")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response deleteAsociacion(MiembroGremioCuatroCreateDto miembroGremioCuatroCreateDto) {
        return gremioCuatroService.desasociarInstitucion(miembroGremioCuatroCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

}
