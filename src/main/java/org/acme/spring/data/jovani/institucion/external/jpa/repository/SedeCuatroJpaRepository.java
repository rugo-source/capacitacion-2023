package org.acme.spring.data.jovani.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SedeCuatroJpaRepository extends JpaRepository<SedeJpa, Integer> {
    @Query("from SedeJpa where idInstitucion = :idInstitucion")
    List<SedeJpa> findAllByIdInstitucion(@Param("idInstitucion") Integer idInstitucion);
    boolean existsByNombreAndIdInstitucion(String nombre, Integer idInstitucion);
    boolean existsByAcronimoAndIdInstitucion(String acronimo, Integer idInstitucion);
    boolean existsByNombreAndIdInstitucionAndIdNot(String nombre, Integer idInstitucion,Integer id);
    boolean existsByAcronimoAndIdInstitucionAndIdNot(String acronimo, Integer idInstitucion,Integer id);
    boolean existsById(Integer id);
}
