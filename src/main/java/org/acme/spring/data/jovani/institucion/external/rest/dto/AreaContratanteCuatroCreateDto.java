package org.acme.spring.data.jovani.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.jovani.institucion.core.entity.AreaContratanteCuatroCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AreaContratanteCuatroCreateDto {

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este atributo hace referencia al id de una institucion")
    Integer idInstitucion;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 255, message = "RNS002")
    @Schema(description = "este atributo hace refencia al área del área contratante")
    String area;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 255, message = "RNS002")
    @Schema(description = "este atributo hace referencia al subarea del área contratante")
    String subarea;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 10, max = 10, message = "RNN014")
    @Schema(description = "este atributo hace referencia al teléfono del área contratante")
    String telefono;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Size(min = 1, max = 5, message = "RNS002")
    @Schema(description = "este atributo hace referencia a la extension del teléfono del área contratante")
    String extension;

    public AreaContratanteCuatroCreate toEntity() {
        return AreaContratanteCuatroCreate.builder()
                .idInstitucion(this.idInstitucion)
                .area(this.area)
                .subarea(this.subarea)
                .telefono(this.telefono)
                .extension(this.extension)
                .build();
    }
}
