package org.acme.spring.data.jovani.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface InstitucionCuatroJpaRepository extends JpaRepository<InstitucionJpa,Integer> {

    @Query("from InstitucionJpa where identificador = :identificador")
    Optional<InstitucionJpa> findByIdentificador(@Param("identificador") String identificador);

    @Query("from InstitucionJpa where nombre = :nombre")
    Optional<InstitucionJpa> findByNombre(@Param("nombre") String nombre);

    @Query("from InstitucionJpa where identificador = :nombre or nombre = :nombre or acronimo = :nombre")
    Optional<InstitucionJpa> findInstitucion(@Param("nombre") String nombre);

    @Query("from InstitucionJpa where identificador = :identificador and id_institucion != :id_institucion")
    Optional<InstitucionJpa> validateRNN013(@Param("id_institucion") Integer idInstitucion, @Param("identificador") String identificador);

    @Query("from InstitucionJpa where nombre = :nombre and id_institucion != :id_institucion")
    Optional<InstitucionJpa> validateRNN094(@Param("id_institucion") Integer idInstitucion, @Param("nombre") String nombre);

    boolean existsById(Integer idInsitucion);

    boolean existsByIdentificadorAndIdNot(String identificador, Integer id);

    boolean existsByNombreAndIdNot(String nombre, Integer id);
}
