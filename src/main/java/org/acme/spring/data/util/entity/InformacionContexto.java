package org.acme.spring.data.util.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
public class InformacionContexto {
    private String primerNivel;
    private Integer idPrimerNivel;
    private String segundoNivel;
    private Integer idSegundoNivel;
    private String tercerNivel;
    private Integer idTercerNivel;
    private String cuartoNivel;
    private Integer idCuartoNivel;
    private String quintoNivel;
    private Integer idQuintoNivel;
    private String sextoNivel;
    private Integer idSextoNivel;
    private String septimoNivel;
    private Integer idSeptimoNivel;
    private String octavoNivel;
    private Integer idOctavoNivel;
    private String novenoNivel;
    private Integer idNovenoNivel;
    private Boolean readOnly;
}
