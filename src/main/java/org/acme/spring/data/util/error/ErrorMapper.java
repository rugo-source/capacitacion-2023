package org.acme.spring.data.util.error;

import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintViolation;
import javax.ws.rs.core.Response;

@Slf4j
public class ErrorMapper {
    private ErrorMapper() {
    }

    public static <T> org.acme.spring.data.util.error.ErrorDetailDto constraintToError(ConstraintViolation<T> ve) {
        String msg = org.acme.spring.data.util.error.ErrorCodes.ERROR.getDetail();
        try {
            org.acme.spring.data.util.error.ErrorCodes temp = org.acme.spring.data.util.error.ErrorCodes.valueOf(ve.getMessage());
            msg = temp.getDetail();
        } catch (IllegalArgumentException e) {
            log.error("Enum del error no encontrado", e);
        }
        return org.acme.spring.data.util.error.ErrorDetailDto.builder().type(org.acme.spring.data.util.error.ErrorType.FIELD)
                .code(ve.getMessage())
                .message(msg)
                .path(ve.getPropertyPath().toString()).build();
    }

    /**
     * De vuelve una respuesta de error con base en el codigo del error
     *
     * @param error Codigo del error de tipo {@link org.acme.spring.data.util.error.ErrorCodes}
     * @return Un objeto de la clase de tipo {@link org.acme.spring.data.util.error.ErrorResponseDto}
     */
    public static org.acme.spring.data.util.error.ErrorResponseDto errorCodeToErrorResponse(org.acme.spring.data.util.error.ErrorCodes error) {
        org.acme.spring.data.util.error.ErrorResponseDto response = new org.acme.spring.data.util.error.ErrorResponseDto();
        response.addDetail(org.acme.spring.data.util.error.ErrorDetailDto.builder()
                .code(error.name())
                .message(error.getDetail())
                .type(org.acme.spring.data.util.error.ErrorType.REQUEST).build());
        return response;
    }

    /**
     * Devuelve una respuesta de error con base en un codigo de error
     *
     * @param code código que indica la regla violada
     * @return respuesta con el codigo 400 o 404 según corresponda
     */
    public static Response.ResponseBuilder errorCodeToResponseBuilder(org.acme.spring.data.util.error.ErrorCodes code) {
        var respuesta = Response.status(Response.Status.BAD_REQUEST).entity(errorCodeToErrorResponse(code));
        if (org.acme.spring.data.util.error.ErrorCodes.NOT_FOUND.equals(code)) {
            respuesta = Response.status(Response.Status.NOT_FOUND).entity(errorCodeToErrorResponse(code));
        }
        return respuesta;
    }

    /**
     * Devuelve una respuesta de error con el estado http indicado con base en un codigo de error
     *
     * @param code   código que indica la regla violada
     * @param status estado http a utilizar en la respuesta
     * @return respuesta con el codigo status que se indico y el error construido
     */
    public static Response.ResponseBuilder errorCodeToResponseBuilder(org.acme.spring.data.util.error.ErrorCodes code, Response.Status status) {
        return Response.status(status).entity(errorCodeToErrorResponse(code));
    }
}
