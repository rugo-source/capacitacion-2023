package org.acme.spring.data.util.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
public class Catalogo {
    protected Integer id;
    protected String nombre;
    protected String descripcion;
    protected Boolean activo;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CatalogoGenerico{");
        sb.append("id=").append(id);
        sb.append(", nombre='").append(nombre).append('\'');
        sb.append(", descripcion='").append(descripcion).append('\'');
        sb.append(", activo=").append(activo);
        sb.append('}');
        return sb.toString();
    }
}
