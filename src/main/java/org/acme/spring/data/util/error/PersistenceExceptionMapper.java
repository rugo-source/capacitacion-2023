package org.acme.spring.data.util.error;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.List;

@Slf4j
@Provider
public class PersistenceExceptionMapper implements ExceptionMapper<PersistenceException> {
    @Override
    public Response toResponse(PersistenceException persistenceException) {
        log.error("PersistenceExceptionMapper", persistenceException);
        var response = new org.acme.spring.data.util.error.ErrorResponseDto();
        ErrorDetailDto error =  ErrorDetailDto.builder()
                .code(org.acme.spring.data.util.error.ErrorCodes.CAPA_PERSISTENCIA.name())
                .type(ErrorType.REQUEST)
                .message(org.acme.spring.data.util.error.ErrorCodes.CAPA_PERSISTENCIA.getDetail())
                .build();
        response.setDetails(List.of(error));
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(response).build();
    }
}
