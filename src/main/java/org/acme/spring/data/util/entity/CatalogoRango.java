package org.acme.spring.data.util.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class CatalogoRango {

    private Integer id;
    private String nombre;
    private String descripcion;
    private Boolean activo;
    private Integer limiteInferior;
    private Integer limiteSuperior;
}
