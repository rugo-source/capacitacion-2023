package org.acme.spring.data.util;


public class StringConstants {
    public static final String ESPACIO_BLANCO = " ";
    public static final String DIAGONAL = "/";
    public static final String COMA_ESPACIO = ", ";
    public static final String LOCAL_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String LOCAL_DATE_TIME_FORMAT_WITHOUT_SECONDS = "dd/MM/yyyy HH:mm";
    public static final String LOCAL_DATE_TIME_FILES_FORMAT = "ddMMyyyyHHmmss";
    public static final String LOCAL_DATE_FORMAT = "dd/MM/yyyy";
    public static final String LOCAL_TIME_FORMAT = "HH:mm";
    public static final String LOCAL_TIME_COMPLETE_FORMAT = "HH:mm:ss";
    public static final String GUION_MEDIO = "-";
    public static final String GUION_MEDIO_ESPACIOS = " - ";
    public static final String GUION_BAJO = "_";
    public static final String OPERACION_EXITOSA = "Operación exitosa";
    public static final String OPERACION_ERRONEA = "Error en la operación";
    public static final String ZONED_DATE_TIME_FORMAT_LEGADO = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String SUCCESS = "SUCCESS";
    public static final String VALIDATE_TOKEN_SERVICE = "validate-token-service";
    public static final String AUTH = "auth";
    public static final String NA = "N/A";
    public static final String SIN_DIRECCION = "Sin dirección registrada";
    public static final String ERROR_EN_S3 = "Error en S3";
    public static final String REGISTRO_SUSTENTANTES = "Registro_sustentantes";
    public static final String PUNTO = ".";

    private StringConstants() {
        super();
    }
}
