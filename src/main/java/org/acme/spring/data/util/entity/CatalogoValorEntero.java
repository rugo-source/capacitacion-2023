package org.acme.spring.data.util.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class CatalogoValorEntero {
    protected Integer id;
    protected String nombre;
    protected String descripcion;
    protected Boolean activo;
    protected Integer valor;
}
