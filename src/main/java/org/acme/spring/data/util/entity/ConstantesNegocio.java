package org.acme.spring.data.util.entity;

public class ConstantesNegocio {
    private ConstantesNegocio() {
        super();
    }

    public static final Integer KIBIBYTE = 1024;
    public static final Integer FACE_CAPTURES_KIBIBYTE = 800;
    public static final Integer SPACE_VIDEO_KIBIBYTE = 2000;
    public static final Integer EVENT_LOG_KIBIBYTE = 80;
    public static final Integer WEBCAM_VIDEO_KIBIBYTE = 450000;
    public static final Integer DESKTOP_VIDEO_KIBIBYTE = 200000;
    public static final String REGEX_CURP = new StringBuilder().append("^([A-Z&]|[a-z&]{1})([AEIOU]|[aeiou]{1})([A-Z&]|[a-z&]{1})")
            .append("([A-Z&]|[a-z&]{1})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])")
            .append("([HM]|[hm]{1})([AS|as|BC|bc|BS|bs|CC|cc|CS|cs|CH|ch|CL|cl|CM|cm|DF|df|DG|dg|GT|gt|GR|gr|HG|hg|JC|jc|MC|mc|MN|mn|MS|ms|NT|nt|NL|nl|OC|oc|PL|pl|QT|qt|QR|qr|SP|sp|SL|sl|SR|sr|TC|tc|TS|ts|TL|tl|VZ|vz|YN|yn|ZS|zs|NE|ne]{2})")
            .append("([^A|a|E|e|I|i|O|o|U|u]{1})([^A|a|E|e|I|i|O|o|U|u]{1})([^A|a|E|e|I|i|O|o|U|u]{1})([0-9|A-Z]{2})$").toString();
    public static final String REGEX_RFC = new StringBuilder().append("^([A-Z&]|[a-z&]{1})([AEIOU]|[aeiou]{1})([A-Z&]|[a-z&]{1})([A-Z&]|[a-z&]{1})")
            .append("([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])")
            .append("([A-Z]){2}([0-9]){1}")
            .toString();
    public static final String REGEX_CORREO = new StringBuilder().append("^(([^<>()\\[\\]\\\\.,;:\\s@”]+(\\.[^<>()\\[\\]\\\\.,;:\\s@”]+)*)|(“.+”))")
            .append("@((\\[[0–9]{1,3}\\.[0–9]{1,3}\\.[0–9]{1,3}\\.[0–9]{1,3}])|(([a-zA-Z\\-0–9]+\\.)+[a-zA-Z]{2,}))$")
            .toString();

}