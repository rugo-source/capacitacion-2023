package org.acme.spring.data.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.quarkus.runtime.annotations.RegisterForReflection;

import java.util.ArrayList;
import java.util.List;

@RegisterForReflection
public class StateMachine {
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("states")
    private List<org.acme.spring.data.util.State> states = new ArrayList<>();
    @JsonProperty("transitions")
    private List<org.acme.spring.data.util.Transition> transitions = new ArrayList<>();
    @JsonProperty("actions")
    private List<org.acme.spring.data.util.Action> actions = new ArrayList<>();
    @JsonProperty("operations")
    private List<org.acme.spring.data.util.Operation> operations = new ArrayList<>();

    public StateMachine() {
        super();
    }

    public StateMachine(String id, String name,
                        List<org.acme.spring.data.util.State> states,
                        List<org.acme.spring.data.util.Transition> transitions,
                        List<org.acme.spring.data.util.Action> actions,
                        List<org.acme.spring.data.util.Operation> operations) {
        this.id = id;
        this.name = name;
        this.states = states;
        this.transitions = transitions;
        this.actions = actions;
        this.operations = operations;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<org.acme.spring.data.util.State> getStates() {
        return states;
    }

    public void setStates(List<org.acme.spring.data.util.State> states) {
        this.states = states;
    }

    public List<org.acme.spring.data.util.Transition> getTransitions() {
        return transitions;
    }

    public void setTransitions(List<org.acme.spring.data.util.Transition> transitions) {
        this.transitions = transitions;
    }

    public List<org.acme.spring.data.util.Action> getActions() {
        return actions;
    }

    public void setActions(List<org.acme.spring.data.util.Action> actions) {
        this.actions = actions;
    }

    public List<org.acme.spring.data.util.Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<org.acme.spring.data.util.Operation> operations) {
        this.operations = operations;
    }
}
