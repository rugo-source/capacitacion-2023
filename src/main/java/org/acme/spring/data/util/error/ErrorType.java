package org.acme.spring.data.util.error;

public enum ErrorType {
    FIELD,
    REQUEST
}
