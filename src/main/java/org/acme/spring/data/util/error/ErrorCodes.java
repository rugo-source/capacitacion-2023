package org.acme.spring.data.util.error;

public enum ErrorCodes {
    /**
     * Codigos de error de reglas de negocio de sistema
     */
    RNS001("Campos obligatorios"),
    RNS002("Formato de los campos"),
    RNS003("Retorno de elementos de búsqueda"),
    RNS004("Máquina de estados"),
    RNS005("Permisos de usuario"),
    RNS006("Elementos registrados en el sistema"),
    RSN007("Contraseña incorrecta"),
    RSN008("No existe usuario"),
    RSN009("Usuario inactivo"),
    RSN010("Usuario sin perfil"),
    ERROR("Error inesperado"),
    /**
     * Errores NO asociados a una regla de negocio
     */
    INVALID_LINK("Liga no vigente"),
    BAD_REQUEST("Error en la petición"),
    NOT_FOUND("Recurso no encontrado"),
    NEW_LINK("Nueva liga para registrar contraseña"),
    CONTRA_REGISTRADA("Contraseña ya registrada"),
    REDIS_SESSION_NOT_FOUND("Sesión no se encontraba registrada"),
    CAPA_PERSISTENCIA("Error en la capa de persistencia"),
    /**
     * Codigos de error de reglas de negocio de negocio
     */
    RNN001("Sedes asociadas a RO"),
    RNN002("Nombre y acrónimo de un edificio"),
    RNN003("Mínimo de capacidad de una sede"),
    RNN004("Formato del identificador de una sede"),
    RNN005("Unicidad del nombre de una área ceneval"),
    RNN006("Unicidad de carrera y nemotecnia de un programa académico de una institución"),
    RNN007("Unicidad de puesto asociado al personal"),
    RNN008("Capacidad del aula"),
    RNN009("Unicidad del nombre de una aula"),
    RNN010("Unicidad del nombre de un puesto CENEVAL"),
    RNN011("Formato válido para la CURP"),
    RNN012("formato válido para el correo electrónico"),
    RNN013("Identificador la institución"),
    RNN014("Formato válido para el número telefónico"),
    RNN015("Horarios laborales"),
    RNN016("Criterios para eliminar una sede de una institución"),
    RNN017("Formato válido para el correo electrónico alterno"),
    RNN018("Registro de área contratante"),
    RNN019("Formato del identificador de una aplicación"),
    RNN020("Fecha de fin mayor a la fecha de inicio"),
    RNN021("Área a la que pertenece el RO"),
    RNN022("Editar el correo electrónico de un Responsable Operativo"),
    RNN023("El área contratante no se puede eliminar si está asociada a un Responsable Operativo"),
    RNN024("Eliminar no permitido"),
    RNN025("Condición para eliminar un edificion"),
    RNN026("Institucion asociada a un gremio"),
    RNN027("Catálogo de plantillas de reglas de operación"),
    RNN028("Asociación de excepciones de plantilla"),
    RNN029("Unicidad de los número teléfonicos de soporte técnico"),
    RNN030("Unicidad de un correo electrónico asociado a una persona"),
    RNN031("Fecha de inicio de una actividad"),
    RNN032("Estado de un enlace para el registro de un aplicador"),
    RNN033("Formato de la contraseña"),
    RNN034("Fecha de entrega de resultados"),
    RNN035("Formato válido para el RFC"),
    RNN036("Número de aulas asociadas en una sede en una solicitud de aplicación"),
    RNN037("Eliminar aula"),
    RNN038("Período inválido"),
    RNN039("Se debe ingresar datos en al menos un campo para poder realizar la búsqueda y este dato debe tener al menos 3 letras"),
    RNN040("Condición para excluir fechas fuera de periordo disponible"),
    RNN041("Unicidad de un perfil"),
    RNN042("El número de sustentantes estimados en el registro o edición de un examen, será estrictamente mayor a cero"),
    RNN043("Obtención de la plantilla de reglas de operación"),
    RNN045("Combinación de exámenes no permitidas"),
    RNN047("Campos establecidos por reglas de operación en un registro"),
    RNN048("Modalidad de aplicación"),
    RNN049("Cálculo de actividades en el registro de examen"),
    RNN050("Regla en gestión de plantilla para las actividades"),
    RNN051("Dependencias de subfamilia de examen"),
    RNN053("Configuración de entrega de resultados"),
    RNN054("Seleccionar tiempo de entrega de resultados"),
    RNN055("Archivos adjuntos"),
    RNN060("Fecha de fin de una actividad"),
    RNN060_1("Fecha de fin de una actividad"),
    RNN061("Configuración de la fecha y hora de una actividad"),
    RNN062("Combinación de examénes o módulos"),
    RNN063("Orden en la combinación de examénes o módulos"),
    RNN065("Sustentantes esperados"),
    RNN069("Búsqueda de responsables"),
    RNN070("Catálogo del tipo de responsables"),
    RNN071("Registro de sustentantes esperados por módulo"),
    RNN072("Código postal no registrado"),
    RNN073("Cantidad de sustentantes esperados"),
    RNN074("Condición para la edición de fechas"),
    RNN075("Combinación examen/módulo y fecha/hora permitidos"),
    RNN076("Fecha y hora de un examen/módulo para el Responsable operativo"),
    RNN077("Publicación de resultados"),
    RNN078("Dependencia entre sedes"),
    RNN079("Cantidad de sedes asociadas para la dependencia de sedes"),
    RNN080("Configuraración de centros de Cierre/Acopio"),
    RNN083("Longitud de Código Postal"),
    RNN084("Forma de entrega de resultados"),
    RNN085("Aplicador CENEVAL en el registro de examen"),
    RNN086("Cantidad de perfiles de un responsable"),
    RNN088("Sustentantes con debilidad visual"),
    RNN089("Unicidad de una persona"),
    RNN090("Editar informacion del usuario"),
    RNN091("Perfil de un usuario"),
    RNN092("Asociar sede a un centro"),
    RNN093("Modalidad de entrega de resultados"),
    RNN094("Nombre de la institución"),
    RNN095("Traslape de fechas de días inhábiles registrados"),
    RNN096("Unicidad del nombre y acrónimo de una sede dentro de una institución"),
    RNN097("Unicidad del nombre y acrrónimo de un gremio"),
    RNN098("Traslape de fechas en asignación del Ejecutivo CENEVAL"),
    RNN099("Fecha de inicio de una solicitud de aplicación"),
    RNN100("Código de colores de los mensajes de la bitácora"),
    RNN101("Hora de fin de un elemento"),
    RNN102("Estado de un puesto del personal del CENEVAL"),
    RNN103("Formato para desplegar una institución"),
    RNN104("Mostrar ícono de registrar nodos"),
    RNN105("Cancelar una solicitud de aplicación"),
    RNN106("Traslape de fechas de módulos y/o examenes"),
    RNN107("Edición de fechas de un módulo"),
    RNN108("Unicidad en las plantillas de reglas de operación"),
    RNN109("Unicidad de las plantilla de logística"),
    RNN110("Condiciones de una plantilla de reglas de operación predeterminada"),
    RNN112("Unicidad del nombre de una plantilla de logística"),
    RNN113("Unicidad del nombre de una actividad dentro de una plantilla"),
    RNN114("Busqueda de solicitudes de aplicación"),
    RNN115("Unicidad de nombres de un horario de reforzamiento precencial"),
    RNN116("Unicidad de nombre de un día inhábil"),
    RNN118("Números de cuadernillos"),
    RNN117("El nombre de la familia y/o el nombre corto ya se encuentran registrados previamente en el sistema"),
    RNN119("Número de cuadernillos excedentes"),
    RNN120("Número de usb's, cuadernillos y nodos"),
    RNN122("Ejecutivo Ceneval vigente"),
    RNN123("Unicidad de un examen o módulo"),
    RNN124("Calcula el horario fin de un examen"),
    RNN125("Número total de cuadernillos"),
    RNN126("Sustentantes soportados en una sede"),
    RNN127("Cálculo para la cantidad de solicitudes pendientes de una institución"),
    RNN128("Número de cantidad y capacidad de equipamiento"),
    RNN133("Número de participantes esperados"),
    RNN140("Unicidad de un criterio de modalidad de entrega de resultados"),
    RNN141("Seleccion de un elemento"),
    RNN144("Periodo de horario inválido"),
    RNN145("Vigencia de token generado por el sistema"),
    RNN147("La institución que seleccionó ya se encuentra asociada a esta subfamilia"),
    RNN148("Asociación del responsable operativo a una institución"),
    RNN149("Asociación del responsable de institución a un área contratante"),
    RNN150("Unicidad del curp de una persona"),
    RNN152("Para el registro y edición de un perfil para el responsable del área contratante, éste no debe traslaparse con la fecha de inicio y/o fin de un perfil del mismo tipo previamente registrado, en caso de haberlo."),
    RNN153("Unicidad de sesión. El nombre de la sesión debe ser único en el examen/módulo al que pertenece."),
    RNN158("La cantidad ingresada debe ser mayor a 0. Favor de verificar la información del campo."),
    RNN161("Error en los horarios de la sesión."),
    RNN167("Obtención de subfamilias por privacidad"),
    RNN171("Fecha de fin de una actividad. La fecha de fin de una actividad a registrar, editar, debe de ser estrictamenten mayor o igual a la fecha actual."),
    RNN173("Para realizar el registro y/o edición de la plantilla de reglas de operación, el nombre de la plantilla debe ser único."),
    RNN175("Para que pueda realizarse el registro y/o edición de una subfamilia de instrumentos de evaluación, el nombre corto debe ser único."),
    RNN179("Sesión única del examen ó módulo."),
    RNN181("Para que se acepte la configuración realizada en la regla de operaci ́on, el usuario debe selec- cionar al menos una opci ́on por cada subfamilia y/o modalidad mostrada."),
    RNN182("Una sesión no puede tener dependencia y descanso asociado a la misma sesión"),
    RNN183("No sepuede eliminar una sesión que tiene asociado dependencia y/o descanso"),
    RNN187("Una sesión no debe tener dependencia y/o tiempo entre sesión de ella misma"),
    RNN188("Toda vez se ha registrado al menos una combinación, para una subfamilia, si se quiere cambiar el número de exámenes que se pueden combinar en dicha subfamilia, deben eliminarse las combinaciones existentes"),
    RNN190("Unicidad de personal asociado a un área"),
    RNN192("Selección de personal CENEVAL"),
    RNN196("Valores de inicio y fin no validos"),
    RNN198("La anticipación no puede ser negativa"),
    RNN203("No se puede borrar un horario en estado de predeterminado"),
    RNN204("No se puede borrar un examen que se encuentre combinado"),
    RNN205("Obtención del cátalogo de subfamilias para la combinación de subfamilias"),
    RNN206("Si la subfamilia ya se habia registrado, no puede volverse a registrar"),
    RNN207("Traslape de fechas de puestos del personal CENEVAL"),
    RNN208("La sub-área del ceneval (área hijo) podrá ser activada si su área padre se encuentra en estado Activada."),
    RNN209("Unicidad de un RFC"),
    RNN210("Coincidencia de correo electrónico y su confirmación"),
    RNN211("Registros previos de un aplicador"),
    RNN219("Cambio de estado a una plantilla de logística"),
    RNN221("Asociación de un responsable de sede a una sede."),
    RNN222("Acceso a terminar seccion dd dependencia entre sedes"),
    RNN224("RNN224"),
    RNN225("Terminar seccion"),
    RNN227("Terminar seccion sustentantes"),
    RNN229("Terminar seccion responsable sede"),
    RNN231("Traslape de fechas de puestos del personal CENEVAL con otro personal CENEVAL"),
    RNN236("RNN236"),
    RNN241("Acceso a la configuración de criterios de entrega de resultados. La configuración de criterios de entrega de resultados se podrá llevar acabo si existe al menos un examen registrado en la solicitud de aplicación."),
    RNN246("Condicion para derogar una plantilla predeterminada"),
    RNN249("Secciones a considerar para envio de solicitud a correccion"),
    RNN250("Condición para el envío de solicitud de corrección"),
    RNN251("Correccion de la informacion de una seccion"),
    RNN258("Subfamilia sin modalidad registrada");

    private final String detail;

    ErrorCodes(String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }
}
