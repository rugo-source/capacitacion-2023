package org.acme.spring.data.ricardo.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.persistence.Column;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EdificioSeisDto {

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al nombre del edificio")
    String nombre;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al acronimo del edificio")
    String acronimo;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia a las referencias del edificio")
    String referencia;

    public static EdificioSeisDto fromEntity(Edificio edificio){
        return EdificioSeisDto.builder()
                .nombre(edificio.getNombre())
                .acronimo(edificio.getAcronimo())
                .referencia(edificio.getReferencia())
                .build();
    }

}
