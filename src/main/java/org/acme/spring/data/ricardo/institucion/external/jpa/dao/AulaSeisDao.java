package org.acme.spring.data.ricardo.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AulaJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.AulaSeisRepository;
import org.acme.spring.data.ricardo.institucion.external.jpa.repository.AulaSeisJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class AulaSeisDao implements AulaSeisRepository {

    @Inject
    AulaSeisJpaRepository aulaSeisJpaRepository;

    @Override
    public void save(Aula aulaPersist) {
    aulaSeisJpaRepository.saveAndFlush(AulaJpa.fromEntity(aulaPersist));
    }

    @Override
    public void update(Aula aulaChange) {
        aulaSeisJpaRepository.saveAndFlush(AulaJpa.fromEntity(aulaChange));
    }

    @Override
    public void delete(Integer idAula) {
        aulaSeisJpaRepository.deleteById(idAula);
    }

    @Override
    public Optional<Aula> findByNombre(String nombre) {
        return aulaSeisJpaRepository.findByNombre(nombre).map(AulaJpa::toEntity);
    }

    @Override
    public List<Aula> findAllByIdEdificio(Integer idEdificio) {
        return aulaSeisJpaRepository.findAllByIdEdificio(idEdificio).stream()
                .map(AulaJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Boolean existsNombreAndIdNot(String nombre, Integer idAula) {
        return aulaSeisJpaRepository.existsByNombreAndIdNot(nombre, idAula);
    }

    @Override
    public Optional<Aula> findById(Integer idAula) {
        return aulaSeisJpaRepository.findById(idAula).map(AulaJpa::toEntity);
    }


}
