package org.acme.spring.data.ricardo.institucion.core.bussines.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.ricardo.institucion.core.bussines.input.AulaSeisService;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.AulaSeisRepository;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.EdificioSeisRepository;
import org.acme.spring.data.ricardo.institucion.core.entity.AulaSeisCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class AulaSeisBs implements AulaSeisService {

    @Inject
    AulaSeisRepository aulaSeisRepository;

    @Inject
    EdificioSeisRepository edificioSeisRepository;

    @Override
    public Either<ErrorCodes, Boolean> create(Integer idEdificio, AulaSeisCreate aulaSeisCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.RNN009);
        var aulaBuscarNombre = aulaSeisRepository.findByNombre(aulaSeisCreate.getNombre());
        if(!aulaBuscarNombre.isPresent()) {
            var aulaPersist = Aula.builder()
                    .idEdificio(idEdificio)
                    .nombre(aulaSeisCreate.getNombre())
                    .idTipoEquipamiento(aulaSeisCreate.getIdTipoEquipamiento())
                    .cantidadEquipamiento(aulaSeisCreate.getCantidadEquipamiento())
                    .capacidadEquipamiento(aulaSeisCreate.getCapacidadEquipamiento())
                    .observaciones(aulaSeisCreate.getObservaciones())
                    .build();
            aulaSeisRepository.save(aulaPersist);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer idAula, AulaSeisCreate aulaSeisCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var aulaBuscar = aulaSeisRepository.findById(idAula);
        if(aulaBuscar.isPresent()) {
            if(validateRNN009(idAula, aulaSeisCreate.getNombre())){
                resultado = Either.left(ErrorCodes.RNN009);
            } else {
                var aulaChange = aulaBuscar.get();
                aulaChange.setNombre(aulaSeisCreate.getNombre());
                aulaChange.setIdTipoEquipamiento(aulaSeisCreate.getIdTipoEquipamiento());
                aulaChange.setCantidadEquipamiento(aulaSeisCreate.getCantidadEquipamiento());
                aulaChange.setCapacidadEquipamiento(aulaSeisCreate.getCapacidadEquipamiento());
                aulaChange.setObservaciones(aulaSeisCreate.getObservaciones());
                aulaSeisRepository.update(aulaChange);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idAula) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var aulaBuscar = aulaSeisRepository.findById(idAula);
        if (aulaBuscar.isPresent()) {
            aulaSeisRepository.delete(idAula);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, List<Aula>> get(Integer idEdificio) {
        Either<ErrorCodes, List<Aula>> resultado=Either.left(ErrorCodes.NOT_FOUND);
        var instSearch=edificioSeisRepository.findById(idEdificio);
        if (instSearch.isPresent()){
            var listAulas=aulaSeisRepository.findAllByIdEdificio(idEdificio);
            if (listAulas.isEmpty()){
                resultado=Either.left(ErrorCodes.NOT_FOUND);
            }else {
                resultado=Either.right(listAulas);

            }
        }
        return resultado;
    }



    public Boolean validateRNN009(Integer idAula, String nombre) {
        return aulaSeisRepository.existsNombreAndIdNot(nombre, idAula);
    }

}
