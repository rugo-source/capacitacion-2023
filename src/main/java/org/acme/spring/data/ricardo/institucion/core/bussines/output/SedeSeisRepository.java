package org.acme.spring.data.ricardo.institucion.core.bussines.output;

import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;

import java.util.List;
import java.util.Optional;

public interface SedeSeisRepository {

    void save(Sede sedePersist);
    List<Sede> findAllById(Integer idInstitucion);
    Boolean existsById (Integer idSede);
    void delete(Integer idSede);
    Optional<Sede> findById(Integer idSede);
    void update(Sede sedeChange);
}
