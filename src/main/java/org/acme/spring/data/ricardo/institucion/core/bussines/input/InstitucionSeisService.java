package org.acme.spring.data.ricardo.institucion.core.bussines.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.ricardo.institucion.core.entity.InstitucionSeisGet;
import org.acme.spring.data.ricardo.institucion.core.entity.InstitucionSeisCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface InstitucionSeisService {

    Either<ErrorCodes, Boolean> create(InstitucionSeisCreate institucionSeisCreate);
    List<Institucion> get();
    Either<ErrorCodes, List<Institucion>> getBy(InstitucionSeisGet institucionSeisGet);
    Either<ErrorCodes, Boolean> update(Integer idInstitucion, InstitucionSeisCreate institucionSeisCreate);


}
