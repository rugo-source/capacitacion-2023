package org.acme.spring.data.ricardo.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ricardo.institucion.core.entity.InstitucionSeisCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;


@Builder
@Setter
@Getter
@AllArgsConstructor
@Schema(name="InstitucionSeisCreate", description = "")
@NoArgsConstructor
public class InstitucionSeisCreateDto {

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id del tipo de institucion")
    Integer idTipo;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id clasificación de la institucion")
    Integer idClasificacion;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id categoria de la institucion")
    Integer idCategoria;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id subsistema universidad")
    Integer idSubsistemaUniversidad;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id subsistema universidad")
    Integer idSubsistemaBachillerato;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Size(min = 0, max = 4, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al identificador de la institucion")
    String identificador;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 255, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al nombre de la institucion")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 20, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al acronimo de la institucion")
    String acronimo;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Size(min = 0, max = 20, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al area contratante de la institucion")
    String cct;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al numero de cedes registradas")
    Integer numeroSedesRegistradas;

    public InstitucionSeisCreate toEntity() {
        return InstitucionSeisCreate.builder()
                .idTipo(this.idTipo)
                .idClasificacion(this.idClasificacion)
                .idCategoria(this.idCategoria)
                .idSubsistemaUniversidad(this.idSubsistemaUniversidad)
                .idSubsistemaBachillerato(this.idSubsistemaBachillerato)
                .identificador(this.identificador)
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .cct(this.cct)
                .numeroSedesRegistradas(this.numeroSedesRegistradas)
                .build();
    }
}
