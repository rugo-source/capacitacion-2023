package org.acme.spring.data.ricardo.examples.external.jpa.repository;


import org.acme.spring.data.ricardo.examples.external.jpa.model.PersonaSeisJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaJpaRepository extends JpaRepository<PersonaSeisJpa,Integer> {
}
