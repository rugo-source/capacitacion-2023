package org.acme.spring.data.ricardo.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SedeSeisDto {

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al nombre de la sede")
    String nombre;
    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al acronimo")
    String acronimo;
    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia a la capacidad de la sede")
    Integer capacidad;
    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia si la sede pertenece a la institucion")
    Boolean propia;
    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al identificador de la institucion")
    String identificador;

    public static SedeSeisDto fromEntity(Sede sede){
        return SedeSeisDto.builder()
                .nombre(sede.getNombre())
                .acronimo(sede.getAcronimo())
                .capacidad(sede.getCapacidad())
                .propia(sede.getPropia())
                .identificador(sede.getIdentificador())
                .build();
    }


}
