package org.acme.spring.data.ricardo.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class InstitucionSeisGet {

    private String identificador;
    private String nombre;
    private String acronimo;
}
