package org.acme.spring.data.ricardo.institucion.core.bussines.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.ricardo.institucion.core.bussines.input.AreaContranteSeisService;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.AreaContratanteSeisRepository;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.InstitucionSeisRepository;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.ResponsableOperativoAreaSeisRepository;

import org.acme.spring.data.ricardo.institucion.core.entity.AreaContratanteSeisCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class AreaContratanteSeisBs implements AreaContranteSeisService {
    @Inject
    AreaContratanteSeisRepository areaContratanteSeisRepository;
    @Inject
    InstitucionSeisRepository institucionSeisRepository;
    @Inject
    ResponsableOperativoAreaSeisRepository responsableOperativoAreaSeisRepository;



    @Override
    public Either<ErrorCodes, Boolean> create(AreaContratanteSeisCreate areaContratanteSeisCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if(existsInstitucion(areaContratanteSeisCreate.getIdInstitucion())){
            var areaContratantePersist = AreaContratante.builder()
                    .idInstitucion(areaContratanteSeisCreate.getIdInstitucion())
                    .area(areaContratanteSeisCreate.getArea())
                    .subarea(areaContratanteSeisCreate.getSubarea())
                    .telefono(areaContratanteSeisCreate.getTelefono())
                    .extension(areaContratanteSeisCreate.getExtension())
                    .build();
            areaContratanteSeisRepository.save(areaContratantePersist);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, List<AreaContratante>> get(Integer idInstitucion) {
        Either<ErrorCodes, List<AreaContratante>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if(existsInstitucion(idInstitucion)){
            var listAreaContratatante = areaContratanteSeisRepository.findAreasContratantes(idInstitucion);
            resultado = Either.right(listAreaContratatante);
        }
        return resultado;
    }
    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idAreaContratante) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if(validateRNN023(idAreaContratante)) {
            resultado = Either.left(ErrorCodes.RNN023);
        } else{
            if (existsAreaContratante(idAreaContratante)) {
                areaContratanteSeisRepository.delete(idAreaContratante);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer idAreaContratante, AreaContratanteSeisCreate areaContratanteSeisCreate) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if(existsInstitucion(areaContratanteSeisCreate.getIdInstitucion())) {
            var areaContratanteSearch = areaContratanteSeisRepository.findById(idAreaContratante);
            if (areaContratanteSearch.isPresent()) {
                var areContratanteChange = areaContratanteSearch.get();
                areContratanteChange.setIdInstitucion(areaContratanteSeisCreate.getIdInstitucion());
                areContratanteChange.setArea(areaContratanteSeisCreate.getArea());
                areContratanteChange.setSubarea(areaContratanteSeisCreate.getSubarea());
                areContratanteChange.setTelefono(areaContratanteSeisCreate.getTelefono());
                areContratanteChange.setExtension(areaContratanteSeisCreate.getExtension());
                areaContratanteSeisRepository.update(areContratanteChange);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    public Boolean validateRNN023(Integer idAreaContratante) {
        return responsableOperativoAreaSeisRepository.existsByIdAreaContratante(idAreaContratante);
    }

    public Boolean existsAreaContratante(Integer idAreaContratante) {
        return areaContratanteSeisRepository.existsById(idAreaContratante);
    }

    public Boolean existsInstitucion(Integer idInstitucion) {
        return institucionSeisRepository.existsById(idInstitucion);
    }
}
