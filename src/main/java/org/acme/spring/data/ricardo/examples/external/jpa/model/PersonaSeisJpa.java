package org.acme.spring.data.ricardo.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.ricardo.examples.core.entity.Persona;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "persona_seis")
public class PersonaSeisJpa {
    @Id
    @Column(name = "id_persona")
    @SequenceGenerator(name = "persona_seis_id_persona_seq", sequenceName = "persona_seis_id_persona_seq", allocationSize = 1)
    @GeneratedValue(generator = "persona_seis_id_persona_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "edad")
    private Integer edad;
    @Column(name ="fk_id_estado")
    private Integer idEstado;


    @ManyToOne
    @JoinColumn(name = "fk_id_estado", referencedColumnName = "id_estado", insertable = false, updatable = false)
    private EstadoPersonaSeisJpa estadoPersonaSeisJpa;
    public Persona toEntity() {
        return Persona.builder().id(this.id).nombre(this.nombre).edad(this.edad).idEstado(this.idEstado).build();
    }

    public static PersonaSeisJpa fromEntity(Persona persona) {
        return PersonaSeisJpa.builder().id(persona.getId()).nombre(persona.getNombre()).edad(persona.getEdad()).idEstado(persona.getIdEstado()).build();
    }
}
