package org.acme.spring.data.ricardo.institucion.core.bussines.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.ricardo.institucion.core.entity.AulaSeisCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface AulaSeisService {
    Either<ErrorCodes, Boolean> create(Integer idEdificio, AulaSeisCreate aulaSeisCreate);
    Either<ErrorCodes, Boolean> update(Integer idAula, AulaSeisCreate aulaSeisCreate);
    Either<ErrorCodes, Boolean> delete (Integer idAula);
    Either<ErrorCodes, List<Aula>>  get (Integer idEdificio);



}
