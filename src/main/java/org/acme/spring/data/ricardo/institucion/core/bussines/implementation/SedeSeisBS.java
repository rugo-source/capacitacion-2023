package org.acme.spring.data.ricardo.institucion.core.bussines.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.ricardo.institucion.core.bussines.input.SedeSeisService;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.InstitucionSeisRepository;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.SedeSeisRepository;
import org.acme.spring.data.ricardo.institucion.core.entity.InstitucionSeisCreate;
import org.acme.spring.data.ricardo.institucion.core.entity.SedeSeisCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class SedeSeisBS implements SedeSeisService {

    @Inject
    SedeSeisRepository sedeSeisRepository;
    @Inject
    InstitucionSeisRepository institucionSeisRepository;

    @Override
    public Either<ErrorCodes, Boolean> create(Integer idInstitucion, SedeSeisCreate sedeSeisCreate) {
        Either<ErrorCodes, Boolean> resultado;
            resultado = Either.left(ErrorCodes.RNN013);
        var sedePersist = Sede.builder()
                .idInstitucion(idInstitucion)
                .nombre(sedeSeisCreate.getNombre())
                .acronimo(sedeSeisCreate.getAcronimo())
                .capacidad(sedeSeisCreate.getCapacidad())
                .identificador(sedeSeisCreate.getIdentificador())
                .propia(sedeSeisCreate.getPropia())
                .build();
            resultado = Either.right(true);
        sedeSeisRepository.save(sedePersist);

        return resultado;
    }

    @Override
    public Either<ErrorCodes, List<Sede>> getSedesById(Integer idInstitucion) {
        Either<ErrorCodes, List<Sede>> resultado=Either.left(ErrorCodes.NOT_FOUND);
        var instSearch=institucionSeisRepository.existsById(idInstitucion);
        if (instSearch){
            var listSedes=sedeSeisRepository.findAllById(idInstitucion);
            if (listSedes.isEmpty()){
                resultado=Either.left(ErrorCodes.NOT_FOUND);
            }else {
                resultado=Either.right(listSedes);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idSede) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
            resultado = Either.left(ErrorCodes.RNN023);
            if (existsSede(idSede)) {
                sedeSeisRepository.delete(idSede);
                resultado = Either.right(true);
            }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer idSede, SedeSeisCreate sedeSeisCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var sedeBuscar = sedeSeisRepository.findById(idSede);
        if(sedeBuscar.isPresent()) {
                var sedeChange = sedeBuscar.get();
                sedeChange.setNombre(sedeSeisCreate.getNombre());
                sedeChange.setAcronimo(sedeSeisCreate.getAcronimo());
                sedeChange.setCapacidad(sedeSeisCreate.getCapacidad());
                sedeChange.setIdentificador(sedeSeisCreate.getIdentificador());
                sedeChange.setPropia(sedeSeisCreate.getPropia());
                sedeSeisRepository.update(sedeChange);
                resultado = Either.right(true);
        }
        return resultado;
    }

    public Boolean validateRNN096 (Integer idInstitucion, String acronimo, String nombre) {
        return null;
    }

    public Boolean existsSede(Integer idSede) {
        return sedeSeisRepository.existsById(idSede);
    }

}
