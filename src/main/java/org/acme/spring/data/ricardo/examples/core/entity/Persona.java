package org.acme.spring.data.ricardo.examples.core.entity;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class Persona {


    private Integer id;
    private Integer edad;
    private Integer idEstado;

    private String nombre;

    private Boolean registrar;
    private  Boolean editar;
    private Boolean eliminar;
    private  Boolean consultar;

    private EstadoPersonaSeis EstadoPersonaSeis;
}
