package org.acme.spring.data.ricardo.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AulaSeisDto {

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al nombre del aula")
    String nombre;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al Id del tipo de equipamiento del aula")
    Integer idTipoEquipamiento;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia a la cantidad de equipamiento del aula")
    Integer cantidadEquipamiento;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia a la capacidad de equipamiento del aula")
    Integer capacidadEquipamiento;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia a las observaciones del aula")
    String observaciones;


    public static AulaSeisDto fromEntity(Aula aula){
        return AulaSeisDto.builder()
                .nombre(aula.getNombre())
                .idTipoEquipamiento(aula.getIdTipoEquipamiento())
                .cantidadEquipamiento(aula.getCantidadEquipamiento())
                .capacidadEquipamiento(aula.getCapacidadEquipamiento())
                .observaciones(aula.getObservaciones())
                .build();
    }
}
