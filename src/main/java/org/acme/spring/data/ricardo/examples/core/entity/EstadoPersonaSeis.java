package org.acme.spring.data.ricardo.examples.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EstadoPersonaSeis {
    private Integer id;
    private String nombre;
}
