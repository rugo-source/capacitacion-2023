package org.acme.spring.data.ricardo.examples.core.business.output;

import org.acme.spring.data.ricardo.examples.core.entity.CuentaSeis;
import org.acme.spring.data.ricardo.examples.external.jpa.model.CuentaSeisJpa;

public interface CuentaSeisRepository {
 CuentaSeisJpa save(CuentaSeis cuentaPersits);
}
