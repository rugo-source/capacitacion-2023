package org.acme.spring.data.ricardo.examples.core.entity;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class CuentaSeis {
    private Integer id;
    private Integer idPersona;

    private String rol;

    private LocalDate fh_inicio;
    private LocalDate fh_fin;

}
