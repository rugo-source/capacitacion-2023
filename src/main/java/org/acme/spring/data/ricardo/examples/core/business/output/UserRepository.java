package org.acme.spring.data.ricardo.examples.core.business.output;


import org.acme.spring.data.cristian.examples.core.entity.Usuario;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<Usuario> findAll();
    List<Usuario> findAllNative();
    List<Usuario> findAllOrm();

   Optional<Usuario> findBy(Integer idUsuario);

    void save(Usuario usuarioPersits);

    void update(Usuario usuarioChange);

    void deleteBy(Integer id);
}
