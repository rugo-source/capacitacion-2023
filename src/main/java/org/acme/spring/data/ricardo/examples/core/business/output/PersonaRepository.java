package org.acme.spring.data.ricardo.examples.core.business.output;

import org.acme.spring.data.ricardo.examples.core.entity.Persona;
import org.acme.spring.data.ricardo.examples.external.jpa.model.PersonaSeisJpa;

import java.util.List;
import java.util.Optional;

public interface PersonaRepository {
    Optional<Persona> findBy(Integer idPersona);

    PersonaSeisJpa save(Persona personaPersits);


    PersonaSeisJpa update(Persona personaChange);

    void deleteBy(Integer id);

    List <Persona> findAll();


}
