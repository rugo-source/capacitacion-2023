package org.acme.spring.data.ricardo.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AreaContratanteSeisCreate {
    private Integer idInstitucion;
    private String area;
    private String subarea;
    private String telefono;
    private String extension;
}
