package org.acme.spring.data.ricardo.examples.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.ricardo.examples.core.business.input.CuentaSeisService;
import org.acme.spring.data.ricardo.examples.core.business.output.CuentaSeisRepository;
import org.acme.spring.data.ricardo.examples.core.entity.CuentaSeis;
import org.acme.spring.data.ricardo.examples.core.entity.CuentaSeisCreate;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CuentaSeisBs implements CuentaSeisService {
@Inject
CuentaSeisRepository cuentaSeisRepository;

    @Override
    public Either<Integer, Boolean> create(CuentaSeisCreate cuentaSeisCreate) {
        Either<Integer, Boolean> resultado = Either.right(true);
        var cuentaPersits = CuentaSeis.builder()
                .rol(cuentaSeisCreate.getRol())
                .fh_inicio(cuentaSeisCreate.getInicio())
                .fh_fin(cuentaSeisCreate.getFin())
                .idPersona(cuentaSeisCreate.getIdPersona())
                .build();
            cuentaSeisRepository.save(cuentaPersits);
        return resultado;
    }
}
