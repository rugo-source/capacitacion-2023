package org.acme.spring.data.ricardo.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ricardo.examples.core.entity.CuentaSeisCreate;
import org.acme.spring.data.util.StringConstants;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "CuentaSeisCreate", description = "Esta en la entidad para guardar una cuenta")
public class CuentaSeisDto {
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3, max = 100, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al rol de la cuenta")
    String rol;
    @JsonProperty
    @NotNull(message = "RNS001")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringConstants.LOCAL_DATE_FORMAT)
    @Schema(description = "este es el atributo que hace referencia a la fecha de la creacion", format = "string", implementation = String.class)
    private LocalDate inicio;
    @JsonProperty
    @NotNull(message = "RNS001")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringConstants.LOCAL_DATE_FORMAT)
    @Schema(description = "este es el atributo que hace referencia a la fecha final", format = "string", implementation = String.class)
    private LocalDate  fin;
    @JsonProperty
    @Schema(description = "este es el atributo que referencia al id de persona")
    private Integer idPersona;

    public CuentaSeisCreate toEntity(){
        return CuentaSeisCreate.builder()
                .rol(this.rol)
                .fin(this.fin)
                .inicio(this.inicio)
                .idPersona(this.idPersona)
                .build();
    }

}
