package org.acme.spring.data.ricardo.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.ricardo.examples.core.entity.CuentaSeis;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "cuenta_dos")
public class CuentaSeisJpa {
    @Id
    @Column(name = "id_cuenta") //cambiar name and generator
    @SequenceGenerator(name = "cuenta_dos_id_cuenta_seq", sequenceName = "cuenta_dos_id_cuenta_seq", allocationSize = 1)
    @GeneratedValue(generator = "cuenta_dos_id_cuenta_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "tx_rol")
    private String rol;
    @Column(name ="fk_id_persona")
    private Integer idPersona;
    @Column(name = "fh_inicio")
    private LocalDate inicio;
    @Column(name = "fh_fin")
    private LocalDate fin;

    @ManyToOne
    @JoinColumn(name = "fk_id_persona", referencedColumnName = "id_persona", insertable = false, updatable = false)
    private PersonaSeisJpa personaDosJpa;

    public CuentaSeis toEntity(){
        return CuentaSeis.builder().id(this.id).rol(this.rol).fh_inicio(this.inicio).fh_fin(this.fin).idPersona(this.idPersona).build();
    }
    public static CuentaSeisJpa fromEntity(CuentaSeis cuentaSeis){
        return CuentaSeisJpa.builder().id(cuentaSeis.getId()).rol(cuentaSeis.getRol())
                .inicio(cuentaSeis.getFh_inicio()).fin(cuentaSeis.getFh_fin()).idPersona(cuentaSeis.getIdPersona()).build();
    }


}
