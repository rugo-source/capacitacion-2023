package org.acme.spring.data.ricardo.examples.external.jpa.dao;

import org.acme.spring.data.ricardo.examples.core.business.output.PersonaRepository;
import org.acme.spring.data.ricardo.examples.core.entity.Persona;
import org.acme.spring.data.ricardo.examples.external.jpa.model.PersonaSeisJpa;
import org.acme.spring.data.ricardo.examples.external.jpa.repository.PersonaJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class PersonaDao implements PersonaRepository {

    @Inject
    PersonaJpaRepository personaJpaRepository;

    @Override
    public Optional<Persona> findBy(Integer idPersona) {
        return personaJpaRepository.findById(idPersona).map(personaDosJpa ->{
            var personaEntidad =personaDosJpa.toEntity();
            personaEntidad.setEstadoPersonaSeis(personaDosJpa.getEstadoPersonaSeisJpa().toEntity());
            return personaEntidad;
        } );
    }

    @Override
    public PersonaSeisJpa save(Persona personaPersits) {

        return personaJpaRepository.saveAndFlush(PersonaSeisJpa.fromEntity(personaPersits));
    }


    @Override
    public PersonaSeisJpa update(Persona personaChange) {
        return personaJpaRepository.saveAndFlush(PersonaSeisJpa.fromEntity(personaChange));
    }

    @Override
    public void deleteBy(Integer id) {
        personaJpaRepository.deleteById(id);
    }

    @Override
    public List<Persona> findAll() {
        return personaJpaRepository.findAll().stream().map(personaSeisJpa -> {
            var personaEntidad = personaSeisJpa.toEntity();
            personaEntidad.setEstadoPersonaSeis(personaSeisJpa.getEstadoPersonaSeisJpa().toEntity());
            return personaEntidad;
        }).collect(Collectors.toList());
    }

}
