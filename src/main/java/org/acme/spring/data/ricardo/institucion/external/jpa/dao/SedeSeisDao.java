package org.acme.spring.data.ricardo.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;

import org.acme.spring.data.ricardo.institucion.core.bussines.output.SedeSeisRepository;
import org.acme.spring.data.ricardo.institucion.external.jpa.repository.SedeSeisJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class SedeSeisDao implements SedeSeisRepository {

    @Inject
    SedeSeisJpaRepository sedeSeisJpaRepository;

    @Override
    public void save(Sede sedePersist) {
        sedeSeisJpaRepository.saveAndFlush(SedeJpa.fromEntity(sedePersist));
    }

    @Override
    public List<Sede> findAllById(Integer idInstitucion) {
        return sedeSeisJpaRepository.findAllByIdInstitucion(idInstitucion).stream()
                .map(SedeJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Boolean existsById(Integer idSede) {
        return sedeSeisJpaRepository.existsById(idSede);
    }

    @Override
    public void delete(Integer idSede) {
        sedeSeisJpaRepository.deleteById(idSede);
    }

    @Override
    public Optional<Sede> findById(Integer idSede) {
        return sedeSeisJpaRepository.findById(idSede).map(SedeJpa::toEntity);
    }

    @Override
    public void update(Sede sedeChange) {
        sedeSeisJpaRepository.saveAndFlush(SedeJpa.fromEntity(sedeChange));
    }


}
