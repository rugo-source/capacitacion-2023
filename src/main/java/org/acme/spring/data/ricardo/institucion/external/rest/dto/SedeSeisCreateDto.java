package org.acme.spring.data.ricardo.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ricardo.institucion.core.entity.SedeSeisCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SedeSeisCreate", description = "Esta en la entidad crea la sede")
public class SedeSeisCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 100, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al nombre de la sede")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 20, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al acronimo")
    String acronimo;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia a la capacidad de la sede")
    Integer capacidad;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia si la sede pertenece a la institucion")
    Boolean propia;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 13, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al identificador de la institucion")
    String identificador;

    public SedeSeisCreate toEntity (){
        return SedeSeisCreate.builder()
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .capacidad(this.capacidad)
                .propia(this.propia)
                .identificador(this.identificador)
                .build();
    }

}
