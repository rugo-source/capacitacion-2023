package org.acme.spring.data.ricardo.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class InstitucionSeisCreate {
    private Integer idTipo;
    private Integer idClasificacion;
    private Integer idCategoria;
    private Integer idSubsistemaUniversidad;
    private Integer idSubsistemaBachillerato;
    private String identificador;
    private String nombre;
    private String acronimo;
    private String cct;
    private Integer numeroSedesRegistradas;

}
