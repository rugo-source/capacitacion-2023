package org.acme.spring.data.ricardo.institucion.external.jpa.dao;

import org.acme.spring.data.ricardo.institucion.core.bussines.output.ResponsableOperativoAreaSeisRepository;
import org.acme.spring.data.ricardo.institucion.external.jpa.repository.ResponsableOperativoAreaSeisJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ResponsableOperativoAreaSeisDao implements ResponsableOperativoAreaSeisRepository {
    @Inject
    ResponsableOperativoAreaSeisJpaRepository responsableOperativoAreaSeisJpaRepository;

    @Override
    public Boolean existsByIdAreaContratante(Integer idAreaContratante) {
        return responsableOperativoAreaSeisJpaRepository.existsByIdAreaContratante(idAreaContratante);
    }
}
