package org.acme.spring.data.ricardo.examples.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.ricardo.examples.core.entity.CuentaSeisCreate;

public interface CuentaSeisService {
    Either<Integer,Boolean> create(CuentaSeisCreate cuentaSeisCreate);
}
