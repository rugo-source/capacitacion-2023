package org.acme.spring.data.ricardo.institucion.core.bussines.output;

import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;

import java.util.List;
import java.util.Optional;

public interface AreaContratanteSeisRepository {
    void save(AreaContratante areaContratantePersist);
    List<AreaContratante> findAreasContratantes(Integer idInstitucion);
    Boolean existsById(Integer idAreaContrantante);
    void delete(Integer idAreaContratante);
    Optional<AreaContratante> findById(Integer idAreaContrantante);
    void update(AreaContratante areaContratanteChange);


}
