package org.acme.spring.data.ricardo.institucion.core.bussines.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.ricardo.institucion.core.entity.EdificioSeisCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface EdificioSeisService {
    Either<ErrorCodes, Boolean> create(Integer idSede, EdificioSeisCreate edificioSeisCreate);
    Either<ErrorCodes, Boolean> update(Integer idEdificio, EdificioSeisCreate edificioSeisCreate);
    Either<ErrorCodes, List<Edificio>> get(Integer idSede);
    Either<ErrorCodes, Boolean> delete (Integer idEdificio);
}
