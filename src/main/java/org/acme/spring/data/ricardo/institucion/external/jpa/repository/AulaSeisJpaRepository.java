package org.acme.spring.data.ricardo.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.AulaJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AulaSeisJpaRepository extends JpaRepository<AulaJpa, Integer> {

    @Query("from AulaJpa where nombre = :nombre")
    Optional<AulaJpa> findByNombre(@Param("nombre") String nombre);

    List<AulaJpa> findAllByIdEdificio(Integer idEdificio);

    Boolean existsByNombreAndIdNot(String nombre, Integer id);
}
