package org.acme.spring.data.ricardo.examples.external.jpa.repository;

import org.acme.spring.data.ricardo.examples.external.jpa.model.CuentaSeisJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CuentaSeisJpaRepositpory extends JpaRepository<CuentaSeisJpa,Integer> {
}
