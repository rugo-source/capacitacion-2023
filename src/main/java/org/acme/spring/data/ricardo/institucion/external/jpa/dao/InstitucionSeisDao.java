package org.acme.spring.data.ricardo.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.InstitucionSeisRepository;
import org.acme.spring.data.ricardo.institucion.external.jpa.repository.InstitucionSeisJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class InstitucionSeisDao implements InstitucionSeisRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    InstitucionSeisJpaRepository institucionSeisJpaRepository;

    public static final String query= "Select * from tin01_institucion where UNACCENT(LOWER(tin01_institucion.tx_identificador)) LIKE '%' || UNACCENT(LOWER(:identificador))  || '%' and UNACCENT(LOWER(tin01_institucion.tx_nombre)) LIKE '%' || UNACCENT(LOWER(:nombre)) || '%' and UNACCENT(LOWER(tin01_institucion.tx_acronimo)) LIKE '%' || UNACCENT(LOWER(:acronimo)) || '%'";


    @Override
    public List<Institucion> findAll() {
        Stream<InstitucionJpa> result = entityManager.createNativeQuery("select distinct tin01.* from tin01_institucion tin01\n" + "left join tin04_ejecutivo tin04 on tin04.id_institucion=tin01.id_institucion\n" + "where tin04.id_ejecutivo is null", InstitucionJpa.class).getResultStream();
        return result.map(InstitucionJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<Institucion> findByIdentificador(String identificador) {
        return institucionSeisJpaRepository.findByIdentificador(identificador).map(InstitucionJpa::toEntity);
    }

    @Override
    public Optional<Institucion> findByNombre(String nombreInstitucion) {
        return institucionSeisJpaRepository.findByNombre(nombreInstitucion).map(InstitucionJpa::toEntity);
    }

    @Override
    public Optional<Institucion> findById(Integer idInstitucion) {
        return institucionSeisJpaRepository.findById(idInstitucion).map(InstitucionJpa::toEntity);
    }

    @Override
    public void update(Institucion institucionChange) {
        institucionSeisJpaRepository.saveAndFlush(InstitucionJpa.fromEntity(institucionChange));
    }

    @Override
    public Optional<Institucion> validateRNN013(Integer idInstitucion, String identificador) {
        return institucionSeisJpaRepository.validateRNN013(idInstitucion,identificador).map(InstitucionJpa::toEntity);
    }

    @Override
    public Optional<Institucion> validateRNN094(Integer idInstitucion, String nombreInstitucion) {
        return institucionSeisJpaRepository.validateRNN094(idInstitucion, nombreInstitucion).map(InstitucionJpa::toEntity);
    }

    @Override
    public void save(Institucion institucionPersist) {
        institucionSeisJpaRepository.saveAndFlush(InstitucionJpa.fromEntity(institucionPersist));
    }

    /*
    * Pasa el string a un public final static string para que lo tengas mas visible a la hora de dar mantenimiento
    * , tambien los nombres de tus parametros manejalos asi.
    *
    * */
    @Override
    public List<Institucion> findByInstituciones(String identificador, String nombre, String acronimo) {
        Query q = entityManager.createNativeQuery(query, InstitucionJpa.class);
        q.setParameter("identificador", identificador);
        q.setParameter("nombre", nombre);
        q.setParameter("acronimo", acronimo);
        return  q.getResultList();
    }

    @Override
    public Boolean existsById(Integer idInstitucion) {
        return institucionSeisJpaRepository.existsById(idInstitucion);
    }


    @Override
    public Boolean existsIdentificadorAndIdNot(String identificador, Integer idInstitucion) {
        return null;
    }

    @Override
    public Boolean existsNombreAndIdNot(String nombre, Integer idInstitucion) {
        return institucionSeisJpaRepository.existsByNombreAndIdNot(nombre, idInstitucion);
    }
}
