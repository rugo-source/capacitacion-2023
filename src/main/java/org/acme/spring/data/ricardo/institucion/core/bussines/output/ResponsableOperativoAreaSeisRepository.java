package org.acme.spring.data.ricardo.institucion.core.bussines.output;

public interface ResponsableOperativoAreaSeisRepository {
    Boolean existsByIdAreaContratante(Integer idAreaContratante);
}
