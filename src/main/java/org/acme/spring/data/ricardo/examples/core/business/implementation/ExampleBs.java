package org.acme.spring.data.ricardo.examples.core.business.implementation;

import io.vavr.control.Either;

import org.acme.spring.data.cristian.examples.core.entity.Usuario;
import org.acme.spring.data.cristian.examples.core.entity.UsuarioCreate;
import org.acme.spring.data.ricardo.examples.core.business.input.ExampleService;
import org.acme.spring.data.ricardo.examples.core.business.output.UserRepository;
import org.acme.spring.data.ricardo.examples.core.statemachine.PersonaSM;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class ExampleBs implements ExampleService {

    @Inject
    UserRepository userRepository;

    @Inject
    PersonaSM personaSM;


    @Override
    public List<String> listaEjemplo() {
        List<Usuario> examples = userRepository.findAll();
        examples.stream().forEach(usuario -> System.out.println(usuario.getEstadoUsuario().getNombre()));
        return examples.stream().map(usuario -> usuario.getEstadoUsuario().getNombre()).collect(Collectors.toList());
    }

    @Override
    public Either<Integer, Usuario> get(Integer idUsuario) {
        Either<Integer, Usuario> result =Either.left(404);
        var usuario = userRepository.findBy(idUsuario);
        if (usuario.isPresent()){
            var usuarioGet = usuario.get();
            usuarioGet.setRegistrar(personaSM.isDoable(personaSM.getRegistrar(), personaSM.getStateById(usuarioGet.getIdEstado())));
            usuarioGet.setEditar(personaSM.isDoable(personaSM.getEditar(), personaSM.getStateById(usuarioGet.getIdEstado())));
            usuarioGet.setEliminar(personaSM.isDoable(personaSM.getEliminar(), personaSM.getStateById(usuarioGet.getIdEstado())));
            usuarioGet.setConsultar(personaSM.isDoable(personaSM.getConsultar(), personaSM.getStateById(usuarioGet.getIdEstado())));
            usuarioGet.setConfigurar(personaSM.isDoable(personaSM.getConfigurar(), personaSM.getStateById(usuarioGet.getIdEstado())));
            result=Either.right(usuario.get());
        }
        return result;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> create(UsuarioCreate usuarioCreate) {
        Either<Integer, Boolean> result =Either.right(true);
        var usuarioPersits = Usuario.builder()
                .login(usuarioCreate.getLogin())
                .password(usuarioCreate.getPassword())
                .intento(0)
                .id(6)
                .idEstado(personaSM.getRegistrado().getId())
                .build();
        userRepository.save(usuarioPersits);
        return result;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> update(Integer id, UsuarioCreate usuarioCreate) {
        Either<Integer, Boolean> result = Either.left(404);
        var usuarioSearch = userRepository.findBy(id);
        if(usuarioSearch.isPresent()){
            var usuarioChange = usuarioSearch.get();
            usuarioChange.setLogin(usuarioCreate.getLogin());
            usuarioChange.setPassword(usuarioCreate.getPassword());
            userRepository.update(usuarioChange);
            result=Either.right(true);
        }
        return result;
    }

    @Override
    public Either<Integer, Boolean> delete(Integer id) {
        Either<Integer, Boolean> result = Either.left(404);
        var usuarioSearch = userRepository.findBy(id);
        if(usuarioSearch.isPresent()){
            userRepository.deleteBy(id);
            result=Either.right(true);
        }
        return result;
    }
}
