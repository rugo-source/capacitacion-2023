package org.acme.spring.data.ricardo.examples.core.entity;

import lombok.*;

import java.time.LocalDate;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CuentaSeisCreate {
    Integer idPersona;
    String rol;
    LocalDate inicio;
    LocalDate  fin;


}
