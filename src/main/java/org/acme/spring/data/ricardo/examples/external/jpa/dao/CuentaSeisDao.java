package org.acme.spring.data.ricardo.examples.external.jpa.dao;

import org.acme.spring.data.ricardo.examples.core.business.output.CuentaSeisRepository;
import org.acme.spring.data.ricardo.examples.core.entity.CuentaSeis;
import org.acme.spring.data.ricardo.examples.external.jpa.repository.CuentaSeisJpaRepositpory;
import org.acme.spring.data.ricardo.examples.external.jpa.model.CuentaSeisJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CuentaSeisDao implements CuentaSeisRepository {
    @Inject
    CuentaSeisJpaRepositpory cuentaSeisJpaRepositpory;

    @Override
    public CuentaSeisJpa save(CuentaSeis cuentaPersits) {
        return cuentaSeisJpaRepositpory.saveAndFlush(CuentaSeisJpa.fromEntity(cuentaPersits));
    }
}
