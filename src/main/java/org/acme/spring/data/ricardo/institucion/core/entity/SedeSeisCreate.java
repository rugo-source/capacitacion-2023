package org.acme.spring.data.ricardo.institucion.core.entity;

import lombok.*;


@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SedeSeisCreate {

    private Integer id;
    private Integer idInstitucion;
    private Integer idDireccion;
    private String nombre;
    private String acronimo;
    private Integer capacidad;
    private Boolean propia;
    private String identificador;

}
