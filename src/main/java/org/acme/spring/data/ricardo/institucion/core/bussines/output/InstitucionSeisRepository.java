package org.acme.spring.data.ricardo.institucion.core.bussines.output;

import org.acme.spring.data.cristian.institucion.core.entity.Institucion;

import java.util.List;
import java.util.Optional;

public interface InstitucionSeisRepository {
    List<Institucion> findAll();
    Optional<Institucion> findByIdentificador(String identificador);
    Optional<Institucion> findByNombre(String nombreInstitucion);
    Optional<Institucion> findById(Integer idInstitucion);
    void update(Institucion institucionChange);

    Optional<Institucion> validateRNN013(Integer idInstitucion, String identificador);

    Optional<Institucion> validateRNN094(Integer idInstitucion, String nombreInstitucion);

    void save(Institucion institucionPersist);
     List<Institucion> findByInstituciones(String identificador, String nombre, String acronimo);

    Boolean existsById(Integer idInstitucion);
    Boolean existsIdentificadorAndIdNot(String identificador, Integer idInstitucion);
    Boolean existsNombreAndIdNot(String nombre, Integer idInstitucion);
}
