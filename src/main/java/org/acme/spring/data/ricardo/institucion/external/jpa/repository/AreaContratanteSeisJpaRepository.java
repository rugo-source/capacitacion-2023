package org.acme.spring.data.ricardo.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AreaContratanteSeisJpaRepository extends JpaRepository<AreaContratanteJpa, Integer> {

    @Query("from AreaContratanteJpa where idInstitucion = :idInstitucion")
    List<AreaContratanteJpa> findAllByIdInstitucion(@Param("idInstitucion") Integer idInstitucion);
    boolean existsById(Integer idAreaContratante);



}
