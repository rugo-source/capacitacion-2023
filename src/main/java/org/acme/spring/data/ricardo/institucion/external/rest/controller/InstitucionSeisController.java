package org.acme.spring.data.ricardo.institucion.external.rest.controller;

import org.acme.spring.data.ricardo.institucion.core.bussines.input.InstitucionSeisService;
import org.acme.spring.data.ricardo.institucion.external.rest.dto.InstitucionSeisCreateDto;
import org.acme.spring.data.ricardo.institucion.external.rest.dto.InstitucionSeisDto;
import org.acme.spring.data.ricardo.institucion.external.rest.dto.InstitucionSeisGetDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;

import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/*revisa si tienes bien las conveciones de nombres entre los controllers y dto asi como en el bs y dao.*/
@Path("/catalogos-instituciones/institucion-seis")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Ricardo")
public class InstitucionSeisController {


        @Inject
        InstitucionSeisService institucionSeisService;

        /* Como este es un metodo que buscara todo hay que renombrarse como list, asi como tambien la parte del path en este caso no es necesario que este
        * asi que se puede eliminar
        * */
        @GET
        @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
        @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
        public Response list(){
                var list = institucionSeisService.get();
                return Response.ok(list).build();
        }

        @POST
        @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
        @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
        public Response create(@Valid InstitucionSeisCreateDto institucionSeisCreateDto) {
            return institucionSeisService.create(institucionSeisCreateDto.toEntity()).map(Response::ok).
                    getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
        }


        /*
        * En este caso re nombra el metodo como listBy por que al final sera una busqueda por filtros. tambien modifica tu retorno para el code 200
        * estas regresando un boolean en lugar de un array.
        * */
        @POST
        @Path("/busquedas")
        @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = InstitucionSeisDto.class)))
        @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
        public Response listBy(@Valid InstitucionSeisGetDto institucionSeisGetDto) {
                return institucionSeisService.getBy(institucionSeisGetDto.toEntity()).map(Response::ok)
                        .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
        }

        @PUT
        @Path("/{idInstitucion}")
        @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
        @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
        public Response update(@PathParam("idInstitucion") Integer idInstitucion, @Valid InstitucionSeisCreateDto institucionSeisCreateDto) {
                return institucionSeisService.update(idInstitucion, institucionSeisCreateDto.toEntity()).map(Response::ok)
                        .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
        }


    }
