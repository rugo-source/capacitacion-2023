package org.acme.spring.data.ricardo.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EdificioSeisCreate {

    private Integer id;
    private Integer idSede;
    private String nombre;
    private String acronimo;
    private String referencia;
}
