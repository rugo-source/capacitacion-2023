package org.acme.spring.data.ricardo.institucion.core.bussines.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.ricardo.institucion.core.bussines.input.EdificioSeisService;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.EdificioSeisRepository;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.SedeSeisRepository;
import org.acme.spring.data.ricardo.institucion.core.entity.EdificioSeisCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class EdificioSeisBS implements EdificioSeisService {

    @Inject
    EdificioSeisRepository edificioSeisRepository;

    @Inject
    SedeSeisRepository sedeSeisRepository;

    @Override
    public Either<ErrorCodes, Boolean> create(Integer idSede, EdificioSeisCreate edificioSeisCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.RNN002);
        var edificioBuscarAcronimo = edificioSeisRepository.findByAcronimo(edificioSeisCreate.getAcronimo());
        if(!edificioBuscarAcronimo.isPresent()) {
            resultado = Either.left(ErrorCodes.RNN002);
            var edificioBuscarNombre = edificioSeisRepository.findByNombre(edificioSeisCreate.getNombre());
            if(!edificioBuscarNombre.isPresent()) {
                var edificioPersist = Edificio.builder()
                        .idSede(idSede)
                        .nombre(edificioSeisCreate.getNombre())
                        .acronimo(edificioSeisCreate.getAcronimo())
                        .referencia(edificioSeisCreate.getReferencia())
                        .build();
                edificioSeisRepository.save(edificioPersist);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer idEdificio, EdificioSeisCreate edificioSeisCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var edificioBuscar = edificioSeisRepository.findById(idEdificio);
        if(edificioBuscar.isPresent()) {
            if (validateRNN002A(idEdificio, edificioSeisCreate.getAcronimo())) {
                resultado = Either.left(ErrorCodes.RNN002);
            } else if(validateRNN002N(idEdificio, edificioSeisCreate.getNombre())){
                resultado = Either.left(ErrorCodes.RNN002);
            } else {
                var edificioChange = edificioBuscar.get();
                edificioChange.setNombre(edificioSeisCreate.getNombre());
                edificioChange.setAcronimo(edificioSeisCreate.getAcronimo());
                edificioChange.setReferencia(edificioSeisCreate.getReferencia());
                edificioSeisRepository.update(edificioChange);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, List<Edificio>> get(Integer idSede) {
        Either<ErrorCodes, List<Edificio>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if(existsSede(idSede)){
            var listEdificio = edificioSeisRepository.findEdificios(idSede);
            resultado = Either.right(listEdificio);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idEdificio) {
        Either<ErrorCodes, Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        resultado = Either.left(ErrorCodes.RNN025);
        var edificioBuscar = edificioSeisRepository.findById(idEdificio);
        if (edificioBuscar.isPresent()) {
            edificioSeisRepository.delete(idEdificio);
            resultado = Either.right(true);
        }
        return resultado;
    }

    public Boolean validateRNN002A(Integer idEdificio, String acronimo) {
        return edificioSeisRepository.existsAcronimoAndIdNot(acronimo, idEdificio);
    }

    public Boolean validateRNN002N(Integer idEdificio, String nombre) {
        return edificioSeisRepository.existsNombreAndIdNot(nombre, idEdificio);
    }

    public Boolean existsSede(Integer idSede) {
        return sedeSeisRepository.existsById(idSede);
    }

}
