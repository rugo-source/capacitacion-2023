package org.acme.spring.data.ricardo.institucion.external.rest.controller;

import org.acme.spring.data.ricardo.institucion.external.rest.dto.AreaContratanteSeisDto;
import org.acme.spring.data.ricardo.institucion.core.bussines.input.AreaContranteSeisService;
import org.acme.spring.data.ricardo.institucion.external.rest.dto.AreaContratanteSeisCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/catalogos-instituciones/area-contratante-Seis")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Ricardo")
public class AreaContratanteSeisController {

    @Inject
    AreaContranteSeisService areaContratanteSeisService;

    @GET
    @Path("{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = AreaContratanteSeisDto.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response listAreasContratantes(@PathParam("idInstitucion") Integer idInstitucion) {
        return areaContratanteSeisService.get(idInstitucion).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid AreaContratanteSeisCreateDto areaContratanteSeisCreateDto) {
        return areaContratanteSeisService.create(areaContratanteSeisCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("{idAreaContratante}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idAreaContratante") Integer idAreaContratante, @Valid AreaContratanteSeisCreateDto areaContratanteSeisCreateDto) {
        return areaContratanteSeisService.update(idAreaContratante, areaContratanteSeisCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("{idAreaContratante}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idAreaContratante") Integer idAreaContratante) {
        return areaContratanteSeisService.delete(idAreaContratante).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }
}
