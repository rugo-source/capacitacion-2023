package org.acme.spring.data.ricardo.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.ricardo.examples.core.entity.EstadoPersonaSeis;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "estado_persona_dos")
public class EstadoPersonaSeisJpa {
    @Id
    @Column(name = "id_estado")
    private Integer id;

    @Column(name = "tx_nombre")
    private String nombre;

    public EstadoPersonaSeis toEntity(){
        return EstadoPersonaSeis.builder()
                .id(this.id)
                .nombre(this.nombre)
                .build();
    }
}
