package org.acme.spring.data.ricardo.examples.external.rest.controllers;

import org.acme.spring.data.ricardo.examples.core.business.input.CuentaSeisService;
import org.acme.spring.data.ricardo.examples.external.rest.dto.CuentaSeisDto;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/data/example/ricardo/cuentaSeis")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion")
public class CuentaSeisController {
    @Inject
    CuentaSeisService cuentaSeisService;
    @POST
    @Path("post")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = CuentaSeisDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create( @Valid CuentaSeisDto cuentaSeisDto) {
        return cuentaSeisService.create(cuentaSeisDto.toEntity()).map(Response::ok)
                .getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }
}
