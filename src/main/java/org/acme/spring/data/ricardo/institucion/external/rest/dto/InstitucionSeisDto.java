package org.acme.spring.data.ricardo.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class InstitucionSeisDto {

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al id del tipo de institucion")
    Integer idTipo;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al id clasificacion de la institucion")
    Integer idClasificacion;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al id categoria de la institucion")
    Integer idCategoria;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al id subsistema universidad")
    Integer idSubsistemaUniversidad;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al id subsistema bachillerato")
    Integer idSubsistemaBachillerato;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al identificador de la institucion")
    String identificador;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al nombre de la institucion")
    String nombre;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al acronimo de la institucion")
    String acronimo;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al cct de la institucion")
    String cct;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al numero de cedes registradas")
    Integer numeroSedesRegistradas;

    public static InstitucionSeisDto fromEntity(Institucion institucion) {
        return InstitucionSeisDto.builder()
                .idTipo(institucion.getIdTipo())
                .idClasificacion(institucion.getIdClasificacion())
                .idCategoria(institucion.getIdCategoria())
                .idSubsistemaUniversidad(institucion.getIdSubsistemaUniversidad())
                .idSubsistemaBachillerato(institucion.getIdSubsistemaBachillerato())
                .identificador(institucion.getIdentificador())
                .nombre(institucion.getNombre())
                .acronimo(institucion.getAcronimo())
                .cct(institucion.getCct())
                .numeroSedesRegistradas(institucion.getNumeroSedesRegistradas())
                .build();
    }
}
