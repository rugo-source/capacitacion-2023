package org.acme.spring.data.ricardo.institucion.core.bussines.implementation;


import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;

import org.acme.spring.data.ricardo.institucion.core.bussines.input.InstitucionSeisService;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.InstitucionSeisRepository;
import org.acme.spring.data.ricardo.institucion.core.entity.InstitucionSeisCreate;
import org.acme.spring.data.ricardo.institucion.core.entity.InstitucionSeisGet;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class InstitucionSeisBs implements InstitucionSeisService {

    @Inject
    InstitucionSeisRepository institucionSeisRepository;

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(InstitucionSeisCreate institucionSeisCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.RNN013);
        var institucionBuscarIdentificador = institucionSeisRepository.findByIdentificador(institucionSeisCreate.getIdentificador());
        if(!institucionBuscarIdentificador.isPresent()) {
            resultado = Either.left(ErrorCodes.RNN094);
            var institucionBuscarNombre = institucionSeisRepository.findByNombre(institucionSeisCreate.getNombre());
            if(!institucionBuscarNombre.isPresent()) {
                var institucionPersist = Institucion.builder()
                        .idTipo(institucionSeisCreate.getIdTipo())
                        .idClasificacion(institucionSeisCreate.getIdClasificacion())
                        .idCategoria(institucionSeisCreate.getIdCategoria())
                        .idSubsistemaUniversidad(institucionSeisCreate.getIdSubsistemaUniversidad())
                        .idSubsistemaBachillerato(institucionSeisCreate.getIdSubsistemaBachillerato())
                        .identificador(institucionSeisCreate.getIdentificador())
                        .nombre(institucionSeisCreate.getNombre())
                        .acronimo(institucionSeisCreate.getAcronimo())
                        .cct(institucionSeisCreate.getCct())
                        .numeroSedesRegistradas(0)
                        .build();
                institucionSeisRepository.save(institucionPersist);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, List<Institucion>> getBy(InstitucionSeisGet institucionSeisGet) {
        Either<ErrorCodes, List<Institucion>> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var instituciones = institucionSeisRepository.findByInstituciones(institucionSeisGet.getIdentificador(),institucionSeisGet.getNombre(),institucionSeisGet.getAcronimo());
        if(!instituciones.isEmpty()) {
            resultado = Either.right(instituciones);
        }
        return resultado;
    }

    @Override
    public List<Institucion> get() {
        return institucionSeisRepository.findAll();
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer idInstitucion, InstitucionSeisCreate institucionSeisCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);
        var institucionBuscar = institucionSeisRepository.findById(idInstitucion);
        if(institucionBuscar.isPresent()) {
            if (validateRNN013(idInstitucion, institucionSeisCreate.getIdentificador())) {
                resultado = Either.left(ErrorCodes.RNN013);
            } else if(validateRNN094(idInstitucion, institucionSeisCreate.getNombre())){
                resultado = Either.left(ErrorCodes.RNN094);
            } else {
                var institucionChange = institucionBuscar.get();
                institucionChange.setIdTipo(institucionSeisCreate.getIdTipo());
                institucionChange.setIdClasificacion(institucionSeisCreate.getIdClasificacion());
                institucionChange.setIdCategoria(institucionSeisCreate.getIdCategoria());
                institucionChange.setIdSubsistemaUniversidad(institucionSeisCreate.getIdSubsistemaUniversidad());
                institucionChange.setIdSubsistemaBachillerato(institucionSeisCreate.getIdSubsistemaBachillerato());
                institucionChange.setIdentificador(institucionSeisCreate.getIdentificador());
                institucionChange.setNombre(institucionSeisCreate.getNombre());
                institucionChange.setAcronimo(institucionSeisCreate.getAcronimo());
                institucionChange.setCct(institucionSeisCreate.getCct());
                institucionChange.setNumeroSedesRegistradas(institucionSeisCreate.getNumeroSedesRegistradas());
                institucionSeisRepository.update(institucionChange);
                resultado = Either.right(true);
            }
        }
        return resultado;
    }


    public Boolean validateRNN013(Integer idInstitucion, String identificador) {
        return institucionSeisRepository.existsIdentificadorAndIdNot(identificador, idInstitucion);
    }

    public Boolean validateRNN094(Integer idInstitucion, String nombre) {
        return institucionSeisRepository.existsNombreAndIdNot(nombre, idInstitucion);
    }

}
