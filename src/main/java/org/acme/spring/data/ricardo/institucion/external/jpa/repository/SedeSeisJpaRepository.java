package org.acme.spring.data.ricardo.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SedeSeisJpaRepository extends JpaRepository<SedeJpa, Integer> {
    List<SedeJpa> findAllByIdInstitucion(Integer idInstitucion);
    boolean existsById(Integer idSede);



}
