package org.acme.spring.data.ricardo.institucion.external.rest.controller;

import org.acme.spring.data.ricardo.institucion.core.bussines.input.SedeSeisService;
import org.acme.spring.data.ricardo.institucion.external.rest.dto.SedeSeisCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/catalogos-instituciones/sedes-seis")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Ricardo")
public class SedeSeisController {

    @Inject
    SedeSeisService sedeSeisService;

    @POST
    @Path("/{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@PathParam("idInstitucion") Integer idInstitucion, @Valid SedeSeisCreateDto sedeSeisCreateDto) {
        return sedeSeisService.create(idInstitucion, sedeSeisCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @Path("/busqueda/{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY,implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response get(@PathParam("idInstitucion")Integer idInstitucion) {
        return sedeSeisService.getSedesById(idInstitucion).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }

    @DELETE
    @Path("/{idSede}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idSede") Integer idSede) {
        return sedeSeisService.delete(idSede).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("/{idSede}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idSede") Integer idSede, @Valid SedeSeisCreateDto sedeSeisCreateDto) {
        return sedeSeisService.update(idSede, sedeSeisCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

}
