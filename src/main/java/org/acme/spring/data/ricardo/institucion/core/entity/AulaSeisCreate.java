package org.acme.spring.data.ricardo.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AulaSeisCreate {

    Integer idAula;
    Integer idEdificio;
    String nombre;
    Integer idTipoEquipamiento;
    Integer cantidadEquipamiento;
    Integer capacidadEquipamiento;
    String observaciones;

}
