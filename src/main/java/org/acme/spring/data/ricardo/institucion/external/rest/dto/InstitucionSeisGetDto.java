package org.acme.spring.data.ricardo.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ricardo.institucion.core.entity.InstitucionSeisGet;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class InstitucionSeisGetDto {

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al identificador de la institucion")
    String identificador;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al nombre de la institucion")
    String nombre;

    @JsonProperty
    @Schema(description = "este es el atributo que hace referencia al acronimo de la institucion")
    String acronimo;

    public InstitucionSeisGet toEntity() {
        return InstitucionSeisGet.builder()
                .identificador(this.identificador)
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .build();
    }
}
