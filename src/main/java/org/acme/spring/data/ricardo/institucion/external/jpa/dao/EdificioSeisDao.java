package org.acme.spring.data.ricardo.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.EdificioSeisRepository;
import org.acme.spring.data.ricardo.institucion.external.jpa.repository.EdificioSeisJpaRepository;
import org.acme.spring.data.ricardo.institucion.external.jpa.repository.InstitucionSeisJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class EdificioSeisDao implements EdificioSeisRepository {


    @Inject
    EdificioSeisJpaRepository edificioSeisJpaRepository;

    @Override
    public void save(Edificio edificioPersist) {
        edificioSeisJpaRepository.saveAndFlush(EdificioJpa.fromEntity(edificioPersist));
    }

    @Override
    public void update(Edificio edificioChange) {
        edificioSeisJpaRepository.saveAndFlush(EdificioJpa.fromEntity(edificioChange));
    }

    @Override
    public void delete(Integer idEdificio) {
        edificioSeisJpaRepository.deleteById(idEdificio);
    }

    @Override
    public Optional<Edificio> validateRNN002A(Integer idEdificio, String acronimo) {
        return edificioSeisJpaRepository.validateRNN002A(idEdificio,acronimo).map(EdificioJpa::toEntity);
    }

    @Override
    public Optional<Edificio> validateRNN002N(Integer idEdificio, String nombre) {
        return edificioSeisJpaRepository.validateRNN002N(idEdificio,nombre).map(EdificioJpa::toEntity);
    }

    @Override
    public Boolean existsAcronimoAndIdNot(String acronimo, Integer idEdificio) {
        return edificioSeisJpaRepository.existsByAcronimoAndIdNot(acronimo, idEdificio);
    }

    @Override
    public Boolean existsNombreAndIdNot(String nombre, Integer idEdificio) {
        return edificioSeisJpaRepository.existsByNombreAndIdNot(nombre, idEdificio);
    }

    @Override
    public List<Edificio> findEdificios(Integer idSede) {
        return edificioSeisJpaRepository.findAllByIdSede(idSede).stream().map(EdificioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<Edificio> findByNombre(String nombre) {
        return edificioSeisJpaRepository.findByNombre(nombre).map(EdificioJpa::toEntity);
    }

    @Override
    public Optional<Edificio> findByAcronimo(String acronimo) {
        return edificioSeisJpaRepository.findByAcronimo(acronimo).map(EdificioJpa::toEntity);
    }

    @Override
    public Optional<Edificio> findById(Integer idEdificio) {
        return edificioSeisJpaRepository.findById(idEdificio).map(EdificioJpa::toEntity);
    }
}
