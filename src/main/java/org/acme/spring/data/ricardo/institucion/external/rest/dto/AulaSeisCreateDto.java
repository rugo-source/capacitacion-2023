package org.acme.spring.data.ricardo.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ricardo.institucion.core.entity.AulaSeisCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AulaSeisCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 100, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al nombre del aula")
    String nombre;

    @JsonProperty
    @Positive(message = "RNN128")
    @NotNull(message = "RNS001")
    Integer idTipoEquipamiento;

    @JsonProperty
    @Positive(message = "RNN128")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia a la cantidad de equipamiento del aula")
    Integer cantidadEquipamiento;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia a la capacidad de equipamiento del aula")
    Integer capacidadEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 100, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia a las observaciones del aula")
    String observaciones;


    public AulaSeisCreate toEntity (){
        return AulaSeisCreate.builder()
                .nombre(this.nombre)
                .cantidadEquipamiento(this.cantidadEquipamiento)
                .idTipoEquipamiento(this.idTipoEquipamiento)
                .capacidadEquipamiento(this.capacidadEquipamiento)
                .observaciones(this.observaciones)
                .build();
    }
}
