package org.acme.spring.data.ricardo.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.ricardo.examples.core.entity.Persona;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonaSeisDto {

    @JsonProperty
    @Schema(description = "este es el atributo que referencia al nombre de la persona")
    private String nombre;
    @JsonProperty
    @Schema(description = "Bandera que puede realizar un registro de una persona")
    private Boolean registrar;
    @JsonProperty
    @Schema(description = "Bandera que puede realizar un editar de una persona")
    private  Boolean editar;
    @JsonProperty
    @Schema(description = "Bandera que puede realizar un eliminar de una persona")
    private Boolean eliminar;
    @JsonProperty
    @Schema(description = "Bandera que puede realizar un consultar de una persona")
    private  Boolean consultar;

    public static PersonaSeisDto fromEntity(Persona persona){
        return PersonaSeisDto.builder()
                .nombre(persona.getNombre())
                .registrar(persona.getRegistrar())
                .editar(persona.getEditar())
                .eliminar(persona.getEliminar())
                .consultar(persona.getConsultar())
                .build();
    }
}
