package org.acme.spring.data.ricardo.institucion.core.bussines.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.ricardo.institucion.core.entity.AreaContratanteSeisCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface AreaContranteSeisService {
    Either<ErrorCodes, Boolean> create(AreaContratanteSeisCreate areaContratanteSeisCreate);
    Either<ErrorCodes, List<AreaContratante>> get(Integer idInstitucion);
    Either<ErrorCodes, Boolean> delete (Integer idAreaContratante);
    Either<ErrorCodes, Boolean> update(Integer idAreaContratante, AreaContratanteSeisCreate areaContratanteSeisCreate);

}
