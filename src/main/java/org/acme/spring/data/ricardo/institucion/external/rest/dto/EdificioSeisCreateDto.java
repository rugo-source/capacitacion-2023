package org.acme.spring.data.ricardo.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.ricardo.institucion.core.entity.EdificioSeisCreate;
import org.acme.spring.data.ricardo.institucion.core.entity.SedeSeisCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "EdificioSeisCreate", description = "Esta en la entidad crea el edificio")
public class EdificioSeisCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 100, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al nombre del edificio")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 20, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al acronimo del edificio")
    String acronimo;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 225, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia a las referencias del edificio")
    String referencia;

    public EdificioSeisCreate toEntity (){
        return EdificioSeisCreate.builder()
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .referencia(this.referencia)
                .build();
    }
}
