package org.acme.spring.data.ricardo.institucion.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.acme.spring.data.ricardo.institucion.external.jpa.repository.AreaContratanteSeisJpaRepository;
import org.acme.spring.data.ricardo.institucion.core.bussines.output.AreaContratanteSeisRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class AreaContratanteSeisDao implements AreaContratanteSeisRepository {
    @Inject
    AreaContratanteSeisJpaRepository areaContratanteSeisJpaRepository;

    @Override
    public void save(AreaContratante areaContratantePersist) {
        areaContratanteSeisJpaRepository.saveAndFlush(AreaContratanteJpa.fromEntity(areaContratantePersist));
    }

    @Override
    public List<AreaContratante> findAreasContratantes(Integer idInstitucion) {
        return areaContratanteSeisJpaRepository.findAllByIdInstitucion(idInstitucion).stream().map(AreaContratanteJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Boolean existsById(Integer idAreaContrantante) {
        return areaContratanteSeisJpaRepository.existsById(idAreaContrantante);
    }

    @Override
    public void delete(Integer idAreaContratante) {
        areaContratanteSeisJpaRepository.deleteById(idAreaContratante);
    }

    @Override
    public Optional<AreaContratante> findById(Integer idAreaContrantante) {
        return areaContratanteSeisJpaRepository.findById(idAreaContrantante).map(AreaContratanteJpa::toEntity);
    }

    @Override
    public void update(AreaContratante areaContratanteChange) {
        areaContratanteSeisJpaRepository.saveAndFlush(AreaContratanteJpa.fromEntity(areaContratanteChange));
    }


}
