package org.acme.spring.data.ricardo.institucion.core.bussines.output;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;



import java.util.List;
import java.util.Optional;

public interface AulaSeisRepository {
    void save(Aula aulaPersist);
    void update(Aula aulaChange);
    void delete(Integer idAula);

    Optional<Aula> findByNombre(String nombre);
    List<Aula> findAllByIdEdificio(Integer idEdificio);
    Boolean existsNombreAndIdNot(String nombre, Integer idAula);
    Optional<Aula> findById(Integer idAula);
}
