package org.acme.spring.data.ricardo.examples.external.jpa.dao;


import org.acme.spring.data.cristian.examples.core.entity.Usuario;
import org.acme.spring.data.cristian.examples.external.jpa.model.UsuarioJpa;
import org.acme.spring.data.ricardo.examples.core.business.output.UserRepository;
import org.acme.spring.data.ricardo.examples.external.jpa.repository.UserJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class UsuarioDao implements UserRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    UserJpaRepository userJpaRepository;

    @Override
    public List<Usuario> findAll() {
        return userJpaRepository.findAll().stream().map(UsuarioJpa -> {
            var usuarioEntidad = UsuarioJpa.toEntity();
            //usuarioEntidad.setEstadoUsuario(UsuarioJpa.getEstadoUsuario().toEntity());
            return usuarioEntidad;
        }).collect(Collectors.toList());
    }

    @Override
    public List<Usuario> findAllNative() {
        Stream <UsuarioJpa> result= entityManager.createNativeQuery("select * from tca02_usuario",UsuarioJpa.class).getResultStream();
        return result.map(UsuarioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<Usuario> findAllOrm() {

        return null;
        //userJpaRepository.findAllOrm().stream().map(UsuarioJpa::toEntity).collect(Collectors.toList())
    }

    @Override
    public Optional<Usuario> findBy(Integer idUsuario) {
        return userJpaRepository.findById(idUsuario).map(UsuarioJpa :: toEntity);
    }

    @Override
    public void save(Usuario usuarioPersits) {
        userJpaRepository.saveAndFlush(UsuarioJpa.fromEntity(usuarioPersits));
    }

    @Override
    public void update(Usuario usuarioChange) {
        userJpaRepository.saveAndFlush(UsuarioJpa.fromEntity(usuarioChange));
    }

    @Override
    public void deleteBy(Integer id) {
        userJpaRepository.deleteById(id);
    }
}
