package org.acme.spring.data.ricardo.institucion.core.bussines.output;

import org.acme.spring.data.cristian.institucion.core.entity.Edificio;


import java.util.List;
import java.util.Optional;

public interface EdificioSeisRepository {

    void save(Edificio edificioPersist);
    void update(Edificio edificioChange);
    void delete(Integer idEdificio);
    Optional<Edificio> validateRNN002A(Integer idEdificio, String acronimo);
    Optional<Edificio> validateRNN002N(Integer idEdificio, String nombre);
    Boolean existsAcronimoAndIdNot(String acronimo, Integer idEdificio);
    Boolean existsNombreAndIdNot(String nombre, Integer idEdificio);
    List<Edificio> findEdificios(Integer idSede);

    Optional<Edificio> findByNombre(String nombre);
    Optional<Edificio> findByAcronimo(String acronimo);
    Optional<Edificio> findById(Integer idEdificio);
}
