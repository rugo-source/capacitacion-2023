package org.acme.spring.data.ricardo.examples.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.ricardo.examples.core.business.input.PersonaService;
import org.acme.spring.data.ricardo.examples.core.business.output.PersonaRepository;
import org.acme.spring.data.ricardo.examples.core.entity.Persona;
import org.acme.spring.data.ricardo.examples.core.entity.PersonaCreate;
import org.acme.spring.data.ricardo.examples.core.statemachine.PersonaSM;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class FirstBs implements PersonaService {

    @Inject
    PersonaRepository personaRepository;
    @Inject
    PersonaSM personaSM;

    @Override
    public List<Persona> listaEjemplo() {
        return personaRepository.findAll().stream().map(persona -> {
            persona.setRegistrar(personaSM.isDoable(personaSM.getRegistrar(),personaSM.getStateById(persona.getIdEstado())));
            persona.setEditar(personaSM.isDoable(personaSM.getEditar(),personaSM.getStateById(persona.getIdEstado())));
            persona.setEliminar(personaSM.isDoable(personaSM.getEliminar(),personaSM.getStateById(persona.getIdEstado())));
            persona.setConsultar(personaSM.isDoable(personaSM.getConsultar(),personaSM.getStateById(persona.getIdEstado())));
            return persona;
        }).collect(Collectors.toList());
    }

    @Override
    public Either<Integer, Persona> get(Integer idPersona) {
        Either<Integer, Persona> resultado = Either.left(404);
        var persona = personaRepository.findBy(idPersona);
        if (persona.isPresent()) {
            var personaGet=persona.get();
            personaGet.setRegistrar(personaSM.isDoable(personaSM.getRegistrar(),personaSM.getStateById(personaGet.getIdEstado())));
            personaGet.setEditar(personaSM.isDoable(personaSM.getEditar(),personaSM.getStateById(personaGet.getIdEstado())));
            personaGet.setEliminar(personaSM.isDoable(personaSM.getEliminar(),personaSM.getStateById(personaGet.getIdEstado())));
            personaGet.setConsultar(personaSM.isDoable(personaSM.getConsultar(),personaSM.getStateById(personaGet.getIdEstado())));
            resultado = Either.right(personaGet);
        }
        return resultado;
    }

    @Override
    public Either<Integer, Boolean> create(PersonaCreate personaCreate) {
        Either<Integer, Boolean> resultado = Either.right(true);
        var personaPersits = Persona.builder()
                .nombre(personaCreate.getNombre())
                .edad(personaCreate.getEdad())
                .idEstado(personaSM.getRegistrado().getId())
                .build();
        personaRepository.save(personaPersits);
        return resultado;
    }

    @Override
    public Either<Integer, Boolean> update(Integer id, PersonaCreate personaCreate) {
        Either<Integer, Boolean> resultado = Either.left(404);
        var personaSearch = personaRepository.findBy(id);
        if (personaSearch.isPresent()) {
            var personaChange = personaSearch.get();
            personaChange.setNombre(personaCreate.getNombre());
            personaChange.setEdad(personaCreate.getEdad());
            personaRepository.update(personaChange);
            resultado = Either.right(true);
        }

        return resultado;
    }


    @Override
    public Either<Integer, Boolean> delete(Integer id) {
        Either<Integer, Boolean> resultado = Either.left(404);
        var personaSearch = personaRepository.findBy(id);
        if (personaSearch.isPresent()) {
            personaRepository.deleteBy(id);
            resultado = Either.right(true);
        }
        return resultado;
    }



}
