package org.acme.spring.data.ricardo.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface EdificioSeisJpaRepository extends JpaRepository<EdificioJpa, Integer> {

    @Query("from EdificioJpa where nombre = :nombre")
    Optional<EdificioJpa> findByNombre(@Param("nombre") String nombre);

    @Query("from EdificioJpa where acronimo = :acronimo")
    Optional<EdificioJpa> findByAcronimo(@Param("acronimo") String acronimo);

    @Query("from EdificioJpa where acronimo = :acronimo and id_edificio != :id_edificio")
    Optional<EdificioJpa> validateRNN002A(@Param("id_edificio") Integer idEdificio, @Param("acronimo") String acronimo);

    @Query("from EdificioJpa where nombre = :nombre and id_edificio != :id_edificio")
    Optional<EdificioJpa> validateRNN002N(@Param("id_edificio") Integer idEdificio, @Param("nombre") String nombre);

    @Query("from EdificioJpa where fk_id_sede = :id_sede")
    List<EdificioJpa> findAllByIdSede(@Param("id_sede") Integer idSede);

    Boolean existsByAcronimoAndIdNot(String acronimo, Integer id);
    Boolean existsByNombreAndIdNot(String nombre, Integer id);
}
