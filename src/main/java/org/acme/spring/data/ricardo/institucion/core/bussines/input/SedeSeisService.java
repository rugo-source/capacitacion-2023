package org.acme.spring.data.ricardo.institucion.core.bussines.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.ricardo.institucion.core.entity.AreaContratanteSeisCreate;
import org.acme.spring.data.ricardo.institucion.core.entity.SedeSeisCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface SedeSeisService {
    Either<ErrorCodes, Boolean> create(Integer idInstitucion, SedeSeisCreate sedesSeisCreate);
    Either<ErrorCodes, List<Sede>> getSedesById(Integer idInstitucion);
    Either<ErrorCodes, Boolean> delete (Integer idSede);
    Either<ErrorCodes, Boolean> update(Integer idSede,SedeSeisCreate sedeSeisCreate);
}
