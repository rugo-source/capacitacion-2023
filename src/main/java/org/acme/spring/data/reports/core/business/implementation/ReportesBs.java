package org.acme.spring.data.reports.core.business.implementation;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.constants.Page;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import io.vavr.control.Either;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.acme.spring.data.reports.core.business.input.ReportesServices;
import org.acme.spring.data.reports.core.entity.PersonaReporte;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import java.io.ByteArrayOutputStream;
import java.util.*;

@ApplicationScoped
public class ReportesBs implements ReportesServices {

    @Override
    public Either<ErrorCodes, String> getReporteEjemploUno() {
        Either<ErrorCodes, String> result = Either.left(ErrorCodes.ERROR);
        List<PersonaReporte> personaReporteList = new ArrayList<>();
        personaReporteList.add(PersonaReporte.builder().nombre("Sofía").apellidoMaterno("Velázquez").apellidoPaterno("Ramírez").curp("AAAA000000MMMMMM05").telefono("5555555555").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Guillermo").apellidoMaterno("Aguilar").apellidoPaterno("Fernández").curp("AAAA000000MMMMMM06").telefono("5555555555").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Karla Claudia").apellidoMaterno("Sosa").apellidoPaterno("Delgado").curp("AAAA000000MMMMMM07").telefono("5555555555").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Maria Arcelia").apellidoMaterno("Rojo").apellidoPaterno("Solis").curp("AAAA000000MMMMMM08").telefono("5555555555").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Pedro").apellidoMaterno("Rodríguez").apellidoPaterno("Fernández").curp("AAAA000000MMMMMM09").telefono("5555555555").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Josué Tonatiuh").apellidoMaterno("Hernández").apellidoPaterno("Cuevas").curp("HECJ841020HDFRVS01").telefono("5536981080").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Francisco Alonso").apellidoMaterno("Hernández").apellidoPaterno("Franco").curp("HEFF791226HDFRRR05").telefono("5516539460").build());
        personaReporteList.add(PersonaReporte.builder().nombre("José Fernando").apellidoMaterno("Rodríguez").apellidoPaterno("García").curp("ROGF950111HDFDRR01").telefono("5617540679").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Carlos").apellidoMaterno("Hernández").apellidoPaterno("Calderón").curp("HECC001022HDFRLRA9").telefono("5578071689").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Olga").apellidoMaterno("Alpizar").apellidoPaterno("Arce").curp("AIAO720905MDFLRL04").telefono("5581199689").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Roxana Jesús").apellidoMaterno("Arias").apellidoPaterno("Rebatet").curp("AIRR671029MDFRBX05").telefono("5543460572").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Rodolfo").apellidoMaterno("Sánchez").apellidoPaterno("Gamboa").curp("SAGR750615HDFNMD09").telefono("5549947767").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Gabriela").apellidoMaterno("Hernández").apellidoPaterno("García").curp("HEGG861115MDFRRB00").telefono("5581295485").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Jhonatan Emanuel").apellidoMaterno("Escobar").apellidoPaterno("Rivera").curp("EORJ011108HMCSVHA6").telefono("5586078336").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Sandra Eloisa").apellidoMaterno("Padilla").apellidoPaterno("Morales").curp("PAMS870722MDFDRN04").telefono("5527388401").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Armando").apellidoMaterno("González").apellidoPaterno("Soto").curp("GOSA780404HDFNTR05").telefono("5526778703").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Oliver").apellidoMaterno("Espinosa").apellidoPaterno("Colchado").curp("EICO741124HDFSLL07").telefono("5536459585").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Edgar Eduardo").apellidoMaterno("González").apellidoPaterno("Godínez").curp("GOGE791110HDFNDD00").telefono("5562122688").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Lizbeth").apellidoMaterno("Correa").apellidoPaterno("Moreno").curp("COML801203MQTRRZ04").telefono("5529000955").build());
        personaReporteList.add(PersonaReporte.builder().nombre("César David").apellidoMaterno("Pérez").apellidoPaterno("Ramírez").curp("AAAA010101HDFRLD01").telefono("5559470926").build());
        personaReporteList.add(PersonaReporte.builder().nombre("José David").apellidoMaterno("Flores").apellidoPaterno("de Jesús").curp("FOJD720112HGRLSV00").telefono("5584277811").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Edgar").apellidoMaterno("Trilla").apellidoPaterno("Elizalde").curp("TIEE871006HTLRLD01").telefono("7481065671").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Elsa").apellidoMaterno("Luna").apellidoPaterno("Garrido").curp("LUGE770220MDFNRL01").telefono("5529194470").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Emilio Daniel").apellidoMaterno("Cadenas").apellidoPaterno("Neria").curp("CANE871129HDFDRM09").telefono("5547628638").build());
        personaReporteList.add(PersonaReporte.builder().nombre("María Del Rosario").apellidoMaterno("Portilla").apellidoPaterno("Zamora").curp("POZR790706MMCRMS08").telefono("5529080462").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Alhondra Cristal").apellidoMaterno("García").apellidoPaterno("Munive").curp("GAMA820111MDFRNL00").telefono("5568008680").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Alma Delia").apellidoMaterno("Gómez").apellidoPaterno("Vega").curp("GOVA781024MDFMGL01").telefono("5539385814").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Cinthia Julieta").apellidoMaterno("Morales").apellidoPaterno("Román").curp("MORC850501MDFRMN06").telefono("5527532689").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Claudia María").apellidoMaterno("Martínez").apellidoPaterno("Chávez").curp("MACC751105MDFRHL11").telefono("5539501316").build());
        personaReporteList.add(PersonaReporte.builder().nombre("María Guadalupe Lilian").apellidoMaterno("Godínez").apellidoPaterno("Delgado").curp("GODG820618MDFDLD08").telefono("5535075587").build());
        personaReporteList.add(PersonaReporte.builder().nombre("Manuel").apellidoMaterno("Pérez").apellidoPaterno("Vázquez").curp("PEVM711217HDFRZN04").telefono("5526838276").build());

        try {
            FastReportBuilder drb = new FastReportBuilder();

            AbstractColumn columnNombre = ColumnBuilder.getNew()
                    .setColumnProperty("nombre", String.class.getName()).setTitle(
                            "Nombre").setWidth(10).build();
            drb.addColumn(columnNombre);

            AbstractColumn columnApellidoPaterno = ColumnBuilder.getNew()
                    .setColumnProperty("apellidoPaterno", String.class.getName()).setTitle(
                            "Apellido Paterno").setWidth(30).build();
            drb.addColumn(columnApellidoPaterno);

            drb.setUseFullPageWidth(true)
                    .setIgnorePagination(false)
                    .setWhenNoData("No hay nada que mostrar", null, true, true);

            DynamicReport dr = drb.build();
            Map<String, Object> params = new HashMap<>();
            JRDataSource ds = new JRBeanCollectionDataSource(personaReporteList);
            JasperReport jr = DynamicJasperHelper.generateJasperReport(dr, new ClassicLayoutManager(), params);
            var jasperprintResult = JasperFillManager.fillReport(jr, params, ds);
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            result = Either.right(exportPdfReport(configuration,jasperprintResult));
        } catch (JRException e) {
            e.printStackTrace();
        }


        return result;
    }

    public static String exportPdfReport(SimplePdfExporterConfiguration configuration, JasperPrint jasperPrint) throws JRException {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        if (configuration != null) {
            exporter.setConfiguration(configuration);
        }
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint)); // set compiled report as input
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
        exporter.exportReport();
        return Base64.getEncoder().encodeToString(outputStream.toByteArray());
    }
}
