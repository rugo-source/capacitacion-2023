package org.acme.spring.data.reports.external.rest.controllers;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.acme.spring.data.reports.core.business.input.ReportesServices;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/reportes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Reportes")
public class ReportesController {

    @Inject
    ReportesServices reportesServices;

    @GET
    @Path("ejemplo-uno")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = String.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response getReporteEjemploUno() {
        var jsonParse = new JsonObject();
        return reportesServices.getReporteEjemploUno().map(string -> {
            jsonParse.addProperty("file",string);
            return jsonParse.toString();
                }).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }


}
