package org.acme.spring.data.reports.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.util.error.ErrorCodes;

public interface ReportesServices {
    Either<ErrorCodes, String> getReporteEjemploUno();
}
