package org.acme.spring.data.reports.core.entity;


import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PersonaReporte {
    String nombre;
    String apellidoPaterno;
    String apellidoMaterno;
    String curp;
    String telefono;
}
