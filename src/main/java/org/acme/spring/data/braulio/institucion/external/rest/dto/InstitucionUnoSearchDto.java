package org.acme.spring.data.braulio.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.braulio.institucion.core.entity.InstitucionUnoSearch;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "InstitucionUnoSearchDto", description = "Esta en la entidad para buscar por identificador de la institucion ,el nombre y acronimo de la misma")
public class InstitucionUnoSearchDto {
    @JsonProperty
    @Size(min = 0, max = 4, message = "RNS002")
    @Schema(description = "este es el atributo que referencia al identificador")
    private String identificador;
    @JsonProperty
    @Schema(description = "este es el atributo que referencia al nombre")
    private String nombre;
    @JsonProperty
    @Size(min = 0, max = 20, message = "RNS002")
    @Schema(description = "este es el atributo que referencia al acronimo")
    private String acronimo;

    public InstitucionUnoSearch toEntity(){
        return InstitucionUnoSearch.builder()
                .identificador(this.identificador)
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .build();
    }

}
