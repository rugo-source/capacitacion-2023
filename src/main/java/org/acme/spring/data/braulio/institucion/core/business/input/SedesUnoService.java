package org.acme.spring.data.braulio.institucion.core.business.input;

import io.netty.util.AsyncMapping;
import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.entity.SedeUnoCreate;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface SedesUnoService {

    Either<ErrorCodes, List<Sede>> get(Integer idInstitucion);

    Either<ErrorCodes, Boolean> create(Integer idInstitucion, SedeUnoCreate sedeUnoCreate);

    Either<ErrorCodes, Boolean> update(Integer id, SedeUnoCreate sedeUnoCreate);

    Either<ErrorCodes, Boolean> delete(Integer id);

}
