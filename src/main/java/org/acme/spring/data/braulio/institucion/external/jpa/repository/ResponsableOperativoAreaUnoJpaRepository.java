package org.acme.spring.data.braulio.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.ResponsableOperativoAreaJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResponsableOperativoAreaUnoJpaRepository extends JpaRepository<ResponsableOperativoAreaJpa,Integer> {
    Boolean existsByIdAreaContratante(Integer id);
}
