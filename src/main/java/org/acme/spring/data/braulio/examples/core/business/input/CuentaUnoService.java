package org.acme.spring.data.braulio.examples.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.examples.core.entity.CuentaUno;
import org.acme.spring.data.braulio.examples.core.entity.CuentaUnoCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface CuentaUnoService {
    Either<ErrorCodes, String> create(CuentaUnoCreate cuentaUnoCreate);

    Either<ErrorCodes, String> update(Integer id, CuentaUnoCreate cuentaUnoCreate);

    Either<ErrorCodes, Boolean> delete(Integer id);

    Either<ErrorCodes, List<CuentaUno>> get(Integer idPersona);
}
