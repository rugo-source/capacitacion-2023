package org.acme.spring.data.braulio.institucion.external.jpa.dao;

import org.acme.spring.data.braulio.institucion.core.business.output.GremioUnoRepository;
import org.acme.spring.data.braulio.institucion.core.entity.GremioAsociasionUnoCreate;
import org.acme.spring.data.braulio.institucion.external.jpa.repository.GremioAsociasionUnoJpaRepository;
import org.acme.spring.data.braulio.institucion.external.jpa.repository.GremioUnoJpaRepository;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.GremioJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class GremioUnoDao implements GremioUnoRepository {
    @Inject
    GremioUnoJpaRepository gremioUnoJpaRepository;
    @Inject
    GremioAsociasionUnoJpaRepository gremioAsociasionUnoJpaRepository;

    @Override
    public List<Gremio> findAll() {
        return gremioUnoJpaRepository.findAll().stream().map(GremioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Gremio gremioPersist) {
        gremioUnoJpaRepository.saveAndFlush(GremioJpa.fromEntity(gremioPersist));
    }

    @Override
    public Optional<Gremio> findById(Integer id) {
        return gremioUnoJpaRepository.findById(id).map(GremioJpa::toEntity);
    }

    @Override
    public void update(Gremio gremioChange) {
        gremioUnoJpaRepository.saveAndFlush(GremioJpa.fromEntity(gremioChange));
    }

    @Override
    public void delete(Integer id) {
        gremioUnoJpaRepository.deleteById(id);
    }

    @Override
    public Boolean existByNombreOrAcronimo(String nombre, String acronimo) {
        return gremioUnoJpaRepository.existsByNombreOrAcronimo(nombre, acronimo);
    }

    @Override
    public void saveAsociasion(GremioAsociasionUnoCreate gremioAsociasionPersit) {
        var miembroGremioJpa = MiembroGremioJpa.builder()
                .id(MiembroGremioIdJpa.builder()
                        .idGremio(gremioAsociasionPersit.getIdGremio())
                        .idInstitucion(gremioAsociasionPersit.getIdInstitucion())
                        .build())
                .build();
        gremioAsociasionUnoJpaRepository.saveAndFlush(miembroGremioJpa);
    }
}
