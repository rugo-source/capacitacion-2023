package org.acme.spring.data.braulio.examples.core.entity;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class CuentaUno {
    private Integer id;
    private Integer idPersona;
    private String rol;
    private LocalDate inicio;
    private LocalDate fin;
    private PersonaUno personaUno;
}
