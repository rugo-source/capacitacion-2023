package org.acme.spring.data.braulio.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.business.input.AreasContratantesUnoService;
import org.acme.spring.data.braulio.institucion.core.business.output.AreasContratantesUnoRepository;
import org.acme.spring.data.braulio.institucion.core.entity.AreaContratanteUnoCreate;
import org.acme.spring.data.braulio.institucion.core.business.output.InstitucionUnoRepository;
import org.acme.spring.data.braulio.institucion.core.business.output.ResponsableOperativoAreaUnoRepository;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class AreasContratantesUnoBs implements AreasContratantesUnoService {
    @Inject
    AreasContratantesUnoRepository areasContratantesUnoRepository;
    @Inject
    InstitucionUnoRepository institucionUnoRepository;
    @Inject
    ResponsableOperativoAreaUnoRepository responsableOperativoAreaUnoRepository;


    @Override
    public Either<ErrorCodes, List<AreaContratante>> get(Integer idInstitucion) {
        Either<ErrorCodes, List<AreaContratante>> result = Either.left(ErrorCodes.NOT_FOUND);
        if (institucionUnoRepository.existById(idInstitucion)) {
            var areasContrantantes = areasContratantesUnoRepository.findAllByIdInstitucion(idInstitucion);
            result = Either.right(areasContrantantes);
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(Integer idInstitucion, AreaContratanteUnoCreate areaContratanteUnoCreate) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        if (institucionUnoRepository.existById(idInstitucion)) {
            var areaContratantePersists = AreaContratante.builder()
                    .area(areaContratanteUnoCreate.getArea())
                    .subarea(areaContratanteUnoCreate.getSubArea())
                    .telefono(areaContratanteUnoCreate.getTelefono())
                    .extension(areaContratanteUnoCreate.getExtension())
                    .idInstitucion(idInstitucion)
                    .build();
            areasContratantesUnoRepository.save(areaContratantePersists);
            result = Either.right(true);
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer id, AreaContratanteUnoCreate areaContratanteUnoCreate) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var areaContratante = areasContratantesUnoRepository.findById(id);
        if (areaContratante.isPresent()) {
            var areaContratanteChange = areaContratante.get();
            areaContratanteChange.setArea(areaContratanteUnoCreate.getArea());
            areaContratanteChange.setSubarea(areaContratanteUnoCreate.getSubArea());
            areaContratanteChange.setTelefono(areaContratanteUnoCreate.getTelefono());
            areaContratanteChange.setExtension(areaContratanteUnoCreate.getExtension());

            areasContratantesUnoRepository.update(areaContratanteChange);
            result = Either.right(true);
        }

        return result;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer id) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var areaContratante = areasContratantesUnoRepository.findById(id);
        if (areaContratante.isPresent()) {
            if (validateRNN023(areaContratante.get().getId())) {
                result = Either.left(ErrorCodes.RNN023);
            } else {
                areasContratantesUnoRepository.deleteBy(id);
                result = Either.right(true);
            }
        }
        return result;
    }

    public Boolean validateRNN023(Integer idAreaContratante) {
        return responsableOperativoAreaUnoRepository.existsByIdAreaContratante(idAreaContratante);
    }
}
