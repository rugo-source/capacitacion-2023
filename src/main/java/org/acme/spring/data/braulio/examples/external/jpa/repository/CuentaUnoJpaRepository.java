package org.acme.spring.data.braulio.examples.external.jpa.repository;

import org.acme.spring.data.braulio.examples.external.jpa.model.CuentaUnoJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CuentaUnoJpaRepository  extends JpaRepository<CuentaUnoJpa, Integer> {
    @Query("from CuentaUnoJpa where inicio <= current_date and fin >= current_date and idPersona = :idPersona")
    List<CuentaUnoJpa> findAllByIdPersona(@Param("idPersona") Integer idPersona);
}
