package org.acme.spring.data.braulio.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.braulio.examples.core.entity.PersonaUno;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonaUnoDTO {
    @JsonProperty
    @Schema(description = "este es el atributo que referencia al nombre")
    private String nombre;
    @JsonProperty
    @Schema(description = "este es el atributo de edad")
    private Integer edad;
    @JsonProperty
    @Schema(description = "este es el atributo para saber si tiene la bandera para registrar")
    private Boolean registrar;
    @JsonProperty
    @Schema(description = "este es el atributo para saber si tiene la bandera para editar")
    private Boolean editar;
    @JsonProperty
    @Schema(description = "este es el atributo para saber si tiene la bandera para eliminar")
    private Boolean eliminar;
    @JsonProperty
    @Schema(description = "este es el atributo para saber si tiene la bandera para consultar")
    private Boolean consultar;
    @JsonProperty
    @Schema(description = "este es el atributo para saber si tiene la bandera para configurar")
    private Boolean configurar;

    public static PersonaUnoDTO fromEntity(PersonaUno personaUno) {
        return PersonaUnoDTO.builder().
                nombre(personaUno.getNombre())
                .edad(personaUno.getEdad())
                .registrar(personaUno.getRegistrar())
                .editar(personaUno.getEditar())
                .consultar(personaUno.getConsultar())
                .configurar(personaUno.getConfigurar())
                .build();
    }
}
