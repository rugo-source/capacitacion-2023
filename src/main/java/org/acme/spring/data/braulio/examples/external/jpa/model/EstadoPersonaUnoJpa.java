package org.acme.spring.data.braulio.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.braulio.examples.core.entity.EstadoPersonaUno;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "estado_persona_uno")
public class EstadoPersonaUnoJpa {
    @Id
    @Column(name = "id_estado")
    private Integer id;

    @Column(name = "tx_nombre")
    private String nombre;

    public EstadoPersonaUno toEntity() {
        return EstadoPersonaUno.builder().id(this.id).nombre(this.nombre).build();
    }
}
