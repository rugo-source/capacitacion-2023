package org.acme.spring.data.braulio.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.business.input.SedesUnoService;
import org.acme.spring.data.braulio.institucion.core.business.output.EdificiosUnoRepository;
import org.acme.spring.data.braulio.institucion.core.business.output.InstitucionUnoRepository;
import org.acme.spring.data.braulio.institucion.core.business.output.SedeUnoRepository;
import org.acme.spring.data.braulio.institucion.core.entity.SedeUnoCreate;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class SedesUnoBs implements SedesUnoService {
    @Inject
    InstitucionUnoRepository institucionUnoRepository;
    @Inject
    SedeUnoRepository sedeUnoRepository;
    @Inject
    EdificiosUnoRepository edificiosUnoRepository;

    @Override
    public Either<ErrorCodes, List<Sede>> get(Integer idInstitucion) {
        Either<ErrorCodes, List<Sede>> result = Either.left(ErrorCodes.NOT_FOUND);
        var institucionSearch = institucionUnoRepository.existById(idInstitucion);
        if (institucionSearch) {
            var sedes = sedeUnoRepository.findAllByIdInstitucion(idInstitucion);
            result = Either.right(sedes);
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(Integer idInstitucion, SedeUnoCreate sedeUnoCreate) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var institucionSearch = institucionUnoRepository.existById(idInstitucion);
        if (institucionSearch) {
            if (ValidateRNN096(sedeUnoCreate.getNombre(), sedeUnoCreate.getAcronimo())) {
                result = Either.left(ErrorCodes.RNN096);
            } else {

                var sedePersist = Sede.builder()
                        .idInstitucion(idInstitucion)
                        .nombre(sedeUnoCreate.getNombre())
                        .acronimo(sedeUnoCreate.getAcronimo())
                        .capacidad(sedeUnoCreate.getCapacidad())
                        .propia(sedeUnoCreate.getPropia())
                        .identificador(generateIdentificador(idInstitucion))
                        .build();
                sedeUnoRepository.save(sedePersist);
                result = Either.right(true);
            }
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer id, SedeUnoCreate sedeUnoCreate) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var sedeSearch = sedeUnoRepository.findById(id);
        if (sedeSearch.isPresent()) {
            if (ValidateRNN096(sedeUnoCreate.getNombre(), sedeUnoCreate.getAcronimo())) {
                result = Either.left(ErrorCodes.RNN096);
            } else {
                var sedeChange = sedeSearch.get();
                sedeChange.setNombre(sedeUnoCreate.getNombre());
                sedeChange.setAcronimo(sedeUnoCreate.getAcronimo());
                sedeChange.setCapacidad(sedeUnoCreate.getCapacidad());
                sedeChange.setPropia(sedeUnoCreate.getPropia());
                sedeUnoRepository.update(sedeChange);
                result = Either.right(true);
            }
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> delete(Integer id) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var sedeSearch = sedeUnoRepository.existById(id);
        if (sedeSearch) {
            if (validateRNN016(id)){
                result = Either.left(ErrorCodes.RNN016);
            }else{
                sedeUnoRepository.deleteById(id);
                result = Either.right(true);
            }
        }
        return result;
    }



    public Boolean ValidateRNN096(String nombre, String acronimo) {
        return sedeUnoRepository.existByNombreOrAcronimo(nombre, acronimo);
    }

    public String generateIdentificador(Integer idInstitucion) {
        StringBuilder result = new StringBuilder();
        var identificadores = sedeUnoRepository.findAllIdentificadores();
        if (identificadores.isEmpty()) {
            var identificador = String.format("%03d", 1);
            result.append(idInstitucion);
            result.append(identificador);
        } else {
            var identificador = String.format("%03d", Integer.parseInt(identificadores.get(0)) + 1);
            result.append(idInstitucion);
            result.append(identificador);
        }
        return result.toString();
    }

    public Boolean validateRNN016(Integer idSede){
        return edificiosUnoRepository.existsByIdSede(idSede);
    }
}
