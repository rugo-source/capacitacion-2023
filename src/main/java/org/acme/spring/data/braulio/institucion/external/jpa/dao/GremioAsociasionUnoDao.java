package org.acme.spring.data.braulio.institucion.external.jpa.dao;

import org.acme.spring.data.braulio.institucion.core.business.output.GremioAsociasionUnoRepository;
import org.acme.spring.data.braulio.institucion.core.entity.GremioAsociasionUnoCreate;
import org.acme.spring.data.braulio.institucion.external.jpa.repository.GremioAsociasionUnoJpaRepository;
import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Optional;

@ApplicationScoped
public class GremioAsociasionUnoDao implements GremioAsociasionUnoRepository {
    public static final String QUERY_BUSCAR_INSTITUCIONES_CON_GREMIO = "select count(tin06.fk_id_institucion) > 0 from tin06_gremio_institucion tin06 where tin06.fk_id_institucion = :idInstitucion";

    @Inject
    GremioAsociasionUnoJpaRepository gremioAsociasionUnoJpaRepository;
    @Inject
    EntityManager entityManager;

    @Override
    public Optional<MiembroGremio> findById(GremioAsociasionUnoCreate gremioAsociasionUnoCreate) {
        return gremioAsociasionUnoJpaRepository.findById(MiembroGremioIdJpa.builder()
                .idGremio(gremioAsociasionUnoCreate.getIdGremio())
                .idInstitucion(gremioAsociasionUnoCreate.getIdInstitucion())
                .build()).map(MiembroGremioJpa::toEntity);
    }

    @Override
    public void delete(MiembroGremio miembroGremio) {
        gremioAsociasionUnoJpaRepository.deleteById(MiembroGremioIdJpa.builder()
                .idGremio(miembroGremio.getIdGremio())
                .idInstitucion(miembroGremio.getIdInstitucion())
                .build());
    }

    @Override
    public Boolean existByIdInstitucion(Integer idInstitucion) {
        Query q = entityManager.createNativeQuery(QUERY_BUSCAR_INSTITUCIONES_CON_GREMIO);
        q.setParameter("idInstitucion", idInstitucion);
        Boolean result = ((Boolean) q.getSingleResult()).booleanValue();
        return result;
    }
}
