package org.acme.spring.data.braulio.institucion.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EdificioUnoCreate {
    private Integer idSede;
    private String nombre;
    private String acronimo;
    private String referencia;
}

