package org.acme.spring.data.braulio.institucion.external.jpa.dao;

import org.acme.spring.data.braulio.institucion.core.business.output.ResponsableOperativoAreaUnoRepository;
import org.acme.spring.data.braulio.institucion.external.jpa.repository.ResponsableOperativoAreaUnoJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ResponsableOperativoAreaUnoDao implements ResponsableOperativoAreaUnoRepository {
    @Inject
    ResponsableOperativoAreaUnoJpaRepository responsableOperativoAreaUnoJpaRepository;

    @Override
    public Boolean existsByIdAreaContratante(Integer id) {
        return responsableOperativoAreaUnoJpaRepository.existsByIdAreaContratante(id);
    }
}
