package org.acme.spring.data.braulio.examples.external.jpa.dao;

import org.acme.spring.data.braulio.examples.core.business.output.PersonaUnoRepository;
import org.acme.spring.data.braulio.examples.core.entity.PersonaUno;
import org.acme.spring.data.braulio.examples.external.jpa.model.PersonaUnoJpa;
import org.acme.spring.data.braulio.examples.external.jpa.repository.PersonaUnoJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class PersonaUnoDao implements PersonaUnoRepository {
    @Inject
    PersonaUnoJpaRepository personaUnoJpaRepository;

    @Override
    public List<PersonaUno> findAll() {
        return personaUnoJpaRepository.findAll().stream().map(personaUnoJpa -> {
            var personaEntidad = personaUnoJpa.toEntity();
            personaEntidad.setEstadoPersonaUno(personaUnoJpa.getEstadoPersonaUnoJpa().toEntity());
            return personaEntidad;
        }).collect(Collectors.toList());
    }

    @Override
    public List<PersonaUno> findAllNative() {
        return null;
    }

    @Override
    public List<PersonaUno> findAllOrm() {
        return null;
    }

    @Override
    public Optional<PersonaUno> findBy(Integer idPersona) {
        return personaUnoJpaRepository.findById(idPersona).map(personaUnoJpa -> {
            var personaEntidad = personaUnoJpa.toEntity();
            personaEntidad.setEstadoPersonaUno(personaUnoJpa.getEstadoPersonaUnoJpa().toEntity());
            return personaEntidad;
        });
    }

    @Override
    public void save(PersonaUno personaUnoPersists) {
        personaUnoJpaRepository.saveAndFlush(PersonaUnoJpa.fromEntity(personaUnoPersists));

    }

    @Override
    public void update(PersonaUno personaUnoChange) {
        personaUnoJpaRepository.saveAndFlush(PersonaUnoJpa.fromEntity(personaUnoChange));
    }

    @Override
    public void deleteBy(Integer id) {
        personaUnoJpaRepository.deleteById(id);
    }

}