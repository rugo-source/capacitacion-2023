package org.acme.spring.data.braulio.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AulaUnoCreate {
    private Integer id;
    private Integer idEdificio;
    private Integer idTipoEquipamiento;
    private String nombre;
    private Integer cantidadEquipamiento;
    private Integer capacidadEquipamiento;
    private String observaciones;
    private Integer capacidad;
    private String nombreEquipamiento;
}
