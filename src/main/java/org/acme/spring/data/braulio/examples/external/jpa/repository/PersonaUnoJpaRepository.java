package org.acme.spring.data.braulio.examples.external.jpa.repository;

import org.acme.spring.data.braulio.examples.external.jpa.model.PersonaUnoJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaUnoJpaRepository extends JpaRepository<PersonaUnoJpa, Integer> {
}
