package org.acme.spring.data.braulio.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.business.input.EdificiosUnoServices;
import org.acme.spring.data.braulio.institucion.core.business.output.EdificiosUnoRepository;
import org.acme.spring.data.braulio.institucion.core.business.output.SedeUnoRepository;
import org.acme.spring.data.braulio.institucion.core.entity.EdificioUnoCreate;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class EdificiosUnoBs implements EdificiosUnoServices {
    @Inject
    EdificiosUnoRepository edificiosUnoRepository;
    @Inject
    SedeUnoRepository sedeUnoRepository;

    @Override
    public Either<ErrorCodes, List<Edificio>> getEdificiosByIdSede(Integer idSede) {
        Either<ErrorCodes, List<Edificio>> result = Either.left(ErrorCodes.NOT_FOUND);
        var sedeSearch = sedeUnoRepository.existById(idSede);
        if (sedeSearch) {
            var edificios = edificiosUnoRepository.findAllByIdSede(idSede);
            result = Either.right(edificios);
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(EdificioUnoCreate edificioUnoCreate) {
        Either<ErrorCodes, Boolean> result;
        if (validateRNN002(edificioUnoCreate.getNombre(), edificioUnoCreate.getAcronimo(), edificioUnoCreate.getIdSede())) {
            result = Either.left(ErrorCodes.RNN002);
        } else {
            var edificioPersist = Edificio.builder()
                    .idSede(edificioUnoCreate.getIdSede())
                    .acronimo(edificioUnoCreate.getAcronimo())
                    .nombre(edificioUnoCreate.getNombre())
                    .referencia(edificioUnoCreate.getReferencia())
                    .build();
            edificiosUnoRepository.save(edificioPersist);
            result = Either.right(true);
        }
        return result;
    }


    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer id, EdificioUnoCreate edificioUnoCreate) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);

        var edificioSearch = edificiosUnoRepository.findById(id);

        if (edificioSearch.isPresent()) {
            if (validateRNN002(edificioUnoCreate.getNombre(), edificioUnoCreate.getAcronimo(), edificioUnoCreate.getIdSede())) {
                result = Either.left(ErrorCodes.RNN002);
            } else {
                var edificioChange = edificioSearch.get();
                edificioChange.setNombre(edificioUnoCreate.getNombre());
                edificioChange.setAcronimo(edificioUnoCreate.getAcronimo());
                edificioChange.setReferencia(edificioUnoCreate.getReferencia());
                edificiosUnoRepository.update(edificioChange);
                result = Either.right(true);
            }
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> delete(Integer id) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var edificio = edificiosUnoRepository.existById(id);
        if (edificio) {
            edificiosUnoRepository.deleteById(id);
            result = Either.right(true);
        }
        return result;
    }

    public Boolean validateRNN002(String nombre, String acronimo, Integer idSede) {
        return edificiosUnoRepository.existByNombreAndAcronimoAndIdSede(nombre, acronimo, idSede);


    }
}