package org.acme.spring.data.braulio.examples.core.business.output;

import org.acme.spring.data.braulio.examples.core.entity.PersonaUno;

import java.util.List;
import java.util.Optional;

public interface PersonaUnoRepository {
    List<PersonaUno> findAll();
    List<PersonaUno> findAllNative();
    List<PersonaUno> findAllOrm();
    Optional<PersonaUno> findBy(Integer idPersona);

    void save(PersonaUno personaUnoPersists);
    void update(PersonaUno personaUnoChange);
    void deleteBy(Integer id);

}
