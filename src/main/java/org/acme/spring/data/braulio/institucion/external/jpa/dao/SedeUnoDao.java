package org.acme.spring.data.braulio.institucion.external.jpa.dao;

import org.acme.spring.data.braulio.institucion.core.business.output.SedeUnoRepository;
import org.acme.spring.data.braulio.institucion.external.jpa.repository.SedeUnoJpaRepository;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class SedeUnoDao implements SedeUnoRepository {
    private static final String QUERY_BUSCAR_ULTIMO_IDENTIFICADOR= "select substring( tx_identificador, LENGTH(tx_identificador)-2, LENGTH(tx_identificador)) identificador from tin05_sede order by identificador desc";
    @Inject
    SedeUnoJpaRepository sedeUnoJpaRepository;
    @Inject
    EntityManager entityManager;

    @Override
    public List<Sede> findAllByIdInstitucion(Integer idInstitucion) {
        return sedeUnoJpaRepository.findAllByIdInstitucion(idInstitucion)
                .stream().map(SedeJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Sede sedePersist) {
        sedeUnoJpaRepository.saveAndFlush(SedeJpa.fromEntity(sedePersist));

    }

    @Override
    public Optional<Sede> findById(Integer id) {
        return sedeUnoJpaRepository.findById(id).map(SedeJpa::toEntity);
    }

    @Override
    public void update(Sede sedeChange) {
        sedeUnoJpaRepository.saveAndFlush(SedeJpa.fromEntity(sedeChange));
    }

    @Override
    public Boolean existById(Integer id) {
        return sedeUnoJpaRepository.existsById(id);
    }

    @Override
    public void deleteById(Integer id) {
        sedeUnoJpaRepository.deleteById(id);
    }

    @Override
    public Boolean existByNombreOrAcronimo(String nombre, String acronimo) {
        return sedeUnoJpaRepository.existsByNombreOrAcronimo(nombre,acronimo);
    }

    @Override
    public List<String> findAllIdentificadores() {
        Query query = entityManager.createNativeQuery(QUERY_BUSCAR_ULTIMO_IDENTIFICADOR);
        return query.getResultList();
    }


}
