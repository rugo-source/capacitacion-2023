package org.acme.spring.data.braulio.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.braulio.examples.core.entity.CuentaUno;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "cuenta_uno")
public class CuentaUnoJpa {
    @Id
    @Column(name = "id_cuenta")
    @SequenceGenerator(name = "cuenta_uno_id_cuenta_seq", sequenceName = "cuenta_uno_id_cuenta_seq", allocationSize = 1)
    @GeneratedValue(generator = "cuenta_uno_id_cuenta_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "fk_id_persona")
    private Integer idPersona;
    @Column(name = "tx_rol")
    private String rol;
    @Column(name = "fh_inicio")
    private LocalDate inicio;
    @Column(name = "fh_fin")
    private LocalDate fin;
    @ManyToOne
    @JoinColumn(name = "fk_id_persona", referencedColumnName = "id_persona", insertable = false, updatable = false)
    private PersonaUnoJpa personaUnoJpa;

    public static CuentaUnoJpa fromEntity(CuentaUno cuentaUno){
        return CuentaUnoJpa.builder().id(cuentaUno.getId())
                .idPersona(cuentaUno.getIdPersona())
                .rol(cuentaUno.getRol())
                .inicio(cuentaUno.getInicio())
                .fin(cuentaUno.getFin())
                .build();
    }

    public CuentaUno toEntity() {
        return CuentaUno.builder().id(this.id)
                .idPersona(this.idPersona)
                .rol(this.rol)
                .inicio(this.inicio)
                .fin(this.fin)
                .build();

    }
}
