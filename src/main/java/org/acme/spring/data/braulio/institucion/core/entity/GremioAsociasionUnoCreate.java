package org.acme.spring.data.braulio.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GremioAsociasionUnoCreate {
    private Integer idGremio;
    private Integer idInstitucion;
}
