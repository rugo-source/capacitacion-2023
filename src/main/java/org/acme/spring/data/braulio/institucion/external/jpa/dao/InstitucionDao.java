package org.acme.spring.data.braulio.institucion.external.jpa.dao;

import org.acme.spring.data.braulio.institucion.core.business.output.InstitucionUnoRepository;
import org.acme.spring.data.braulio.institucion.core.entity.InstitucionUnoSearch;
import org.acme.spring.data.braulio.institucion.external.jpa.repository.InstitucionUnoJpaRepository;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@ApplicationScoped
public class InstitucionDao implements InstitucionUnoRepository {
    @Inject
    InstitucionUnoJpaRepository institucionUnoJpaRepository;
    @Inject
    EntityManager entityManager;

    @Override
    public List<Institucion> findByArguments(InstitucionUnoSearch institucionUnoSearch) {
        return institucionUnoJpaRepository.findAllByIdentificadorOrNombreOrAcronimo(institucionUnoSearch.getIdentificador(), institucionUnoSearch.getNombre(), institucionUnoSearch.getAcronimo()).stream().map(InstitucionJpa::toEntity).collect(Collectors.toList());
    }


    /*Aquie Braulio declara antes de tus injects una public final static string con tu query, esto solo con el fin de estructurerar y tener ubicado donde estan los queries
    * esto mismo aplicalo cuando tengas parametros declaralos igual que los queries por si en un momento ocupas ese mismo parametro en otro query solo llames a tu constante
    * y asi te evitas escribirla otra vez
    * */
    @Override
    public List<Institucion> findAllSinEjecutivo() {
        Stream<InstitucionJpa> result = entityManager.createNativeQuery("select distinct tin01.* from tin01_institucion tin01\n" +
                "left join tin04_ejecutivo tin04 on tin04.id_institucion=tin01.id_institucion\n" +
                "where tin04.id_ejecutivo is null", InstitucionJpa.class).getResultStream();
        return result.map(InstitucionJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Institucion institucionUnoCreate) {
        institucionUnoJpaRepository.saveAndFlush(InstitucionJpa.fromEntity(institucionUnoCreate));
    }

    @Override
    public Optional<Institucion> findById(Integer id) {
        return institucionUnoJpaRepository.findById(id).map(institucionJpa -> {
            var institucionEntidad = institucionJpa.toEntity();
            return institucionEntidad;
        });
    }

    @Override
    public void update(Institucion institucionChange) {
        institucionUnoJpaRepository.saveAndFlush(InstitucionJpa.fromEntity(institucionChange));
    }

    @Override
    public Boolean existsByNombre(String nombre) {
        return institucionUnoJpaRepository.existsByNombre(nombre);
    }

    @Override
    public Boolean existByIdentidicador(String identificador) {
        return institucionUnoJpaRepository.existsByIdentificador(identificador);
    }

    @Override
    public Boolean existById(Integer id) {
        return institucionUnoJpaRepository.existsById(id);
    }


}
