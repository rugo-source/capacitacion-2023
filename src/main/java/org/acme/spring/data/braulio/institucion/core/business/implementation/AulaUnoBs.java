package org.acme.spring.data.braulio.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.business.input.AulaUnoService;
import org.acme.spring.data.braulio.institucion.core.business.output.AulaUnoRepository;

import org.acme.spring.data.braulio.institucion.core.business.output.EdificiosUnoRepository;
import org.acme.spring.data.braulio.institucion.core.entity.AulaUnoCreate;

import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class AulaUnoBs implements AulaUnoService {
    @Inject
    AulaUnoRepository aulaUnoRepository;

    @Inject
    EdificiosUnoRepository edificiosUnoRepository;

    @Override
    public Either<ErrorCodes, List<Aula>> getAulas(Integer idEdificio) {
        Either<ErrorCodes, List<Aula>> result = Either.left(ErrorCodes.NOT_FOUND);
        if (edificiosUnoRepository.existById(idEdificio)) {
            var aulasSearch = aulaUnoRepository.findAllByIdEdificio(idEdificio);
            if (!aulasSearch.isEmpty()) {
                var aulas = validateRNN129(aulasSearch);
                result = Either.right(aulas);
            }

        }

        return result;
    }

    @Override
    public Either<ErrorCodes, Boolean> create(AulaUnoCreate aulaUnoCreate) {
        Either<ErrorCodes, Boolean> result;

        var aulaPersist = Aula.builder()
                .idEdificio(aulaUnoCreate.getIdEdificio())
                .nombre(aulaUnoCreate.getNombre())
                .idTipoEquipamiento(aulaUnoCreate.getIdTipoEquipamiento())
                .cantidadEquipamiento(aulaUnoCreate.getCantidadEquipamiento())
                .capacidadEquipamiento(aulaUnoCreate.getCapacidadEquipamiento())
                .observaciones(aulaUnoCreate.getObservaciones())
                .build();
        aulaUnoRepository.save(aulaPersist);
        result = Either.right(true);
        return result;
    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer id, AulaUnoCreate aulaUnoCreate) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var aulaSearch = aulaUnoRepository.findById(id);
        if (aulaSearch.isPresent()) {
            var aulaChange = aulaSearch.get();
            aulaChange.setNombre(aulaUnoCreate.getNombre());
            aulaChange.setIdTipoEquipamiento(aulaUnoCreate.getIdTipoEquipamiento());
            aulaChange.setCapacidad(aulaUnoCreate.getCapacidad());
            aulaChange.setCantidadEquipamiento(aulaUnoCreate.getCantidadEquipamiento());
            aulaChange.setCapacidadEquipamiento(aulaUnoCreate.getCapacidadEquipamiento());
            aulaChange.setObservaciones(aulaUnoCreate.getObservaciones());

            aulaUnoRepository.update(aulaChange);
            result = Either.right(true);
        }
        return result;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer id) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);

        var aulaSearch = aulaUnoRepository.existById(id);
        if (aulaSearch) {
            aulaUnoRepository.delete(id);
            result = Either.right(true);
        }
        return result;
    }

    public List<Aula> validateRNN129(List<Aula> aulas) {
        return aulas.stream().map(aula -> {
            aula.setCapacidad(aula.getCapacidadEquipamiento() * aula.getCantidadEquipamiento());
            return aula;
        }).collect(Collectors.toList());

    }

   public Boolean validateRNN009(String nombre, Integer idEdificio){
        Boolean result = false;
        var aulas = aulaUnoRepository.findAllByNombre(nombre);
        var edificioSearch = edificiosUnoRepository.findById(idEdificio);
        if(aulas.isEmpty()){
            result = false;
        }
        else {
            for (Aula aula:aulas) {
                var edificioCheck = edificiosUnoRepository.findById(aula.getIdEdificio());
                if (edificioSearch.get().getIdSede().equals(edificioCheck.get().getIdSede())){
                    result = true;
                    break;
                }
            }
        }
       return result;
   }

}
