package org.acme.spring.data.braulio.institucion.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.entity.EdificioUnoCreate;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface EdificiosUnoServices {
    Either<ErrorCodes, List<Edificio>> getEdificiosByIdSede(Integer idSede);

    Either<ErrorCodes, Boolean> create(EdificioUnoCreate edificioUnoCreate);

    Either<ErrorCodes, Boolean> update(Integer id, EdificioUnoCreate edificioUnoCreate);

    Either<ErrorCodes, Boolean> delete(Integer id);
}
