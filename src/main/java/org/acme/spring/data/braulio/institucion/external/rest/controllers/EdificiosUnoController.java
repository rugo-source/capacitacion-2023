package org.acme.spring.data.braulio.institucion.external.rest.controllers;


import org.acme.spring.data.braulio.institucion.core.business.input.EdificiosUnoServices;
import org.acme.spring.data.braulio.institucion.external.rest.dto.EdificiosUnoCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Tag(name = " Catalogo de Institución - Braulio")
@Path("/catalogos-instituciones/edificios-uno")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EdificiosUnoController {

    @Inject
    EdificiosUnoServices edificiosUnoServices;

    @GET
    @Path("{idSede}")
    @APIResponse(responseCode = "200", description = "Petici贸n exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Petici贸n exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response get(@PathParam("idSede") Integer idSede) {
        return edificiosUnoServices.getEdificiosByIdSede(idSede).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }

    @POST
    @Path("")
    @APIResponse(responseCode = "200", description = "Petici贸n exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "404", description = "Petici贸n exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid EdificiosUnoCreateDto edificiosUnoCreateDto) {
        return edificiosUnoServices.create(edificiosUnoCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();

    }

    @PUT
    @Path("{id}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("id") Integer id, @Valid EdificiosUnoCreateDto edificiosUnoCreateDto) {
        return edificiosUnoServices.update(id, edificiosUnoCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("{id}")
    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("id") Integer id) {
        return edificiosUnoServices.delete(id).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
