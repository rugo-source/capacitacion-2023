package org.acme.spring.data.braulio.institucion.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.entity.InstitucionUnoCreate;
import org.acme.spring.data.braulio.institucion.core.entity.InstitucionUnoSearch;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface InstitucionUnoService {
    Either<ErrorCodes, List<Institucion>> getByAcronimo(InstitucionUnoSearch institucionUnoSearch);


    List<Institucion> get();

    Either<ErrorCodes, Boolean> create(InstitucionUnoCreate institucionUnoCreate);

    Either<ErrorCodes, Boolean> update(Integer id, InstitucionUnoCreate toEntity);
}
