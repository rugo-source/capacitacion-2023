package org.acme.spring.data.braulio.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.braulio.examples.core.entity.PersonaUnoCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "PersonaUnoCreate", description = "Esta en la entidad para guardar en persona Uno")

public class PersonaUnoCreateDto {
    @JsonProperty
    @Schema(description = "este es el atributo que referencia al nombre")
    private String nombre;
    @JsonProperty
    @Schema(description = "este es el atributo de edad")
    private Integer edad;

    public PersonaUnoCreate toEntity(){
        return PersonaUnoCreate.builder()
                .nombre(this.nombre)
                .edad(this.edad)
                .build();
    }
}
