package org.acme.spring.data.braulio.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EdificiosUnoJpaRepository extends JpaRepository<EdificioJpa, Integer> {
    List<EdificioJpa> findAllByIdSede(Integer idSede);

    Boolean existsByIdSede(Integer idSede);

    Boolean existsByNombreAndAcronimoAndIdSede(String nombre, String acronimo, Integer idSede);

    List<EdificioJpa> findAllById(Integer idEdificio);

}
