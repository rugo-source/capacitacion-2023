package org.acme.spring.data.braulio.institucion.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Aula;

import java.util.List;
import java.util.Optional;

public interface AulaUnoRepository {
    List<Aula> findAllByIdEdificio(Integer idEdificio);

    void save(Aula aulaPersist);

    Optional<Aula> findById(Integer id);

    void update(Aula aulaChange);

    Boolean existById(Integer id);

    void delete(Integer id);

    List<Aula> findAllByNombre(String nombre);

}
