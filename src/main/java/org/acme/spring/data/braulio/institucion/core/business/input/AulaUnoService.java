package org.acme.spring.data.braulio.institucion.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.entity.AulaUnoCreate;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface AulaUnoService {
    Either<ErrorCodes, List<Aula>> getAulas(Integer idEdificio);

    Either<ErrorCodes, Boolean> create(AulaUnoCreate aulaUnoCreate);

    Either<ErrorCodes, Boolean> update(Integer id, AulaUnoCreate aulaUnoCreate);

    Either<ErrorCodes, Boolean> delete(Integer id);

}
