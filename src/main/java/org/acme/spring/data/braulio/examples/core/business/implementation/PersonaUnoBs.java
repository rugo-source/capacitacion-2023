package org.acme.spring.data.braulio.examples.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.examples.core.business.input.PersonaUnoService;
import org.acme.spring.data.braulio.examples.core.business.output.PersonaUnoRepository;
import org.acme.spring.data.braulio.examples.core.entity.PersonaUno;
import org.acme.spring.data.braulio.examples.core.entity.PersonaUnoCreate;
import org.acme.spring.data.braulio.examples.core.statemachine.PersonaUnoSM;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class PersonaUnoBs implements PersonaUnoService {

    @Inject
    PersonaUnoRepository personaUnoRepository;

    @Inject
    PersonaUnoSM personaUnoSM;

    @Override
    public List<PersonaUno> listaEjemplo() {
        return personaUnoRepository.findAll().stream().map(persona -> {
            colocarBanderas(persona);
            return persona;
        }).collect(Collectors.toList());

    }

    private void colocarBanderas(PersonaUno persona) {
        persona.setRegistrar(personaUnoSM.isDoable(personaUnoSM.getRegistrar(), personaUnoSM.getStateById(persona.getIdEstado())));
        persona.setEditar(personaUnoSM.isDoable(personaUnoSM.getEditar(), personaUnoSM.getStateById(persona.getIdEstado())));
        persona.setEliminar(personaUnoSM.isDoable(personaUnoSM.getEliminar(), personaUnoSM.getStateById(persona.getIdEstado())));
        persona.setConsultar(personaUnoSM.isDoable(personaUnoSM.getConsultar(), personaUnoSM.getStateById(persona.getIdEstado())));
        persona.setConfigurar(personaUnoSM.isDoable(personaUnoSM.getConfigurar(), personaUnoSM.getStateById(persona.getIdEstado())));
    }

    @Override
    public Either<Integer, PersonaUno> get(Integer idPersona) {
        Either<Integer, PersonaUno> result = Either.left(404);
        var persona = personaUnoRepository.findBy(idPersona);
        if (persona.isPresent()) {
            var personaGet = persona.get();
           colocarBanderas(personaGet);
            result = Either.right(personaGet);
        }
        return result;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> create(PersonaUnoCreate personaUnoCreate) {
        Either<Integer, Boolean> result = Either.right(true);
        var personaUnoPersists = PersonaUno.builder()
                .nombre(personaUnoCreate.getNombre())
                .edad(personaUnoCreate.getEdad())
                .idEstado(personaUnoSM.getRegistrado().getId())
                .build();
        personaUnoRepository.save(personaUnoPersists);
        return result;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> update(Integer id, PersonaUnoCreate personaUnoCreate) {
        Either<Integer,Boolean > result = Either.left(404);
        var personaUnoSearch = personaUnoRepository.findBy(id);
        if (personaUnoSearch.isPresent()){
            var personaUnoChange = personaUnoSearch.get();
            personaUnoChange.setNombre(personaUnoCreate.getNombre());
            personaUnoChange.setEdad(personaUnoCreate.getEdad());
            personaUnoRepository.update(personaUnoChange);
            result = Either.right(true);
        }
        return result;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> delete(Integer id) {
        Either<Integer, Boolean> result = Either.left(404);
        var personaUnoSearch = personaUnoRepository.findBy(id);
        if (personaUnoSearch.isPresent()){
            personaUnoRepository.deleteBy(id);
            result = Either.right(true);
        }
        return result;
    }

}
