package org.acme.spring.data.braulio.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.braulio.institucion.core.entity.GremioUnoCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "GremioUnoCreate", description = "Esta en la entidad regristra una area contratante")
public class GremioUnoCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 255, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al nombre del gremio")
    private String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 20, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al acronimo del gremio")
    private String acronimo;

    public GremioUnoCreate toEntity() {
        return GremioUnoCreate.builder()
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .build();
    }

}
