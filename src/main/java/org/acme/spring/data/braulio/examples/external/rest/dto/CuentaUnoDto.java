package org.acme.spring.data.braulio.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDate;
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CuentaUnoDto {
    @JsonProperty
    @Schema(description = "este es el atributo que referencia al id de la persona")
    private Integer idPersona;
    @JsonProperty
    @Schema(description = "este es el atributo que referencia al rol")
    private String rol;
    @JsonProperty
    @Schema(description = "este es el atributo que referencia al fecha inicio ")
    private LocalDate inicio;
    @JsonProperty
    @Schema(description = "este es el atributo que referencia al fecha final")
    private LocalDate fin;
    public static CuentaUnoDto fromEntity(CuentaUnoDto cuentaUnoDto) {
        return CuentaUnoDto.builder()
                .idPersona(cuentaUnoDto.getIdPersona())
                .rol(cuentaUnoDto.getRol())
                .inicio(cuentaUnoDto.getInicio())
                .fin(cuentaUnoDto.getFin())
                .build();
    }
}
