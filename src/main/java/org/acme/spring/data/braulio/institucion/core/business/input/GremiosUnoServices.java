package org.acme.spring.data.braulio.institucion.core.business.input;


import io.netty.util.AsyncMapping;
import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.entity.GremioAsociasionUnoCreate;
import org.acme.spring.data.braulio.institucion.core.entity.GremioUnoCreate;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface GremiosUnoServices {
    Either<ErrorCodes, List<Gremio>> get();

    Either<ErrorCodes, Boolean> create(GremioUnoCreate gremioUnoCreate);

    Either<ErrorCodes, Boolean> update(Integer id, GremioUnoCreate gremioUnoCreate);

    Either<ErrorCodes, Boolean> delete(Integer id);

    Either<ErrorCodes, Boolean> createAsociasion(GremioAsociasionUnoCreate gremioAsociasionUnoCreate);

    Either<ErrorCodes, Boolean> desasociarInstitucion(GremioAsociasionUnoCreate gremioAsociasionUnoCreate);
}
