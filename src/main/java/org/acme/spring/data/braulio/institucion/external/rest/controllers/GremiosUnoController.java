package org.acme.spring.data.braulio.institucion.external.rest.controllers;


import org.acme.spring.data.braulio.institucion.core.business.input.GremiosUnoServices;
import org.acme.spring.data.braulio.institucion.core.entity.GremioAsociasionUnoCreate;
import org.acme.spring.data.braulio.institucion.external.rest.dto.GremioAsociasionUnoCreateDto;
import org.acme.spring.data.braulio.institucion.external.rest.dto.GremioUnoCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/catalogos-instituciones")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Braulio")

public class GremiosUnoController {

    @Inject
    GremiosUnoServices gremiosUnoServices;
    @GET
    @Path("gremio-uno/")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response get() {
        return gremiosUnoServices.get().map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @Path("gremio-uno/")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid GremioUnoCreateDto gremioUnoCreateDto) {
        return gremiosUnoServices.create(gremioUnoCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("gremio-uno/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("id") Integer id, @Valid GremioUnoCreateDto gremioUnoCreateDto) {
        return gremiosUnoServices.update(id, gremioUnoCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("gremio-uno/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("id") Integer id) {
        return gremiosUnoServices.delete(id).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
    @POST
    @Path("/asociar-institucion")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response createAsociasion(@Valid GremioAsociasionUnoCreateDto gremioAsociasionUnoCreateDto) {
        return gremiosUnoServices.createAsociasion(gremioAsociasionUnoCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
    @DELETE
    @Path("/asociar-institucion")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Petición erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response deleteAsociacion(@Valid  GremioAsociasionUnoCreateDto gremioAsociasionUnoCreateDto) {
        return gremiosUnoServices.desasociarInstitucion(gremioAsociasionUnoCreateDto.toEntity()).map(Response::ok)
                .getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
