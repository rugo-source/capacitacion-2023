package org.acme.spring.data.braulio.examples.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface ExampleService {
    List<String> listaEjemplo();
    Either<ErrorCodes, String > get(Integer idProducto);
}
