package org.acme.spring.data.braulio.institucion.core.entity;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class InstitucionUnoSearch {

    private String identificador;

    private String nombre;

    private String acronimo;
}
