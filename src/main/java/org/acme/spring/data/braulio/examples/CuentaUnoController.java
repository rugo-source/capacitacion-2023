package org.acme.spring.data.braulio.examples;

import org.acme.spring.data.braulio.examples.core.business.input.CuentaUnoService;
import org.acme.spring.data.braulio.examples.external.rest.dto.CuentaUnoCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/data/example/braulio/cuentaUno")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion")
public class CuentaUnoController {
    @Inject
    CuentaUnoService cuentaUnoService;
    @GET
    @Path("{idPersona}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Petición erronea", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response get(@PathParam("idPersona") Integer idPersona) {
        return cuentaUnoService.get(idPersona).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
    @POST
    @Path("post")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = String.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = String.class)))
    public Response create( @Valid CuentaUnoCreateDto cuentaUnoCreateDto) {
        return cuentaUnoService.create(  cuentaUnoCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
    @PUT
    @Path("put/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = String.class)))
    public Response update(@PathParam("id") Integer id, @Valid CuentaUnoCreateDto cuentaUnoCreateDto) {
        return cuentaUnoService.update(id, cuentaUnoCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
    @DELETE
    @Path("delete/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    public Response delete(@PathParam("id") Integer id) {
        return cuentaUnoService.delete(id).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
