package org.acme.spring.data.braulio.institucion.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;

import java.util.List;
import java.util.Optional;

public interface AreasContratantesUnoRepository {
    List<AreaContratante> findAllByIdInstitucion(Integer idInstitucion);

    void save(AreaContratante areaContratantePersists);

    Optional<AreaContratante> findById(Integer id);

    void update(AreaContratante areaContratanteChange);

    void deleteBy(Integer id);
}
