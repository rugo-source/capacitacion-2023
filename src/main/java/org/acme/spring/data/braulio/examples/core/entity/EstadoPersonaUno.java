package org.acme.spring.data.braulio.examples.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EstadoPersonaUno {
    private Integer id;
    private String nombre;
}
