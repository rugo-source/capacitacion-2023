package org.acme.spring.data.braulio.examples.external.jpa.repository;

import org.acme.spring.data.cristian.examples.external.jpa.model.UsuarioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuariosJpaRepository extends JpaRepository<UsuarioJpa,Integer> {
}
