package org.acme.spring.data.braulio.institucion.external.jpa.dao;

import org.acme.spring.data.braulio.institucion.core.business.output.AulaUnoRepository;
import org.acme.spring.data.braulio.institucion.external.jpa.repository.AulaUnoJpaRepository;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AulaJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class AulaUnoDao implements AulaUnoRepository {
    @Inject
    AulaUnoJpaRepository aulaUnoJpaRepository;

    @Override
    public List<Aula> findAllByIdEdificio(Integer idEdificio) {
        return aulaUnoJpaRepository.findAllByIdEdificio(idEdificio).stream().map(AulaJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Aula aulaPersist) {
        aulaUnoJpaRepository.saveAndFlush(AulaJpa.fromEntity(aulaPersist));
    }

    @Override
    public Optional<Aula> findById(Integer id) {
        return aulaUnoJpaRepository.findById(id).map(AulaJpa::toEntity);
    }

    @Override
    public void update(Aula aulaChange) {
        aulaUnoJpaRepository.saveAndFlush(AulaJpa.fromEntity(aulaChange));

    }

    @Override
    public Boolean existById(Integer id) {
        return aulaUnoJpaRepository.existsById(id);
    }

    @Override
    public void delete(Integer id) {
        aulaUnoJpaRepository.deleteById(id);
    }

    @Override
    public List<Aula> findAllByNombre(String nombre) {
        return aulaUnoJpaRepository.findByNombre(nombre).stream().map(AulaJpa::toEntity).collect(Collectors.toList());
    }

}
