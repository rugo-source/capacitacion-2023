package org.acme.spring.data.braulio.institucion.external.rest.dto;

import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.acme.spring.data.braulio.institucion.core.entity.InstitucionUnoCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "InstitucionCreate", description = "Esta en la entidad regristra una institucion")

public class InstitucionUnoCreateDto {
    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id del tipo de institucion")
    Integer idTipo;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id clasificación de la institucion")
    Integer idClasificacion;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id categoria de la institucion")
    Integer idCategoria;

    @JsonProperty
    @Positive(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id subsistema universidad")
    Integer idSubsistemaUniversidad;

    @JsonProperty
    @Positive(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id subsistema universidad")
    Integer idSubsistemaBachillerato;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Size(min = 0, max = 4, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al identificador de la institucion")
    String identificador;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 255, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al nombre de la institucion")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 20, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al acronimo de la institucion")
    String acronimo;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Size(min = 0, max = 20, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al area contratante de la institucion")
    String cct;

    public InstitucionUnoCreate toEntity() {
        return InstitucionUnoCreate.builder()
                .idCategoria(this.idCategoria)
                .acronimo(this.acronimo)
                .cct(this.cct)
                .idClasificacion(this.idClasificacion)
                .identificador(this.identificador)
                .idSubsistemaBachillerato(this.idSubsistemaBachillerato)
                .idSubsistemaUniversidad(this.idSubsistemaUniversidad)
                .idTipo(this.idTipo)
                .nombre(this.nombre)
                .build();
    }

}
