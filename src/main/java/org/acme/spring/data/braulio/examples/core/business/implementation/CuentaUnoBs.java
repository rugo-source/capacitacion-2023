package org.acme.spring.data.braulio.examples.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.examples.core.business.input.CuentaUnoService;
import org.acme.spring.data.braulio.examples.core.business.output.CuentaUnoRepository;
import org.acme.spring.data.braulio.examples.core.business.output.PersonaUnoRepository;
import org.acme.spring.data.braulio.examples.core.entity.CuentaUno;
import org.acme.spring.data.braulio.examples.core.entity.CuentaUnoCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@ApplicationScoped
public class CuentaUnoBs implements CuentaUnoService {
    @Inject
    CuentaUnoRepository cuentaUnoRepository;
    @Inject
    PersonaUnoRepository personaUnoRepository;

    @Override
    @Transactional
    public Either<ErrorCodes, String> create(CuentaUnoCreate cuentaUnoCreate) {
        Either<ErrorCodes,String> result = Either.left(ErrorCodes.NOT_FOUND);
        var personaUnoSearch = personaUnoRepository.findBy(cuentaUnoCreate.getIdPersonal());
        if(personaUnoSearch.isPresent()) {
            if(validateRNN001(cuentaUnoCreate.getInicio(),cuentaUnoCreate.getFin())) {
                if(validateRNN002(cuentaUnoCreate.getInicio())){
                    var cuentaUnoPersists = CuentaUno.builder()
                            .idPersona(cuentaUnoCreate.getIdPersonal())
                            .rol(cuentaUnoCreate.getRol())
                            .inicio(cuentaUnoCreate.getInicio())
                            .fin(cuentaUnoCreate.getFin()).build();
                    cuentaUnoRepository.save(cuentaUnoPersists);
                    result =Either.right("Se creo con exito");
                } else {
                    result = Either.left(ErrorCodes.RNN002);
                }
            } else {
                result = Either.left(ErrorCodes.RNN001);
            }
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, String> update(Integer id, CuentaUnoCreate cuentaUnoCreate) {
        Either<ErrorCodes, String> result = Either.left(ErrorCodes.NOT_FOUND);
        var personaUnoSearch = personaUnoRepository.findBy(cuentaUnoCreate.getIdPersonal());
        var cuentaUnoSearch = cuentaUnoRepository.findBy(id);
        if (cuentaUnoSearch.isPresent()) {
            if (personaUnoSearch.isPresent()) {
                if (validateRNN001(cuentaUnoCreate.getInicio(), cuentaUnoCreate.getFin())) {
                    if (validateRNN002(cuentaUnoCreate.getInicio())) {
                        var cuentaUnoChange = cuentaUnoSearch.get();
                        cuentaUnoChange.setIdPersona(cuentaUnoCreate.getIdPersonal());
                        cuentaUnoChange.setRol(cuentaUnoCreate.getRol());
                        cuentaUnoChange.setInicio(cuentaUnoCreate.getInicio());
                        cuentaUnoChange.setFin(cuentaUnoCreate.getFin());
                        cuentaUnoRepository.update(cuentaUnoChange);
                        result = Either.right("Se creo con exito");
                    } else {
                        result = Either.left(ErrorCodes.RNN002);
                    }
                } else {
                    result = Either.left(ErrorCodes.RNN001);
                }
            }
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> delete(Integer id) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var cuentaUnoSearch = cuentaUnoRepository.findBy(id);
        if (cuentaUnoSearch.isPresent()){
            cuentaUnoRepository.deleteBy(id);
            result = Either.right(true);
        }
        return result;
    }

    @Override
    public Either<ErrorCodes, List<CuentaUno>> get(Integer idPersona) {
        Either<ErrorCodes, List<CuentaUno>> result = Either.left(ErrorCodes.NOT_FOUND);
        var personaUnoSearch = personaUnoRepository.findBy(idPersona);
        if (personaUnoSearch.isPresent()){
            var cuentasById = cuentaUnoRepository.findAllById(idPersona);
            result= Either.right(cuentasById);
        }
        return result;
    }

    public Boolean validateRNN001(LocalDate inicio, LocalDate fin) {
        return inicio.isBefore(fin);
    }

    public Boolean validateRNN002(LocalDate inicio) {
        return inicio.isAfter(LocalDate.now()) || inicio.equals(LocalDate.now());
    }
}
