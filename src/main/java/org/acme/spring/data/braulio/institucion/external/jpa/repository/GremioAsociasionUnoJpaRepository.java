package org.acme.spring.data.braulio.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GremioAsociasionUnoJpaRepository extends JpaRepository<MiembroGremioJpa, MiembroGremioIdJpa> {
}
