package org.acme.spring.data.braulio.institucion.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.entity.AreaContratanteUnoCreate;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface AreasContratantesUnoService {

   Either<ErrorCodes,List<AreaContratante>> get(Integer idInstitucion);

   Either<ErrorCodes, Boolean> create(Integer idInstitucion,AreaContratanteUnoCreate areaContratanteUnoCreate);

   Either<ErrorCodes, Boolean> update(Integer id, AreaContratanteUnoCreate areaContratanteUnoCreate);

   Either<ErrorCodes, Boolean> delete(Integer id);
}
