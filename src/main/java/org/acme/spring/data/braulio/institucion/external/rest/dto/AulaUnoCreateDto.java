package org.acme.spring.data.braulio.institucion.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.braulio.institucion.core.entity.AulaUnoCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AulaUnoCreate", description = "Esta en la entidad que regitra una aula")
public class AulaUnoCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @Positive(message = "RNS001")
    @Schema(description = "este es el atributo que referencia al id de edificio")
    Integer idEdificio;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Schema(description = "este es el atributo que referencia al nombre")
    String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Positive(message = "RNS001")
    @Schema(description = "este es el atributo que referencia al id del equipamento")
    Integer idTipoEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @PositiveOrZero(message = "RNS001")
    @Schema(description = "este es el atributo que referencia ala cantidad de equipamento")
    Integer cantidadEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @PositiveOrZero(message = "RNS001")
    @Schema(description = "este es el atributo que referencia ala capacidad de equipamento")
    Integer capacidadEquipamiento;

    @JsonProperty
    @Schema(description = "este es el atributo que referencia al alguna observacion")
    private String observaciones;


    public AulaUnoCreate toEntity() {
        return AulaUnoCreate.builder()
                .idEdificio(this.idEdificio)
                .nombre(this.nombre)
                .idTipoEquipamiento(this.idTipoEquipamiento)
                .cantidadEquipamiento(this.cantidadEquipamiento)
                .capacidadEquipamiento(this.capacidadEquipamiento)
                .build();
    }
}
