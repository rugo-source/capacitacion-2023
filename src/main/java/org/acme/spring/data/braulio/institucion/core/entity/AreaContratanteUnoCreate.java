package org.acme.spring.data.braulio.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AreaContratanteUnoCreate {
    private Integer id;
    private Integer idDomicilio;
    private Integer idInstitucion;
    private String area;
    private String subArea;
    private String telefono;
    private String extension;
}
