package org.acme.spring.data.braulio.institucion.core.entity;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class SedeUnoCreate {
    private Integer id;
    private Integer idInstitucion;
    private Integer idDireccion;
    private String nombre;
    private String acronimo;
    private Integer capacidad;
    private Boolean propia;
    private String identificador;

}
