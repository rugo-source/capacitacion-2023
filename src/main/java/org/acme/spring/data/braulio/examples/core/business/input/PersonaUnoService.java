package org.acme.spring.data.braulio.examples.core.business.input;


import io.vavr.control.Either;
import org.acme.spring.data.braulio.examples.core.entity.PersonaUno;
import org.acme.spring.data.braulio.examples.core.entity.PersonaUnoCreate;

import java.util.List;

public interface PersonaUnoService {
    Either<Integer, PersonaUno> get(Integer idPersona);

    Either<Integer, Boolean> create(PersonaUnoCreate toEntity);

    Either<Integer, Boolean> update(Integer id, PersonaUnoCreate personaUnoCreate);

    Either<Integer, Boolean> delete(Integer id);

    List<PersonaUno> listaEjemplo();


}
