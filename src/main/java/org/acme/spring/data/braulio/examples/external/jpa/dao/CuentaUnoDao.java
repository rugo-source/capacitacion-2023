package org.acme.spring.data.braulio.examples.external.jpa.dao;

import org.acme.spring.data.braulio.examples.core.business.output.CuentaUnoRepository;
import org.acme.spring.data.braulio.examples.core.entity.CuentaUno;
import org.acme.spring.data.braulio.examples.external.jpa.repository.CuentaUnoJpaRepository;
import org.acme.spring.data.braulio.examples.external.jpa.model.CuentaUnoJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class CuentaUnoDao implements CuentaUnoRepository {

    @Inject
    CuentaUnoJpaRepository cuentaUnoJpaRepository;
    @Inject
    EntityManager entityManager;

    @Override
    public void save(CuentaUno cuentaUnoPersists) {
        cuentaUnoJpaRepository.saveAndFlush(CuentaUnoJpa.fromEntity(cuentaUnoPersists));
    }

    @Override
    public void update(CuentaUno cuentaUnoChange) {
        cuentaUnoJpaRepository.saveAndFlush(CuentaUnoJpa.fromEntity(cuentaUnoChange));
    }

    @Override
    public void deleteBy(Integer id) {
        cuentaUnoJpaRepository.deleteById(id);
    }

    @Override
    public List<CuentaUno> findAllById(Integer idPersona) {

        return  cuentaUnoJpaRepository.findAllByIdPersona(idPersona).stream().map(CuentaUnoJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<CuentaUno> findBy(Integer id) {
        return cuentaUnoJpaRepository.findById(id).map(cuentaUnoJpa -> {
            var cuentaUnoEntidad = cuentaUnoJpa.toEntity();
            cuentaUnoEntidad.setPersonaUno(cuentaUnoJpa.getPersonaUnoJpa().toEntity());
            return cuentaUnoEntidad;
        });
    }
}
