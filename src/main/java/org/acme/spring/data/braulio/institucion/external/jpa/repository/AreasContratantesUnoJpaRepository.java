package org.acme.spring.data.braulio.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AreasContratantesUnoJpaRepository extends JpaRepository<AreaContratanteJpa,Integer> {
    List<AreaContratanteJpa> findAllByIdInstitucion(Integer idInstitucion);
}
