package org.acme.spring.data.braulio.institucion.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GremioUnoCreate {
    private Integer id;
    private String nombre;
    private String acronimo;
}
