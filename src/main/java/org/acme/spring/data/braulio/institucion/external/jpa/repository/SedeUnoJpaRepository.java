package org.acme.spring.data.braulio.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SedeUnoJpaRepository extends JpaRepository<SedeJpa,Integer> {

    List<SedeJpa> findAllByIdInstitucion(Integer idInstitucion);

    Boolean existsByNombreOrAcronimo(String nombre, String acronimo);
}
