package org.acme.spring.data.braulio.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.GremioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GremioUnoJpaRepository extends JpaRepository<GremioJpa,Integer> {

    Boolean existsByNombreOrAcronimo(String nombre, String acronimo);

}
