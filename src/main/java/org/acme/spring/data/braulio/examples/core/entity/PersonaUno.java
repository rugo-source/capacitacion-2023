package org.acme.spring.data.braulio.examples.core.entity;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class PersonaUno {
    private Integer id;
    private String nombre;
    private Integer edad;
    private Integer idEstado;

    private Boolean registrar;
    private Boolean editar;
    private Boolean eliminar;
    private Boolean consultar;
    private Boolean configurar;

    private EstadoPersonaUno estadoPersonaUno;

}
