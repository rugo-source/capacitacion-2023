package org.acme.spring.data.braulio.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InstitucionUnoJpaRepository extends JpaRepository<InstitucionJpa, Integer> {

    List<InstitucionJpa> findAllByIdentificadorOrNombreOrAcronimo(String identificador, String nombre, String acronimo);

    Boolean existsByNombre(String nombre);

    Boolean existsByIdentificador(String identificador);

    boolean existsById(Integer id);


}
