package org.acme.spring.data.braulio.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.braulio.institucion.core.entity.GremioAsociasionUnoCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "GremioAsociasionUnoCreate", description = "Esta en la entidad regristra una asociacion entre gremio e institucion")
public class GremioAsociasionUnoCreateDto {
    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id del gremio")
    Integer idGremio;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id la institucion")
    Integer idInstitucion;

    public GremioAsociasionUnoCreate toEntity() {
        return GremioAsociasionUnoCreate.builder()
                .idGremio(this.idGremio)
                .idInstitucion(this.idInstitucion)
                .build();
    }
}
