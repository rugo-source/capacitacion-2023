package org.acme.spring.data.braulio.institucion.external.jpa.dao;

import org.acme.spring.data.braulio.institucion.core.business.output.AreasContratantesUnoRepository;
import org.acme.spring.data.braulio.institucion.external.jpa.repository.AreasContratantesUnoJpaRepository;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class AreasContratantesUnoDao implements AreasContratantesUnoRepository {
    @Inject
    AreasContratantesUnoJpaRepository areasContratantesUnoJpaRepository;

    @Override
    public List<AreaContratante> findAllByIdInstitucion(Integer idInstitucion) {
        return areasContratantesUnoJpaRepository.findAllByIdInstitucion(idInstitucion).stream().map(AreaContratanteJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(AreaContratante areaContratantePersists) {
        areasContratantesUnoJpaRepository.saveAndFlush(AreaContratanteJpa.fromEntity(areaContratantePersists));
    }

    @Override
    public Optional<AreaContratante> findById(Integer id) {
        return areasContratantesUnoJpaRepository.findById(id).map(AreaContratanteJpa::toEntity);
    }

    @Override
    public void update(AreaContratante areaContratanteChange) {
        areasContratantesUnoJpaRepository.saveAndFlush(AreaContratanteJpa.fromEntity(areaContratanteChange));

    }

    @Override
    public void deleteBy(Integer id) {
        areasContratantesUnoJpaRepository.deleteById(id);
    }
}
