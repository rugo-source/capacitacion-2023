package org.acme.spring.data.braulio.examples.core.business.implementation;

import io.vavr.control.Either;

import org.acme.spring.data.braulio.examples.core.business.input.ExampleService;
import org.acme.spring.data.braulio.examples.core.business.output.UsuariosRepository;
import org.acme.spring.data.cristian.examples.core.entity.Usuario;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class ExampleBs implements ExampleService {
    @Inject
    UsuariosRepository usuariosRepository;

    @Override
    public List<String> listaEjemplo() {
        List<Usuario> examples = usuariosRepository.findAll();
      examples.stream().forEach(usuario -> System.out.println(usuario.getLogin()));
      return examples.stream().map(usuario -> usuario.getLogin()).collect(Collectors.toList());

    }

    @Override
    public Either<ErrorCodes, String> get(Integer idProducto) {
        Either<ErrorCodes, String> resultado = Either.left(ErrorCodes.NOT_FOUND);
        if (idProducto.equals(10)) {
            resultado = Either.right("Es el producto");
        }
        return resultado;
    }
}
