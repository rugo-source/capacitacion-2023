package org.acme.spring.data.braulio.institucion.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.AulaJpa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AulaUnoJpaRepository extends JpaRepository<AulaJpa, Integer> {
    List<AulaJpa> findAllByIdEdificio(Integer idEdificio);

    List<AulaJpa> findByNombre(String nombre);
}
