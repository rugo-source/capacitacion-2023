package org.acme.spring.data.braulio.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.business.input.InstitucionUnoService;
import org.acme.spring.data.braulio.institucion.core.business.output.InstitucionUnoRepository;
import org.acme.spring.data.braulio.institucion.core.entity.InstitucionUnoCreate;
import org.acme.spring.data.braulio.institucion.core.entity.InstitucionUnoSearch;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class InstitucionUnoUnoBs implements InstitucionUnoService {
    @Inject
    InstitucionUnoRepository institucionUnoRepository;

    @Override
    public Either<ErrorCodes, List<Institucion>> getByAcronimo(InstitucionUnoSearch institucionUnoSearch) {
        Either<ErrorCodes, List<Institucion>> result = Either.left(ErrorCodes.NOT_FOUND);
        var institucionesUno = institucionUnoRepository.findByArguments(institucionUnoSearch);
        if (!institucionesUno.isEmpty()) {
            result = Either.right(institucionesUno);
        }
        return result;
    }

    @Override
    public List<Institucion> get() {
        return institucionUnoRepository.findAllSinEjecutivo();
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(InstitucionUnoCreate institucionUnoCreate) {
        Either<ErrorCodes, Boolean> result ;
        if (validateRNN013(institucionUnoCreate.getIdentificador())) {
            result = Either.left(ErrorCodes.RNN013);
        } else if (validateRNN094(institucionUnoCreate.getNombre())) {
            result = Either.left(ErrorCodes.RNN094);
        } else {
            var institucionPersists = Institucion.builder()
                    .idCategoria(institucionUnoCreate.getIdCategoria())
                    .acronimo(institucionUnoCreate.getAcronimo())
                    .cct(institucionUnoCreate.getCct())
                    .idClasificacion(institucionUnoCreate.getIdClasificacion())
                    .identificador(institucionUnoCreate.getIdentificador())
                    .idSubsistemaBachillerato(institucionUnoCreate.getIdSubsistemaBachillerato())
                    .idSubsistemaUniversidad(institucionUnoCreate.getIdSubsistemaUniversidad())
                    .idTipo(institucionUnoCreate.getIdTipo())
                    .nombre(institucionUnoCreate.getNombre())
                    .numeroSedesRegistradas(2).build();
            institucionUnoRepository.save(institucionPersists);
            result = Either.right(true);
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer id, InstitucionUnoCreate institucionUnoCreate) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var institucionSearch = institucionUnoRepository.findById(id);
        if (institucionSearch.isPresent()) {
            if (validateRNN013(institucionUnoCreate.getIdentificador())) {
                result = Either.left(ErrorCodes.RNN013);
            } else if (validateRNN094(institucionUnoCreate.getNombre())) {
                result = Either.left(ErrorCodes.RNN094);
            } else {
                var institucionChange = institucionSearch.get();
                institucionChange.setAcronimo(institucionUnoCreate.getAcronimo());
                institucionChange.setCct(institucionUnoCreate.getCct());
                institucionChange.setNombre(institucionUnoCreate.getNombre());
                institucionChange.setIdCategoria(institucionUnoCreate.getIdCategoria());
                institucionChange.setIdClasificacion(institucionUnoCreate.getIdClasificacion());
                institucionChange.setIdSubsistemaBachillerato(institucionUnoCreate.getIdSubsistemaBachillerato());
                institucionChange.setIdSubsistemaUniversidad(institucionUnoCreate.getIdSubsistemaUniversidad());
                institucionChange.setIdTipo(institucionUnoCreate.getIdTipo());
                institucionChange.setIdentificador(institucionUnoCreate.getIdentificador());

                institucionUnoRepository.update(institucionChange);
                result = Either.right(true);
            }
        }
        return result;
    }

    public Boolean validateRNN013(String identificador) {
        return institucionUnoRepository.existByIdentidicador(identificador);
    }

    public Boolean validateRNN094(String nombre) {
        return institucionUnoRepository.existsByNombre(nombre);
    }

}

