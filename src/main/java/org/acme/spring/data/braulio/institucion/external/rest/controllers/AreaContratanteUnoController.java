package org.acme.spring.data.braulio.institucion.external.rest.controllers;

import org.acme.spring.data.braulio.institucion.core.business.input.AreasContratantesUnoService;
import org.acme.spring.data.braulio.institucion.external.rest.dto.AreaContratanteUnoCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/catalogos-instituciones")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Braulio")
public class AreaContratanteUnoController {

    @Inject
    AreasContratantesUnoService areasContratantesUnoService;

    @GET
    @Path("area-contratante-uno/{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response get(@PathParam("idInstitucion") Integer idInstitucion) {
        return areasContratantesUnoService.get(idInstitucion).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @Path("area-contratante-uno/{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@PathParam("idInstitucion") Integer idInstitucion, @Valid AreaContratanteUnoCreateDto areaContratanteUnoCreateDto) {
        return areasContratantesUnoService.create(idInstitucion, areaContratanteUnoCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("area-contratante-uno/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("id") Integer id, @Valid AreaContratanteUnoCreateDto areaContratanteUnoCreateDto) {
        return areasContratantesUnoService.update(id, areaContratanteUnoCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("area-contratante-uno/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "404", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("id") Integer id) {
        return areasContratantesUnoService.delete(id).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
