package org.acme.spring.data.braulio.institucion.core.business.output;

import org.acme.spring.data.braulio.institucion.core.entity.GremioAsociasionUnoCreate;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;

import java.util.List;
import java.util.Optional;

public interface GremioUnoRepository {
    List<Gremio> findAll();

    void save(Gremio gremioPersist);

    Optional<Gremio> findById(Integer id);

    void update(Gremio gremioChange);

    void delete(Integer id);

    Boolean existByNombreOrAcronimo(String nombre, String acronimo);

    void saveAsociasion(GremioAsociasionUnoCreate gremioAsociasionPersit);
}
