package org.acme.spring.data.braulio.institucion.core.business.output;

import org.acme.spring.data.braulio.institucion.core.entity.GremioAsociasionUnoCreate;
import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;

import java.util.Optional;

public interface GremioAsociasionUnoRepository {
    Optional<MiembroGremio> findById(GremioAsociasionUnoCreate gremioAsociasionUnoCreate);
    void delete(MiembroGremio miembroGremio);
    Boolean existByIdInstitucion(Integer idInstitucion);
}
