package org.acme.spring.data.braulio.institucion.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.braulio.institucion.core.entity.SedeUnoCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SedeUnoCreate", description = "Esta en la entidad regristra una sede")
public class SedeUnoCreateDto {
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 100, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al nombre de una sede")
    private String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 20, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al acronimo de una sede")
    private String acronimo;

    @JsonProperty
    @Positive(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia ala capacidad de una sede")
    Integer capacidad;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia si la sede tiene una institucion")
    private Boolean propia;

    public SedeUnoCreate toEntity(){
        return SedeUnoCreate.builder()
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .capacidad(this.capacidad)
                .propia(this.propia)
                .build();
    }

}
