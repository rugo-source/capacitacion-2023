package org.acme.spring.data.braulio.examples;

import org.acme.spring.data.braulio.examples.core.business.input.PersonaUnoService;
import org.acme.spring.data.braulio.examples.core.entity.PersonaUnoCreate;
import org.acme.spring.data.braulio.examples.external.rest.dto.PersonaUnoCreateDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/data/example/braulio/personaUno")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion")
public class PersonaUnoController {
    @Inject
    PersonaUnoService personaUnoService;

    @GET
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class)))
    public Response list() {
        var list = personaUnoService.listaEjemplo();
        return Response.ok(list).build();
    }

    @GET
    @Path("{idPersona}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class)))
    @APIResponse(responseCode = "404", description = "Petición erronea", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response get(@PathParam("idPersona") Integer idPersona) {
        return personaUnoService.get(idPersona).map(Response::ok).getOrElseGet(resultado -> Response.ok().status(404)).build();
    }

    @POST
    @Path("post")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = Boolean.class)))
    public Response create(PersonaUnoCreateDto personaUnoCreateDto) {
        return personaUnoService.create(personaUnoCreateDto.toEntity()).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }


    @PUT
    @Path("put/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response update(@PathParam("id") Integer id, PersonaUnoCreate personaUnoCreate) {
        return personaUnoService.update(id, personaUnoCreate).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

    @DELETE
    @Path("delete/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response delete(@PathParam("id") Integer id) {
        return personaUnoService.delete(id).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }
}
