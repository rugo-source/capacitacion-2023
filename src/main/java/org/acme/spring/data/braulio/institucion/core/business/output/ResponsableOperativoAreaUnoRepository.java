package org.acme.spring.data.braulio.institucion.core.business.output;

public interface ResponsableOperativoAreaUnoRepository {
    Boolean existsByIdAreaContratante(Integer id);
}
