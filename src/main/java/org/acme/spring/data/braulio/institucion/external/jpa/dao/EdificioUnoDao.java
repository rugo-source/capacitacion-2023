package org.acme.spring.data.braulio.institucion.external.jpa.dao;

import org.acme.spring.data.braulio.institucion.core.business.output.EdificiosUnoRepository;
import org.acme.spring.data.braulio.institucion.external.jpa.repository.EdificiosUnoJpaRepository;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class EdificioUnoDao implements EdificiosUnoRepository {
    @Inject
    EdificiosUnoJpaRepository edificiosUnoJpaRepository;

    @Override
    public List<Edificio> findAllByIdSede(Integer idSede) {
        return edificiosUnoJpaRepository.findAllByIdSede(idSede).stream().map(EdificioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(Edificio edificioPersist) {
        edificiosUnoJpaRepository.saveAndFlush(EdificioJpa.fromEntity(edificioPersist));
    }

    @Override
    public Optional<Edificio> findById(Integer id) {
        return edificiosUnoJpaRepository.findById(id).map(EdificioJpa::toEntity);
    }

    @Override
    public void update(Edificio edificioChange) {
        edificiosUnoJpaRepository.saveAndFlush(EdificioJpa.fromEntity(edificioChange));
    }

    @Override
    public Boolean existById(Integer id) {
        return edificiosUnoJpaRepository.existsById(id);
    }

    @Override
    public void deleteById(Integer id) {
        edificiosUnoJpaRepository.deleteById(id);
    }


    @Override
    public Boolean existsByIdSede(Integer idSede) {
        return edificiosUnoJpaRepository.existsByIdSede(idSede);
    }

    @Override
    public Boolean existByNombreAndAcronimoAndIdSede(String nombre, String acronimo, Integer idSede) {
        return edificiosUnoJpaRepository.existsByNombreAndAcronimoAndIdSede(nombre,acronimo,idSede);
    }

    @Override
    public List<Edificio> findAllById(Integer idEdificio) {
        return edificiosUnoJpaRepository.findAllById(idEdificio).stream().map(EdificioJpa::toEntity).collect(Collectors.toList());
    }


}
