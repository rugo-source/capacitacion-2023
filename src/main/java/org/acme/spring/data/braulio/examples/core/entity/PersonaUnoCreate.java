package org.acme.spring.data.braulio.examples.core.entity;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class PersonaUnoCreate {
    private String nombre;
    private Integer edad;
}
