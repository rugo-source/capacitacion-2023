package org.acme.spring.data.braulio.institucion.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Edificio;

import java.util.List;
import java.util.Optional;

public interface EdificiosUnoRepository {
    List<Edificio> findAllByIdSede(Integer idSede);

    void save(Edificio edificioPersist);

    Optional<Edificio> findById(Integer id);

    void update(Edificio edificioChange);

    Boolean existById(Integer id);

    void deleteById(Integer id);

    Boolean existsByIdSede(Integer idSede);


    Boolean existByNombreAndAcronimoAndIdSede(String nombre, String acronimo, Integer idSede);

    List<Edificio> findAllById(Integer idEdificio);

}