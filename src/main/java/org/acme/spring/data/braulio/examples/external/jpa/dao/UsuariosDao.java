package org.acme.spring.data.braulio.examples.external.jpa.dao;

import org.acme.spring.data.braulio.examples.core.business.output.UsuariosRepository;

import org.acme.spring.data.braulio.examples.external.jpa.repository.UsuariosJpaRepository;

import org.acme.spring.data.cristian.examples.core.entity.Usuario;

import org.acme.spring.data.cristian.examples.external.jpa.model.UsuarioJpa;


import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class UsuariosDao implements UsuariosRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    UsuariosJpaRepository usuariosJpaRepository;

    @Override
    public List<Usuario> findAll() {
        return usuariosJpaRepository.findAll().stream().map(UsuarioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<Usuario> findAllNative() {
        Stream<UsuarioJpa> result= entityManager.createNativeQuery("select * from tca02_usuario",UsuarioJpa.class).getResultStream();
        return result.map(UsuarioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<Usuario> findAllOrm() {
        return null;
    }
}
