package org.acme.spring.data.braulio.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.braulio.examples.core.entity.CuentaUnoCreate;
import org.acme.spring.data.util.StringConstants;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "CuentaUnoCreate", description = "Esta en la entidad para guardar en cuenta Uno")

public class CuentaUnoCreateDto {
    @JsonProperty
    @Positive(message = "RNS001")
    @Schema(description = "este es el atributo que referencia al idPersona")
    private Integer idPersonal;
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3, max = 100, message = "RNS002")
    @Schema(description = "este es el atributo que referencia al rol")
    private String rol;
    @JsonProperty
    @NotNull(message = "RNS001")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringConstants.LOCAL_DATE_FORMAT)
    @Schema(description = "este es el atributo que referencia a la fecha de la creacion", format = "string", implementation = String.class)
    private LocalDate inicio;
    @JsonProperty
    @NotNull(message = "RNS001")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringConstants.LOCAL_DATE_FORMAT)
    @Schema(description = "este es el atributo que referencia a la fecha del final", format = "string", implementation = String.class)
    private LocalDate  fin;

    public CuentaUnoCreate toEntity() {
        return CuentaUnoCreate.builder().idPersonal(this.idPersonal).rol(this.rol).inicio(this.inicio).fin(this.fin).build();
    }
}
