package org.acme.spring.data.braulio.institucion.core.business.output;

import org.acme.spring.data.braulio.institucion.core.entity.InstitucionUnoSearch;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;

import java.util.List;
import java.util.Optional;

public interface InstitucionUnoRepository {
    List<Institucion> findByArguments(InstitucionUnoSearch institucionUnoSearch);

    List<Institucion> findAllSinEjecutivo();

    void save(Institucion institucionUnoCreate);

    Optional<Institucion> findById(Integer id);

    void update(Institucion institucionChange);

    Boolean existsByNombre(String nombre);

    Boolean existByIdentidicador(String identificador);

    Boolean existById(Integer id);
}
