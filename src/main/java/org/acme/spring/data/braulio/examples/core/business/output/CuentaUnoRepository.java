package org.acme.spring.data.braulio.examples.core.business.output;

import org.acme.spring.data.braulio.examples.core.entity.CuentaUno;

import java.util.List;
import java.util.Optional;

public interface CuentaUnoRepository {

    Optional<CuentaUno> findBy(Integer id);

    void save(CuentaUno cuentaUnoPersists);


    void update(CuentaUno cuentaUnoChange);

    void deleteBy(Integer id);

    List<CuentaUno> findAllById(Integer idPersona);
}
