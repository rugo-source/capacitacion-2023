package org.acme.spring.data.braulio.institucion.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.braulio.institucion.core.entity.AreaContratanteUnoCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AreaContratanteUnoCreate", description = "Esta en la entidad regristra una area contratante")
public class AreaContratanteUnoCreateDto {
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 255, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia al area de una area contratante")
    private String area;
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 1, max = 255, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia ala subarea de una area contratante")

    private String subArea;
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 10, max = 10, message = "RNN014")
    @Schema(description = "este es el atributo que hace referencia al telefono de una area contratante")
    private String telefono;
    @JsonProperty
    @Size(min = 0, max = 5, message = "RNS002")
    @Schema(description = "este es el atributo que hace referencia ala extension de una area contratante")
    private String extension;

    public AreaContratanteUnoCreate toEntity() {
        return AreaContratanteUnoCreate.builder()
                .area(this.area)
                .subArea(this.subArea)
                .telefono(this.telefono)
                .extension(this.extension)
                .build();
    }

}
