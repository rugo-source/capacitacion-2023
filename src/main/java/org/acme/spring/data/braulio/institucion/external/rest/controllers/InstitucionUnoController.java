package org.acme.spring.data.braulio.institucion.external.rest.controllers;

import org.acme.spring.data.braulio.institucion.core.business.input.InstitucionUnoService;
import org.acme.spring.data.braulio.institucion.external.rest.dto.InstitucionUnoCreateDto;
import org.acme.spring.data.braulio.institucion.external.rest.dto.InstitucionUnoSearchDto;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/*Braulio de los cambios que te pido notar mucho, cuando regreses el array en cualquiera de tus enpoints, cuida que el nombre de estos quede identificado con el
* nombre de list en lugar de get, get solo se queda cuando regresemos un unico objecto.
* en cuanto los paths puedes colocar el nombre de institucion en el path principal de la clase para que no tengas que repetirlo en cada path de tus metodos
*
* */
@Path("/catalogos-instituciones")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Braulio")
public class InstitucionUnoController {
    @Inject
    InstitucionUnoService institucionUnoService;

    @POST
    @Path("institucion/busquedas")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response getbyAcronimo(@Valid InstitucionUnoSearchDto institucionUnoSearchDto){
        return  institucionUnoService.getByAcronimo(institucionUnoSearchDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @GET
    @Path("institucion")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response get(){
        var instituciones = institucionUnoService.get();
        return Response.ok(instituciones).build();
    }

    @POST
    @Path("institucion/")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid InstitucionUnoCreateDto institucionUnoCreateDto){
        return  institucionUnoService.create(institucionUnoCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
    @PUT
    @Path("institucion/{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idInstitucion") Integer id, @Valid InstitucionUnoCreateDto institucionUnoCreateDto){
        return  institucionUnoService.update(id, institucionUnoCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

}
