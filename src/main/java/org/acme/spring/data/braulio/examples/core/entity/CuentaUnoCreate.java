package org.acme.spring.data.braulio.examples.core.entity;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class CuentaUnoCreate {
    private Integer idPersonal;
    private String rol;
    private LocalDate inicio;
    private LocalDate fin;
}
