package org.acme.spring.data.braulio.institucion.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Sede;

import java.util.List;
import java.util.Optional;

public interface SedeUnoRepository {
    List<Sede> findAllByIdInstitucion(Integer idInstitucion);

    void save(Sede sedePersist);

    Optional<Sede> findById(Integer id);

    void update(Sede sedeChange);

    Boolean existById(Integer id);

    void deleteById(Integer id);

    Boolean existByNombreOrAcronimo(String nombre, String acronimo);

    List<String> findAllIdentificadores();
}
