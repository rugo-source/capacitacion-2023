package org.acme.spring.data.braulio.institucion.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.braulio.institucion.core.business.input.GremiosUnoServices;
import org.acme.spring.data.braulio.institucion.core.business.output.GremioAsociasionUnoRepository;
import org.acme.spring.data.braulio.institucion.core.business.output.GremioUnoRepository;
import org.acme.spring.data.braulio.institucion.core.entity.GremioAsociasionUnoCreate;
import org.acme.spring.data.braulio.institucion.core.entity.GremioUnoCreate;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class GremioUnoBs implements GremiosUnoServices {

    @Inject
    GremioUnoRepository gremioUnoRepository;
    @Inject
    GremioAsociasionUnoRepository gremioAsociasionUnoRepository;

    @Override
    public Either<ErrorCodes, List<Gremio>> get() {
        Either<ErrorCodes, List<Gremio>> result;
        var gremios = gremioUnoRepository.findAll();
        if (gremios.isEmpty()) {
            result = Either.left(ErrorCodes.NOT_FOUND);
        } else {
            result = Either.right(gremios);
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(GremioUnoCreate gremioUnoCreate) {
        Either<ErrorCodes, Boolean> result;
        if (validateRNN097(gremioUnoCreate.getNombre(), gremioUnoCreate.getAcronimo())) {
            result = Either.left(ErrorCodes.RNN097);
        } else {
            var gremioPersist = Gremio.builder()
                    .nombre(gremioUnoCreate.getNombre())
                    .acronimo(gremioUnoCreate.getAcronimo())
                    .build();
            gremioUnoRepository.save(gremioPersist);
            result = Either.right(true);
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer id, GremioUnoCreate gremioUnoCreate) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var gremioSearch = gremioUnoRepository.findById(id);
        if (gremioSearch.isPresent()) {
            if (validateRNN097(gremioUnoCreate.getNombre(), gremioUnoCreate.getAcronimo())) {
                result = Either.left(ErrorCodes.RNN097);
            } else {
                var gremioChange = gremioSearch.get();
                gremioChange.setNombre(gremioUnoCreate.getNombre());
                gremioChange.setAcronimo(gremioUnoCreate.getAcronimo());
                gremioUnoRepository.update(gremioChange);
                result = Either.right(true);
            }
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> delete(Integer id) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var gremioSearch = gremioUnoRepository.findById(id);
        if (gremioSearch.isPresent()) {
            if (validateRNN024(gremioSearch.get().getInstitucionList())) {
                result = Either.left(ErrorCodes.RNN024);
            } else {
                gremioUnoRepository.delete(id);
                result = Either.right(true);
            }
        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> createAsociasion(GremioAsociasionUnoCreate gremioAsociasionUnoCreate) {
        Either<ErrorCodes, Boolean> result;
        if (validateRNN026(gremioAsociasionUnoCreate.getIdInstitucion())) {
            result = Either.left(ErrorCodes.RNN026);
        } else {
            var gremioAsociasionPersit = GremioAsociasionUnoCreate.builder()
                    .idGremio(gremioAsociasionUnoCreate.getIdGremio())
                    .idInstitucion(gremioAsociasionUnoCreate.getIdInstitucion())
                    .build();
            gremioUnoRepository.saveAsociasion(gremioAsociasionPersit);
            result = Either.right(true);

        }
        return result;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> desasociarInstitucion(GremioAsociasionUnoCreate gremioAsociasionUnoCreate) {
        Either<ErrorCodes, Boolean> result = Either.left(ErrorCodes.NOT_FOUND);
        var gremioAsociasionSearch = gremioAsociasionUnoRepository.findById(gremioAsociasionUnoCreate);
        if (gremioAsociasionSearch.isPresent()) {
            gremioAsociasionUnoRepository.delete(gremioAsociasionSearch.get());
            result = Either.right(true);
        }
        return result;
    }

    Boolean validateRNN097(String nombre, String acronimo) {

        return gremioUnoRepository.existByNombreOrAcronimo(nombre, acronimo);
    }

    Boolean validateRNN024(List<Institucion> instituciones) {
        return instituciones.isEmpty();
    }

    public Boolean validateRNN026(Integer idInstitucion) {
        return gremioAsociasionUnoRepository.existByIdInstitucion(idInstitucion);
    }
}
