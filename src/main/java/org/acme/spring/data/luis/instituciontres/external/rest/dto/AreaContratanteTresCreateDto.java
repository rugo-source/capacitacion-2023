package org.acme.spring.data.luis.instituciontres.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.instituciontres.core.entity.AreaContratanteTresCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AreaContratanteTresCreate", description = "Esta es la entidad para guardar un area contratante")
public class AreaContratanteTresCreateDto {

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de tipo de la institucion")
    private Integer idInstitucion;


    @JsonProperty
    @Positive(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de tipo de la direccion")
    private Integer idDireccion;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3,max=255, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al area de el area contratante")
    private String area;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3,max=255, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al subarea de el area contratante")
    private String subarea;

    @JsonProperty
    @Pattern(regexp = "^([0-9]{10})$", message = "RNN014")
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(max=10, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al telefono de el area contratante")
    private String telefono;

    @JsonProperty
    @Size(max=5, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia a la extension de el area contratante")
    private String extension;

//    @JsonProperty
//    @NotNull(message = "RNS001")
//    @NotEmpty(message = "RNS001")
//    @Size(min = 3,max=255, message = "RNS002")
//    @Schema(description = "Este es el atributo que referencia al nombre de la institucion")
//    private String institucion;

    public AreaContratanteTresCreate toEntity(){
        return AreaContratanteTresCreate.builder()
                .idInstitucion(this.idInstitucion)
//                .idDireccion(this.idDireccion)
                .area(this.area)
                .subarea(this.subarea)
                .telefono(this.telefono)
                .extension(this.extension)
//                .institucion(this.institucion)
                .build();
    }

}
