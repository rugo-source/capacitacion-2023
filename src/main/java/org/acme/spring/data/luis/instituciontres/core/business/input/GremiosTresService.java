package org.acme.spring.data.luis.instituciontres.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.luis.instituciontres.core.entity.GremioTresCreate;
import org.acme.spring.data.luis.instituciontres.core.entity.MiembroGremioTresCreate;

import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface GremiosTresService {

    List<Gremio> listGremios();

    Either<ErrorCodes,Boolean> create(GremioTresCreate toEntity);
    Either<ErrorCodes,Boolean> createMiembro(MiembroGremioTresCreate toEntity);
    Either<ErrorCodes,Boolean> update(Integer id,GremioTresCreate gremioTresCreate);
    Either<ErrorCodes,Boolean> delete(Integer id);

}
