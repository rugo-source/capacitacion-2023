package org.acme.spring.data.luis.examples.external.rest.controllers;



import org.acme.spring.data.luis.examples.core.business.input.ExampleService;
import org.acme.spring.data.luis.examples.core.entity.PersonaCreate;
import org.acme.spring.data.luis.examples.external.rest.dto.PersonaCreateDto;
import org.acme.spring.data.luis.examples.external.rest.dto.PersonaDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;

import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;


import javax.inject.Inject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/data/example-luis")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion")
public class ExampleController {


    //Inyectamos los metodos del business
    @Inject
    ExampleService exampleService;
    @GET
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = PersonaDto.class)))
    public Response list() {
        var list = exampleService.listaEjemplo();
        return Response.ok(list).build();
    }



    @GET
    @Path("{idPersona}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = PersonaDto.class)))
    @APIResponse(responseCode = "404", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response get(@PathParam("idPersona") Integer idPersona) {
        return exampleService.get(idPersona).map(PersonaDto::fromEntity).map(Response::ok)
                .getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();

    }


//    @POST
//    @Path("post")
//    @APIResponse(responseCode = "400", description = "Error en la peticion", content = @Content(schema = @Schema(implementation = Boolean.class)))
//    public Response create() { return Response.ok(true).status(400).build();}

    @POST
    @Path("post")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = Boolean.class)))
    public Response create(PersonaCreateDto personaCreateDto) {
        return exampleService.create(personaCreateDto.toEntity()).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

//    @PUT
//    @Path("put/{id}")
//    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
//    public Response update(@PathParam("id") Integer id) {return Response.ok(id).build();}

    @PUT
    @Path("put/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response update(@PathParam("id") Integer id, PersonaCreate personaCreate) {
        return exampleService.update(id,personaCreate).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

//    @DELETE
//    @Path("delete/{id}")
//    @APIResponse(responseCode = "200", description = "Peticion exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
//    public Response delete(@PathParam("id") Integer id) { return
//            Response.ok("hola").build();}

    @DELETE
    @Path("delete/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response delete(@PathParam("id") Integer id) {
        return exampleService.delete(id).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

}
