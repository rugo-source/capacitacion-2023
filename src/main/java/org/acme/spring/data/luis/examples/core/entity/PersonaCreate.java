package org.acme.spring.data.luis.examples.core.entity;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class PersonaCreate {

    private String nombre;
    private Integer edad;
    private Integer idEstado;
}
