package org.acme.spring.data.luis.instituciontres.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "EdificioTres", description = "Esta es la entidad para recuperar datos de un edificio")
public class EdificioTresDto {

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al id de la institucion")
    private Integer idSede;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al nombre de la sede")
    private String nombre;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al acronimo de la sede")
    private String acronimo;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al acronimo de la sede")
    private String referencias;



    public static EdificioTresDto fromEntity(Edificio edificio){
        return EdificioTresDto.builder()
                .idSede(edificio.getIdSede())
                .nombre(edificio.getNombre())
                .acronimo(edificio.getAcronimo())
                .referencias(edificio.getReferencia())
                .build();
    }

}
