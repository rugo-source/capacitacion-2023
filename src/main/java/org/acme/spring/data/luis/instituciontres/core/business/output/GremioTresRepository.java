package org.acme.spring.data.luis.instituciontres.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Gremio;

import java.util.List;
import java.util.Optional;

public interface GremioTresRepository {

    List<Gremio> findAllNative();

    boolean existByNombre(String nombre);
    boolean existByAcronimo(String acronimo);

    Optional<Gremio> findBy(Integer idGremio);

    void save(Gremio gremioPersist);
    void update(Gremio gremioChange);

    void deleteBy(Integer id);
}
