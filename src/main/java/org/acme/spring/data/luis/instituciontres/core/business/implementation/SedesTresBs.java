package org.acme.spring.data.luis.instituciontres.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.luis.instituciontres.core.business.input.SedesTresService;
import org.acme.spring.data.luis.instituciontres.core.business.output.EdificioTresRepository;
import org.acme.spring.data.luis.instituciontres.core.business.output.InstitucionTresRepository;
import org.acme.spring.data.luis.instituciontres.core.business.output.SedesTresRepository;
import org.acme.spring.data.luis.instituciontres.core.entity.SedeTresBusqueda;
import org.acme.spring.data.luis.instituciontres.core.entity.SedesTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.Parameter;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class SedesTresBs implements SedesTresService {

    @Inject
    SedesTresRepository sedesTresRepository;

    @Inject
    InstitucionTresRepository institucionTresRepository;

    @Inject
    EdificioTresRepository edificioTresRepository;


    @Override
    public Either<ErrorCodes, List<Sede>> getSedesBy(SedeTresBusqueda sedeTresBusqueda) {
        Either<ErrorCodes, List<Sede>> resultado;

        var idInstitucion = institucionTresRepository.findBy(sedeTresBusqueda.getIdInstitucion());

        if(idInstitucion.isPresent()){
            var listSedes = sedesTresRepository.findAllByIdInstitucion(sedeTresBusqueda.getIdInstitucion());
            resultado = Either.right(listSedes);
        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }

        return resultado;

    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(SedesTresCreate sedesTresCreate) {
        Either<ErrorCodes, Boolean> resultado;

        var identificadorSede = validateRNN004(sedesTresCreate.getIdInstitucion());

        var sedePersist = Sede.builder()
                .idInstitucion(sedesTresCreate.getIdInstitucion())
                .nombre(sedesTresCreate.getNombre())
                .acronimo(sedesTresCreate.getAcronimo())
                .capacidad(sedesTresCreate.getCapacidad())
                .propia(sedesTresCreate.getPropia())
                .identificador(identificadorSede)
                .build();

        if(validateInstitucion(sedePersist.getIdInstitucion())){
            if(validateRNN096Nombre(sedePersist.getNombre())){
                resultado = Either.left(ErrorCodes.RNN096);
            }else if(validateRNN096Acronimo(sedePersist.getAcronimo())){
                resultado = Either.left(ErrorCodes.RNN096);
            }else if(validateRNN003(sedePersist.getCapacidad())) {
                resultado = Either.left(ErrorCodes.RNN003);
            }
            else{
                sedesTresRepository.save(sedePersist);
                resultado = Either.right(true);
            }
        }else{
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;

    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer idSede, SedesTresCreate sedesTresCreate) {
        Either<ErrorCodes,Boolean> resultado;
        var sedeSearch = sedesTresRepository.findBy(idSede);

        if(sedeSearch.isPresent()){
            if(validateRNN096Nombre(sedesTresCreate.getNombre())){
                resultado = Either.left(ErrorCodes.RNN096);
            }else if(validateRNN096Acronimo(sedesTresCreate.getAcronimo())){
                resultado = Either.left(ErrorCodes.RNN096);
            }else if(validateRNN003(sedesTresCreate.getCapacidad())) {
                resultado = Either.left(ErrorCodes.RNN003);
            }
            else{
               var sedeChange = sedeSearch.get();
               sedeChange.setIdInstitucion(sedesTresCreate.getIdInstitucion());
               sedeChange.setNombre(sedesTresCreate.getNombre());
               sedeChange.setAcronimo(sedesTresCreate.getAcronimo());
               sedeChange.setCapacidad(sedesTresCreate.getCapacidad());
               sedeChange.setPropia(sedesTresCreate.getPropia());
               sedesTresRepository.update(sedeChange);
               resultado = Either.right(true);
            }
        }else{
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idSede) {
        Either<ErrorCodes,Boolean> resultado;
        var sedeSearch = sedesTresRepository.findBy(idSede);
        if(sedeSearch.isPresent()){
            if(validateRNN016(idSede)){
                resultado = Either.left(ErrorCodes.RNN016);
            }else {
                sedesTresRepository.deleteBy(idSede);
                resultado = Either.right(true);
            }
        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return  resultado;
    }


    public boolean validateRNN096Nombre(String nombre){
        return sedesTresRepository.existByNombre(nombre);
    }

    public boolean validateRNN096Acronimo(String acronimo){
        return sedesTresRepository.existByAcronimo(acronimo);
    }

    public boolean validateRNN003(Integer capacidad){
        var bandera = false;
        if(capacidad <= 0){
            bandera = true;
        }
        return bandera;
    }

    public boolean validateInstitucion(Integer idInstitucion){
        var bandera = false;
        var institucionSearch = institucionTresRepository.findBy(idInstitucion);
        if(institucionSearch.isPresent()){
            bandera = true;
        }
        return bandera;
    }

    public String validateRNN004(Integer idInstitucion){
        var identificadorInstitucion = institucionTresRepository.getIdentificadorById(idInstitucion);
        var strIdInstitucion = identificadorInstitucion.get();
        var strIdDigitos = String.format("%04d",sedesTresRepository.getIdDigitos());
        String identificador = strIdInstitucion + strIdDigitos;
        return identificador;
    }

    public boolean validateRNN016(Integer idSede){
        return edificioTresRepository.existByIdSede(idSede);
    }
}
