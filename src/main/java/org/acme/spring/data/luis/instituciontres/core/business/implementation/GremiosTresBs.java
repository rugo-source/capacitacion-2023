package org.acme.spring.data.luis.instituciontres.core.business.implementation;


import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.luis.instituciontres.core.business.input.GremiosTresService;
import org.acme.spring.data.luis.instituciontres.core.business.output.GremioTresRepository;
import org.acme.spring.data.luis.instituciontres.core.business.output.InstitucionTresRepository;
import org.acme.spring.data.luis.instituciontres.core.business.output.MiembroGremioTresRepository;
import org.acme.spring.data.luis.instituciontres.core.entity.GremioTresCreate;
import org.acme.spring.data.luis.instituciontres.core.entity.MiembroGremioTresCreate;

import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class GremiosTresBs implements GremiosTresService {

    @Inject
    GremioTresRepository gremioTresRepository;

    @Inject
    MiembroGremioTresRepository miembroGremioTresRepository;

    @Inject
    InstitucionTresRepository institucionTresRepository;


    @Override
    public List<Gremio> listGremios() {
        return gremioTresRepository.findAllNative();
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(GremioTresCreate gremioTresCreate) {
        Either<ErrorCodes,Boolean> resultado;

        var gremioPersist = Gremio.builder()
                .nombre(gremioTresCreate.getNombre())
                .acronimo(gremioTresCreate.getAcronimo())
                .build();
        if(validateRNN097Nombre(gremioPersist.getNombre())){
            resultado = Either.left(ErrorCodes.RNN097);
        }else if (validateRNN097Acronimo(gremioPersist.getAcronimo())){
            resultado = Either.left(ErrorCodes.RNN097);
        }else{
            gremioTresRepository.save(gremioPersist);
            resultado = Either.right(true);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer id, GremioTresCreate gremioTresCreate) {
        Either<ErrorCodes,Boolean> resultado;
        var gremioSearch = gremioTresRepository.findBy(id);
        if(gremioSearch.isPresent()){
            var nombre = gremioTresCreate.getNombre();
            var acronimo = gremioTresCreate.getAcronimo();
            if(validateRNN097Nombre(nombre)){
                resultado = Either.left(ErrorCodes.RNN097);
            }else if(validateRNN097Acronimo(acronimo)){
                resultado = Either.left(ErrorCodes.RNN097);
            } else {
                var gremioChange = gremioSearch.get();
                gremioChange.setNombre(gremioTresCreate.getNombre());
                gremioChange.setAcronimo(gremioTresCreate.getAcronimo());
                gremioTresRepository.update(gremioChange);
                resultado = Either.right(true);
            }
        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer id) {
        Either<ErrorCodes,Boolean> resultado;
        var gremioSearch = gremioTresRepository.findBy(id);
        if(gremioSearch.isPresent()){
            if(validateRNN024(id)){
                resultado = Either.left(ErrorCodes.RNN024);
            }else{
                gremioTresRepository.deleteBy(id);
                resultado = Either.right(true);
            }
        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;

    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> createMiembro(MiembroGremioTresCreate miembroGremioTresCreate) {
        Either<ErrorCodes,Boolean> resultado;
        var miembroGremioPersist = MiembroGremio.builder()
                .idGremio(miembroGremioTresCreate.getIdGremio())
                .idInstitucion(miembroGremioTresCreate.getIdInstitucion())
                .build();
        var institucionSearch = institucionTresRepository.findBy(miembroGremioPersist.getIdInstitucion());
        var gremioSearch = gremioTresRepository.findBy(miembroGremioPersist.getIdGremio());
        if (institucionSearch.isPresent() && gremioSearch.isPresent()){
            if(validateRNN026(miembroGremioPersist.getIdInstitucion())){
                resultado = Either.left(ErrorCodes.RNN026);
            }else{
                miembroGremioTresRepository.createMiembro(miembroGremioPersist);
                resultado = Either.right(true);
            }
        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }







    public boolean validateRNN097Nombre(String nombre){
        return gremioTresRepository.existByNombre(nombre);
    }

    public boolean validateRNN097Acronimo(String acronimo){
        return gremioTresRepository.existByAcronimo(acronimo);
    }

    public boolean validateRNN024(Integer id){
        return miembroGremioTresRepository.getGremio(id);
    }

    public boolean validateRNN026(Integer idInstitucion){
        return miembroGremioTresRepository.getGremioInstitucion(idInstitucion);
    }


}
