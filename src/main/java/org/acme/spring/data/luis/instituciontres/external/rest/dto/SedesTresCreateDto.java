package org.acme.spring.data.luis.instituciontres.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.instituciontres.core.entity.SedesTresCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SedesTresCreate", description = "Esta es la entidad para guardar una sede")
public class SedesTresCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de la institucion asociado a una sede")
    private Integer idInstitucion;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3,max=100, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al nombre de una sede")
    private String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3,max=100, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al acronimo de una sede")
    private String acronimo;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia a la capacidad de una sede")
//    @Size(min = 1, message = "RNS002")
    private Integer capacidad;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia si la sede pertenece a una institucion")
    private Boolean propia;

    public SedesTresCreate toEntity(){
        return SedesTresCreate.builder()
                .idInstitucion(this.idInstitucion)
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .capacidad(this.capacidad)
                .propia(this.propia)
                .build();
    }
}
