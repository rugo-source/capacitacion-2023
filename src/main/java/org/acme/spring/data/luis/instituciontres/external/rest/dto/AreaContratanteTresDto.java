package org.acme.spring.data.luis.instituciontres.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AreaContratanteTres", description = "Esta es la entidad para recuperar datos de un area contratante")
public class AreaContratanteTresDto {

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al id de tipo de la institucion")
    private Integer idInstitucion;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al area de el area contratante")
    private String area;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al subarea de el area contratante")
    private String subarea;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al telefono de el area contratante")
    private String telefono;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia a la extension de el area contratante")
    private String extension;

    public static AreaContratanteTresDto fromEntity(AreaContratante areaContratante){
        return AreaContratanteTresDto.builder()
                .idInstitucion(areaContratante.getIdInstitucion())
                .area(areaContratante.getArea())
                .subarea(areaContratante.getSubarea())
                .telefono(areaContratante.getTelefono())
                .extension(areaContratante.getExtension())
                .build();
    }


}
