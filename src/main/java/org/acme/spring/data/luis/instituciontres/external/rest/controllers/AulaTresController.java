package org.acme.spring.data.luis.instituciontres.external.rest.controllers;


import org.acme.spring.data.luis.instituciontres.core.business.input.AulaTresService;
import org.acme.spring.data.luis.instituciontres.external.rest.dto.AulaTresCreateDto;
import org.acme.spring.data.luis.instituciontres.external.rest.dto.SedesTresCreateDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("catalogos-instituciones/aula-tres")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Luis")
public class AulaTresController {

    @Inject
    AulaTresService aulaTresService;


    @POST
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = AulaTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid AulaTresCreateDto aulaTresCreateDto){
        return aulaTresService.create(aulaTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("{idAula}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = AulaTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idAula") Integer idAula,@Valid AulaTresCreateDto aulaTresCreateDto){
        return aulaTresService.update(idAula,aulaTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("{idAula}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = AulaTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idAula") Integer idAula ){
        return aulaTresService.delete(idAula).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }
}
