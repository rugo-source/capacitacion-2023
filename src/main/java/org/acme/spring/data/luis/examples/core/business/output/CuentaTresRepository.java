package org.acme.spring.data.luis.examples.core.business.output;

import org.acme.spring.data.luis.examples.core.entity.CuentaTres;

import java.util.List;
import java.util.Optional;


public interface CuentaTresRepository {

    List<CuentaTres> findAllNative();

    Optional<CuentaTres> findBy(Integer idCuenta);

    void save(CuentaTres cuentaPersits);

    void update(CuentaTres cuentaChenge);

    void deleteBy(Integer idCuenta);




}
