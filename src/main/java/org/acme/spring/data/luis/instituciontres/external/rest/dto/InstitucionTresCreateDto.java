package org.acme.spring.data.luis.instituciontres.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.instituciontres.core.entity.InstitucionTresCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "InstitucionTresCreate", description = "Esta es la entidad para guardar una institucion")
public class InstitucionTresCreateDto {


    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de tipo de la institucion")
    private Integer idTipo;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de clasificacion de la institucion")
    private Integer idClasificacion;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de categoria de institucion")
    private Integer idCategoria;


    @JsonProperty
    @Positive(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id el subsistema de universidad de la institucion")
    private Integer isSubsistemaUniversidad;

    @JsonProperty
    @Positive(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id del subsistema de bachillerato de la institucion")
    private Integer idSubsistemaBachillerato;

    @JsonProperty
    @Pattern(regexp = "[0-9]{0,4}$")
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(max=4, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al identificador de institucion")
    private String identificador;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3,max=100, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al nombre de la institucion")
    private String nombre;

    @JsonProperty
    @NotNull(message = "RNN001")
    @NotEmpty(message = "RNS001")
    @Size(min = 2,max=25, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al acronimo de la institucion")
    private String acronimo;

    @JsonProperty
    @Size(max = 100, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al cct de la institucion")
    private String cct;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al numero de sedes registradas de una institucion")
    private Integer numeroSedesRegistradas;

    public InstitucionTresCreate toEntity(){
        return InstitucionTresCreate.builder()
                .idTipo(this.idTipo)
                .idClasificacion(this.idClasificacion)
                .idCategoria(this.idCategoria)
                .idSubsistemaUniversidad(this.isSubsistemaUniversidad)
                .idSubsistemaBachillerato(this.idSubsistemaBachillerato)
                .identificador(this.identificador)
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .cct(this.cct)
                .numeroSedesRegistradas(this.numeroSedesRegistradas)
                .build();
    }


}
