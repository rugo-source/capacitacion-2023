package org.acme.spring.data.luis.instituciontres.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.instituciontres.core.entity.EdificioTresBusqueda;
import org.acme.spring.data.luis.instituciontres.core.entity.SedeTresBusqueda;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "EdificioTresBusqueda", description = "Esta es la entidad para buscar un edificion por el id de la sed")
public class EdificioTresBusquedaDto {

    @JsonProperty
    @Schema(description = "Este es el atributo que refere al id de la sede")
    private Integer idSede;

    public EdificioTresBusqueda toEntity(){
        return EdificioTresBusqueda.builder()
                .idSede(this.idSede)
                .build();
    }
}
