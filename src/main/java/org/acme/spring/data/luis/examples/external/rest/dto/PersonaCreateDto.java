package org.acme.spring.data.luis.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.examples.core.entity.PersonaCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "PersonaCreate", description = "Esta en la entidad para guardar una persona")
public class PersonaCreateDto {
    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al nombre de la persona")
    private String nombre;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia a la edad de la persona")
    private Integer edad;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al idEstado de la persona")
    private Integer idEstado;


    public PersonaCreate toEntity(){
        return PersonaCreate.builder()
                .nombre(this.nombre)
                .idEstado(this.idEstado)
                .edad(this.edad)
                .build();
    }


}
