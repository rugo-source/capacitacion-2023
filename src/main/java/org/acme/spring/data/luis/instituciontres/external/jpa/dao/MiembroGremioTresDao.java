package org.acme.spring.data.luis.instituciontres.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.GremioJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.acme.spring.data.luis.instituciontres.core.business.output.MiembroGremioTresRepository;
import org.acme.spring.data.luis.instituciontres.external.jpa.repository.MiembroGremioTresJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Optional;

@ApplicationScoped
public class MiembroGremioTresDao implements MiembroGremioTresRepository {

    @Inject
    MiembroGremioTresJpaRepository miembroGremioTresJpaRepository;

    @Inject
    EntityManager entityManager;


    @Override
    @SuppressWarnings("unchecked")
    public Boolean getGremio(Integer idGremio){
        String queryStr = "select count(tin06.fk_id_gremio) > 0 from tin06_gremio_institucion tin06 where tin06.fk_id_gremio = :idGremio";
        Query query = entityManager.createNativeQuery(queryStr);
        query.setParameter("idGremio", idGremio);
        return (Boolean) query.getResultStream().findFirst().orElseGet(() -> false);
    }

    @Override
    public void createMiembro(MiembroGremio miembroGremio){
        var miembroGremioJpa = MiembroGremioJpa.builder()
                .id(MiembroGremioIdJpa.builder()
                        .idGremio(miembroGremio.getIdGremio())
                        .idInstitucion(miembroGremio.getIdInstitucion())
                        .build())
                .build();
        miembroGremioTresJpaRepository.saveAndFlush(miembroGremioJpa);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Boolean getGremioInstitucion(Integer idInstitucion){
        String queryStr = "select count(tin06.fk_id_institucion) > 0 from tin06_gremio_institucion tin06 where tin06.fk_id_institucion = :idInstitucion";
        Query query = entityManager.createNativeQuery(queryStr);
        query.setParameter("idInstitucion", idInstitucion);
        return (Boolean) query.getResultStream().findFirst().orElseGet(() -> false);

    }
}
