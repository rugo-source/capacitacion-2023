package org.acme.spring.data.luis.instituciontres.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SedesTresCreate {
    private Integer idInstitucion;
    private String nombre;
    private String acronimo;
    private Integer capacidad;
    private Boolean propia;
}
