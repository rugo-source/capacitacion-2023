package org.acme.spring.data.luis.examples.core.entity;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class Persona {

    private Integer id;
    private Integer idEstado;
    private String nombre;
    private Integer edad;

    private Boolean registrar;
    private Boolean editar;
    private Boolean eliminar;
    private Boolean consultar;
    private Boolean configurar;


    private EstadoPersonaTres estadoPersonaTres;
}
