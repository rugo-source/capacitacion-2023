package org.acme.spring.data.luis.instituciontres.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.instituciontres.core.entity.InstitucionTresBusqueda;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "InstitucionTresBusqueda", description = "Esta es la entidad para buscar una  institucion")
public class InstitucionTresBusquedasDto {

    @JsonProperty
    @Schema(description = "Este es el atributo que refere al identificador de la institucion")
    private String identificador;

    @JsonProperty
    @Schema(description = "Este es el atributo que refere al nombre de la institucion")
    private String nombre;

    @JsonProperty
    @Schema(description = "Este es el atributo que refere al acronimo de la institucion")
    private String acronimo;


    public InstitucionTresBusqueda toEntity(){
        return InstitucionTresBusqueda.builder()
                .identificador(this.identificador)
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .build();
    }

}
