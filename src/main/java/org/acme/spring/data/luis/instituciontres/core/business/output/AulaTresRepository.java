package org.acme.spring.data.luis.instituciontres.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Aula;

import java.util.Optional;

public interface AulaTresRepository {

    void save(Aula aulaPersist);
    void update(Aula aulaChange);
    void deleteBy(Integer idAula);

    Optional<Aula> findBy(Integer idAula);
}
