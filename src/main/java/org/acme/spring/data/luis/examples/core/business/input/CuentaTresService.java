package org.acme.spring.data.luis.examples.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.luis.examples.core.entity.CuentaTres;
import org.acme.spring.data.luis.examples.core.entity.CuentaTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface CuentaTresService {

    List<CuentaTres> listaCuentas();

    //Cuenta
    Either<ErrorCodes,Boolean> create(CuentaTresCreate toEntity);

    Either<ErrorCodes,Boolean> update(Integer id, CuentaTresCreate cuentaTresCreate);

    Either<ErrorCodes,Boolean> delete(Integer id);


}
