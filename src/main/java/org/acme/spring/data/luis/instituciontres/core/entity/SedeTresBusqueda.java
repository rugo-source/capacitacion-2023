package org.acme.spring.data.luis.instituciontres.core.entity;


import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SedeTresBusqueda {

    private Integer idInstitucion;
}
