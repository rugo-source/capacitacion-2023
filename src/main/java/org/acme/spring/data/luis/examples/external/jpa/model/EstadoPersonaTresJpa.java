package org.acme.spring.data.luis.examples.external.jpa.model;


import lombok.*;
import org.acme.spring.data.luis.examples.core.entity.EstadoPersonaTres;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "estado_persona_tres")
public class EstadoPersonaTresJpa {

    @Id
    @Column(name = "id_estado")
    private Integer id;

    @Column(name = "tx_nombre")
    private String nombre;

    public EstadoPersonaTres toEntity(){
        return EstadoPersonaTres.builder()
                .id(this.id)
                .nombre(this.nombre)
                .build();
    }
}
