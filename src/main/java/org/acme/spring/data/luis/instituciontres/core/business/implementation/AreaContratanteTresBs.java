package org.acme.spring.data.luis.instituciontres.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.luis.instituciontres.core.business.input.AreaContratanteTresService;
import org.acme.spring.data.luis.instituciontres.core.business.output.AreaContratanteTresRepository;
import org.acme.spring.data.luis.instituciontres.core.entity.AreaContratanteTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;


@ApplicationScoped
public class AreaContratanteTresBs implements AreaContratanteTresService {

    @Inject
    AreaContratanteTresRepository areaContratanteTresRepository;

    @Override
    public List<AreaContratante> listaAreasContratantes() {
        return areaContratanteTresRepository.findAllNative();
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(AreaContratanteTresCreate areaContratanteTresCreate) {
        Either<ErrorCodes,Boolean> resultado = Either.left(ErrorCodes.NOT_FOUND);

//        if()
        var areaContratantePersist = AreaContratante.builder()
                .idInstitucion(areaContratanteTresCreate.getIdInstitucion())
                .area(areaContratanteTresCreate.getArea())
                .subarea(areaContratanteTresCreate.getSubarea())
                .telefono(areaContratanteTresCreate.getTelefono())
                .extension(areaContratanteTresCreate.getExtension())
                .build();
        areaContratanteTresRepository.save(areaContratantePersist);
        resultado = Either.right(true);

        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer id, AreaContratanteTresCreate areaContratanteTresCreate) {
        Either<ErrorCodes, Boolean> resultado;
        var areaContratanteSearch = areaContratanteTresRepository.findBy(id);
        if(areaContratanteSearch.isPresent()){
            var areaContratanteChange = areaContratanteSearch.get();
                areaContratanteChange.setArea(areaContratanteTresCreate.getArea());
                areaContratanteChange.setSubarea(areaContratanteTresCreate.getSubarea());
                areaContratanteChange.setTelefono(areaContratanteTresCreate.getTelefono());
                areaContratanteChange.setExtension(areaContratanteTresCreate.getExtension());
                areaContratanteTresRepository.update(areaContratanteChange);
                resultado = Either.right(true);
        }
        else{
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer id) {
       Either<ErrorCodes,Boolean> resultado;
       var areaContratanteSearch = areaContratanteTresRepository.findBy(id);
       if(areaContratanteSearch.isPresent()){
           areaContratanteTresRepository.deleteBy(id);
           resultado = Either.right(true);
       }else{
           resultado = Either.left(ErrorCodes.NOT_FOUND);
       }
       return resultado;
    }

}
