package org.acme.spring.data.luis.instituciontres.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.luis.instituciontres.core.entity.InstitucionTresBusqueda;
import org.acme.spring.data.luis.instituciontres.core.entity.InstitucionTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface InstitucionTresService {



    Either<ErrorCodes, List<Institucion>> getInstitutesBy(InstitucionTresBusqueda institucionTresBusqueda);
    Either<ErrorCodes, Boolean> create(InstitucionTresCreate toEntity);
    Either<ErrorCodes, Boolean> update(Integer id, InstitucionTresCreate institucionTresCreate);


}
