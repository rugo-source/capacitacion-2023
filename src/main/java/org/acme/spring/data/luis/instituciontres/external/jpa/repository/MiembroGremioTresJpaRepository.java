package org.acme.spring.data.luis.instituciontres.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MiembroGremioTresJpaRepository extends JpaRepository<MiembroGremioJpa,Integer> {


}



