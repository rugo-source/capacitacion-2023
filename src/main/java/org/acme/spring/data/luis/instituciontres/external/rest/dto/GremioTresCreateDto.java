package org.acme.spring.data.luis.instituciontres.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.instituciontres.core.entity.GremioTresCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "GremioTresCreate", description = "Esta es la enitdad para guardar un gremio")
public class GremioTresCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3,max=100, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al nombre de el gremio")
    private String nombre;

    @JsonProperty
    @NotNull(message = "RNN001")
    @NotEmpty(message = "RNS001")
    @Size(min = 2,max=10, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al acronimo de el gremio")
    private String acronimo;

    public GremioTresCreate toEntity(){
        return GremioTresCreate.builder()
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .build();
    }
}
