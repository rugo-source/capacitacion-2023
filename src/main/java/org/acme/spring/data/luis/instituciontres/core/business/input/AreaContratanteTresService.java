package org.acme.spring.data.luis.instituciontres.core.business.input;

import io.vavr.control.Either;

import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.luis.instituciontres.core.entity.AreaContratanteTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface AreaContratanteTresService {

    List<AreaContratante> listaAreasContratantes();

    Either<ErrorCodes,Boolean> create(AreaContratanteTresCreate toEntity);
    Either<ErrorCodes,Boolean> update(Integer id, AreaContratanteTresCreate areaContratanteTresCreate);
    Either<ErrorCodes,Boolean> delete(Integer id);

}
