package org.acme.spring.data.luis.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.examples.core.entity.Persona;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PersonaDto {

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al nombre de la persona")
    private String nombre;

    @JsonProperty
    @Schema(description = "Bandera que indica si se puede realizar el registro de una persona")
    private Boolean registrar;

    @JsonProperty
    @Schema(description = "Bandera que indica si se puede realizar el editar de una persona")
    private Boolean editar;

    @JsonProperty
    @Schema(description = "Bandera que indica si se puede realizar el eliminado de una persona")
    private Boolean eliminar;

    @JsonProperty
    @Schema(description = "Bandera que indica si se puede realizar la consulta de una persona")
    private Boolean consultar;

    @JsonProperty
    @Schema(description = "Bandera que indica si se puede realizar la configuracion de una persona")
    private Boolean configurar;

    public static PersonaDto fromEntity(Persona persona){
        return PersonaDto.builder()
                .nombre(persona.getNombre())
                .registrar(persona.getRegistrar())
                .editar(persona.getEditar())
                .eliminar(persona.getEliminar())
                .consultar(persona.getConsultar())
                .configurar(persona.getConfigurar())
                .build();
    }


}
