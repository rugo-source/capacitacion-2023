package org.acme.spring.data.luis.instituciontres.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.instituciontres.core.entity.AulaTresCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AulaTresCreate", description = "Esta es la entidad para guardar un aula")
public class AulaTresCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de el edificio asociado a un aula")
    private Integer idEdificio;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3,max=100, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al nombre de un aula")
    private String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de el tipo de equipamiento asociado a un aula")
    private Integer idTipoEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia a la capacidad de equipamiento de un aula")
    private Integer capacidadEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia a la cantidad de equipamiento de un aula")
    private Integer cantidadEquipamiento;

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia a la capacidad de un aula")
    private Integer capacidad;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3,max=100, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia las observaciones de un aula")
    private String observaciones;

    public AulaTresCreate toEntity(){
        return AulaTresCreate.builder()
                .idEdificio(this.idEdificio)
                .nombre(this.nombre)
                .idTipoEquipamiento(this.idTipoEquipamiento)
                .capacidadEquipamiento(this.capacidadEquipamiento)
                .cantidadEquipamiento(this.cantidadEquipamiento)
                .capacidad(this.capacidad)
                .observaciones(this.observaciones)
                .build();
    }



}
