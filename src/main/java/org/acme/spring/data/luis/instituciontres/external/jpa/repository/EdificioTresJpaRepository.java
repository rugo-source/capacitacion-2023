package org.acme.spring.data.luis.instituciontres.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EdificioTresJpaRepository extends JpaRepository<EdificioJpa,Integer> {

    boolean existsByIdSede(Integer idSede);
}
