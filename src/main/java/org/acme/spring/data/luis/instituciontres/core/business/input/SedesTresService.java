package org.acme.spring.data.luis.instituciontres.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.luis.instituciontres.core.entity.SedeTresBusqueda;
import org.acme.spring.data.luis.instituciontres.core.entity.SedesTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface SedesTresService {


    Either<ErrorCodes,List<Sede>> getSedesBy(SedeTresBusqueda sedeTresBusqueda);

    Either<ErrorCodes, Boolean> create(SedesTresCreate toEntity);
    Either<ErrorCodes,Boolean> update(Integer idSede, SedesTresCreate sedesTresCreate);
    Either<ErrorCodes,Boolean> delete(Integer idSede);
}
