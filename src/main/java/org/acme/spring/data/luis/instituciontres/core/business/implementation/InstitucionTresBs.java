package org.acme.spring.data.luis.instituciontres.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.luis.instituciontres.core.business.input.InstitucionTresService;
import org.acme.spring.data.luis.instituciontres.core.business.output.InstitucionTresRepository;
import org.acme.spring.data.luis.instituciontres.core.entity.InstitucionTresBusqueda;
import org.acme.spring.data.luis.instituciontres.core.entity.InstitucionTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class InstitucionTresBs implements InstitucionTresService {

    @Inject
    InstitucionTresRepository institucionTresRepository;


    @Override
    public Either<ErrorCodes, List<Institucion>> getInstitutesBy(InstitucionTresBusqueda institucionTresBusqueda) {
        Either<ErrorCodes, List<Institucion>> resultado;

        var identificadorSearch = institucionTresRepository.existByIdentificador(institucionTresBusqueda.getIdentificador());
        var nombreSearch = institucionTresRepository.existByNombre(institucionTresBusqueda.getNombre());
        var acronimoSearch = institucionTresRepository.existByAcronimo(institucionTresBusqueda.getAcronimo());

        if (identificadorSearch || nombreSearch || acronimoSearch){
            var identificador = institucionTresBusqueda.getIdentificador();
            var nombre = institucionTresBusqueda.getNombre();
            var acronimo = institucionTresBusqueda.getAcronimo();
            var listInstitucion = institucionTresRepository.findInstitutesBy(identificador, nombre, acronimo);
            resultado = Either.right(listInstitucion);
        }
        else{
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }


    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(InstitucionTresCreate institucionTresCreate) {
        Either<ErrorCodes, Boolean> resultado;

        var institucionPersist = Institucion.builder()
                .identificador(institucionTresCreate.getIdentificador())
                .nombre(institucionTresCreate.getNombre())
                .acronimo(institucionTresCreate.getAcronimo())
                .cct(institucionTresCreate.getCct())
                .idTipo(institucionTresCreate.getIdTipo())
                .idSubsistemaUniversidad(institucionTresCreate.getIdSubsistemaUniversidad())
                .idSubsistemaBachillerato(institucionTresCreate.getIdSubsistemaBachillerato())
                .idClasificacion(institucionTresCreate.getIdClasificacion())
                .idCategoria(institucionTresCreate.getIdCategoria())
                .numeroSedesRegistradas(institucionTresCreate.getNumeroSedesRegistradas())
                .build();
            if(validateRNN013(institucionPersist.getIdentificador())){
                resultado = Either.left(ErrorCodes.RNN013);
            }else if (validateRNN094(institucionPersist.getNombre())){
                resultado = Either.left(ErrorCodes.RNN094);
            }else {
                institucionTresRepository.save(institucionPersist);
                resultado = Either.right(true);
            }
            return resultado;
    }


    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer idInstitucion, InstitucionTresCreate institucionTresCreate) {
        Either<ErrorCodes, Boolean> resultado;
        var institucionSearch = institucionTresRepository.findBy(idInstitucion);
        if (institucionSearch.isPresent()) {
            var identificador = institucionTresCreate.getIdentificador();
            var nombre = institucionTresCreate.getNombre();
            if (validateRNN013(identificador)) {
                resultado = Either.left(ErrorCodes.RNN013);
            } else if (validateRNN094(nombre)) {
                resultado = Either.left(ErrorCodes.RNN094);
            } else {
                var institucionChange = institucionSearch.get();
                institucionChange.setIdentificador(institucionTresCreate.getIdentificador());
                institucionChange.setNombre(institucionTresCreate.getNombre());
                institucionChange.setAcronimo(institucionTresCreate.getAcronimo());
                institucionChange.setCct(institucionTresCreate.getCct());
                institucionChange.setIdSubsistemaUniversidad(institucionTresCreate.getIdSubsistemaUniversidad());
                institucionChange.setIdSubsistemaBachillerato(institucionTresCreate.getIdSubsistemaBachillerato());
                institucionChange.setIdClasificacion(institucionTresCreate.getIdClasificacion());
                institucionChange.setIdCategoria(institucionTresCreate.getIdCategoria());
                institucionChange.setNumeroSedesRegistradas(institucionTresCreate.getNumeroSedesRegistradas());
                institucionTresRepository.update(institucionChange);
                resultado = Either.right(true);
            }
        } else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }

    /* aqui lo mismo Luis cosas comentadas si nos las vas usar borralas y si aun no lo sabes marcalas con un TODO
    * seguido del por que esta ese bloque comentado.
    * */
//    public boolean validateRNN013(Integer idInstitucion, String identificador) {
//        return institucionTresRepository.existByIdentificadorAndIdNot(identificador, idInstitucion);
//    }

    public boolean validateRNN013(String identificador) {
        return institucionTresRepository.existByIdentificador(identificador);
    }

    public boolean validateRNN094(String nombre) {
        return institucionTresRepository.existByNombre(nombre);
    }

//    public boolean validateRNN094(Integer idInstitucion, String nombre) {
//        return institucionTresRepository.existByNombreAndIdNot(nombre, idInstitucion);
//    }


}
