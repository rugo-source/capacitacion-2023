package org.acme.spring.data.luis.instituciontres.core.entity;


import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AreaContratanteTresCreate {

    private Integer idInstitucion;
//    private Integer idDireccion;
    private String area;
    private String subarea;
    private String telefono;
    private String extension;
//    private String institucion;
}
