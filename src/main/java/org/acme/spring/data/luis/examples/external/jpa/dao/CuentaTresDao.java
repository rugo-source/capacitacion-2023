package org.acme.spring.data.luis.examples.external.jpa.dao;

import org.acme.spring.data.luis.examples.core.business.output.CuentaTresRepository;
import org.acme.spring.data.luis.examples.core.entity.CuentaTres;
import org.acme.spring.data.luis.examples.external.jpa.model.CuentaTresJpa;
import org.acme.spring.data.luis.examples.external.jpa.repository.CuentaTresJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class CuentaTresDao implements CuentaTresRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    CuentaTresJpaRepository cuentaTresJpaRepository;

    @Override
    public List<CuentaTres> findAllNative(){
        Stream<CuentaTresJpa> resultado = entityManager.createNativeQuery("Select * from cuenta_tres where fh_inicio <= current_date and fh_fin >= current_date",CuentaTresJpa.class).getResultStream();
        return resultado.map(CuentaTresJpa::toEntity).collect(Collectors.toList());
    }


    @Override
    public Optional<CuentaTres> findBy(Integer idCuenta){
        return cuentaTresJpaRepository.findById(idCuenta).map(CuentaTresJpa::toEntity);
    }

    @Override
    public void save(CuentaTres cuentaPersits){
        cuentaTresJpaRepository.saveAndFlush(CuentaTresJpa.fromEntity(cuentaPersits));
    }

    @Override
    public void update(CuentaTres cuentaChange){
        cuentaTresJpaRepository.saveAndFlush(CuentaTresJpa.fromEntity(cuentaChange));
    }

    @Override
    public void deleteBy(Integer idCuenta){
        cuentaTresJpaRepository.deleteById(idCuenta);
    }






}
