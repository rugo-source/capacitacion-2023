package org.acme.spring.data.luis.examples.core.business.output;

import org.acme.spring.data.luis.examples.core.entity.Persona;

import java.util.List;
import java.util.Optional;

public interface PersonaRepository {

    List<Persona> findAll();

    Optional<Persona> findBy(Integer idPersona);

    void save(Persona personaPersits);

    void update(Persona personaChange);

    void deleteBy(Integer id);
}
