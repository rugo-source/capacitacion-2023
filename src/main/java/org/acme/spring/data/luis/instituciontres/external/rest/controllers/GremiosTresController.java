package org.acme.spring.data.luis.instituciontres.external.rest.controllers;

import org.acme.spring.data.luis.instituciontres.core.business.input.GremiosTresService;
import org.acme.spring.data.luis.instituciontres.core.entity.GremioTresCreate;
import org.acme.spring.data.luis.instituciontres.external.rest.dto.*;

import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("catalogos-instituciones/gremio-tres")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Luis")
public class GremiosTresController {

    @Inject
    GremiosTresService gremiosTresService;

    @POST
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = GremioTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid GremioTresCreateDto gremioTresCreateDto){
        return gremiosTresService.create(gremioTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @Path("asociar-gremio")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = MiembroGremioTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response createMiembro(@Valid MiembroGremioTresCreateDto miembroGremioTresCreateDto){
        return gremiosTresService.createMiembro(miembroGremioTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("{idGremio}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = GremioTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idGremio") Integer idGremio,@Valid GremioTresCreateDto gremioTresCreateDto){
        return gremiosTresService.update(idGremio,gremioTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @GET
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = GremioTresDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response list(){
        var list = gremiosTresService.listGremios();
        return Response.ok(list).build();
    }


    @DELETE
    @Path("{idGremio}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = GremioTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idGremio") Integer idGremio ){
        return gremiosTresService.delete(idGremio).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }




}
