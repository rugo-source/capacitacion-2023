package org.acme.spring.data.luis.examples.core.entity;

import lombok.*;
import org.acme.spring.data.luis.examples.external.jpa.model.PersonaTresJpa;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class CuentaTres {

    private Integer idCuenta;
    private Integer idPersona;
    private String rol;
    private LocalDate inicio;
    private LocalDate fin;

    private PersonaTresJpa persona;
}
