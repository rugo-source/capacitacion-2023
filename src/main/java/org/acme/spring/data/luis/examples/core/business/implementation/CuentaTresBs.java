package org.acme.spring.data.luis.examples.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.luis.examples.core.business.input.CuentaTresService;
import org.acme.spring.data.luis.examples.core.business.output.CuentaTresRepository;
import org.acme.spring.data.luis.examples.core.business.output.PersonaRepository;
import org.acme.spring.data.luis.examples.core.entity.CuentaTres;
import org.acme.spring.data.luis.examples.core.entity.CuentaTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;


@ApplicationScoped
public class CuentaTresBs implements CuentaTresService {

    @Inject
    CuentaTresRepository cuentaTresRepository;

    @Inject
    PersonaRepository personaRepository;


    @Override
    public List<CuentaTres> listaCuentas() {
        return cuentaTresRepository.findAllNative();
    }

    //Crear una cuenta
    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(CuentaTresCreate cuentaTresCreate) {
        Either<ErrorCodes,Boolean> resultado;
//        Either<ErrorCodes, Boolean> resultado = Either.right(true);
        var persona = personaRepository.findBy(cuentaTresCreate.getIdPersona());
        if(persona.isPresent()){
            LocalDate inicio = cuentaTresCreate.getInicio();
            LocalDate fin = cuentaTresCreate.getFin();
            if(validateRNN001(inicio,fin)){
                resultado = Either.left(ErrorCodes.RNN001);
            }
            else if(validateRNN002(inicio)){
                resultado = Either.left(ErrorCodes.RNN002);
            }
            else {
                var cuentaPersits = CuentaTres.builder()
                        .rol(cuentaTresCreate.getRol())
                        .inicio(cuentaTresCreate.getInicio())
                        .fin(cuentaTresCreate.getFin())
                        .idPersona(cuentaTresCreate.getIdPersona())
                        .build();
                cuentaTresRepository.save(cuentaPersits);
                resultado = Either.right(true);
            }
        }else{
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }

    public boolean validateRNN001(LocalDate inicio, LocalDate fin){
        //Logica para validar que la fecha de inicio no sea mayor a la de fin
        if(inicio.isAfter(fin)){
            return true;
        }
        return false;
    }

    public boolean validateRNN002(LocalDate inicio){
        //Logica para validar que la fecha de inicio no es menor que la actual
        LocalDate actual = LocalDate.now();
        if(inicio.isBefore(actual)){
            return true;
        }
        return false;
    }

    //Update
    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer id, CuentaTresCreate cuentaTresCreate){
        Either<ErrorCodes,Boolean> resultado;
        var cuentaSearch = cuentaTresRepository.findBy(id);
        if(cuentaSearch.isPresent()){
            LocalDate inicio = cuentaTresCreate.getInicio();
            LocalDate fin = cuentaTresCreate.getFin();
            if(validateRNN001(inicio,fin)){
                resultado = Either.left(ErrorCodes.RNN001);
            }
            else if(validateRNN002(inicio)){
                resultado = Either.left(ErrorCodes.RNN002);
            }
            else {
               var cuentaChange = cuentaSearch.get();
               cuentaChange.setIdPersona(cuentaTresCreate.getIdPersona());
               cuentaChange.setRol(cuentaTresCreate.getRol());
               cuentaChange.setInicio(cuentaTresCreate.getInicio());
               cuentaChange.setFin(cuentaTresCreate.getFin());
               cuentaTresRepository.update(cuentaChange);
               resultado = Either.right(true);
            }
        }else{
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer id){
        Either<ErrorCodes, Boolean> resultado;
        var cuentaSearch = cuentaTresRepository.findBy(id);
        if(cuentaSearch.isPresent()) {
            cuentaTresRepository.deleteBy(id);
            resultado = Either.right(true);
        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }



}
