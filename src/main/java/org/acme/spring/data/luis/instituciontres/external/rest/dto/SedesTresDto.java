package org.acme.spring.data.luis.instituciontres.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SedesTres", description = "Esta es la entidad para recuperar datos de una sede")
public class SedesTresDto {



    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al id de la institucion")
    private Integer idInstitucion;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al nombre de la sede")
    private String nombre;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al acronimo de la sede")
    private String acronimo;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia a la direccion de la sede")
    private Integer direccion;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia a la capacidad de la sede")
    private Integer capacidad;

    public static SedesTresDto fromEntity(Sede sede){
        return SedesTresDto.builder()
                .idInstitucion(sede.getIdInstitucion())
                .nombre(sede.getNombre())
                .acronimo(sede.getAcronimo())
                .direccion(sede.getIdDireccion())
                .capacidad(sede.getCapacidad())
                .build();
    }



}
