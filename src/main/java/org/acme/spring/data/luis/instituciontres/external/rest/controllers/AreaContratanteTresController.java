package org.acme.spring.data.luis.instituciontres.external.rest.controllers;
import org.acme.spring.data.luis.instituciontres.core.business.input.AreaContratanteTresService;
import org.acme.spring.data.luis.instituciontres.external.rest.dto.AreaContratanteTresCreateDto;
import org.acme.spring.data.luis.instituciontres.external.rest.dto.AreaContratanteTresDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/catalogos-instituciones/area-contratante-tres")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Luis")
public class AreaContratanteTresController {

    @Inject
    AreaContratanteTresService areaContratanteTresService;


    @POST
//    @Path("post")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = AreaContratanteTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid AreaContratanteTresCreateDto areaContratanteTresCreateDto){
        return areaContratanteTresService.create(areaContratanteTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }



    @PUT
    @Path("/{idAreaContratante}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = AreaContratanteTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idAreaContratante") Integer idAreaContratante,@Valid AreaContratanteTresCreateDto areaContratanteTresCreateDto){
        return areaContratanteTresService.update(idAreaContratante,areaContratanteTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("/{idAreaContratante}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = AreaContratanteTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idAreaContratante") Integer idAreaContratante ){
        return areaContratanteTresService.delete(idAreaContratante).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }


    @GET
//    @Path("lista")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = AreaContratanteTresDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response list(){
        var list = areaContratanteTresService.listaAreasContratantes();
        return Response.ok(list).build();
    }

}
