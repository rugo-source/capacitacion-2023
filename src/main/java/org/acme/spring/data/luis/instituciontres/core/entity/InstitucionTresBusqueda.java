package org.acme.spring.data.luis.instituciontres.core.entity;


import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class InstitucionTresBusqueda {

    private String identificador;
    private String nombre;
    private String acronimo;

}
