package org.acme.spring.data.luis.instituciontres.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;

public interface SedesTresJpaRepository extends JpaRepository<SedeJpa, Integer> {

    boolean existsByNombre(String nombre);
    boolean existsByAcronimo(String acronimo);

    @Query(value = "SELECT MAX(id) FROM SedeJpa")
    public Integer max();
}
