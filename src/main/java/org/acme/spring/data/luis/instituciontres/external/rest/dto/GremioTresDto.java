package org.acme.spring.data.luis.instituciontres.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "GremioTres", description = "Esta es la entidad para recuperar datos de un gremio")
public class GremioTresDto {

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al nombre de el gremio")
    private String nombre;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al acronimo del gremio")
    private String acronimo;

    public static GremioTresDto fromEntity(Gremio gremio){
        return GremioTresDto.builder()
                .nombre(gremio.getNombre())
                .acronimo(gremio.getAcronimo())
                .build();
    }
}
