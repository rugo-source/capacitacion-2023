package org.acme.spring.data.luis.instituciontres.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Institucion;

import java.util.List;
import java.util.Optional;

public interface InstitucionTresRepository {


    boolean existByIdentificadorAndIdNot(String identificador, Integer id);
    boolean existByNombreAndIdNot(String nombre, Integer id);
    boolean existByIdentificador(String identificador);
    boolean existByNombre(String nombre);
    boolean existByAcronimo(String acronimo);

    Optional<Institucion> findBy(Integer idInstitucion);

    void save(Institucion institucionPersist);
    void update(Institucion institucionChange);


    Optional<String> getIdentificadorById(Integer idInstitucion);
    List<Institucion> findInstitutesBy(String identificador, String nombre, String acronimo);
}
