package org.acme.spring.data.luis.examples.external.jpa.dao;

import org.acme.spring.data.cristian.examples.core.entity.Usuario;
import org.acme.spring.data.cristian.examples.external.jpa.model.UsuarioJpa;
import org.acme.spring.data.cristian.examples.external.jpa.repository.UsuarioJpaRepository;
import org.acme.spring.data.luis.examples.core.business.output.UsuarioRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class UsuarioDao implements UsuarioRepository {

    @Inject
    EntityManager entityManager;
    @Inject
    UsuarioJpaRepository usuarioJpaRepository;

    @Override
    public List<Usuario> findAll() {
        return usuarioJpaRepository.findAll().stream().map(UsuarioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<Usuario> findAllNative() {
        return null;
    }

    @Override
    public List<Usuario> findAllOrm() {
        return null;
    }

}
