package org.acme.spring.data.luis.instituciontres.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.MiembroGremioIdJpa;

import java.util.Optional;

public interface MiembroGremioTresRepository {


    Boolean getGremio(Integer idGremio);
    Boolean getGremioInstitucion(Integer idInstitucion);
    void createMiembro(MiembroGremio miembroGremio);

}
