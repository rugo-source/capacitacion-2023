package org.acme.spring.data.luis.examples.external.jpa.dao;

import org.acme.spring.data.luis.examples.core.business.output.PersonaRepository;
import org.acme.spring.data.luis.examples.core.entity.Persona;
import org.acme.spring.data.luis.examples.external.jpa.model.PersonaTresJpa;
import org.acme.spring.data.luis.examples.external.jpa.repository.PersonaJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class PersonaDao implements PersonaRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    PersonaJpaRepository personaJpaRepository;

    @Override
    public List<Persona> findAll(){
        return personaJpaRepository.findAll().stream().map(personaJpa -> {
            var personaEntidad = personaJpa.toEntity();
            personaEntidad.setEstadoPersonaTres(personaJpa.getEstadoPersonaTres().toEntity());
            return personaEntidad;
        }).collect(Collectors.toList());
    }


    @Override
    public Optional<Persona> findBy(Integer idPersona){
        return personaJpaRepository.findById(idPersona).map(personaTresJpa -> {
           var personaEntidad = personaTresJpa.toEntity();
           personaEntidad.setEstadoPersonaTres(personaTresJpa.getEstadoPersonaTres().toEntity());
           return personaEntidad;
      });
    }


    @Override
    public void save(Persona personaPersits){
        personaJpaRepository.saveAndFlush(PersonaTresJpa.fromEntity(personaPersits));
    }

    @Override
    public void update(Persona personaChange){
        personaJpaRepository.saveAndFlush(PersonaTresJpa.fromEntity(personaChange));
    }

    @Override
    public void deleteBy(Integer id){
        personaJpaRepository.deleteById(id);
    }

}
