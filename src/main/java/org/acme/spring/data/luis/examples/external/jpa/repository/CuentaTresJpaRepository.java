package org.acme.spring.data.luis.examples.external.jpa.repository;

import org.acme.spring.data.luis.examples.external.jpa.model.CuentaTresJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CuentaTresJpaRepository extends JpaRepository<CuentaTresJpa,Integer> {

    @Query("from CuentaTresJpa cj")
    List<CuentaTresJpa> findAllOrm();


}
