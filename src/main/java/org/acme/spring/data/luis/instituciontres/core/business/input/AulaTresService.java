package org.acme.spring.data.luis.instituciontres.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.luis.instituciontres.core.entity.AulaTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

public interface AulaTresService {

    Either<ErrorCodes,Boolean> create(AulaTresCreate toEntity);
    Either<ErrorCodes,Boolean> update(Integer idAula,AulaTresCreate aulaTresCreate);
    Either<ErrorCodes,Boolean> delete(Integer idAula);
}
