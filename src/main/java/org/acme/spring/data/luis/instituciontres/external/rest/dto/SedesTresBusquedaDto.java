package org.acme.spring.data.luis.instituciontres.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.instituciontres.core.entity.SedeTresBusqueda;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SedeTresBusqueda", description = "Esta es la entidad para buscar una sede por el id institucion")
public class SedesTresBusquedaDto {

    @JsonProperty
    @Schema(description = "Este es el atributo que refere al identificador de la institucion")
    private Integer idInstitucion;

    public SedeTresBusqueda toEntity(){
        return SedeTresBusqueda.builder()
                .idInstitucion(this.idInstitucion)
                .build();
    }
}
