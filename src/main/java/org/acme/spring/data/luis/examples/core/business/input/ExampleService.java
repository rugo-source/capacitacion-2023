package org.acme.spring.data.luis.examples.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.luis.examples.core.entity.Persona;
import org.acme.spring.data.luis.examples.core.entity.PersonaCreate;

import java.util.List;

//Implementamos todos los metodos que se esten utilizando siempre y cuando apunten al negocio
public interface ExampleService {

    List<Persona> listaEjemplo();

    //Manejo de errores
    Either<Integer,Persona> get(Integer idPersona);

    Either<Integer,Boolean> create(PersonaCreate toEntity);

    Either<Integer,Boolean> update(Integer id, PersonaCreate personaCreate);

    Either<Integer,Boolean> delete(Integer id);



}
