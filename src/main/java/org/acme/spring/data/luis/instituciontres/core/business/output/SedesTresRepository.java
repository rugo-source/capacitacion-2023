package org.acme.spring.data.luis.instituciontres.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Sede;

import javax.persistence.Parameter;
import java.util.List;
import java.util.Optional;

public interface SedesTresRepository {

    List<Sede> findAllByIdInstitucion(Integer idInstitucion);

    void save(Sede sedePersist);
    void update(Sede sedeChange);
    void deleteBy(Integer idSede);

    Optional<Sede> findBy(Integer idSede);
    Integer getIdDigitos();


    boolean existByNombre(String nombre);
    boolean existByAcronimo(String acronimo);
}
