package org.acme.spring.data.luis.examples.core.entity;

import lombok.*;

import java.time.LocalDate;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CuentaTresCreate {
    Integer idPersona;
    String rol;
    LocalDate inicio;
    LocalDate fin;
}
