package org.acme.spring.data.luis.examples.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EstadoPersonaTres {

    private Integer id;
    private String nombre;
}
