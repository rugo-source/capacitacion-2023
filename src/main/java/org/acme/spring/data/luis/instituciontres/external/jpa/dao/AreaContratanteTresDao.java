package org.acme.spring.data.luis.instituciontres.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.acme.spring.data.luis.instituciontres.core.business.output.AreaContratanteTresRepository;
import org.acme.spring.data.luis.instituciontres.external.jpa.repository.AreaContratanteTresJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class AreaContratanteTresDao implements AreaContratanteTresRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    AreaContratanteTresJpaRepository areaContratanteTresJpaRepository;


    @Override
    public List<AreaContratante> findAllNative() {
        Stream<AreaContratanteJpa> resultado = entityManager.createNativeQuery("Select * from tin10_area_contratante", AreaContratanteJpa.class).getResultStream();
        return resultado.map(AreaContratanteJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public void save(AreaContratante areaContratantePersist){
        areaContratanteTresJpaRepository.saveAndFlush(AreaContratanteJpa.fromEntity(areaContratantePersist));
    }


    @Override
    public void update(AreaContratante areaContratanteChange) {
        areaContratanteTresJpaRepository.saveAndFlush(AreaContratanteJpa.fromEntity(areaContratanteChange));
    }

    @Override
    public void deleteBy(Integer idAreaContratante) {
        areaContratanteTresJpaRepository.deleteById(idAreaContratante);
    }

    @Override
    public Optional<AreaContratante> findBy(Integer idAreaContratante) {
        return areaContratanteTresJpaRepository.findById(idAreaContratante).map(AreaContratanteJpa::toEntity);
    }

}
