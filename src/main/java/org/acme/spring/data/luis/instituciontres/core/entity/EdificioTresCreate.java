package org.acme.spring.data.luis.instituciontres.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EdificioTresCreate {

    private Integer idSede;
    private String nombre;
    private String acronimo;
    private String referencia;
}
