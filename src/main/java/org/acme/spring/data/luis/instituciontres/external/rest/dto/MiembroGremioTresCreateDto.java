package org.acme.spring.data.luis.instituciontres.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;
import org.acme.spring.data.luis.instituciontres.core.entity.MiembroGremioTresCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "MiembroGremioTres", description = "Esta es la entidad para asignar un gremio a una institucion")
public class MiembroGremioTresCreateDto {

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de el gremio")
    private Integer idGremio;

    @JsonProperty
    @Positive(message = "RNS001")
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de la institucion")
    private Integer idInstitucion;

    public MiembroGremioTresCreate toEntity(){
        return MiembroGremioTresCreate.builder()
                .idGremio(this.idGremio)
                .idInstitucion(this.idInstitucion)
                .build();
    }


}
