package org.acme.spring.data.luis.instituciontres.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.EdificioJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;
import org.acme.spring.data.luis.instituciontres.core.business.output.EdificioTresRepository;
import org.acme.spring.data.luis.instituciontres.core.entity.EdificioTresBusqueda;
import org.acme.spring.data.luis.instituciontres.external.jpa.repository.EdificioTresJpaRepository;
import org.acme.spring.data.luis.instituciontres.external.rest.dto.EdificioTresDto;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class EdificioTresDao implements EdificioTresRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    EdificioTresJpaRepository edificioTresJpaRepository;

    @Override
    public List<Edificio> findAllByIdSede(Integer idSede) {
        String queryStr = "Select * from tin08_edificio tin08  where tin08.fk_id_sede  = :idSede";
        Query query = entityManager.createNativeQuery(queryStr, EdificioJpa.class);
        query.setParameter("idSede", idSede);
        return query.getResultList();
    }

    @Override
    public void save(Edificio edificioPersist) {
        edificioTresJpaRepository.saveAndFlush(EdificioJpa.fromEntity(edificioPersist));
    }

    @Override
    public void update(Edificio edificioChange) {
        edificioTresJpaRepository.saveAndFlush(EdificioJpa.fromEntity(edificioChange));
    }

    @Override
    public void deleteBy(Integer idEdificio) {
        edificioTresJpaRepository.deleteById(idEdificio);
    }

    @Override
    public Optional<Edificio> findBy(Integer idEdifico) {
        return edificioTresJpaRepository.findById(idEdifico).map(EdificioJpa::toEntity);
    }

    @Override
    public boolean existByIdSede(Integer idSede){
        return edificioTresJpaRepository.existsByIdSede(idSede);
    }
}
