package org.acme.spring.data.luis.instituciontres.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.cristian.institucion.external.jpa.model.SedeJpa;
import org.acme.spring.data.luis.instituciontres.core.business.output.SedesTresRepository;
import org.acme.spring.data.luis.instituciontres.external.jpa.repository.SedesTresJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class SedesTresDao implements SedesTresRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    SedesTresJpaRepository sedesTresJpaRepository;


    @Override
    public List<Sede> findAllByIdInstitucion(Integer idInstitucion) {
        String queryStr = "Select * from tin05_sede tin05  where tin05.fk_id_institucion = :idInstitucion";
        Query query = entityManager.createNativeQuery(queryStr, SedeJpa.class);
        query.setParameter("idInstitucion", idInstitucion);
        return query.getResultList();
    }

    @Override
    public void save(Sede sedePersist) {
        sedesTresJpaRepository.saveAndFlush(SedeJpa.fromEntity(sedePersist));
    }

    @Override
    public void update(Sede sedeChange) {
        sedesTresJpaRepository.saveAndFlush(SedeJpa.fromEntity(sedeChange));
    }

    @Override
    public void deleteBy(Integer idSede) {
        sedesTresJpaRepository.deleteById(idSede);
    }

    @Override
    public Optional<Sede> findBy(Integer idSede) {
        return sedesTresJpaRepository.findById(idSede).map(SedeJpa::toEntity);
    }



    @Override
    public Integer getIdDigitos() {
        var idDigitos = sedesTresJpaRepository.max()+1 ;
        return idDigitos;
    }

    @Override
    public boolean existByNombre(String nombre) {
        return sedesTresJpaRepository.existsByNombre(nombre);
    }

    @Override
    public boolean existByAcronimo(String acronimo) {
        return sedesTresJpaRepository.existsByAcronimo(acronimo);
    }
}
