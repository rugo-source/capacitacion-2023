package org.acme.spring.data.luis.instituciontres.core.business.output;

import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;

import java.util.List;
import java.util.Optional;

public interface EdificioTresRepository {


    List<Edificio> findAllByIdSede(Integer idSede);
    void save(Edificio edificioPersist);
    void update(Edificio edificioChange);
    void deleteBy(Integer idEdificio);

    boolean existByIdSede(Integer idSede);

    Optional<Edificio> findBy(Integer idEdifico);
}
