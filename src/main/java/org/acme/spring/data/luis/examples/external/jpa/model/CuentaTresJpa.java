package org.acme.spring.data.luis.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.luis.examples.core.entity.CuentaTres;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "cuenta_tres")
public class CuentaTresJpa {

    @Id
    @Column(name = "id_cuenta")
    @SequenceGenerator(name = "cuenta_tres_id_cuenta_seq", sequenceName = "cuenta_tres_id_cuenta_seq", allocationSize = 1)
    @GeneratedValue(generator = "cuenta_tres_id_cuenta_seq", strategy = GenerationType.SEQUENCE)
    private Integer idCuenta;

    @Column(name = "fk_id_persona")
    private Integer idPersona;

    @Column(name = "tx_rol")
    private String rol;

    @Column(name = "fh_inicio")
    private LocalDate inicio;

    @Column(name = "fh_fin")
    private LocalDate fin;



    @ManyToOne
    @JoinColumn(name = "fk_id_persona", referencedColumnName = "id_persona", insertable = false, updatable = false)
    private PersonaTresJpa personaTresJpa;


    public CuentaTres toEntity(){
        return CuentaTres.builder()
                .idCuenta(this.idCuenta)
                .idPersona(this.idPersona)
                .rol(this.rol)
                .inicio(this.inicio)
                .fin(this.fin)
                .persona(this.personaTresJpa)
                .build();
    }

    public static CuentaTresJpa fromEntity(CuentaTres cuentaTres){
        return CuentaTresJpa.builder()
                .idCuenta(cuentaTres.getIdCuenta())
                .idPersona(cuentaTres.getIdPersona())
                .rol(cuentaTres.getRol())
                .inicio(cuentaTres.getInicio())
                .fin(cuentaTres.getFin())
                .build();
    }


}
