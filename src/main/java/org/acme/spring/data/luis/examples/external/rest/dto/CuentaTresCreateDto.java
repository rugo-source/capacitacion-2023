package org.acme.spring.data.luis.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.examples.core.entity.CuentaTresCreate;
import org.acme.spring.data.util.StringConstants;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "CuentaTresCreate", description = "Esta es la entidad para guardar una cuenta")
public class CuentaTresCreateDto {

//    @JsonProperty
//    @Positive(message = "RNS001")
//    @Schema(description = "Este es el atributo que hace referencia al id de la cuenta")
//    Integer id;

    @JsonProperty
    @Positive(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al idPersona de la persona")
    private Integer idPersona;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3, max = 100, message = "RNS002")
    @Schema(description = "Este es el atributo que hace referencia al nombre del rol")
    String rol;

    @JsonProperty
    @NotNull(message = "RNS001")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringConstants.LOCAL_DATE_FORMAT)
    @Schema(description = "Este es el atributo que hace referencia a la fecha de inicio de la cuenta del usuario", format = "string", implementation = String.class)
    LocalDate inicio;

    @JsonProperty
    @NotNull(message = "RNS001")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringConstants.LOCAL_DATE_FORMAT)
    @Schema(description = "Este es el atributo que hace referencia a la fecha de fin de la cuenta del usuario", format = "string", implementation = String.class)
    LocalDate fin;



    public CuentaTresCreate toEntity(){
        return CuentaTresCreate.builder()
                .idPersona(this.idPersona)
                .rol(this.rol)
                .inicio(this.inicio)
                .fin(this.fin)
                .build();

    }




}
