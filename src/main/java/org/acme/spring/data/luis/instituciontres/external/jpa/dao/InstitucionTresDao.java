package org.acme.spring.data.luis.instituciontres.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.acme.spring.data.luis.instituciontres.core.business.output.InstitucionTresRepository;
import org.acme.spring.data.luis.instituciontres.external.jpa.repository.InstitucionTresJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class InstitucionTresDao implements InstitucionTresRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    InstitucionTresJpaRepository institucionTresJpaRepository;


    @Override
    public boolean existByIdentificadorAndIdNot(String identificador, Integer id){
        return institucionTresJpaRepository.existsByIdentificadorAndIdNot(identificador,id);
    }

    @Override
    public boolean existByNombreAndIdNot(String nombre, Integer id) {
        return institucionTresJpaRepository.existsByNombreAndIdNot(nombre,id);
    }

    @Override
    public boolean existByIdentificador(String identificador){
        return institucionTresJpaRepository.existsByIdentificador(identificador);
    }

    @Override
    public boolean existByNombre(String nombre){
        return institucionTresJpaRepository.existsByNombre(nombre);

    }

    @Override
    public boolean existByAcronimo(String acronimo){
        return institucionTresJpaRepository.existsByAcronimo(acronimo);
    }



    @Override
    public Optional<Institucion> findBy(Integer idInstitucion){
        return institucionTresJpaRepository.findById(idInstitucion).map(InstitucionJpa::toEntity);
    }


    @Override
    public void save(Institucion institucionPersist){
        institucionTresJpaRepository.saveAndFlush(InstitucionJpa.fromEntity(institucionPersist));
    }

    @Override
    public void update(Institucion institucionChange){
        institucionTresJpaRepository.saveAndFlush(InstitucionJpa.fromEntity(institucionChange));
    }

    @Override
    public Optional<String> getIdentificadorById(Integer idInstitucion){
        return institucionTresJpaRepository.findById(idInstitucion).map(InstitucionJpa::getIdentificador);
    }


    /*Aqui luis solo cambia tu avariable string a un public final static string, ponlo al nivel de tuis inject, esto solo con el fin de tener esa estructura y tener mas visible los queries
    * esto igual aplicalo con los nombre de los parametros por si en algun momento tienes el mismo parametro en un query solo invoques una sola declaracion.
    * */
    @Override
    public List<Institucion> findInstitutesBy(String identificador, String nombre, String acronimo) {

//        String queryStr = "select * from tin01_institucion where UNACCENT(LOWER(tin01_institucion.tx_identificador)) LIKE '%' || UNACCENT(LOWER(:identificador))  || '%' \n" +
//                "and UNACCENT(LOWER(tin01_institucion.tx_nombre)) LIKE '%' || UNACCENT(LOWER(:nombre)) || '%' \n" +
//        "and UNACCENT(LOWER(tin01_institucion.tx_acronimo)) LIKE '%' || UNACCENT(LOWER(:acronimo)) || '%'";
//

        String queryStr = "select *, cin03.tx_nombre as tx_nombre_categoria, (select count(tso01.id_solicitud) from tso01_solicitud tso01 where tso01.fk_id_estado = 1 and tso01.fk_id_institucion = tin01_institucion.id_institucion) as nu_solicitud_pendiente, \n" +
                "coalesce(tca01.tx_nombre || ' ' || tca01.tx_primer_apellido || coalesce(' ' || tca01.tx_segundo_apellido, ''), 'Sin ejecutivo') as tx_nombre_ejecutivo from tin01_institucion  \n" +
                "inner join cin03_categoria cin03 on cin03.id_categoria = tin01_institucion.fk_id_categoria  \n" +
                "left join tin04_ejecutivo tin04 on tin04.id_institucion = tin01_institucion.id_institucion  \n" +
                "left join tca01_persona tca01 on tin04.id_persona = tca01.id_persona \n" +
                "where UNACCENT(LOWER(tin01_institucion.tx_identificador)) LIKE '%' || UNACCENT(LOWER(:identificador))  || '%' \n" +
                "and UNACCENT(LOWER(tin01_institucion.tx_nombre)) LIKE '%' || UNACCENT(LOWER(:nombre)) || '%' \n" +
                "and UNACCENT(LOWER(tin01_institucion.tx_acronimo)) LIKE '%' || UNACCENT(LOWER(:acronimo)) || '%'";

        Query query = entityManager.createNativeQuery(queryStr,InstitucionJpa.class);
        query.setParameter("identificador", identificador);
        query.setParameter("nombre", nombre);
        query.setParameter("acronimo", acronimo);

        return query.getResultList();

    }






}
