package org.acme.spring.data.luis.instituciontres.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;
import org.acme.spring.data.luis.instituciontres.core.business.input.EdificioTresService;
import org.acme.spring.data.luis.instituciontres.core.business.output.EdificioTresRepository;
import org.acme.spring.data.luis.instituciontres.core.business.output.SedesTresRepository;
import org.acme.spring.data.luis.instituciontres.core.entity.EdificioTresBusqueda;
import org.acme.spring.data.luis.instituciontres.core.entity.EdificioTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class EdificioTresBs implements EdificioTresService {

    @Inject
    EdificioTresRepository edificioTresRepository;

    @Inject
    SedesTresRepository sedesTresRepository;


    @Override
    public Either<ErrorCodes, List<Edificio>> getSedesBy(EdificioTresBusqueda edificioTresBusqueda) {
        Either<ErrorCodes, List<Edificio>> resultado;

        var idSede = sedesTresRepository.findBy(edificioTresBusqueda.getIdSede());

        if(idSede.isPresent()){
            var listaEdificios = edificioTresRepository.findAllByIdSede(edificioTresBusqueda.getIdSede());
            resultado = Either.right(listaEdificios);
        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }

        return resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(EdificioTresCreate edificioTresCreate) {
        Either<ErrorCodes,Boolean> resultado;

        var edificioPersist = Edificio.builder()
                .idSede(edificioTresCreate.getIdSede())
                .nombre(edificioTresCreate.getNombre())
                .acronimo(edificioTresCreate.getAcronimo())
                .referencia(edificioTresCreate.getReferencia())
                .build();
        edificioTresRepository.save(edificioPersist);
        resultado = Either.right(true);
        return  resultado;
    }

    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> update(Integer idEdificio, EdificioTresCreate edificioTresCreate) {
        Either<ErrorCodes,Boolean> resultado;
        var edificioSearh = edificioTresRepository.findBy(idEdificio);

        if(edificioSearh.isPresent()){
            var edificioChange = edificioSearh.get();
            edificioChange.setIdSede(edificioTresCreate.getIdSede());
            edificioChange.setNombre(edificioTresCreate.getNombre());
            edificioChange.setAcronimo(edificioTresCreate.getAcronimo());
            edificioChange.setReferencia(edificioTresCreate.getReferencia());
            edificioTresRepository.update(edificioChange);
            resultado = Either.right(true);
        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idEdificio) {
        Either<ErrorCodes,Boolean> resultado;
        var edificioSearch = edificioTresRepository.findBy(idEdificio);
        if(edificioSearch.isPresent()){
            edificioTresRepository.deleteBy(idEdificio);
            resultado = Either.right(true);
        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }
}
