package org.acme.spring.data.luis.examples.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.luis.examples.core.business.input.ExampleService;
import org.acme.spring.data.luis.examples.core.business.output.PersonaRepository;
import org.acme.spring.data.luis.examples.core.entity.Persona;
import org.acme.spring.data.luis.examples.core.entity.PersonaCreate;
import org.acme.spring.data.luis.examples.core.statemachine.PersonaTresSM;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped  //Para definir el alcance
public class ExampleBs implements ExampleService {

    @Inject
    PersonaRepository personaRepository;

    @Inject
    PersonaTresSM personaTresSM;





    @Override
    public List<Persona> listaEjemplo() {
        return personaRepository.findAll().stream().map(persona -> {
            persona.setRegistrar(personaTresSM.isDoable(personaTresSM.getRegistrar(), personaTresSM.getStateById(persona.getIdEstado())));
            persona.setEditar(personaTresSM.isDoable(personaTresSM.getEditar(), personaTresSM.getStateById(persona.getIdEstado())));
            persona.setEliminar(personaTresSM.isDoable(personaTresSM.getEliminar(), personaTresSM.getStateById(persona.getIdEstado())));
            persona.setConsultar(personaTresSM.isDoable(personaTresSM.getConsultar(), personaTresSM.getStateById(persona.getIdEstado())));
            persona.setConfigurar(personaTresSM.isDoable(personaTresSM.getConfigurar(), personaTresSM.getStateById(persona.getIdEstado())));
            return persona;
        }).collect(Collectors.toList());
    }


    @Override
    public Either<Integer, Persona> get(Integer idPersona) {
        Either<Integer, Persona> resultado = Either.left(404);
        var persona = personaRepository.findBy(idPersona);
        if (persona.isPresent()) {
            var personaGet = persona.get();
            personaGet.setRegistrar(personaTresSM.isDoable(personaTresSM.getRegistrar(),personaTresSM.getStateById(personaGet.getIdEstado())));
            personaGet.setEditar(personaTresSM.isDoable(personaTresSM.getEditar(),personaTresSM.getStateById(personaGet.getIdEstado())));
            personaGet.setEliminar(personaTresSM.isDoable(personaTresSM.getEliminar(),personaTresSM.getStateById(personaGet.getIdEstado())));
            personaGet.setConsultar(personaTresSM.isDoable(personaTresSM.getConsultar(),personaTresSM.getStateById(personaGet.getIdEstado())));
            personaGet.setConfigurar(personaTresSM.isDoable(personaTresSM.getConfigurar(),personaTresSM.getStateById(personaGet.getIdEstado())));
            resultado = Either.right(personaGet);
        }
        return resultado;
    }


    @Override
    @Transactional
    public Either<Integer, Boolean> create(PersonaCreate personaCreate) {
        Either<Integer, Boolean> resultado = Either.right(true);
        var personaPersits = Persona.builder()
                .nombre(personaCreate.getNombre())
                .edad(personaCreate.getEdad())
                .idEstado(personaTresSM.getRegistrado().getId())
                .build();
        personaRepository.save(personaPersits);

        return resultado;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> update(Integer id, PersonaCreate personaCreate) {
        Either<Integer, Boolean> resultado = Either.left(404);
        var personaSearch = personaRepository.findBy(id);
        if (personaSearch.isPresent()) {
            var personaChange = personaSearch.get();
            personaChange.setNombre(personaCreate.getNombre());
            personaChange.setIdEstado(personaCreate.getIdEstado());
            personaChange.setEdad(personaCreate.getEdad());
            personaRepository.update(personaChange);
            resultado = Either.right(true);
        }

        return resultado;
    }

    @Override
    public Either<Integer, Boolean> delete(Integer id) {
        Either<Integer, Boolean> resultado = Either.left(404);
        var personaSearch = personaRepository.findBy(id);
        if (personaSearch.isPresent()) {
            personaRepository.deleteBy(id);
            resultado = Either.right(true);
        }
        return resultado;
    }





}
