package org.acme.spring.data.luis.instituciontres.core.entity;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MiembroGremioTresCreate {
    private Integer idGremio;
    private Integer idInstitucion;
}
