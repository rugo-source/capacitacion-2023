package org.acme.spring.data.luis.examples.external.jpa.repository;


import org.acme.spring.data.luis.examples.external.jpa.model.PersonaTresJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PersonaJpaRepository extends JpaRepository<PersonaTresJpa,Integer> {

    @Query("from PersonaJpa pj")
    List<PersonaTresJpa> findAllOrm();

}
