package org.acme.spring.data.luis.instituciontres.core.business.output;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;

import java.util.List;

import java.util.Optional;

public interface AreaContratanteTresRepository {

    List<AreaContratante> findAllNative();

    void save(AreaContratante areaContratantePersist);

    void update(AreaContratante areaContratanteChange);

    void deleteBy(Integer idAreaContratante);

    Optional<AreaContratante> findBy(Integer idAreaContratante);



}
