package org.acme.spring.data.luis.instituciontres.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AulaJpa;
import org.acme.spring.data.luis.instituciontres.core.business.output.AulaTresRepository;
import org.acme.spring.data.luis.instituciontres.external.jpa.repository.AulaTresJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.Optional;

@ApplicationScoped
public class AulaTresDao implements AulaTresRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    AulaTresJpaRepository aulaTresJpaRepository;


    @Override
    public void save(Aula aulaPersist) {
        aulaTresJpaRepository.saveAndFlush(AulaJpa.fromEntity(aulaPersist));
    }

    @Override
    public void update(Aula aulaChange) {
        aulaTresJpaRepository.saveAndFlush(AulaJpa.fromEntity(aulaChange));
    }

    @Override
    public void deleteBy(Integer idAula) {
        aulaTresJpaRepository.deleteById(idAula);
    }

    @Override
    public Optional<Aula> findBy(Integer idAula) {
        return aulaTresJpaRepository.findById(idAula).map(AulaJpa::toEntity);
    }
}
