package org.acme.spring.data.luis.instituciontres.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaContratanteTresJpaRepository extends JpaRepository<AreaContratanteJpa,Integer> {


}
