package org.acme.spring.data.luis.instituciontres.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.InstitucionJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface InstitucionTresJpaRepository extends JpaRepository<InstitucionJpa,Integer> {




    @Query("select ti from InstitucionJpa ti where ti.identificador = :identificador ")
    Optional<InstitucionJpa> findByIdentificador(@Param("identificador") String identificador);

    @Query("select ti from InstitucionJpa ti where ti.nombre = :nombre ")
    Optional<InstitucionJpa> findByNombre(@Param("nombre") String nombre);

    @Query("select ti from InstitucionJpa ti where ti.acronimo = :acronimo ")
    Optional<InstitucionJpa> findByAcronimo(@Param("acronimo") String acronimo);


    boolean existsByIdentificadorAndIdNot(String identificador, Integer id);
    boolean existsByNombreAndIdNot(String nombre, Integer id);
    boolean existsByIdentificador(String identificador);
    boolean existsByNombre(String nombre);
    boolean existsByAcronimo(String acronimo);


}
