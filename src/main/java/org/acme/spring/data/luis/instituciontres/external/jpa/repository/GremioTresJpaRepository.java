package org.acme.spring.data.luis.instituciontres.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.GremioJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GremioTresJpaRepository extends JpaRepository<GremioJpa,Integer> {


    boolean existsByNombre(String nombre);
    boolean existsByAcronimo(String acronimo);

}
