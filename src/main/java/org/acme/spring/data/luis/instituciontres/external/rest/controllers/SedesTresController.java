package org.acme.spring.data.luis.instituciontres.external.rest.controllers;

import org.acme.spring.data.luis.instituciontres.core.business.input.SedesTresService;
import org.acme.spring.data.luis.instituciontres.external.rest.dto.*;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("catalogos-instituciones/sede-tres")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Luis")
public class SedesTresController {

    @Inject
    SedesTresService sedesTresService;

    @POST
    @Path("busqueda")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = SedesTresDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response get(@Valid SedesTresBusquedaDto sedesTresBusquedaDto){
        return sedesTresService.getSedesBy(sedesTresBusquedaDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @POST
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = SedesTresDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid SedesTresCreateDto sedesTresCreateDto){
        return sedesTresService.create(sedesTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @PUT
    @Path("{idSede}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = SedesTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idSede") Integer idSede,@Valid SedesTresCreateDto sedesTresCreateDto){
        return sedesTresService.update(idSede,sedesTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

    @DELETE
    @Path("{idSede}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = SedesTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response delete(@PathParam("idSede") Integer idSede ){
        return sedesTresService.delete(idSede).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

}
