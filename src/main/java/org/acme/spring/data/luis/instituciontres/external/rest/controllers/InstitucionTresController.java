package org.acme.spring.data.luis.instituciontres.external.rest.controllers;

import org.acme.spring.data.luis.instituciontres.core.business.input.InstitucionTresService;
import org.acme.spring.data.luis.instituciontres.external.rest.dto.InstitucionTresBusquedasDto;
import org.acme.spring.data.luis.instituciontres.external.rest.dto.InstitucionTresCreateDto;
import org.acme.spring.data.luis.instituciontres.external.rest.dto.InstitucionTresDto;
import org.acme.spring.data.util.error.ErrorMapper;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/*Luis en general tu implementacion esta bien solo te algo unas ciertas notas a considerar, como la de no dejar cosas comentadas, o si es que las vas a dejar
* solo etiquetalas con un TODO
*  para saber que se hara una modificacion futura o estas en espera de que te solucionen una duda para completar la programacion del negocio
*
* */
@Path("/catalogos-instituciones/institucion-tres")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = " Catalogo de Institución - Luis")
public class InstitucionTresController {

    @Inject
    InstitucionTresService institucionTresService;


    @POST
//    @Path("post")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = InstitucionTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid InstitucionTresCreateDto institucionTresCreateDto){
        return institucionTresService.create(institucionTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }


    @PUT
    @Path("/{idInstitucion}")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = InstitucionTresCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response update(@PathParam("idInstitucion") Integer idInstitucion,@Valid InstitucionTresCreateDto institucionTresCreateDto){
        return institucionTresService.update(idInstitucion,institucionTresCreateDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }


    /*aqui como esto es una busqueda por filtros, tendrias que regresar mas de una institucion, por ende en tu @APIResponse indica que regresaras un Array
    *
    *, tambien tendrias que modificar el nombre del metodo en lugar de get por list,  tambien hechale una checada a la imagen de las conveciones de nombres
    * que les di para constatar que estas siguiendo el nombre correcto en cualquiera de las capas.
    *
    * */
    @POST
    @Path("busquedas")
    @APIResponse(responseCode = "200", description = "Peticion Exitosa", content = @Content(schema = @Schema(implementation = InstitucionTresDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion Erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response get(@Valid InstitucionTresBusquedasDto busquedasDto)
    {
        return institucionTresService.getInstitutesBy(busquedasDto.toEntity()).map(Response::ok).getOrElseGet(ErrorMapper::errorCodeToResponseBuilder).build();
    }

}
