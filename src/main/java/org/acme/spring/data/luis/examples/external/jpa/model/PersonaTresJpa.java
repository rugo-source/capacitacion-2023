package org.acme.spring.data.luis.examples.external.jpa.model;


import lombok.*;
import org.acme.spring.data.luis.examples.core.entity.Persona;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "persona_tres")
public class PersonaTresJpa {

    @Id
    @Column(name = "id_persona")
    @SequenceGenerator(name = "persona_tres_id_persona_seq", sequenceName = "persona_tres_id_persona_seq", allocationSize = 1)
    @GeneratedValue(generator = "persona_tres_id_persona_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "fk_id_estado")
    private Integer idEstado;
    @Column(name = "edad")
    private Integer edad;


    @ManyToOne
    @JoinColumn(name = "fk_id_estado", referencedColumnName = "id_estado", insertable = false, updatable = false)
    private EstadoPersonaTresJpa estadoPersonaTres;

    public Persona toEntity() {
        return Persona.builder().id(this.id).nombre(this.nombre).idEstado(this.idEstado).edad(this.edad).build();
    }

    public static PersonaTresJpa fromEntity(Persona persona) {
        return PersonaTresJpa.builder()
                .idEstado(persona.getIdEstado())
                .id(persona.getId())
                .nombre(persona.getNombre())
                .edad(persona.getEdad())
                .build();
    }

}
