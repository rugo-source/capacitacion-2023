package org.acme.spring.data.luis.instituciontres.external.jpa.repository;

import org.acme.spring.data.cristian.institucion.external.jpa.model.AulaJpa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AulaTresJpaRepository extends JpaRepository<AulaJpa,Integer> {


}
