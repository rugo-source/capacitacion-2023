package org.acme.spring.data.luis.instituciontres.external.jpa.dao;

import org.acme.spring.data.cristian.institucion.core.entity.Gremio;
import org.acme.spring.data.cristian.institucion.external.jpa.model.AreaContratanteJpa;
import org.acme.spring.data.cristian.institucion.external.jpa.model.GremioJpa;
import org.acme.spring.data.luis.instituciontres.core.business.output.GremioTresRepository;
import org.acme.spring.data.luis.instituciontres.external.jpa.repository.GremioTresJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class GremioTresDao implements GremioTresRepository {


    @Inject
    EntityManager entityManager;

    @Inject
    GremioTresJpaRepository gremioTresJpaRepository;

    @Override
    public List<Gremio> findAllNative() {
        Stream<GremioJpa> resultado = entityManager.createNativeQuery("Select * from tin02_gremio", GremioJpa.class).getResultStream();
        return resultado.map(GremioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public boolean existByNombre(String nombre) {
        return gremioTresJpaRepository.existsByNombre(nombre);
    }

    @Override
    public boolean existByAcronimo(String acronimo) {
        return gremioTresJpaRepository.existsByAcronimo(acronimo);
    }

    @Override
    public Optional<Gremio> findBy(Integer idGremio) {
        return gremioTresJpaRepository.findById(idGremio).map(GremioJpa::toEntity);
    }

    @Override
    public void save(Gremio gremioPersist) {
        gremioTresJpaRepository.saveAndFlush(GremioJpa.fromEntity(gremioPersist));
    }

    @Override
    public void update(Gremio gremioChange) {
        gremioTresJpaRepository.saveAndFlush(GremioJpa.fromEntity(gremioChange));
    }

    @Override
    public void deleteBy(Integer id) {
        gremioTresJpaRepository.deleteById(id);
    }
}
