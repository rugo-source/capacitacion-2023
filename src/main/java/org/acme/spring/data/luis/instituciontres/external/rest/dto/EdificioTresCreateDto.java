package org.acme.spring.data.luis.instituciontres.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.luis.instituciontres.core.entity.EdificioTresCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "EdificioTresCreate", description = "Esta es la entidad para guardar un edificio")
public class EdificioTresCreateDto {

    @JsonProperty
    @NotNull(message = "RNS001")
    @Schema(description = "Este es el atributo que referencia al id de el edificio asociado a una sede")
    private Integer idSede;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3,max=100, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al nombre de un edificio")
    private String nombre;

    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3,max=100, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia al acronimo de un edificio")
    private String acronimo;

    @JsonProperty
    @Size(min = 3,max=100, message = "RNS002")
    @Schema(description = "Este es el atributo que referencia a las referencias de un edificio")
    private String referencias;

    public EdificioTresCreate toEntity(){
        return EdificioTresCreate.builder()
                .idSede(this.idSede)
                .nombre(this.nombre)
                .acronimo(this.acronimo)
                .referencia(this.referencias)
                .build();
    }
}
