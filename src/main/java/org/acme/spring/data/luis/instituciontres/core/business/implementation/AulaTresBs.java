package org.acme.spring.data.luis.instituciontres.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;
import org.acme.spring.data.luis.instituciontres.core.business.input.AulaTresService;
import org.acme.spring.data.luis.instituciontres.core.business.output.AulaTresRepository;
import org.acme.spring.data.luis.instituciontres.core.entity.AulaTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class AulaTresBs implements AulaTresService {

    @Inject
    AulaTresRepository aulaTresRepository;


    @Override
    @Transactional
    public Either<ErrorCodes, Boolean> create(AulaTresCreate aulaTresCreate) {
        Either<ErrorCodes,Boolean> resultado;

        var aulaPersist = Aula.builder()
                .idEdificio(aulaTresCreate.getIdEdificio())
                .nombre(aulaTresCreate.getNombre())
                .idTipoEquipamiento(aulaTresCreate.getIdTipoEquipamiento())
                .cantidadEquipamiento(aulaTresCreate.getCantidadEquipamiento())
                .capacidadEquipamiento(aulaTresCreate.getCapacidadEquipamiento())
                .capacidad(aulaTresCreate.getCapacidad())
                .observaciones(aulaTresCreate.getObservaciones())
                .build();
        aulaTresRepository.save(aulaPersist);
        resultado = Either.right(true);

        return resultado;

    }

    @Override
    public Either<ErrorCodes, Boolean> update(Integer idAula, AulaTresCreate aulaTresCreate) {
        Either<ErrorCodes,Boolean> resultado;

        var aulaSearch = aulaTresRepository.findBy(idAula);

        if(aulaSearch.isPresent()){
            var aulaChange = aulaSearch.get();
            aulaChange.setIdEdificio(aulaTresCreate.getIdEdificio());
            aulaChange.setNombre(aulaTresCreate.getNombre());
            aulaChange.setIdTipoEquipamiento(aulaTresCreate.getIdTipoEquipamiento());
            aulaChange.setCantidadEquipamiento(aulaTresCreate.getCantidadEquipamiento());
            aulaChange.setCapacidadEquipamiento(aulaTresCreate.getCapacidadEquipamiento());
            aulaChange.setCapacidad(aulaTresCreate.getCapacidad());
            aulaChange.setObservaciones(aulaTresCreate.getObservaciones());
            aulaTresRepository.update(aulaChange);
            resultado = Either.right(true);


        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }
        return resultado;
    }

    @Override
    public Either<ErrorCodes, Boolean> delete(Integer idAula) {
        Either<ErrorCodes,Boolean> resultado;

        var aulaSearch = aulaTresRepository.findBy(idAula);

        if(aulaSearch.isPresent()){
            aulaTresRepository.deleteBy(idAula);
            resultado = Either.right(true);
        }else {
            resultado = Either.left(ErrorCodes.NOT_FOUND);
        }

        return resultado;
    }
}
