package org.acme.spring.data.luis.instituciontres.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "InstitucionTres", description = "Esta es la entidad para recuperar datos de una institucion")
public class InstitucionTresDto {

//    @JsonProperty
//    @Schema(description = "Este es el atributo que referencia al id de la institucion")
//    private Integer id;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al id de tipo de la institucion")
    private Integer idTipo;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al id de clasificacion de la institucion")
    private Integer idClasificacion;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al id de categoria de institucion")
    private Integer idCategoria;


    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al id de categoria de institucion")
    private Integer isSubsistemaUniversidad;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al id de categoria de institucion")
    private Integer idSubsistemaBachillerato;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al id de categoria de institucion")
    private String identificador;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al nombre de la institucion")
    private String nombre;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al acronimo de la institucion")
    private String acronimo;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al cct de la institucion")
    private String cct;

    @JsonProperty
    @Schema(description = "Este es el atributo que referencia al numero de sedes registradas de una institucion")
    private Integer numeroSedesRegistradas;


//    @JsonProperty
//    @Schema(description = "Este es el atributo que refere a las solicitudes pendientes de la institucion")
//    private Integer solicitudesPendientes;


//    @JsonProperty
//    @Schema(description = "Este es el atributo que refere al ejecutivo asignado de la institucion")
//    private String ejecutivoAsignado;

    public static InstitucionTresDto fromEntity(Institucion institucion){
        return InstitucionTresDto.builder()
                .idTipo(institucion.getIdTipo())
                .idClasificacion(institucion.getIdClasificacion())
                .idCategoria(institucion.getIdCategoria())
                .isSubsistemaUniversidad(institucion.getIdSubsistemaUniversidad())
                .idSubsistemaBachillerato(institucion.getIdSubsistemaBachillerato())
                .identificador(institucion.getIdentificador())
                .nombre(institucion.getNombre())
                .acronimo(institucion.getAcronimo())
                .cct(institucion.getCct())
                .numeroSedesRegistradas(institucion.getNumeroSedesRegistradas())
                .build();
    }



}
