package org.acme.spring.data.luis.instituciontres.core.entity;


import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AulaTresCreate {

    private Integer idEdificio;
    private String nombre;
    private Integer idTipoEquipamiento;
    private Integer capacidadEquipamiento;
    private Integer cantidadEquipamiento;
    private Integer capacidad;
    private String observaciones;


}
