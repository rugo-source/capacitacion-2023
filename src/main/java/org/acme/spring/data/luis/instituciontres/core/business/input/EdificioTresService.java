package org.acme.spring.data.luis.instituciontres.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;
import org.acme.spring.data.luis.instituciontres.core.entity.EdificioTresBusqueda;
import org.acme.spring.data.luis.instituciontres.core.entity.EdificioTresCreate;
import org.acme.spring.data.util.error.ErrorCodes;

import java.util.List;

public interface EdificioTresService {

    Either<ErrorCodes, List<Edificio>> getSedesBy(EdificioTresBusqueda edificioTresBusqueda);
    Either<ErrorCodes,Boolean> create(EdificioTresCreate toEntity);
    Either<ErrorCodes,Boolean> update(Integer idEdificio, EdificioTresCreate edificioTresCreate);
    Either<ErrorCodes, Boolean> delete(Integer idEdificio);
}
