package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Institucion;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin01_institucion")
public class InstitucionJpa {
    @Id
    @SequenceGenerator(name = "tin01_institucion_id_institucion_seq", sequenceName = "tin01_institucion_id_institucion_seq", allocationSize = 1)
    @GeneratedValue(generator = "tin01_institucion_id_institucion_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_institucion")
    private Integer id;
    @Column(name = "fk_id_tipo")
    private Integer idTipo;
    @Column(name = "fk_id_clasificacion")
    private Integer idClasificacion;
    @Column(name = "fk_id_categoria")
    private Integer idCategoria;
    @Column(name = "fk_id_subsistema_universidad")
    private Integer idSubsistemaUniversidad;
    @Column(name = "fk_id_subsistema_bachillerato")
    private Integer idSubsistemaBachillerato;
    @Column(name = "tx_identificador")
    private String identificador;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "tx_acronimo")
    private String acronimo;
    @Column(name = "tx_cct")
    private String cct;
    @Column(name = "nu_sede")
    private Integer numeroSedesRegistradas;


    public Institucion toEntity() {
        return Institucion.builder()
                .id(id)
                .idTipo(idTipo)
                .idClasificacion(idClasificacion)
                .idCategoria(idCategoria)
                .idSubsistemaUniversidad(idSubsistemaUniversidad)
                .idSubsistemaBachillerato(idSubsistemaBachillerato)
                .identificador(identificador)
                .nombre(nombre)
                .acronimo(acronimo)
                .cct(cct)
                .numeroSedesRegistradas(numeroSedesRegistradas)
                .build();
    }

    public static InstitucionJpa fromEntity(Institucion entity) {
        return InstitucionJpa.builder()
                .id(entity.getId())
                .idTipo(entity.getIdTipo())
                .idClasificacion(entity.getIdClasificacion())
                .idCategoria(entity.getIdCategoria())
                .idSubsistemaUniversidad(entity.getIdSubsistemaUniversidad())
                .idSubsistemaBachillerato(entity.getIdSubsistemaBachillerato())
                .identificador(entity.getIdentificador())
                .nombre(entity.getNombre())
                .acronimo(entity.getAcronimo())
                .cct(entity.getCct())
                .numeroSedesRegistradas(entity.getNumeroSedesRegistradas())
                .build();
    }
}
