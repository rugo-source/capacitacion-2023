package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.acme.spring.data.util.StringConstants;

@Builder
@Getter
@Setter
public class PersonaExterna {
    private Integer id;
    private Integer idInstitucion;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String curp;
    private String correo;

    @Override
    public String toString() {
        var sb = new StringBuilder(nombre).append(StringConstants.ESPACIO_BLANCO).append(primerApellido);
        if (segundoApellido != null) {
            sb.append(StringConstants.ESPACIO_BLANCO).append(segundoApellido);
        }
        return sb.toString();

    }
}
