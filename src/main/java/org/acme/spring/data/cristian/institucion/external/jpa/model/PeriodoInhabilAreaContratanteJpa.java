package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.PeriodoInhabilAreaContratante;

import javax.persistence.*;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin11_periodo_inhabil_area_contratante")
public class PeriodoInhabilAreaContratanteJpa {

	@Id
    @EmbeddedId
    private PeriodoInhabilAreaContratanteIdJpa id;
	@Column(name = "fk_id_area_contratante", insertable = false, updatable = false)
    private Integer idAreaContratante;
    @Column(name = "fk_id_periodo_inhabil", insertable = false, updatable = false)
    private Integer idPeriodoInhabil;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeriodoInhabilAreaContratanteJpa that = (PeriodoInhabilAreaContratanteJpa) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
    
    public PeriodoInhabilAreaContratante toEntity() {
        return PeriodoInhabilAreaContratante.builder()
        		.idAreaContratante(idAreaContratante)
        		.idPeriodoInhabil(idPeriodoInhabil)
                .build();
    }
}
