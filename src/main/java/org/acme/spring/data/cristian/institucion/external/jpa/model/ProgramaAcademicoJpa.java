package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.ProgramaAcademico;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin17_programa_academico")
public class ProgramaAcademicoJpa {
	@Id
	@SequenceGenerator(name = "tin17_programa_academico_id_programa_academico_seq", sequenceName = "tin17_programa_academico_id_programa_academico_seq", allocationSize = 1)
	@GeneratedValue(generator = "tin17_programa_academico_id_programa_academico_seq", strategy = GenerationType.SEQUENCE)
	@Column(name = "id_programa_academico")
	private Integer id;
	@Column(name = "fk_id_area_contratante")
	private Integer idAreaContratante;
	@Column(name = "fk_id_estado")
	private Integer idEstado;
	@Column(name = "tx_nemotecnia")
	private String nemotecnia;
	@Column(name = "tx_carrera")
	private String carrera;
	@ManyToOne
	@JoinColumn(name = "fk_id_estado", referencedColumnName = "id_estado", insertable = false, updatable = false)
	private EstadoProgramaJpa estado;

	public ProgramaAcademico toEntity() {
		return ProgramaAcademico.builder().id(id).idAreaContratante(idAreaContratante).idEstado(idEstado)
				.nemotecnia(nemotecnia).carrera(carrera).build();
	}

	public static ProgramaAcademicoJpa fromEntity(ProgramaAcademico entity) {
		return ProgramaAcademicoJpa.builder().id(entity.getId()).idAreaContratante(entity.getIdAreaContratante())
				.idEstado(entity.getIdEstado()).nemotecnia(entity.getNemotecnia()).carrera(entity.getCarrera()).build();
	}
}