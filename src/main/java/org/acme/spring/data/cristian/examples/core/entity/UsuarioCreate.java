package org.acme.spring.data.cristian.examples.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioCreate {

    private String login;
    private String password;

}
