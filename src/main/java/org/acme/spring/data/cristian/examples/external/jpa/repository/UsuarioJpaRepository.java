package org.acme.spring.data.cristian.examples.external.jpa.repository;

import org.acme.spring.data.cristian.examples.external.jpa.model.UsuarioJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UsuarioJpaRepository extends JpaRepository<UsuarioJpa,Integer> {

    @Query("from UsuarioJpa uj")
    List<UsuarioJpa> findAllOrm();
}
