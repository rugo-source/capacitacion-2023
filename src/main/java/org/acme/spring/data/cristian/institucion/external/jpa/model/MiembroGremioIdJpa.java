package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Embeddable
public class MiembroGremioIdJpa implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2994004189297118727L;
	
	@Column(name = "fk_id_gremio", nullable = false)
    private Integer idGremio;
    @Column(name = "fk_id_institucion", nullable = false)
    private Integer idInstitucion;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MiembroGremioIdJpa that = (MiembroGremioIdJpa) o;
        return idGremio.equals(that.idGremio) && idInstitucion.equals(that.idInstitucion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idGremio, idInstitucion);
    }
}
