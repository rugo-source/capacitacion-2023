package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Builder
@Getter
@Setter
public class Sede {
	
	private Integer id;
	private Integer idInstitucion;
	private Integer idDireccion;
	private String nombre;
	private String acronimo;
	private Integer capacidad;
	private String nombreEstado;
	private Boolean propia;
	private String identificador;

	private List<Edificio> edificios;

	private Boolean desasociar;
	
	@Override
	public int hashCode() {
		final var prime = 31;
		var result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sede other = (Sede) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
