package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ContactoPersonaExterna {
    private Integer id;
    private String correoAlterno;
    private String celular;
    private String telefono;
    private String extension;
}
