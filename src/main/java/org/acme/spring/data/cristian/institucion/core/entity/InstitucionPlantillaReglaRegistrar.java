package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InstitucionPlantillaReglaRegistrar {
    private Integer idFamilia;
    private Integer idPlantillaOperacion;
    private String observaciones;

}
