package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class MiembroGremio {
	private Integer idGremio;
	private Integer idInstitucion;
}
