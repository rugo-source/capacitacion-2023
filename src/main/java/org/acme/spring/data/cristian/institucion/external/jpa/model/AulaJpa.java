package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Aula;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin09_aula")
public class AulaJpa {
    @Id
    @SequenceGenerator(name = "tin09_aula_id_aula_seq", sequenceName = "tin09_aula_id_aula_seq", allocationSize = 1)
    @GeneratedValue(generator = "tin09_aula_id_aula_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_aula")
    private Integer id;
    @Column(name = "fk_id_edificio")
    private Integer idEdificio;
    @Column(name = "fk_id_tipo_equipamiento")
    private Integer idTipoEquipamiento;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "nu_equipamiento")
    private Integer cantidadEquipamiento;
    @Column(name = "nu_capacidad_equipamiento")
    private Integer capacidadEquipamiento;
    @Column(name = "tx_observacion")
    private String observaciones;

    public Aula toEntity(){
        return Aula.builder()
                .id(id)
                .idEdificio(idEdificio)
                .idTipoEquipamiento(idTipoEquipamiento)
                .nombre(nombre)
                .cantidadEquipamiento(cantidadEquipamiento)
                .capacidadEquipamiento(capacidadEquipamiento)
                .observaciones(observaciones)
                .build();
    }

    public static AulaJpa fromEntity(Aula aula) {
        return AulaJpa.builder()
                .id(aula.getId())
                .idEdificio(aula.getIdEdificio())
                .idTipoEquipamiento(aula.getIdTipoEquipamiento())
                .nombre(aula.getNombre())
                .cantidadEquipamiento(aula.getCantidadEquipamiento())
                .capacidadEquipamiento(aula.getCapacidadEquipamiento())
                .observaciones(aula.getObservaciones())
                .build();
    }
}
