package org.acme.spring.data.cristian.examples.core.entity;

import lombok.*;
import org.acme.spring.data.util.StringConstants;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class Persona {
    private Integer id;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String curp;
    private String correo;
    private String celular;
    private String telefono;
    private String correoAlterno;
    private String extension;

    @Override
    public String toString() {
        var fullName = new StringBuilder(nombre).append(StringConstants.ESPACIO_BLANCO).append(primerApellido);
        if (segundoApellido != null && !segundoApellido.equals("")) {
            fullName.append(StringConstants.ESPACIO_BLANCO).append(segundoApellido);
        }
        return fullName.toString();
    }
}
