package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class InstitucionPlantillaLogistica {
	private Integer id;
	private Integer idInstitucion;
	private Integer idFamilia;
	private Integer idSubfamilia;
	private Integer idModalidad;
	private Integer idTipoAplicacion;
	private Integer idPlantillaLogistica;
	private String observaciones;
	private String nombrePlantilla;
	private String nombreFamilia;
	private String nombreSubFamilia;
}
