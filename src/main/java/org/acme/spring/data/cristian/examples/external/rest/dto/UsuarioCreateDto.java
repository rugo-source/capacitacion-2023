package org.acme.spring.data.cristian.examples.external.rest.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.examples.core.entity.UsuarioCreate;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "UsuarioCreate", description = "Esta en la entidad para guardar un usuario")
public class UsuarioCreateDto {

    @JsonProperty
    @Schema(description = "este es el atributo que referencia al correo del usuario")
    private String login;
    @JsonProperty
    @Schema(description = "este es el password con el se creara la cuenta.")
    private String password;



    public UsuarioCreate toEntity(){
        return UsuarioCreate.builder()
                .login(this.login)
                .password(this.password)
                .build();
    }

}
