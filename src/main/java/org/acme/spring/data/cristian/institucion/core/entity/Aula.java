package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Aula {
    private Integer id;
    private Integer idEdificio;
    private Integer idTipoEquipamiento;
    private String nombre;
    private Integer cantidadEquipamiento;
    private Integer capacidadEquipamiento;
    private String observaciones;
    private Integer capacidad;
    private String nombreEquipamiento;
}
