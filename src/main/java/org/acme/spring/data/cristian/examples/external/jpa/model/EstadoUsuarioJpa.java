package org.acme.spring.data.cristian.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.examples.core.entity.EstadoUsuario;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "estado_usuario")
public class EstadoUsuarioJpa {

    @Id
    @Column(name = "id_estado")
    private Integer id;

    @Column(name = "tx_nombre")
    private String nombre;

    public EstadoUsuario toEntity(){
        return EstadoUsuario.builder()
                .id(this.id)
                .nombre(this.nombre)
                .build();
    }

}
