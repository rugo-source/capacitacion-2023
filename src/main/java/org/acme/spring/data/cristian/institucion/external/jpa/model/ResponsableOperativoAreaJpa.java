package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.ResponsableOperativoArea;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin20_responsable_operativo_area")
public class ResponsableOperativoAreaJpa {

    @Id
    @EmbeddedId
    private ResponsableOperativoAreaIdJpa id;
    @Column(name = "fk_id_responsable_operativo", insertable = false, updatable = false)
    private Integer idResponsableOperativo;
    @Column(name = "fk_id_area_contratante", insertable = false, updatable = false)
    private Integer idAreaContratante;

    public ResponsableOperativoArea toEntity() {
        return ResponsableOperativoArea.builder().idResponsableOperativo(idResponsableOperativo)
                .idAreaContratante(idAreaContratante).build();
    }

    public static ResponsableOperativoAreaJpa formEntity(ResponsableOperativoArea entity) {
        return ResponsableOperativoAreaJpa.builder()
                .id(ResponsableOperativoAreaIdJpa.builder().idAreaContratante(entity.getIdAreaContratante())
                        .idResponsableOperativo(entity.getIdResponsableOperativo()).build())
                .idResponsableOperativo(entity.getIdResponsableOperativo())
                .idAreaContratante(entity.getIdAreaContratante()).build();
    }

}