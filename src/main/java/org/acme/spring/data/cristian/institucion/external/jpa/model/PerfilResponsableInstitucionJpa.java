package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.PerfilResponsableInstitucion;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin19_perfil_responsable_institucion")
public class PerfilResponsableInstitucionJpa {
    @Id
    @SequenceGenerator(name = "tin19_perfil_responsable_institucion_id_perfil_responsable_seq",sequenceName = "tin19_perfil_responsable_institucion_id_perfil_responsable_seq", allocationSize = 1)
    @GeneratedValue(generator = "tin19_perfil_responsable_institucion_id_perfil_responsable_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_perfil_responsable")
    private Integer id;
    @Column(name = "fk_id_responsable")
    private Integer idResponsableArea;
    @Column(name = "fk_id_tipo")
    private Integer idTipo;
    @Column(name = "fh_inicio")
    private LocalDate inicio;
    @Column(name = "fh_fin")
    private LocalDate fin;

    public PerfilResponsableInstitucion toEntity() {
        return PerfilResponsableInstitucion.builder()
                .id(id)
                .idResponsableArea(idResponsableArea)
                .idTipo(idTipo)
                .inicio(inicio)
                .fin(fin)
                .build();
    }

    public static PerfilResponsableInstitucionJpa fromEntity(PerfilResponsableInstitucion entity) {
        return PerfilResponsableInstitucionJpa.builder()
                .id(entity.getId())
                .idResponsableArea(entity.getIdResponsableArea())
                .idTipo(entity.getIdTipo())
                .inicio(entity.getInicio())
                .fin(entity.getFin())
                .build();
    }
}
