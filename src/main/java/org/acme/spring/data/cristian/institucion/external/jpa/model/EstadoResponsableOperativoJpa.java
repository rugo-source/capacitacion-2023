package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.util.entity.Estado;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin21_estado_responsable_operativo")
public class EstadoResponsableOperativoJpa {
    @Id
    @Column(name = "id_estado")
    private Integer id;
    @Column(name = "tx_nombre")
    private String nombre;

    public Estado toEntity() {
        return Estado.builder()
                .id(id)
                .nombre(nombre)
                .build();
    }
}
