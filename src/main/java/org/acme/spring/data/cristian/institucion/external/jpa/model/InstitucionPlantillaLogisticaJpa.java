package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.InstitucionPlantillaLogistica;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin07_institucion_plantilla_logistica")
public class InstitucionPlantillaLogisticaJpa {
    @Id
    @SequenceGenerator(name = "tin07_institucion_plantilla_logist_id_institucion_plantilla_seq", sequenceName = "tin07_institucion_plantilla_logist_id_institucion_plantilla_seq", allocationSize = 1)
    @GeneratedValue(generator = "tin07_institucion_plantilla_logist_id_institucion_plantilla_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_institucion_plantilla")
    private Integer id;
    @Column(name = "fk_id_institucion")
    private Integer idInstitucion;
    @Column(name = "fk_id_familia")
    private Integer idFamilia;
    @Column(name = "fk_id_subfamilia")
    private Integer idSubFamilia;
    @Column(name = "fk_id_modalidad")
    private Integer idModalidad;
    @Column(name = "fk_id_tipo_aplicacion")
    private Integer idTipoAplicacion;
    @Column(name = "fk_id_plantilla")
    private Integer idPlantillaLogistica;
    @Column(name = "tx_observacion")
    private String observaciones;

    public InstitucionPlantillaLogistica toEntity() {
        return InstitucionPlantillaLogistica.builder()
                .id(id)
                .idInstitucion(idInstitucion)
                .idFamilia(idFamilia)
                .idSubfamilia(idSubFamilia)
                .idModalidad(idModalidad)
                .idTipoAplicacion(idTipoAplicacion)
                .idPlantillaLogistica(idPlantillaLogistica)
                .observaciones(observaciones)
                .build();
    }

    public static InstitucionPlantillaLogisticaJpa fromEntity(InstitucionPlantillaLogistica entity) {
        return InstitucionPlantillaLogisticaJpa.builder()
                .id(entity.getId())
                .idInstitucion(entity.getIdInstitucion())
                .idFamilia(entity.getIdFamilia())
                .idSubFamilia(entity.getIdSubfamilia())
                .idModalidad(entity.getIdModalidad())
                .idTipoAplicacion(entity.getIdTipoAplicacion())
                .idPlantillaLogistica(entity.getIdPlantillaLogistica())
                .observaciones(entity.getObservaciones())
                .build();
    }
}
