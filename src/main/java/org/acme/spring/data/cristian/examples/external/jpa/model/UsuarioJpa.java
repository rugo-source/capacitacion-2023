package org.acme.spring.data.cristian.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.examples.core.entity.Usuario;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tca02_usuario")
public class UsuarioJpa {
    @Id
    @SequenceGenerator(name = "tca02_usuario_id_usuario_seq", sequenceName = "tca02_usuario_id_usuario_seq", allocationSize = 1)
    @GeneratedValue(generator = "tca02_usuario_id_usuario_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_usuario")
    private Integer id;
    @Column(name = "fk_id_estado")
    private Integer idEstado;
    @Column(name = "tx_login")
    private String login;
    @Column(name = "tx_password")
    private String password;
    @Column(name = "nu_intento")
    private Integer intento;
    @Column(name = "fh_bloqueo")
    private LocalDateTime bloqueo;
    @Column(name = "fh_aviso_privacidad")
    private LocalDateTime avisoPrivacidad;

    public static UsuarioJpa fromEntity(Usuario usuario) {
        return UsuarioJpa.builder()
                .id(usuario.getId())
                .idEstado(usuario.getIdEstado())
                .login(usuario.getLogin())
                .password(usuario.getPassword())
                .intento(usuario.getIntento())
                .bloqueo(usuario.getBloqueo())
                .avisoPrivacidad(usuario.getAvisoPrivacidad())
                .build();
    }

    public Usuario toEntity() {
        return Usuario.builder()
                .id(this.id)
                .idEstado(this.idEstado)
                .login(this.login)
                .password(this.password)
                .intento(this.intento)
                .bloqueo(this.bloqueo)
                .avisoPrivacidad(this.avisoPrivacidad)
                .build();

    }

}
