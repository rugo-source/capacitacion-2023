package org.acme.spring.data.cristian.examples.core.business.implementation;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.examples.core.business.input.ExampleService;
import org.acme.spring.data.cristian.examples.core.business.output.UsuarioRepository;
import org.acme.spring.data.cristian.examples.core.entity.Usuario;
import org.acme.spring.data.cristian.examples.core.entity.UsuarioCreate;
import org.acme.spring.data.cristian.examples.core.statemachine.UsuarioSM;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class ExampleBs implements ExampleService {

    @Inject
    UsuarioRepository usuarioRepository;

    @Inject
    UsuarioSM usuarioSM;

    @Override
    public List<String> listaEjemplo() {
        List<Usuario> examples = usuarioRepository.findAll();
        examples.stream().forEach(usuario -> System.out.println(usuario.getEstadoUsuario().getNombre()));
        return examples.stream().map(usuario -> usuario.getEstadoUsuario().getNombre()).collect(Collectors.toList());
    }

    @Override
    public Either<Integer, Usuario> get(Integer idUsuario) {
        Either<Integer, Usuario> resultado = Either.left(404);
        var usuario = usuarioRepository.findBy(idUsuario);
        if (usuario.isPresent()) {
            var usuarioGet = usuario.get();
            usuarioGet.setRegistrar(usuarioSM.isDoable(usuarioSM.getRegistrar(),usuarioSM.getStateById(usuarioGet.getIdEstado())));
            usuarioGet.setEditar(usuarioSM.isDoable(usuarioSM.getEditar(), usuarioSM.getStateById(usuarioGet.getIdEstado())));
            usuarioGet.setEliminar(usuarioSM.isDoable(usuarioSM.getEliminar(), usuarioSM.getStateById(usuarioGet.getIdEstado())));
            usuarioGet.setConsultar(usuarioSM.isDoable(usuarioSM.getConsultar(), usuarioSM.getStateById(usuarioGet.getIdEstado())));
            usuarioGet.setConfigurar(usuarioSM.isDoable(usuarioSM.getConfigurar(), usuarioSM.getStateById(usuarioGet.getIdEstado())));
            resultado = Either.right(usuarioGet);
        }
        return resultado;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> create(UsuarioCreate usuarioCreate) {
        Either<Integer, Boolean> resultado = Either.right(true);
        var usuarioPersits = Usuario.builder()
                .login(usuarioCreate.getLogin())
                .password(usuarioCreate.getPassword())
                .intento(0)
                .idEstado(usuarioSM.getRegistrado().getId())
                .build();
        usuarioRepository.save(usuarioPersits);

        return resultado;
    }

    @Override
    @Transactional
    public Either<Integer, Boolean> update(Integer id, UsuarioCreate usuarioCreate) {
        Either<Integer, Boolean> resultado = Either.left(404);
        var usuarioSearch = usuarioRepository.findBy(id);
        if (usuarioSearch.isPresent()) {
            if(usuarioSM.isDoable(usuarioSM.getEditar(),usuarioSM.getStateById(usuarioSearch.get().getIdEstado()))) {
                var usuarioChange = usuarioSearch.get();
                usuarioChange.setLogin(usuarioCreate.getLogin());
                usuarioChange.setPassword(usuarioCreate.getPassword());
                usuarioRepository.update(usuarioChange);
                resultado = Either.right(true);
            }
        }

        return resultado;
    }

    @Override
    public Either<Integer, Boolean> delete(Integer id) {
        Either<Integer, Boolean> resultado = Either.left(404);
        var usuarioSearch = usuarioRepository.findBy(id);
        if (usuarioSearch.isPresent()) {
            usuarioRepository.deleteBy(id);
            resultado = Either.right(true);
        }
        return resultado;
    }
}
