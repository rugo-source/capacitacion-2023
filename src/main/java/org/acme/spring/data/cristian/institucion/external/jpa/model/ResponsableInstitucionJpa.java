package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.ResponsableInstitucion;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin18_responsable_institucion")
public class ResponsableInstitucionJpa {
	@Id
	@SequenceGenerator(name = "tin18_responsable_institucion_id_responsable_seq", sequenceName = "tin18_responsable_institucion_id_responsable_seq", allocationSize = 1)
	@GeneratedValue(generator = "tin18_responsable_institucion_id_responsable_seq", strategy = GenerationType.SEQUENCE)
	@Column(name = "id_responsable")
	private Integer id;
	@Column(name = "fk_id_persona")
	private Integer idPersona;
	@Column(name = "fk_id_area_contratante")
	private Integer idAreaContratante;
	@Column(name = "fk_id_direccion")
	private Integer idDireccion;
	@Column(name = "tx_abreviatura_titulo")
	private String abrevituraTitulo;
	@Column(name = "tx_cargo")
	private String cargo;

	public ResponsableInstitucion toEntity() {
		return ResponsableInstitucion.builder().id(id).idPersona(idPersona).idAreaContratante(idAreaContratante)
				.idDireccion(idDireccion).abreviaturaTitulo(abrevituraTitulo).cargo(cargo).build();
	}

	public static ResponsableInstitucionJpa fromEntity(ResponsableInstitucion entity) {
		return ResponsableInstitucionJpa.builder().id(entity.getId()).idPersona(entity.getIdPersona())
				.idAreaContratante(entity.getIdAreaContratante()).idDireccion(entity.getIdDireccion())
				.abrevituraTitulo(entity.getAbreviaturaTitulo()).cargo(entity.getCargo()).build();
	}
}
