package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.AreaContratante;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin10_area_contratante")
public class AreaContratanteJpa {
	@Id
	@SequenceGenerator(name = "tin10_area_contratante_id_area_contratante_seq", sequenceName = "tin10_area_contratante_id_area_contratante_seq", allocationSize = 1)
	@GeneratedValue(generator = "tin10_area_contratante_id_area_contratante_seq", strategy = GenerationType.SEQUENCE)
	@Column(name = "id_area_contratante")
	private Integer id;
	@Column(name = "fk_id_institucion")
	private Integer idInstitucion;
	@Column(name = "fk_id_direccion")
	private Integer idDireccion;
	@Column(name = "tx_area")
	private String area;
	@Column(name = "tx_subarea")
	private String subarea;
	@Column(name = "tx_telefono")
	private String telefono;
	@Column(name = "tx_extension")
	private String extension;


	public AreaContratante toEntity() {
		return AreaContratante.builder()
				.id(id)
				.idInstitucion(idInstitucion)
				.idDireccion(idDireccion)
				.area(area)
				.subarea(subarea)
				.telefono(telefono)
				.extension(extension)
				.build();
	}

	public static AreaContratanteJpa fromEntity(AreaContratante areaContratante) {
		return AreaContratanteJpa.builder()
				.id(areaContratante.getId())
				.idInstitucion(areaContratante.getIdInstitucion())
				.idDireccion(areaContratante.getIdDireccion())
				.area(areaContratante.getArea())
				.subarea(areaContratante.getSubarea())
				.telefono(areaContratante.getTelefono())
				.extension(areaContratante.getExtension())
				.build();
	}
}
