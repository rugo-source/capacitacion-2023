package org.acme.spring.data.cristian.examples.core.business.input;

import io.vavr.control.Either;
import org.acme.spring.data.cristian.examples.core.entity.Usuario;
import org.acme.spring.data.cristian.examples.core.entity.UsuarioCreate;

import java.util.List;

public interface ExampleService {

    List<String> listaEjemplo();
    Either<Integer, Usuario> get(Integer idUsuario);

    Either<Integer,Boolean> create(UsuarioCreate toEntity);

    Either<Integer,Boolean> update(Integer id, UsuarioCreate usuarioCreate);

    Either<Integer,Boolean> delete(Integer id);
}
