package org.acme.spring.data.cristian.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.examples.core.entity.CuentaCreate;
import org.acme.spring.data.util.StringConstants;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CuentaCreateDto {

    @JsonProperty
    @Positive(message = "RNS001")
    @Schema(description = "este es el atributo que hace referencia al id de del usuario")
    Integer idUsuario;
    @JsonProperty
    @NotNull(message = "RNS001")
    @NotEmpty(message = "RNS001")
    @Size(min = 3, max = 100, message = "RNS002")
    @Schema(description = "este es el atributo que referencia al nombre del rol")
    String rol;
    @JsonProperty
    @NotNull(message = "RNS001")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringConstants.LOCAL_DATE_FORMAT)
    @Schema(description = "este es el atributo que referencia a la fecha de inicio de la cuenta del usuario" )
    LocalDate inicio;
    @JsonProperty
    @NotNull(message = "RNS001")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringConstants.LOCAL_DATE_FORMAT)
    @Schema(description = "este es el atributo que referencia a la fecha de fin de la cuenta del usuario")
    LocalDate  fin;


    public CuentaCreate toEntity(){
        return CuentaCreate.builder()
                .idUsuario(this.idUsuario)
                .rol(this.rol)
                .inicio(this.inicio)
                .fin(this.fin)
                .build();
    }

}
