package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.PersonaExterna;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin12_persona_externa")
public class PersonaExternaJpa {
    @Id
    @SequenceGenerator(name = "tin12_persona_externa_id_persona_seq", sequenceName = "tin12_persona_externa_id_persona_seq", allocationSize = 1)
    @GeneratedValue(generator = "tin12_persona_externa_id_persona_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_persona")
    private Integer id;
    @Column(name = "fk_id_institucion")
    private Integer idInstitucion;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "tx_primer_apellido")
    private String primerApellido;
    @Column(name = "tx_segundo_apellido")
    private String segundoApellido;
    @Column(name = "tx_curp")
    private String curp;
    @Column(name = "tx_correo")
    private String correo;

    public PersonaExterna toEntity() {
        return PersonaExterna.builder()
                .id(id)
                .idInstitucion(idInstitucion)
                .nombre(nombre)
                .primerApellido(primerApellido)
                .segundoApellido(segundoApellido)
                .curp(curp)
                .correo(correo)
                .build();
    }

    public static PersonaExternaJpa fromEntity(PersonaExterna entity) {
        return PersonaExternaJpa.builder()
                .id(entity.getId())
                .idInstitucion(entity.getIdInstitucion())
                .nombre(entity.getNombre())
                .primerApellido(entity.getPrimerApellido())
                .segundoApellido(entity.getSegundoApellido())
                .curp(entity.getCurp())
                .correo(entity.getCorreo())
                .build();
    }
}
