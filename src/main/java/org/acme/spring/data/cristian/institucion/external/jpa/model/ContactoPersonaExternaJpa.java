package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.ContactoPersonaExterna;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin13_contacto")
public class ContactoPersonaExternaJpa {
    @Id
    @Column(name = "id_contacto")
    private Integer id;
    @Column(name = "tx_correo_alterno")
    private String correoAlterno;
    @Column(name = "tx_celular")
    private String celular;
    @Column(name = "tx_telefono")
    private String telefono;
    @Column(name = "tx_extension")
    private String extension;

    public ContactoPersonaExterna toEntity() {
        return ContactoPersonaExterna.builder()
                .id(id)
                .correoAlterno(correoAlterno)
                .celular(celular)
                .telefono(telefono)
                .extension(extension)
                .build();
    }

    public static ContactoPersonaExternaJpa fromEntity(ContactoPersonaExterna entity) {
        return ContactoPersonaExternaJpa.builder()
                .id(entity.getId())
                .correoAlterno(entity.getCorreoAlterno())
                .celular(entity.getCelular())
                .telefono(entity.getTelefono())
                .extension(entity.getExtension())
                .build();
    }
}
