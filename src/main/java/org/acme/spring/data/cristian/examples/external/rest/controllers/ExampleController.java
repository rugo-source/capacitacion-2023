package org.acme.spring.data.cristian.examples.external.rest.controllers;

import org.acme.spring.data.cristian.examples.core.business.input.ExampleService;
import org.acme.spring.data.cristian.examples.core.entity.UsuarioCreate;
import org.acme.spring.data.cristian.examples.external.rest.dto.UsuarioCreateDto;
import org.acme.spring.data.cristian.examples.external.rest.dto.UsuarioDto;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/data/example")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion")
public class ExampleController {

    @Inject
    ExampleService exampleService;

    @GET
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(type = SchemaType.ARRAY, implementation = String.class)))
    public Response list() {
        var list = exampleService.listaEjemplo();
        return Response.ok(list).build();
    }

    /**
     * .map(resultado -> Response.ok(resultado)) == .map(Response::ok)
     *
     * RNNXXX - RNN001
     * RNSXXX - RNS001
     *
     * @param idProducto
     * @return
     */
    @GET
    @Path("{idUsuario}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = UsuarioDto.class)))
    @APIResponse(responseCode = "404", description = "Petición erronea", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response get(@PathParam("idUsuario") Integer idUsuario) {
        return exampleService.get(idUsuario).map(UsuarioDto::fromEntity).map(Response::ok)
                .getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

    @POST
    @Path("post")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Boolean.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = Boolean.class)))
    public Response create(UsuarioCreateDto usuarioCreateDto) {
        return exampleService.create(usuarioCreateDto.toEntity()).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

    @PUT
    @Path("put/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response update(@PathParam("id") Integer id, UsuarioCreate usuarioCreate) {
        return exampleService.update(id,usuarioCreate).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }

    @DELETE
    @Path("delete/{id}")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = Integer.class)))
    public Response delete(@PathParam("id") Integer id) {
        return exampleService.delete(id).map(Response::ok).getOrElseGet(resultado -> Response.ok(resultado).status(404)).build();
    }


}
