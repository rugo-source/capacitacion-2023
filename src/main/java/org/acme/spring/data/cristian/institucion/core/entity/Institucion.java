package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Institucion {
	private Integer id;
	private Integer idTipo;
	private Integer idClasificacion;
	private Integer idCategoria;
	private String nombreCategoria;
	private Integer idSubsistemaUniversidad;
	private Integer idSubsistemaBachillerato;
	private String identificador;
	private String nombre;
	private String acronimo;
	private String cct;
	private Integer solicitudesPendientes;
	private Integer numeroSedesRegistradas;
	private String nombreEjecutivo;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Institucion other = (Institucion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
