package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.acme.spring.data.util.entity.Estado;


@Builder
@Getter
@Setter
public class ProgramaAcademico {
    private Integer id;
    private Integer idAreaContratante;
    private Integer idEstado;
    private String nemotecnia;
    private String carrera;
    private Estado estado;
    private Boolean activable;
    private Boolean desactivable;
    private Boolean editable;
}
