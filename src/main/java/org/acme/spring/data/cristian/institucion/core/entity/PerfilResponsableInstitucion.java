package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.acme.spring.data.util.entity.Catalogo;

import java.time.LocalDate;

@Builder
@Getter
@Setter
public class PerfilResponsableInstitucion {
    private Integer id;
    private Integer idResponsableArea;
    private Integer idTipo;
    private LocalDate inicio;
    private LocalDate fin;
    private Catalogo tipo;
    private Boolean editarFechaInicio;
    private Boolean editarFechas;
}
