package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.ResponsableOperativo;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin15_responsable_operativo")
public class ResponsableOperativoJpa {
    @Id
    @SequenceGenerator(name = "tin15_responsable_operativo_id_responsable_operativo_seq", sequenceName = "tin15_responsable_operativo_id_responsable_operativo_seq", allocationSize = 1)
    @GeneratedValue(generator = "tin15_responsable_operativo_id_responsable_operativo_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_responsable_operativo")
    private Integer id;
    @Column(name = "fk_id_persona")
    private Integer idPersona;
    @Column(name = "fk_id_institucion")
    private Integer idInstitucion;
    @Column(name = "fk_id_tipo")
    private Integer idTipo;
    @Column(name = "fk_id_estado")
    private Integer idEstado;
    @Column(name = "fk_id_direccion")
    private Integer idDireccion;
    @Column(name = "tx_abreviatura_titulo")
    private String abreviaturaTitulo;
    @Column(name = "tx_telefono_movil")
    private String telefonoMovil;
    @Column(name = "tx_telefono")
    private String telefono;
    @Column(name = "tx_extension")
    private String extension;
    @Column(name = "tx_correo_alterno")
    private String correoAlterno;
    @Column(name = "tx_cargo")
    private String cargo;


    public ResponsableOperativo toEntity() {
        return ResponsableOperativo.builder()
				.id(id)
				.idPersona(idPersona)
				.idInstitucion(idInstitucion)
				.idTipo(idTipo)
				.idEstado(idEstado)
                .idDireccion(idDireccion)
                .correoAlterno(correoAlterno)
                .telefono(telefono)
                .telefonoMovil(telefonoMovil)
                .extension(extension)
				.abreviaturaTitulo(abreviaturaTitulo)
				.cargo(cargo)
				.build();
    }

    public static ResponsableOperativoJpa formEntity(ResponsableOperativo entity) {
        return ResponsableOperativoJpa.builder()
				.id(entity.getId())
				.idPersona(entity.getIdPersona())
				.idInstitucion(entity.getIdInstitucion())
                .idTipo(entity.getIdTipo())
				.idEstado(entity.getIdEstado())
				.idDireccion(entity.getIdDireccion())
                .correoAlterno(entity.getCorreoAlterno())
                .telefono(entity.getTelefono())
                .telefonoMovil(entity.getTelefonoMovil())
                .extension(entity.getExtension())
                .abreviaturaTitulo(entity.getAbreviaturaTitulo())
				.cargo(entity.getCargo()).build();
    }
}
