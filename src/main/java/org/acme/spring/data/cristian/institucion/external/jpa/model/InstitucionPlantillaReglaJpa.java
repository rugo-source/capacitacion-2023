package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.InstitucionPlantillaRegla;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin03_institucion_plantilla_regla")
public class InstitucionPlantillaReglaJpa {
    @Id
    @SequenceGenerator(name = "tin03_institucion_plantilla_regla_id_institucion_plantilla_seq", sequenceName = "tin03_institucion_plantilla_regla_id_institucion_plantilla_seq", allocationSize = 1)
    @GeneratedValue(generator = "tin03_institucion_plantilla_regla_id_institucion_plantilla_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_institucion_plantilla")
    private Integer id;
    @Column(name = "fk_id_familia")
    private Integer idFamilia;
    @Column(name = "fk_id_plantilla")
    private Integer idPlantilla;
    @Column(name = "fk_id_institucion")
    private Integer idInstitucion;
    @Column(name = "tx_observacion")
    private String observaciones;

    public InstitucionPlantillaRegla toEntity() {
        return InstitucionPlantillaRegla.builder()
                .id(id)
                .idFamilia(idFamilia)
                .idPlantilla(idPlantilla)
                .idInstitucion(idInstitucion)
                .observaciones(observaciones)
                .build();
    }

    public static InstitucionPlantillaReglaJpa fromEntity(InstitucionPlantillaRegla entity) {
        return InstitucionPlantillaReglaJpa.builder()
                .id(entity.getId())
                .idFamilia(entity.getIdFamilia())
                .idPlantilla(entity.getIdPlantilla())
                .idInstitucion(entity.getIdInstitucion())
                .observaciones(entity.getObservaciones())
                .build();
    }
}
