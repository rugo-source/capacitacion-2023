package org.acme.spring.data.cristian.examples.external.jpa.dao;

import org.acme.spring.data.cristian.examples.core.business.output.UsuarioRepository;
import org.acme.spring.data.cristian.examples.core.entity.Usuario;
import org.acme.spring.data.cristian.examples.external.jpa.model.UsuarioJpa;
import org.acme.spring.data.cristian.examples.external.jpa.repository.UsuarioJpaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class UsuarioDao implements UsuarioRepository {

    @Inject
    EntityManager entityManager;

    @Inject
    UsuarioJpaRepository usuarioJpaRepository;

    @Override
    public List<Usuario> findAll() {
        return usuarioJpaRepository.findAll().stream().map(usuarioJpa -> {
            var usuarioEntidad  = usuarioJpa.toEntity();
            //usuarioEntidad.setEstadoUsuario(usuarioJpa.getEstadoUsuario().toEntity());
            return usuarioEntidad;
        }).collect(Collectors.toList());
    }

    @Override
    public List<Usuario> findAllNative() {
        Stream<UsuarioJpa> resultado = entityManager.createNativeQuery("Select * from tca02_usuario",UsuarioJpa.class).getResultStream();
        return resultado.map(UsuarioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<Usuario> findAllOrm() {
        return usuarioJpaRepository.findAllOrm().stream().map(UsuarioJpa::toEntity).collect(Collectors.toList());
    }

    @Override
    public Optional<Usuario> findBy(Integer idUsuario) {
        return usuarioJpaRepository.findById(idUsuario).map(UsuarioJpa::toEntity);
    }

    @Override
    public void save(Usuario usuarioPersits) {

        usuarioJpaRepository.saveAndFlush(UsuarioJpa.fromEntity(usuarioPersits));
    }

    @Override
    public void update(Usuario usuarioChange) {
        usuarioJpaRepository.saveAndFlush(UsuarioJpa.fromEntity(usuarioChange));
    }

    @Override
    public void deleteBy(Integer id) {
        usuarioJpaRepository.deleteById(id);
    }
}
