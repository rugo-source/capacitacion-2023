package org.acme.spring.data.cristian.examples.core.entity;

import lombok.*;

import java.time.LocalDate;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CuentaCreate {
    Integer idUsuario;
    String rol;
    LocalDate inicio;
    LocalDate  fin;
}
