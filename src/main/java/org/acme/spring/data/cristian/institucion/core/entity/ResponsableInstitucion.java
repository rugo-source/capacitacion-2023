package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class ResponsableInstitucion {
    private Integer id;
    private Integer idPersona;
    private Integer idAreaContratante;
    private Integer idDireccion;
    private String abreviaturaTitulo;
    private String cargo;

    private String curp;
    private String nombre;
    private String estado;
    private Boolean activo;

    private String telefono;
    private String telefonoMovil;
    private String correo;
    private String correoAlterno;

    private PersonaExterna persona;
}
