package org.acme.spring.data.cristian.examples.external.rest.controllers;

import org.acme.spring.data.cristian.examples.external.rest.dto.CuentaCreateDto;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/data/cuenta")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "Capacitacion")
public class CuentaController {

    @POST
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = CuentaCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response create(@Valid CuentaCreateDto cuentaCreateDto){
        return Response.ok().build();
    }

}
