package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.acme.spring.data.util.entity.Catalogo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "cin05_subsistema_bachillerato")
public class SubsistemaBachilleratoJpa {
    @Id
    @Column(name = "id_subsistema")
    private Integer id;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "tx_descripcion")
    private String descripcion;
    @Column(name = "st_activo")
    private Boolean activo;

    public Catalogo toEntity() {
        return Catalogo.builder()
                .id(id)
                .nombre(nombre)
                .descripcion(descripcion)
                .activo(activo)
                .build();
    }
}
