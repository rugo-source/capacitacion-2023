package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Builder
@Getter
@Setter
@ToString
public class ResponsableOperativo {

    private Integer id;
    private Integer idPersona;
    private Integer idTipo;
    private Integer idEstado;
    private Integer idInstitucion;
    private Integer idDireccion;
    private List<Integer> idAreasContratantes;
    private String correoAlterno;
    private String telefono;
    private String telefonoMovil;
    private String extension;
    private String abreviaturaTitulo;
    private String cargo;

    private String nombre;
    private String correo;
    private String area;
    private String horario;
    private List<AreaContratante> areasContratantes;
    private Boolean activable;
    private Boolean desactivable;
    private Boolean registrado;
    private Boolean activo;
    private Boolean inactivo;

}
