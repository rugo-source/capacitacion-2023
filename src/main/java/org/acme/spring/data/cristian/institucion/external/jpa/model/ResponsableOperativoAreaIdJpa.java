package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Embeddable
public class ResponsableOperativoAreaIdJpa implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5929677742036545272L;
	
	@Column(name = "fk_id_responsable_operativo", nullable = false)
    private Integer idResponsableOperativo;
    @Column(name = "fk_id_area_contratante", nullable = false)
    private Integer idAreaContratante;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ResponsableOperativoAreaIdJpa that = (ResponsableOperativoAreaIdJpa) o;
		return idResponsableOperativo.equals(that.idResponsableOperativo) && idAreaContratante.equals(that.idAreaContratante);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idResponsableOperativo, idAreaContratante);
	}
}
