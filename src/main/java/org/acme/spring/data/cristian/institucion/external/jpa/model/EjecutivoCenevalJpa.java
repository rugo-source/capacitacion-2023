package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.examples.external.jpa.model.PersonaJpa;
import org.acme.spring.data.cristian.institucion.core.entity.EjecutivoCeneval;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin04_ejecutivo")
public class EjecutivoCenevalJpa {

	@Id
	@SequenceGenerator(name = "tin04_ejecutivo_id_ejecutivo_seq", sequenceName = "tin04_ejecutivo_id_ejecutivo_seq", allocationSize = 1)
	@GeneratedValue(generator = "tin04_ejecutivo_id_ejecutivo_seq", strategy = GenerationType.SEQUENCE)
	@Column(name = "id_ejecutivo")
	private Integer id;
	@Column(name = "id_persona")
	private Integer idPersona;
	@Column(name = "id_institucion")
	private Integer idInstitucion;
	@Column(name = "fh_inicio")
	private LocalDateTime inicio;
	@Column(name = "fh_fin")
	private LocalDateTime fin;
	@ManyToOne
	@JoinColumn(name = "id_persona", referencedColumnName = "id_persona", insertable = false, updatable = false)
	PersonaJpa persona;

	public static EjecutivoCenevalJpa fromEntity(EjecutivoCeneval ejecutivoCeneval) {
		return EjecutivoCenevalJpa.builder().id(ejecutivoCeneval.getId())
				.idPersona(ejecutivoCeneval.getIdPersona()).idInstitucion(ejecutivoCeneval.getIdInstitucion())
				.inicio(ejecutivoCeneval.getInicio()).fin(ejecutivoCeneval.getFin()).build();
	}

	public EjecutivoCeneval toEntity() {
		return EjecutivoCeneval.builder().id(id).idPersona(idPersona).idInstitucion(idInstitucion)
				.inicio(inicio).fin(fin).build();
	}

}
