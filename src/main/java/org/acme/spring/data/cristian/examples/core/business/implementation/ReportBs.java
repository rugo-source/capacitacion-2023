package org.acme.spring.data.cristian.examples.core.business.implementation;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.acme.spring.data.cristian.examples.core.business.input.ReportService;
import org.acme.spring.data.cristian.examples.core.business.output.UsuarioRepository;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.ext.Provider;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class ReportBs implements ReportService {
    @Value("classpath:template/RP01Prueba2.jasper")
    String reporteIncidencias;
    @Inject
    UsuarioRepository usuarioRepository;


    @Override
    public void generarReporte(String outputFileName) {
        try {
            FastReportBuilder frb = new FastReportBuilder();

            frb.addColumn("State", "login", String.class.getName(), 30)
                    .addColumn("Branch", "login", String.class.getName(), 30)
                    .addColumn("Item", "login", String.class.getName(), 50)
                    .addColumn("Amount", "login", Float.class.getName(), 60, true)
                    .addGroups(10)
                    .setTitle("November " + 2022 +" sales report")
                    .setSubtitle("This report was generated at " + new LocalDate())
                    .setColumnsPerPage(1, 10)
                    .setUseFullPageWidth(true)
                    .setColspan(1, 2, "Estimated");

            DynamicReport dr = frb.build();
            dr.getOptions().getDefaultHeaderStyle();

            JRDataSource ds = new JRBeanCollectionDataSource(usuarioRepository.findAll());
            JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), ds);
            JasperExportManager.exportReportToPdfFile(jp,"/home/wargreymon/Documents/"+outputFileName);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void generarReporteXls(String nombreReporte) {

    }

    @Override
    public void generar() {
        try {
            InputStream input = Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream(reporteIncidencias);
            JRDataSource ds = new JRBeanCollectionDataSource(new ArrayList<>());
            Map<String, Object> params = new HashMap<>();
            params.put("urlImagen1", "images/usicamm.png");
            params.put("urlImagen2", "images/ceneval.png");
            params.put("urlImagen3", "images/footer.png");

            params.put("expediente", 123);
            params.put("fecha", Date.valueOf("2022-05-03"));
            params.put("departamento", "contabilidad");
            params.put("empleado", "José");
            params.put("resumen", "adwsa");

            JasperPrint jp = JasperFillManager.fillReport(input, params, ds);
            JasperExportManager.exportReportToPdfFile(jp,"src/main/resources/reporte.pdf");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
