package org.acme.spring.data.cristian.examples.core.business.input;

import java.io.OutputStream;

public interface ReportService {

    void generarReporte(String outputFileName);

    void generarReporteXls(String nombreReporte);
    void generar();

}
