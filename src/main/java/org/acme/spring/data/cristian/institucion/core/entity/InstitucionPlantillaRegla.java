package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class InstitucionPlantillaRegla {
    private Integer id;
    private Integer idFamilia;
    private Integer idPlantilla;
    private Integer idInstitucion;
    private String observaciones;
}
