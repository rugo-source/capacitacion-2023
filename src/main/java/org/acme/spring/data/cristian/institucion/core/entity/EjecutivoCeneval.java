package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Builder
@Setter
@Getter
@ToString
public class EjecutivoCeneval {
	
	private Integer id;
	private Integer idPersona;
	private Integer idInstitucion;
	private LocalDateTime inicio;
	private LocalDateTime fin;
	private Boolean inicioEditable;
	private Boolean finEditable;

	private String nombre;
	private String estado;
}
