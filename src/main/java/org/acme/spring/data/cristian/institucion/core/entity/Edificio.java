package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Edificio {
    private Integer id;
    private Integer idSede;
    private String nombre;
    private String acronimo;
    private String referencia;
    private Integer capacidad;
}
