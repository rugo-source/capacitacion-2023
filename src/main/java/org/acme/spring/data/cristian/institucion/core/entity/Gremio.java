package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class Gremio {
    private Integer id;
    private String nombre;
    private String acronimo;
    private List<Institucion> institucionList;
}
