package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.MiembroGremio;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin06_gremio_institucion")
public class MiembroGremioJpa {
	
    @Id
    @EmbeddedId
    private MiembroGremioIdJpa id;

    public static MiembroGremioJpa fromEntity(MiembroGremio entity) {
        return MiembroGremioJpa.builder()
                .id(MiembroGremioIdJpa.builder()
                        .idGremio(entity.getIdGremio())
                        .idInstitucion(entity.getIdInstitucion())
                        .build())
                .build();
    }

    public MiembroGremio toEntity() {
        return MiembroGremio.builder()
                .idGremio(id.getIdGremio())
                .idInstitucion(id.getIdInstitucion())
                .build();
    }
}