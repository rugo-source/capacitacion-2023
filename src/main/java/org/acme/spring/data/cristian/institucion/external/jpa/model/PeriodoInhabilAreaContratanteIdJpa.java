package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Embeddable
public class PeriodoInhabilAreaContratanteIdJpa implements Serializable {

	private static final long serialVersionUID = -5929677742036545272L;
	
	@Column(name = "fk_id_area_contratante", nullable = false)
    private Integer idAreaContratante;
    @Column(name = "fk_id_periodo_inhabil", nullable = false)
    private Integer idPeriodoInhabil;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeriodoInhabilAreaContratanteIdJpa that = (PeriodoInhabilAreaContratanteIdJpa) o;
        return idAreaContratante.equals(that.idAreaContratante) && idPeriodoInhabil.equals(that.idPeriodoInhabil);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAreaContratante, idPeriodoInhabil);
    }
}
