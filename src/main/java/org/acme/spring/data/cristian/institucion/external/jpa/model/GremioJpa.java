package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Gremio;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin02_gremio")
public class GremioJpa {
	@Id
	@SequenceGenerator(name = "tin02_gremio_id_gremio_seq", sequenceName = "tin02_gremio_id_gremio_seq", allocationSize = 1)
	@GeneratedValue(generator = "tin02_gremio_id_gremio_seq", strategy = GenerationType.SEQUENCE)
	@Column(name = "id_gremio")
	private Integer id;
	@Column(name = "tx_nombre")
	private String nombre;
	@Column(name = "tx_acronimo")
	private String acronimo;
	@ManyToMany
	@JoinTable(name = "tin06_gremio_institucion", joinColumns = {
			@JoinColumn(name = "fk_id_gremio", referencedColumnName = "id_gremio", insertable = false, updatable = false) }, inverseJoinColumns = {
			@JoinColumn(name = "fk_id_institucion", referencedColumnName = "id_institucion", insertable = false, updatable = false) })
	private List<InstitucionJpa> instituciones;

	public Gremio toEntity(){
		return Gremio.builder()
				.id(this.id)
				.nombre(this.nombre)
				.acronimo(this.acronimo)
				.build();
	}

	public static GremioJpa fromEntity(Gremio entity) {
		return GremioJpa.builder().id(entity.getId()).nombre(entity.getNombre()).acronimo(entity.getAcronimo()).build();
	}
}
