package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class PeriodoInhabilAreaContratante {
	private Integer idAreaContratante;
	private Integer idPeriodoInhabil;
}
