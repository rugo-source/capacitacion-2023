package org.acme.spring.data.cristian.institucion.core.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class InstitucionPlantillaLogisticaRegistrar {
    private Integer idInstitucion;
    private Integer idFamilia;
    private Integer idSubfamilia;
    private Integer idModalidad;
    private Integer idTipoAplicacion;
    private Integer idPlantillaLogistica;
    private String observaciones;
}
