package org.acme.spring.data.cristian.examples.external.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.acme.spring.data.cristian.examples.core.entity.Usuario;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDto {

    @JsonProperty
    @Schema(description = "este es el atributo que referencia al correo del usuario")
    private String login;
    @JsonProperty
    @Schema(description = "Bandera que indica que si se puede realizar el registro de un usuario.")
    private Boolean registrar;
    @JsonProperty
    @Schema(description = "Bandera que indica que si se puede realizar la edicion de un usuario.")
    private Boolean editar;
    @JsonProperty
    @Schema(description = "Bandera que indica que si se puede realizar la eliminacion de un usuario.")
    private Boolean eliminar;
    @JsonProperty
    @Schema(description = "Bandera que indica que si se puede realizar la consulta de un usuario.")
    private Boolean consultar;
    @JsonProperty
    @Schema(description = "Bandera que indica que si se puede realizar la configuracion de un usuario.")
    private Boolean configurar;


    public static UsuarioDto fromEntity(Usuario usuario){
        return UsuarioDto.builder()
                .login(usuario.getLogin())
                .registrar(usuario.getRegistrar())
                .editar(usuario.getEditar())
                .eliminar(usuario.getEliminar())
                .consultar(usuario.getConsultar())
                .configurar(usuario.getConfigurar())
                .build();
    }
}
