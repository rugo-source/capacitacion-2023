package org.acme.spring.data.cristian.reports;

import org.acme.spring.data.cristian.examples.core.business.input.ExampleService;
import org.acme.spring.data.cristian.examples.core.business.input.ReportService;
import org.acme.spring.data.cristian.examples.core.entity.Usuario;
import org.acme.spring.data.cristian.examples.external.jpa.repository.UsuarioJpaRepository;
import org.acme.spring.data.cristian.examples.external.rest.controllers.ExampleController;
import org.acme.spring.data.cristian.examples.external.rest.dto.CuentaCreateDto;
import org.acme.spring.data.util.error.ErrorResponseDto;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.OutputStream;
import java.util.List;

@Path("/report/dynamic-jasper")
@Tag(name = "Reportes")
public class DynamicJasperExampleController {

    @Inject
    ReportService reportService;
    @Inject
    ExampleService exampleService;

    @GET
    @Produces("application/pdf")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = CuentaCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response generateReport(@QueryParam("nombre") String nombreReporte) {
        try {
            reportService.generarReporte(nombreReporte);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().build();
    }

    @GET
    @Produces("application/pdf")
    @Path("ricardo")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = CuentaCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response generateReporte() {
        try {
            reportService.generar();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().build();
    }


    @GET
    @Produces("application/pdf")
    @APIResponse(responseCode = "200", description = "Petición exitosa", content = @Content(schema = @Schema(implementation = CuentaCreateDto.class)))
    @APIResponse(responseCode = "400", description = "Peticion erronea", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    public Response generateReportXls(@QueryParam("nombre") String nombreReporte) {
        try {
            reportService.generarReporteXls(nombreReporte);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().build();
    }

}
