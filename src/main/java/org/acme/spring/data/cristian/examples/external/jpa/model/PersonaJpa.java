package org.acme.spring.data.cristian.examples.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.examples.core.entity.Persona;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tca01_persona")
public class PersonaJpa {
    @Id
    @SequenceGenerator(name = "tca01_persona_id_persona_seq", sequenceName = "tca01_persona_id_persona_seq", allocationSize = 1)
    @GeneratedValue(generator = "tca01_persona_id_persona_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_persona")
    private Integer id;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "tx_primer_apellido")
    private String primerApellido;
    @Column(name = "tx_segundo_apellido")
    private String segundoApellido;
    @Column(name = "tx_telefono")
    private String telefono;
    @Column(name = "tx_curp")
    private String curp;
    @Column(name = "tx_telefono_movil")
    private String celular;
    @Column(name = "tx_extension")
    private String extension;
    @Column(name = "tx_correo_alterno")
    private String correoAlterno;

    public Persona toEntity() {
        return Persona.builder().id(this.id).nombre(this.nombre).primerApellido(this.primerApellido)
                .segundoApellido(this.segundoApellido).curp(this.curp).celular(this.celular)
                .telefono(this.telefono).correoAlterno(this.correoAlterno).extension(this.extension).build();
    }

    public static PersonaJpa fromEntity(Persona persona) {
        return PersonaJpa.builder().id(persona.getId()).nombre(persona.getNombre())
                .primerApellido(persona.getPrimerApellido()).segundoApellido(persona.getSegundoApellido())
                .curp(persona.getCurp()).correoAlterno(persona.getCorreoAlterno()).extension(persona.getExtension())
                .celular(persona.getCelular())
                .telefono(persona.getTelefono()).build();
    }
}
