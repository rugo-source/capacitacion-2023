package org.acme.spring.data.cristian.examples.core.entity;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EstadoUsuario {

    private Integer id;
    private String nombre;
}
