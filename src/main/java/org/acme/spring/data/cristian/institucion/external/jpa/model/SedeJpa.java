package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Sede;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin05_sede")
public class SedeJpa {
    @Id
    @SequenceGenerator(name = "tin05_sede_id_sede_seq", sequenceName = "tin05_sede_id_sede_seq", allocationSize = 1)
    @GeneratedValue(generator = "tin05_sede_id_sede_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_sede")
    private Integer id;
    @Column(name = "fk_id_institucion")
    private Integer idInstitucion;
    @Column(name = "fk_id_direccion")
    private Integer idDireccion;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "tx_acronimo")
    private String acronimo;
    @Column(name = "nu_capacidad")
    private Integer capacidad;
    @Column(name = "st_propia")
    private Boolean propia;
    @Column(name = "tx_identificador")
    private String identificador;

    public Sede toEntity() {
        return Sede.builder()
                .id(id)
                .idInstitucion(idInstitucion)
                .idDireccion(idDireccion)
                .nombre(nombre)
                .acronimo(acronimo)
                .capacidad(capacidad)
                .propia(propia)
                .identificador(identificador)
                .build();
    }

    public static SedeJpa fromEntity(Sede entity) {
        return SedeJpa.builder()
                .id(entity.getId())
                .idInstitucion(entity.getIdInstitucion())
                .idDireccion(entity.getIdDireccion())
                .nombre(entity.getNombre())
                .acronimo(entity.getAcronimo())
                .capacidad(entity.getCapacidad())
                .propia(entity.getPropia())
                .identificador(entity.getIdentificador())
                .build();
    }
}
