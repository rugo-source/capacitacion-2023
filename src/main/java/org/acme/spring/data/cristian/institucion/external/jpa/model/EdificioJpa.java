package org.acme.spring.data.cristian.institucion.external.jpa.model;

import lombok.*;
import org.acme.spring.data.cristian.institucion.core.entity.Edificio;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "tin08_edificio")
public class EdificioJpa {
    @Id
    @SequenceGenerator(name = "tin08_edificio_id_edificio_seq", sequenceName = "tin08_edificio_id_edificio_seq", allocationSize = 1)
    @GeneratedValue(generator = "tin08_edificio_id_edificio_seq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_edificio")
    private Integer id;
    @Column(name = "fk_id_sede")
    private Integer idSede;
    @Column(name = "tx_nombre")
    private String nombre;
    @Column(name = "tx_acronimo")
    private String acronimo;
    @Column(name = "tx_referencia")
    private String referencia;
    @OneToMany
    @JoinColumn(name = "fk_id_edificio", referencedColumnName = "id_edificio", insertable = false, updatable = false)
    private List<AulaJpa> aulas;

    public Edificio toEntity() {
        return Edificio.builder()
                .id(id)
                .idSede(idSede)
                .nombre(nombre)
                .acronimo(acronimo)
                .referencia(referencia)
                .build();
    }

    public static EdificioJpa fromEntity(Edificio entity) {
        return EdificioJpa.builder()
                .id(entity.getId())
                .idSede(entity.getIdSede())
                .nombre(entity.getNombre())
                .acronimo(entity.getAcronimo())
                .referencia(entity.getReferencia())
                .build();
    }
}
